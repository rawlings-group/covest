%% Copyright (C) 2018 Travis Arnold
%%
%% This file is part of Octave.
%%
%% Octave is free software; you can redistribute it and/or modify it
%% under the terms of the GNU General Public License as published by
%% the Free Software Foundation; either version 3 of the License, or (at
%% your option) any later version.
%%
%% Octave is distributed in the hope that it will be useful, but
%% WITHOUT ANY WARRANTY; without even the implied warranty of
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
%% General Public License for more details.
%%
%% You should have received a copy of the GNU General Public License
%% along with Octave; see the file COPYING.  If not, see
%% <http://www.gnu.org/licenses/>.

%% bool = iseven(N)
%%
%% Return bool = true if N is even.
%%
%% See also: isinteger.

%% Author: Travis Arnold <tjarnold@wisc.edu>
%% November 2018.

function bool = iseven(N)

[Nisinteger, N] = myisinteger(N);

if ~Nisinteger
    error('N must be an integer');
end%if

bool = mod(N, 2) == 0;

end%function iseven

