classdef ceestimator < handle

% TODO note for documentation: If [Q, S; S', R] is low rank, then errors and warnings are likely, especially for smoothing problems. This is due to poor numerical conditioning.

% TODO consider implementing a way to track if data.Nd changes. Data can change, but if the number of data points does not, then we don't want to have to redo all of the calculations for the Sigkj, Sigkk, K, etc. sequences. To do this I probably need to add an Nd property to ceestimator, and then find a clever way to see if it changes. I can probably do this by looking at self.props.store.Nd before looking at data property.

% TODO consider implementing a way to allow the user to control some of the options that get passed to dlqe_iter.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% properties %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

properties (GetAccess = public, SetAccess = private)
    props
        % Struct. Fields:
        %   base
        %       {'data', 'model', 'xhat10', 'Sig10'}
        %   dependent
        %       Cell array of strings givng the current dependent properties of
        %       the cemodel object.
        %   dependsspec
        %       Struct. Field names are the dependent properties of the cemodel
        %       object. Field values are cell arrays of strings of the
        %       corresponding base and dependent properties that are used to
        %       calculate each dependent property.
        %   depends
        %       Struct. Similar to dependsspec, but shows the dependencies in
        %       terms of base properties only.
        %   current
        %       Struct. Field names are the dependent properties of the cemodel
        %       object. Field values are true for dependent properties that are
        %       up to date and false for dependent properties that are out of
        %       date (or that have not been calculated).
        %   store
        %       Struct. Field names are the dependent properties of the cemodel
        %       object. Field values are the cached calculated values of the
        %       dependent properties.
    checks
        % Struct. Fields:
        %   names
        %       Cell array of strings with the names of the checks that are
        %       relevant to the cemodel object.
        %   depends
        %       Struct. Field names are the names of the checks that are
        %       relevant to the cemodel object. Field values are cell arrays of
        %       strings with the base properties that each check depends on.
        %   current
        %       Struct. Field names are the names of the checks that are
        %       relevant to the cemodel object. Field values are true for
        %       checks that are up to date and false for those that are out of
        %       date/have not passed.
    spec
        % Struct. Fields:
        %   xhat10
        %       True if user has provided xhat10 and false otherwise.
        %   Sig10
        %       True if user has provided Sig10 and false otherwise.
        %   modelspecstring
        %       The spec.string property from the linked cemodel object.
        %   datastateFlag
        %       The stateFlag property from the linked cedata object.
        %   string
        %       String. The spec.string property from the linked cemodel object
        %       concatenated with 'Sig10' if Sig10 is given and with 'stateFlag'
        %       if the linked cedata object includes states. Used internally to
        %       load self.props and self.checks.
end%properties

% props_, checks_, and spec_ are identical to props, checks, and spec, but they
% can be accessed without calling the normal get methods.
properties (Hidden, GetAccess = public, SetAccess = private)
    props_
    checks_
    spec_ = struct('xhat10', false(), ...
                   'Sig10', false(), ...
                   'modelspecstring', 'none', ...
                   'datastateFlag', false(), ...
                   'string', 'none')
end%properties

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% base props %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

properties (GetAccess = public, SetAccess = public)
    data
    model
    xhat10
    Sig10
end%properties

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% dependent props %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

properties (GetAccess = public, SetAccess = private)
    z
    e
    xhatkj
    xhatkk
    xhatkN
    Sigkj
    Sigkk
    SigkN
    SiglkN 
    K
    M
    L
    mu
    P
    Phi
    Theta
    T
    E
    s
    ehatkj
    ehatkk
    ehatkN
    Sigbarkj
    Sigbarkk
    SigbarkN
    SigbarlkN 
    Abar
    Abarstable
    Gbar
    Mbar
    Lbar
    mubar
    Pbar
    Phibar
    Ebar
    zbb
    ebb
    xbbhatkj
    xbbhatkk % this is calculated if Lcal is provided
    sbb
    ebbhatkj
    ebbhatkk
    ebbhatkN
    Sigbbkj
    Sigbbkk
    SigbbkN
    SigbblkN 
    Kbb
    Mbb
    Lbb
    mubb
    Pbb
    Phibb
    Thetabb
    Tbb
    Ebb
    Gw
    Hv
end%properties

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

properties (Hidden, GetAccess = public, SetAccess = private)
    id
end%properties

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

properties (Hidden, GetAccess = public, SetAccess = public)
    flags = struct('als', struct());
end%properties

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% methods %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

methods (Access = public)
    function self = ceestimator(varargin)
        % Constructor.

        % Generate uuid.
        self.id = uuidgen();

        % Set initial values for props and checks.
        self.load_constants();

        % Set default values of data and model properties. Do this here instead
        % of in property declaration to make sure that set.data/model gets
        % called; this ensures that they are properly linked to self.
        self.model = cemodel();
        self.data = cedata();

        % Get kwargs struct.
        kwargs = args2kwargs(varargin{:});

        % Make sure kwargs are valid; throw an error if not.
        checkkwargs(kwargs, self.props_.base); % data, model, xhat10, and Sig10

        % Set fields of self.
        for X = fieldnames(kwargs)'
            self.(X{:}) = kwargs.(X{:});
        end%for
    end%function ceestimator

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function bool = eq(self, b)
        % Determine if two ceestimator instances refer to the same object by
        % comparing their id property. This functionality is built into Matlab,
        % but not Octave.
        if nargin() ~= 2
            error('exactly two input arguments required');
        end%if
        if ~isa(b, 'ceestimator')
            error('second argument is not a ceestimator object');
        end%if
        bool = strcmp(self.id, b.id);
    end%function eq

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function bool = ne(self, b)
        % Determine if two ceestimator instances do not refer to the same object
        % by comparing their id property. This functionality is built into
        % Matlab, but not Octave.
        if nargin() ~= 2
            error('exactly two input arguments required');
        end%if
        if ~isa(b, 'ceestimator')
            error('second argument is not a ceestimator object');
        end%if
        bool = ~(self == b);
    end%function ne

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function bool = isempty(self)
        % Check if everything in ceestimator object is empty.
        if nargin() ~= 1
            error('exactly one input argument required');
        end%if
        bool = self.model.isempty() ...
               && (self.data.isempty() ...
               && (isempty(self.xhat10) ...
               && isempty(self.Sig10)));
    end%function isempty

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function check(self, varargin)
        % Perform checks on ceestimator object.

        % Make sure input arguments are character vectors.
        if ~all(cellfun(@ischarrow, varargin))
            error('all arguments (other than self) must be character vectors');
        end%if

        % Check for unrecoginized check names. Note that this line calls
        % get.checks which will call seeifmdechanged. We don't need to call
        % seeifmdechanged again, so for the rest of this function we use props_,
        % checks_, and spec_.
        unrec = setdiff(varargin, [self.checks.names]);
        if ~isempty(unrec)
            error(['unrecognized input argument(s): ', ...
                   strjoin(unrec, ', '), ' (acceptable arguments are ''', ...
                   strjoin(self.checks_.names, ''', '''), ''')']);
        end%if

        % If no additional arguments are passed, perform all checks.
        checkall = isempty(varargin);

        % Function that tells if each check needs to be performed.
        checkX = @(X) ismember(X, self.checks_.names) ...
                      && (~self.checks_.current.(X) ...
                      && (checkall || ismember(X, varargin)));

        msg = {}; % initialize cell to hold error messages

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% empty %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        if checkX('empty')
            % Check if everything in ceestimator object is empty.
            bool = self.isempty();
            if bool
                msg = [msg, 'ceestimator object is empty'];
            end%if
            self.checks_.current.empty = ~bool;
        end%if checkX('empty')

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%% compatible %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        if checkX('compatible')
            % Check data/model compatibility.
            [bool, cmsg] = self.model.compatible(self.data);
            if ~bool
                msg = [msg, ['data/model are not compatible: ', cmsg]];
            end%if
            self.checks_.current.compatible = bool;
        end%if checkX('compatible')

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% xhat10 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        if checkX('xhat10')
            % Check size of xhat10, if it is provided.
            bool = true();
            self.check('compatible'); % make sure self.model.n will work
            if self.spec_.xhat10 && (size(self.xhat10, 1) ~= self.model.n)
                bool = false();
                msg = [msg, 'size of given xhat10 is inconsistent with model'];
            end%if
            self.checks_.current.xhat10 = bool;
        end%if checkX('xhat10')

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Sig10size %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        if checkX('Sig10size')
            % Check size of Sig10, if it is provided.
            bool = true();
            self.check('compatible'); % make sure self.model.n will work
            if size(self.Sig10, 1) ~= self.model.n
                bool = false();
                msg = [msg, 'size of given Sig10 is inconsistent with model'];
            end%if
            self.checks_.current.Sig10size = bool;
        end%if checkX('Sig10')

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Sig10spec %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        if checkX('Sig10spec')
            % If Sig10 is provided, check that self.model.spec.stochtype is
            % 'QRS'.
            bool = true();
            if self.spec_.Sig10 && ~strcmp(self.model.spec.stochtype, 'QRS')
                bool = false();
                msg = [msg, ['Sig10 may not be given unless ', ...
                             'model.spec.stochtype is ''QRS''']];
            end%if
            self.checks_.current.Sig10spec = bool;
        end%if checkX('Sig10spec')

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Lcalspec %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        if checkX('Lcalspec')
            bool = true();
            if self.model.spec.Lcal && ~self.model.spec.Kcal
                bool = false();
                msg = [msg, 'Lcal may not be given unless Kcal is also given'];
            end%if
            self.checks_.current.Lcalspec = bool;
        end%if checkX('Lcalspec')

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        % If any checks did not pass, throw an error.
        if ~isempty(msg)
            msg = strjoin(msg, char(10));
            error(msg);
        end%if
    end%function check

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function disp(self)
        if isoctave()
            disp('<object ceestimator>');
        else % Matlab
            disp(['<object ceestimator>', char(10)]);
        end%if
    end%function
end%methods (Access = public)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

methods (Access = private)
    function calculate(self, X)
        % Calculate dependent props. Updates values in self.props.store.

        % Check that estimator is valid; throw an error if it is not.
        self.check();

        %%%%%%%%%%%%%%%%%%%%%%% L, K, Sigkj, Sigkk, E, M %%%%%%%%%%%%%%%%%%%%%%%

        % L_k = Sigkj C' (C Sigkj C' + HRH')^+
        % K_k = (A Sigkj C' + GSH') (C Sigkj C' + HRH')^+
        % Sigkj = E [(x_k - xhatkj) (x_k - xhatkj)']
        % Sigkk = E [(x_k - xhatkk) (x_k - xhatkk)']
        % E_k = eig(A - K_k C)
        % M_k = C' (C Sigkj C' + HRH')^+
        if strcmp(X, 'L') ...
            || (strcmp(X, 'K') ...
            || (strcmp(X, 'Sigkj') ...
            || (strcmp(X, 'Sigkk') ...
            || (strcmp(X, 'E') ...
            || strcmp(X, 'M')))))
            kwargs = struct();
            kwargs.S = self.model.GSH;
            kwargs.X0 = self.Sig10;
            kwargs.maxiters = self.data.Nd - 1;
            kwargs.forcealliters = true();
            kwargs.returnalliters = 'all';
            kwargs.warnifnotcnv = false();
            [self.props_.store.L, ...
             self.props_.store.K, ...
             self.props_.store.Sigkj, ...
             self.props_.store.Sigkk, ...
             self.props_.store.E, ...
             self.props_.store.M] ...
             = dlqe_iter(self.model.A, self.model.C, self.model.GQG, ...
                         self.model.HRH, kwargs);
            self.props_.current.L = true();
            self.props_.current.K = true();
            self.props_.current.Sigkj = true();
            self.props_.current.Sigkk = true();
            self.props_.current.E = true();
            self.props_.current.M = true();
            return;
        end%if strcmp(X, L, K, Sigkj, Sigkk, E, M)

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% P %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        % P_{k+1} = A (P_k) A' + GQG'
        if strcmp(X, 'P')
            if ~self.model.Astable
                warning(['A is not stable; P will most likely increase ', ...
                         'without bound']);
            end%if
            % Preallocate and initialize.
            self.props_.store.P = cell(1, self.data.Nd);
            self.props_.store.P{1} = self.Sig10;
            for i = 2:self.data.Nd
                self.props_.store.P{i} = forceherm( ...
                                     self.model.A * self.props_.store.P{i-1} ...
                                      * self.model.A') + self.model.GQG;
            end%for
            self.props_.current.P = true();
            return;
        end%if strcmp(X, 'P')

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Phi %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        % Phi_k = C P_k C' + HRH'
        if strcmp(X, 'Phi')
            self.props_.store.Phi = cellfun( ...
                      @(X) forceherm(self.model.C * X * self.model.C') ...
                            + self.model.HRH, self.P, 'UniformOutput', false());
            self.props_.current.Phi = true();
            return;
        end%if strcmp(X, 'Phi')

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Theta %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        % Theta_k = A P_k C' + GSH'
        if strcmp(X, 'Theta')
            self.props_.store.Theta = cellfun( ...
                     @(X) self.model.A * X * self.model.C' + self.model.GSH, ...
                     self.P, 'UniformOutput', false());
            self.props_.current.Theta = true();
            return;
        end%if strcmp(X, 'Theta')

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% T %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        % T_k = P_k - Sigkj
        if strcmp(X, 'T')
            self.props_.store.T = cellfun(@minus, self.P, self.Sigkj, ...
                                          'UniformOutput', false());
            self.props_.current.T = true();
            return;
        end%if strcmp(X, 'T')

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Abar %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        % Abar_k = A - K_k C
        if strcmp(X, 'Abar')
            self.props_.store.Abar = cellfun(@(X) self.model.A - ...
                                             X * self.model.C, self.K, ...
                                             'UniformOutput', false());
            self.props_.current.Abar = true();
            return;
        end%if strcmp(X, 'Abar')

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Abarstable %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        if strcmp(X, 'Abarstable')
            self.props_.store.Abarstable = arrayfun( ...
                            @(k) max(abs(self.E(:, k))) < 1, 1:self.data.Nd, ...
                            'UniformOutput', false());
            self.props_.current.Abarstable = true();
            return;
        end%if strcmp(X, 'Abarstable')

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Gbar %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        % Gbar_k = [G, -K_k H]
        if strcmp(X, 'Gbar')
            if ismember('G', self.model.spec.GH)
                G = self.model.G;
            else
                G = eye(self.model.n);
            end%if
            if ismember('H', self.model.spec.GH)
                H = self.model.H;
            else
                H = eye(self.model.p);
            end%if
            self.props_.store.Gbar = cellfun(@(X) [G, -X * H], self.K, ...
                                             'UniformOutput', false());
            self.props_.current.Gbar = true();
            return;
        end%if strcmp(X, 'Gbar')

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Phibar %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        % Phibar_k = C Pbar_k C' + HRH' = C Sigkj C' + HRH'
        % Remember that Pbar_k = Sigkj!
        if strcmp(X, 'Phibar')
            self.props_.store.Phibar = cellfun( ...
                   @(X) forceherm(self.model.C * X * self.model.C') ...
                         + self.model.HRH, self.Pbar, 'UniformOutput', false());
            self.props_.current.Phibar = true();
            return;
        end%if strcmp(X, 'Phibar')

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Kbb %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        % Kbb_k = K_k - Kcal (Phi_k - C T_k C') (Phi_k - C T_k C')^+
        %       = K_k - Kcal Phibar_k (Phibar_k)^+
        if strcmp(X, 'Kbb')
            self.props_.store.Kbb = cellfun(@minus, self.K, ...
                             cellfun(@(X) self.model.Kcal * ApinvB(X, X), ...
                                     self.Phibar, 'UniformOutput', false()), ...
                                           'UniformOutput', false());
            self.props_.current.Kbb = true();
            return;
        end%if strcmp(X, 'Kbb')

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Pbb %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        % Pbb_{k + 1} = Abb Pbb_k Abb' + Gbb [Q S; S' R] Gbb'
        if strcmp(X, 'Pbb')
            if ~self.model.Abbstable
                warning(['Abb is not stable; Pbb will most likely ', ...
                         'increase without bound']);
            end%if
            % Preallocate and initialize.
            self.props_.store.Pbb = cell(1, self.data.Nd);
            self.props_.store.Pbb{1} = self.Sig10;
            for i = 2:self.data.Nd
                self.props_.store.Pbb{i} = forceherm( ...
                                 self.model.Abb * self.props_.store.Pbb{i-1} ...
                                  * self.model.Abb') + self.model.GQGbb;
            end%for
            self.props_.current.Pbb = true();
            return;
        end%if strcmp(X, 'Pbb')

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Phibb %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        % Phibb_k = C Pbb_k C' + HRH'
        if strcmp(X, 'Phibb')
            self.props_.store.Phibb = cellfun( ...
                    @(X) forceherm(self.model.C * X * self.model.C') ...
                          + self.model.HRH, self.Pbb, 'UniformOutput', false());
            self.props_.current.Phibb = true();
            return;
        end%if strcmp(X, 'Phibb')

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Thetabb %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        % Thetabb_k = Abb Pbb_k C' + Gbb [S; R] H
        if strcmp(X, 'Thetabb')
            self.props_.store.Thetabb = cellfun( ...
                 @(X) self.model.Abb * X * self.model.C' + self.model.GSHbb, ...
                 self.Pbb, 'UniformOutput', false());
            self.props_.current.Thetabb = true();
            return;
        end%if strcmp(X, 'Thetabb')

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Tbb %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        % Tbb_k = Pbb_k - Sigbbkj
        if strcmp(X, 'Tbb')
            self.props_.store.Tbb = cellfun(@minus, self.Pbb, self.Sigbbkj, ...
                                           'UniformOutput', false());
            self.props_.current.Tbb = true();
            return;
        end%if strcmp(X, 'Tbb')

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Ebb %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        % Ebb_k = eig(Abb - Kbb_k C)
        if strcmp(X, 'Ebb')
            self.props_.store.Ebb = cellfun(@(X) eig(self.model.Abb ...
                                                     - X * self.model.C), ...
                                            self.Kbb, 'UniformOutput', false());
            self.props_.current.Ebb = true();
            return;
        end%if strcmp(X, 'Ebb')

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% mubar %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        % mubar_k = E [e_k]
        if strcmp(X, 'mubar')
            self.props_.store.mubar = zeros(self.model.n, 1);
            self.props_.current.mubar = true();
            return;
        end%if strcmp(X, 'mubar')

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% mubb %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        % mubb_k = E [ebb_k]
        if strcmp(X, 'mubb')
            self.props_.store.mubb = zeros(self.model.n, 1);
            self.props_.current.mubb = true();
            return;
        end%if strcmp(X, 'mubb')

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% mu %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        % mu_k = E [x_k]
        if strcmp(X, 'mu')
            if ~self.model.Astable
                warning(['A is not stable; mu will most likely ', ...
                         'increase without bound']);
            end%if

            % Preallocate and initialize.
            self.props_.store.mu = NaN(self.model.n, self.data.Nd);
            if self.spec_.xhat10
                self.props_.store.mu(:, 1) = self.xhat10;
            else
                self.props_.store.mu(:, 1) = zeros(self.model.n, 1);
            end%if

            % Do this here to avoid calling ismember over every loop iteration.
            haveB = ismember('B', self.model.spec.BD);
            for i = 2:self.data.Nd
                self.props_.store.mu(:, i) =  ...
                                    self.model.A * self.props_.store.mu(:, i-1);
                if haveB
                    self.props_.store.mu(:, i) = ...
                                          self.props_.store.mu(:, i) ...
                                           + self.model.B * self.data.u(:, i-1);
                end%if
            end%for
            self.props_.current.mu = true();
            return;
        end%if strcmp(X, 'mu')

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%% xhatkj, z %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        % xhatkj = E [x_k | y_{0:k-1}]
        % z_k = y_k - C xhatkj - D u_k
        if strcmp(X, 'xhatkj') || strcmp(X, 'z')
            if ~self.model.Abarstable
                warning(['steady state Abar is unstable; estimates ', ...
                         'xhatkj may diverge']);
            end%if
            [self.props_.store.xhatkj, self.props_.store.z] = self.dofilter();
            self.props_.current.xhatkj = true();
            self.props_.current.z = true();
            return;
        end%if strcmp(X, 'xhatkj') || strcmp(X, 'z')

        %%%%%%%%%%%%%%%%%%%%%%%%%%%% xbbhatkj, zbb %%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        % zbb_k = y_k - C xbbhatkj - D u_k
        if strcmp(X, 'xbbhatkj') || strcmp(X, 'zbb')
            if ~self.model.Abbstable
                warning(['steady state Abb is unstable; estimates ', ...
                         'xbbhatkj may diverge']);
            end%if
            [self.props_.store.xbbhatkj, self.props_.store.zbb] = ...
                                                            self.dofilter('bb');
            self.props_.current.xbbhatkj = true();
            self.props_.current.zbb = true();
            return;
        end%if strcmp(X, 'xbbhatkj') || strcmp(X, 'zbb')

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% xhatkk %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        % xhatkk = E [x_k | y_{0:k}]
        if strcmp(X, 'xhatkk')
            if self.spec_.Sig10
                self.props_.store.xhatkk = self.xhatkj + cell2mat(arrayfun( ...
                                     @(k) self.L{k} * self.z(:, k), ...
                                     1:self.data.Nd, 'UniformOutput', false()));
            else
                self.props_.store.xhatkk = self.xhatkj + self.model.L * self.z;
            end%if
            self.props_.current.xhatkk = true();
            return;
        end%if strcmp(X, 'xhatkk')

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% xbbhatkk %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        if strcmp(X, 'xbbhatkk')
            self.props_.store.xbbhatkk = self.xbbhatkj ...
                                                   + self.model.Lcal * self.zbb;
            self.props_.current.xbbhatkk = true();
            return;
        end%if strcmp(X, 'xbbhatkk')

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% xhatkN %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        % xhatkN = E [x_k | y_{0:Tf}]
        if strcmp(X, 'xhatkN')
            % Preallocate and initialize.
            self.props_.store.xhatkN = [NaN(self.model.n, self.data.Nd-1), ...
                                        self.xhatkk(:, self.data.Nd)];

            % Grab steady state properties if necessary.
            if ~self.spec_.Sig10
                M = self.model.M;
                Sigkj = self.model.Sigkj;
                Abar = self.model.Abar;
            end%if

            xsum = zeros(self.model.n, 1);
            for i = (self.data.Nd-1):-1:1
                if self.spec_.Sig10
                    M = self.M{i+1};
                    Sigkj = self.Sigkj{i};
                    Abar = self.Abar{i};
                end%if

                xsum = Abar' * (xsum + M * self.z(:, i+1));
                self.props_.store.xhatkN(:, i) = self.xhatkk(:, i) ...
                                                 + Sigkj * xsum;
            end%for

            self.props_.current.xhatkN = true();
            return;
        end%if strcmp(X, 'xhatkN')

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%% SigkN, SiglkN %%%%%%%%%%%%%%%%%%%%%%%%%%%%

        % SigkN = E [(x_k - xhatkN) (x_k - xhatkN)']
        % SiglkN = E [(x_{k+1} - xhat(k+1)N) (x_k - xhatkN)']
        if strcmp(X, 'SigkN') || strcmp(X, 'SiglkN')
            % Preallocate.
            self.props_.store.SigkN = cell(1, self.data.Nd);
            self.props_.store.SiglkN = cell(1, self.data.Nd - 1);

            % At = A - (GSH') * (HRH')^+ * C
            At = self.model.A;
            if ismember('S', self.model.spec.stoch)
                At = At - ApinvB(self.model.GSH, self.model.HRH) * self.model.C;
            end%if

            % Initialize and grab steady state properties if necessary.
            if self.spec_.Sig10
                self.props_.store.SigkN{self.data.Nd} = ...
                                                       self.Sigkk{self.data.Nd};
            else
                L = self.model.L;
                M = self.model.M;
                Sigkj = self.model.Sigkj;
                Sigkk = self.model.Sigkk;
                Abar = self.model.Abar;
                self.props_.store.SigkN{self.data.Nd} = Sigkk;
            end%if

            Sigsum = zeros(self.model.n, self.model.n);
            for i = (self.data.Nd - 1):-1:1
                if self.spec_.Sig10
                    L = self.L{i+1};
                    Sigkj = self.Sigkj{i+1};
                    Sigkk = self.Sigkk{i};
                end%if

                self.props_.store.SiglkN{i} = (eye(self.model.n) - ...
                                L * self.model.C - Sigkj * Sigsum) * At * Sigkk;

                if self.spec_.Sig10
                    M = self.M{i+1};
                    Sigkj = self.Sigkj{i};
                    Abar = self.Abar{i};
                end%if

                Sigsum = forceherm(Abar' * (Sigsum + M * self.model.C) * Abar);
                self.props_.store.SigkN{i} = forceherm(Sigkk ...
                                                      - Sigkj * Sigsum * Sigkj);
            end%for

            self.props_.current.SigkN = true();
            self.props_.current.SiglkN = true();
            return;
        end%if strcmp(X, 'SigkN', 'SiglkN')

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% e %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        % e_k = x_k - xhatkj
        if strcmp(X, 'e')
            self.props_.store.e = self.data.x - self.xhatkj;
            self.props_.current.e = true();
            return;
        end%if strcmp(X, 'e')

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% ebb %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        % ebb_k = x_k - xbbhatkj
        if strcmp(X, 'ebb')
            self.props_.store.ebb = self.data.x - self.xbbhatkj;
            self.props_.current.ebb = true();
            return;
        end%if strcmp(X, 'ebb')

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% ehatkj %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        % ehatkj = E [e_k | z_{0:k-1}]
        if strcmp(X, 'ehatkj')
            self.props_.store.ehatkj = zeros(self.model.n, self.data.Nd);
            self.props_.current.ehatkj = true();
            return;
        end%if strcmp(X, 'ehatkj')

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% ehatkk %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        % ehatkk = E [e_k | z_{0:k}]
        if strcmp(X, 'ehatkk')
            self.props_.store.ehatkk = self.xhatkk - self.xhatkj;
            self.props_.current.ehatkk = true();
            return;
        end%if strcmp(X, 'ehatkk')

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% ehatkN %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        % ehatkN = E [e_k | z_{0:Tf}]
        if strcmp(X, 'ehatkN')
            self.props_.store.ehatkN = self.xhatkN - self.xhatkj;
            self.props_.current.ehatkN = true();
            return;
        end%if strcmp(X, 'ehatkN')

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% ebbhatkj %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        % ebbhatkj = E [ebb_k | zbb_{0:k-1}]
        if strcmp(X, 'ebbhatkj')
            self.props_.store.ebbhatkj = self.xhatkj - self.xbbhatkj;
            self.props_.current.ebbhatkj = true();
            return;
        end%if strcmp(X, 'ebbhatkj')

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% ebbhatkk %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        % ebbhatkk = E [ebb_k | zbb_{0:k}]
        if strcmp(X, 'ebbhatkk')
            self.props_.store.ebbhatkk = self.xhatkk - self.xbbhatkj;
            self.props_.current.ebbhatkk = true();
            return;
        end%if strcmp(X, 'ebbhatkk')

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% ebbhatkN %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        % ebbhatkN = E [ebb_k | zbb_{0:Tf}]
        if strcmp(X, 'ebbhatkN')
            self.props_.store.ebbhatkN = self.xhatkN - self.xbbhatkj;
            self.props_.current.ebbhatkN = true();
            return;
        end%if strcmp(X, 'ebbhatkN')

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Gw, Hv %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        % (Gw)_k = G w_k
        % (Hv)_k = H v_k
        if strcmp(X, 'Gw') || strcmp(X, 'Hv')
            [self.props_.store.Gw, self.props_.store.Hv] ...
                                               = self.model.calcGwHv(self.data);
            self.props_.current.Gw = true();
            self.props_.current.Hv = true();
            return;
        end%if strcmp(X, 'Gw') || strcmp(X, 'Hv')
    end%function calculate

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function [xhat, z] = dofilter(self, bb)
        % Preallocate and initialize.
        z = NaN(self.model.p, self.data.Nd);
        if self.spec_.xhat10
            xhat = [self.xhat10, NaN(self.model.n, self.data.Nd-1)];
        else
            xhat = [zeros(self.model.n, 1), NaN(self.model.n, self.data.Nd-1)];
        end%if

        dobb = nargin >= 2;
        getKi = ~dobb && self.spec_.Sig10; % if dobb, getKi is false
        if dobb % use user provided gain, Kcal
            K = self.model.Kcal;
        else
            if ~getKi
                K = self.model.K; % steady state predictor
            end%if
        end%if

        % Do this here to avoid calling ismember at each iteration in the loop.
        haveB = ismember('B', self.model.spec.BD);
        haveD = ismember('D', self.model.spec.BD);

        for i = 1:self.data.Nd
            % Calculate state estimate, xhat.
            if i >= 2
                if getKi % time varying predictor
                    K = self.K{i-1};
                end%if

                xhat(:, i) = self.model.A * xhat(:, i-1) + K * z(:, i-1);

                % If B is in model, account for it.
                if haveB
                    xhat(:, i) = xhat(:, i) ...
                                  + self.model.B * self.data.u(:, i-1);
                end%if
            end%if

            % Calculate innovation, z.
            z(:, i) = self.data.y(:, i) - self.model.C * xhat(:, i);

            % If D is in model, account for it.
            if haveD
                z(:, i) = z(:, i) - self.model.D * self.data.u(:, i);
            end%if
        end%for
    end%function dofilter

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function load_constants(self)
        filename = [ceestimator.installdir(), 'ceestimator_constants.mat'];
        if exist(filename, 'file') ~= 2
            try
                ceestimator.generate_constants();
            catch err
                error(['unable to generate file ', filename, ' from ', ...
                       'function ceestimator.generate_constants(); got ', ...
                       'the following error message: ', err.message]);
            end%trycatch
        end%if
        try
            if isoctave()
                S = load('-7', filename);
            else % Matlab
                S = load(filename);
            end%if
            str = self.spec_.string;
            self.props_ = S.props.(str);
            self.checks_ = S.checks.(str);
        catch err
            error(['unable to load props.', str, ' and checks.', str, ...
                   ' from file ', filename, '; got the following error ', ...
                   'message: ', err.message]);
        end%trycatch
    end%function load_constants

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function link(self, mdnew)
        % Link self to given cedata or cemodel object mdnew.

        x = class(mdnew); % 'cedata' or 'cemodel'
        y = x(3:end); % remove leading 'ce', so y is 'data' or 'model'

        % Create a separate handle first to avoid calling set.model/data.
        mdold = self.(y);

        % When set.data/model is called in constructor, self.data/model = [].
        inconstructor = isequal(mdold, []);

        if ~inconstructor
            % If same object is being relinked for some reason, return.
            if mdold == mdnew
                return;
            end%if

            % Remove link in old cedata or cemodel object.
            mdold.flags.ceestimator = rmfield(mdold.flags.ceestimator, self.id);
        end%if

        % Create link in new cedata or cemodel object.
        mdnew.flags.ceestimator.(self.id) = true();

        if ~inconstructor
            % Run updatespec so that self.spec is updated if necessary.
            need2reload = self.updatespec(y, mdnew);

            % If updatespec did not reload self.props and self.checks, we still
            % need to set self.checks.current and self.props.current to false
            % for dependent checks and dependent properties.
            if ~need2reload
                self.updatecurrent(y);
            end%if
        end%if
    end%function link

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function raiseflags(self)
        for s = {'als'}
            self.flags.(s{:}) = structfun(@(x) true(), self.flags.(s{:}), ...
                                          'UniformOutput', false());
        end%for
    end%function raiseflags

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% get methods %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = getdependentprop(self, X)
        % Helper method used to get properties in self.props.dependent.

        % Make sure input argument is a recognized value.
        if ~ismember(X, self.props.dependent)
            error(['cannot access property ''', X, '''; props.dependent ', ...
                   'of ceestimator object is currently: ''', ...
                   strjoin(self.props_.dependent, ''', '''), '''']);
        end%if

        % If self.X isn't current, update self.props.store.X. Remember,
        % self.calculate will update self.props.current.
        if ~self.props_.current.(X)
            self.calculate(X);
        end%if

        % Return self.props.store.X.
        val = self.props_.store.(X);
    end%function getdependentprop
end%methods (Access = private)

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

methods
    function seeifmdchanged(self)
        % Check if model and data have changed, and if so make the necessary
        % updates.
        for X = {'model', 'data'}
            if self.(X{:}).flags.ceestimator.(self.id)
                % check if model/data has changed

                % Update self.spec and reload self.props and self.checks if
                % necessary.
                need2reload = self.updatespec(X{:});
                if ~need2reload
                    % If updatespec did not reload self.props and self.checks,
                    % we still need to set self.checks.current and
                    % self.props.current to false for dependent checks and
                    % dependent properties.
                    self.updatecurrent(X{:});
                end%if

                % Lower the flag in linked cemodel/cedata object. Create a
                % separate handle first to avoid calling set.model/set.data.
                md = self.(X{:});
                md.flags.ceestimator.(self.id) = false();

                % Raise flags for linked als objects.
                self.raiseflags();
            end%if
        end%for
    end%function seeifmdchanged

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.xhatkj(self)
        val = self.getdependentprop('xhatkj');
    end%function get.xhatkj

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.xhatkk(self)
        val = self.getdependentprop('xhatkk');
    end%function get.xhatkk

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.xhatkN(self)
        val = self.getdependentprop('xhatkN');
    end%function get.xhatkN

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.Sigkj(self)
        val = self.getdependentprop('Sigkj');
    end%function get.Sigkj

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.Sigkk(self)
        val = self.getdependentprop('Sigkk');
    end%function get.Sigkk

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.SigkN(self)
        val = self.getdependentprop('SigkN');
    end%function get.SigkN

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.SiglkN(self)
        val = self.getdependentprop('SiglkN');
    end%function get.SiglkN

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.K(self)
        val = self.getdependentprop('K');
    end%function get.K

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.M(self)
        val = self.getdependentprop('M');
    end%function get.M

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.L(self)
        val = self.getdependentprop('L');
    end%function get.L

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.mu(self)
        val = self.getdependentprop('mu');
    end%function get.mu

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.P(self)
        val = self.getdependentprop('P');
    end%function get.P

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.Phi(self)
        val = self.getdependentprop('Phi');
    end%function get.Phi

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.Theta(self)
        val = self.getdependentprop('Theta');
    end%function get.Theta

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.T(self)
        val = self.getdependentprop('T');
    end%function get.T

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.E(self)
        val = self.getdependentprop('E');
    end%function get.E

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.e(self)
        val = self.getdependentprop('e');
    end%function get.e

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.z(self)
        val = self.getdependentprop('z');
    end%function get.z

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.Abar(self)
        val = self.getdependentprop('Abar');
    end%function get.Abar

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.Abarstable(self)
        val = self.getdependentprop('Abarstable');
    end%function get.Abarstable

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.Gbar(self)
        val = self.getdependentprop('Gbar');
    end%function get.Gbar

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.ehatkj(self)
        val = self.getdependentprop('ehatkj');
    end%function get.ehatkj

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.ehatkk(self)
        val = self.getdependentprop('ehatkk');
    end%function get.ehatkk

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.ehatkN(self)
        val = self.getdependentprop('ehatkN');
    end%function get.ehatkN

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.Sigbarkj(self)
        val = self.Sigkj;
    end%function get.Sigbarkj

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.Sigbarkk(self)
        val = self.Sigkk;
    end%function get.Sigbarkk

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.SigbarkN(self)
        val = self.SigkN;
    end%function get.SigbarkN

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.SigbarlkN(self)
        val = self.SiglkN;
    end%function get.SigbarlkN

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.Mbar(self)
        val = self.M;
    end%function get.Mbar

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.Lbar(self)
        val = self.L;
    end%function get.Lbar

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.mubar(self)
        val = self.getdependentprop('mubar');
    end%function get.mubar

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.Pbar(self)
        val = self.Sigkj;
    end%function get.Pbar

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.Phibar(self)
        val = self.getdependentprop('Phibar');
    end%function get.Phibar

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.Ebar(self)
        val = self.E;
    end%function get.Ebar

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.s(self)
        val = self.z;
    end%function get.s

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.xbbhatkj(self)
        val = self.getdependentprop('xbbhatkj');
    end%function get.xbbhatkj

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.xbbhatkk(self)
        val = self.getdependentprop('xbbhatkk');
    end%function get.xbbhatkk

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.ebb(self)
        val = self.getdependentprop('ebb');
    end%function get.ebb

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.zbb(self)
        val = self.getdependentprop('zbb');
    end%function get.zbb

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.ebbhatkj(self)
        val = self.getdependentprop('ebbhatkj');
    end%function get.ebbhatkj

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.ebbhatkk(self)
        val = self.getdependentprop('ebbhatkk');
    end%function get.ebbhatkk

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.ebbhatkN(self)
        val = self.getdependentprop('ebbhatkN');
    end%function get.ebbhatkN

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.Sigbbkj(self)
        val = self.Sigkj;
    end%function get.Sigbbkj

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.Sigbbkk(self)
        val = self.Sigkk;
    end%function get.Sigbbkk

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.SigbbkN(self)
        val = self.SigkN;
    end%function get.SigbbkN

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.SigbblkN(self)
        val = self.SiglkN;
    end%function get.SigbblkN

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.Kbb(self)
        val = self.getdependentprop('Kbb');
    end%function get.Kbb

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.Mbb(self)
        val = self.M;
    end%function get.Mbb

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.Lbb(self)
        val = self.L;
    end%function get.Lbb

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.mubb(self)
        val = self.getdependentprop('mubb');
    end%function get.mubb

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.Pbb(self)
        val = self.getdependentprop('Pbb');
    end%function get.Pbb

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.Phibb(self)
        val = self.getdependentprop('Phibb');
    end%function get.Phibb

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.Thetabb(self)
        val = self.getdependentprop('Thetabb');
    end%function get.Thetabb

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.Tbb(self)
        val = self.getdependentprop('Tbb');
    end%function get.Tbb

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.Ebb(self)
        val = self.getdependentprop('Ebb');
    end%function get.Ebb

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.sbb(self)
        val = self.z;
    end%function get.sbb

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.Gw(self)
        val = self.getdependentprop('Gw');
    end%function get.Gw

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.Hv(self)
        val = self.getdependentprop('Hv');
    end%function get.Hv

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.props(self)
        self.seeifmdchanged();
        val = self.props_;
    end%function get.props

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.checks(self)
        self.seeifmdchanged();
        val = self.checks_;
    end%function get.checks

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.spec(self)
        self.seeifmdchanged();
        val = self.spec_;
    end%function get.spec
end%methods

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% set methods %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

methods (Access = private)
    function need2set = helpsetX(self, X, val)
        % Helper method that is used to update self.props, self.checks, and
        % self.spec when xhat10 and Sig10 are set.

        % Determine if X needs to be set.
        need2set = ~isequal(val, self.(X));
        need2updatespec = need2set && (isempty(self.(X)) ~= isempty(val));
            % The second condition is because we only need to use
            % self.updatespec here when xhat10 or Sig10 are being changed
            % between empty and nonempty. If these change from one numerical
            % value to another, nothing in self.spec chages, so we don't need to
            % use self.updatespec.
        if need2updatespec
            self.updatespec(X);
        end%if

        % If ceestimator object changes, update self.checks.current and
        % self.props.current, raise flags, and set the property.
        if need2set
            if ~need2updatespec
                % This would be redundant if need2updatespec is true.
                self.updatecurrent(X);
            end%if
            self.raiseflags();
        end%if
    end%function helpsetX

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function updatecurrent(self, X)
        % For base property X, mark all dependent checks and dependent
        % properties as out of date.
        for Y = self.checks_.names
            if ismember(X, self.checks_.depends.(Y{:}))
                self.checks_.current.(Y{:}) = false();
            end%if
        end%for
        for Y = self.props_.dependent
            if ismember(X, self.props_.depends.(Y{:}))
                self.props_.current.(Y{:}) = false();
            end%if
        end%for
    end%function updatecurrent

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function need2reload = updatespec(self, X, md)
        newmd = nargin() >= 3;
        need2reload = false();

        switch X
            case 'xhat10'
                % If xhat10 was just changed either from empty to nonempty or
                % vice versa, update value of self.spec.xhat10.
                self.spec_.xhat10 = ~self.spec_.xhat10;

            case 'Sig10'
                % If Sig10 was just changed either from empty to nonempty or
                % vice versa, update value of self.spec.Sig10.
                self.spec_.Sig10 = ~self.spec_.Sig10;
                need2reload = true();

            case 'model'
                % If model changed (or a new cemodel object was linked), update
                % value of self.spec.modelspecstring if necessary.
                old_modelspecstring = self.spec_.modelspecstring;
                if newmd
                    new_modelspecstring = md.spec.string;
                else
                    new_modelspecstring = self.model.spec.string;
                end%if
                if ~strcmp(old_modelspecstring, new_modelspecstring)
                    self.spec_.modelspecstring = new_modelspecstring;
                    need2reload = true();
                end%if

            case 'data'
                % If data changed (or a new cedata object was linked), update
                % value of self.spec.datastateFlag if necessary.
                old_datastateFlag = self.spec_.datastateFlag();
                if newmd
                    new_datastateFlag = md.stateFlag;
                else
                    new_datastateFlag = self.data.stateFlag;
                end%if
                if ~strcmp(old_datastateFlag, new_datastateFlag)
                    self.spec_.datastateFlag = new_datastateFlag;
                    need2reload = true();
                end%if
        end%switch

        % If self.spec.Sig10, self.spec.modelspecstring, or
        % self.spec.datastateFlag changed, set self.spec.string and use it to
        % load self.props and self.checks.
        if need2reload
            if newmd && strcmp(X, 'model')
                str = md.spec.string;
            else
                str = self.model.spec.string;
            end%if

            if self.spec_.Sig10
                str = [str, 'Sig10'];
            end%if

            if self.spec_.datastateFlag
                str = [str, 'stateFlag'];
            end%if

            self.spec_.string = str;
            self.load_constants(); % load self.props and self.checks
        end%if
    end%function updatespec
end%methods (Access = private)

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

methods
    function set.data(self, val)
        if ~isa(val, 'cedata')
            error('data must be a cedata object');
        end%if
        self.link(val);
        self.data = val;
    end%function set.data

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function set.model(self, val)
        if ~isa(val, 'cemodel')
            error('model must be a cemodel object');
        end%if
        self.link(val);
        self.model = val;
    end%function set.model

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function set.xhat10(self, val)
        if ~isnumeric(val)
            error('xhat10 must be a numeric column vector');
        end%if
        if ~isempty(val) && ~iscolumn(val)
            error('xhat10 must be a numeric column vector');
        end%if
        need2set = self.helpsetX('xhat10', val);
        if need2set
            self.xhat10 = val;
        end%if
    end%function set.xhat10

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function set.Sig10(self, val)
        if ~isnumericmatrix(val)
            error('Sig10 must be a numeric matrix');
        end%if
        if ~myissquare(val) && ~isempty(val)
            error('Sig10 must be square');
        end%if
        if ~ishermitian(val)
            error(['Sig10 must be Hermitian (if Sig10 is only slightly ', ...
                   'non-Hermitian due to round-off error, use of the ', ...
                   'forceherm function is recommended)']);
        end%if
        if ~isempty(val) && myisdefinite(val) == -1
            error('Sig10 must be positive semidefinite');
        end%if
        need2set = self.helpsetX('Sig10', val);
        if need2set
            self.Sig10 = val;
        end%if
    end%function set.Sig10

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function set.flags(self, val)
        % The flags property has public SetAccess, because attached als objects
        % need to be able to change it. However, the user should not be able to
        % change this property, so check to make sure it is being changed from
        % an appropriate place, and if not, throw an error.

        d = dbstack();
        dnames = {d.name};

        if isoctave()
            cmpfunc = @strcmp;
        else % Matlab
            cmpfunc = @endsWith;
        end%if

        % Check if flags is being changed from changing a base prop of the
        % ceestimator object (helpsetX calls raiseflags) or from checking if
        % data/model have changed (seeifmdchanged calls raiseflags).
        tests(1) = numel(dnames) >= 2 && cmpfunc(dnames{2}, 'raiseflags');

        % Check if flags are being lowered by linked als object.
        tests(2) = numel(dnames) >= 2 && cmpfunc(dnames{2}, 'seeifmdechanged');

        % Check if flags is being changed by set.estimator --> link in als.
        tests(3) = numel(dnames) >= 2 && cmpfunc(dnames{2}, 'link');

        if ~any(tests(:))
            error('cannot set property ''flags'' in this context');
        end%if

        self.flags = val;
    end%function
end%methods

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

methods (Static)
    function generate_constants()
        props = struct();
        checks = struct();
        base = {'model', ...
                'data', ...
                'xhat10', ...
                'Sig10'};
        allchecks = struct('empty',      {base}, ...
                           'compatible', {{'data', 'model'}}, ...
                           'xhat10',     {{'model', 'xhat10'}}, ...
                           'Sig10size',  {{'model', 'Sig10'}}, ...
                           'Sig10spec',  {{'model', 'Sig10'}}, ...
                           'Lcalspec',   {{'model'}});
        alwayschecks = {'empty', ...
                        'compatible', ...
                        'xhat10', ...
                        'Sig10spec', ...
                        'Lcalspec'};
        Sig10checks =  {'Sig10size'};

        allX = {};
        ds = 'dependsspec';
        dp = 'dependent';
        b = 'base';
        n = 'names';

        %%%%%%%%%%%%%%%%%%%%%%%%%% stochtype = 'none' %%%%%%%%%%%%%%%%%%%%%%%%%%

        X = 'Kcalnone';
        allX = [allX, X];
        props.(X).(ds) = struct('xbbhatkj', {{'model', 'data', 'xhat10'}}, ...
                                'zbb',      {{'data', 'model', 'xbbhatkj'}});
        checks.(X).(n) = alwayschecks;

        %%%%%%%%%%%%%%%%%%%%%%%%%% stochtype = 'QRS' %%%%%%%%%%%%%%%%%%%%%%%%%%%

        X = 'QRS';
        allX = [allX, X];
        props.(X).(ds) = struct('z',      {{'data', 'model', 'xhatkj'}}, ...
                                'xhatkj', {{'data', 'model', 'xhat10'}}, ...
                                'xhatkk', {{'xhatkj', 'model', 'z'}}, ...
                                'xhatkN', {{'xhatkk', 'model', 'z'}}, ...
                                'SigkN',  {{'model', 'data'}}, ...
                                'SiglkN', {{'model', 'data'}}, ...
                                'mu',     {{'model', 'data', 'xhat10'}}, ...
                                'ehatkj', {{'data', 'model'}}, ...
                                'ehatkk', {{'xhatkk', 'xhatkj'}}, ...
                                'ehatkN', {{'xhatkN', 'xhatkj'}}, ...
                                'mubar',  {{'model'}});
        checks.(X).(n) = alwayschecks;

        X = 'KcalQRS';
        allX = [allX, X];
        props.(X).(ds) = catstruct(props.QRS.(ds), ...
                         struct('zbb',      {{'data', 'model', 'xbbhatkj'}}, ...
                                'xbbhatkj', {{'data', 'model', 'xhat10'}}, ...
                                'ebbhatkj', {{'xhatkj', 'xbbhatkj'}}, ...
                                'ebbhatkk', {{'xhatkk', 'xbbhatkj'}}, ...
                                'ebbhatkN', {{'xhatkN', 'xbbhatkj'}}, ...
                                'mubb',     {{'model'}}));
        checks.(X).(n) = alwayschecks;

        X = 'QRSSig10';
        allX = [allX, X];
        props.(X).(ds) = ...
       struct('z',          {{'data', 'model', 'xhatkj'}}, ...
              'xhatkj',     {{'data', 'model', 'K', 'xhat10'}}, ...
              'xhatkk',     {{'xhatkj', 'L', 'z'}}, ...
              'xhatkN',     {{'xhatkk', 'Sigkj', 'Abar', 'M', 'z'}}, ...
              'Sigkj',      {{'model', 'Sig10', 'data'}}, ...
              'Sigkk',      {{'Sigkj', 'L', 'model'}}, ...
              'SigkN',      {{'Sigkk', 'Sigkj', 'Abar', 'model', 'M'}}, ...
              'SiglkN',     {{'Sigkk', 'Sigkj', 'Abar', 'model', 'M', 'L'}}, ...
              'K',          {{'model', 'Sigkj'}}, ...
              'M',          {{'model', 'Sigkj'}}, ...
              'L',          {{'model', 'Sigkj'}}, ...
              'mu',         {{'model', 'data', 'xhat10'}}, ...
              'P',          {{'model', 'Sig10', 'data'}}, ...
              'Phi',        {{'model', 'P'}}, ...
              'Theta',      {{'model', 'P'}}, ...
              'T',          {{'P', 'Sigkj'}}, ...
              'E',          {{'Abar'}}, ...
              'ehatkj',     {{'data', 'model'}}, ...
              'ehatkk',     {{'xhatkk', 'xhatkj'}}, ...
              'ehatkN',     {{'xhatkN', 'xhatkj'}}, ...
              'Abar',       {{'model', 'K'}}, ...
              'Abarstable', {{'Abar'}}, ...
              'Gbar',       {{'model', 'K'}}, ...
              'mubar',      {{'model'}}, ...
              'Phibar',     {{'model', 'Sigkj'}});
        checks.(X).(n) = [alwayschecks, Sig10checks];

        X = 'KcalQRSSig10';
        allX = [allX, X];
        props.(X).(ds) = catstruct(props.QRSSig10.(ds), ...
                         struct('zbb',      {{'data', 'model', 'xbbhatkj'}}, ...
                                'xbbhatkj', {{'data', 'model', 'xhat10'}}, ...
                                'ebbhatkj', {{'xhatkj', 'xbbhatkj'}}, ...
                                'ebbhatkk', {{'xhatkk', 'xbbhatkj'}}, ...
                                'ebbhatkN', {{'xhatkN', 'xbbhatkj'}}, ...
                                'Kbb',      {{'model', 'K', 'Sigkj'}}, ...
                                'mubb',     {{'model'}}, ...
                                'Pbb',      {{'model', 'Sig10', 'data'}}, ...
                                'Phibb',    {{'model', 'Pbb'}}, ...
                                'Thetabb',  {{'model', 'Pbb'}}, ...
                                'Tbb',      {{'Pbb', 'Sigkj'}}, ...
                                'Ebb',      {{'model', 'Kbb'}}));
        checks.(X).(n) = [alwayschecks, Sig10checks];

        %%%%%%%%%%%%%%%%%%%%%%%% stochtype = 'PhiTheta' %%%%%%%%%%%%%%%%%%%%%%%%

        X = 'PhiTheta';
        allX = [allX, X];
        props.(X).(ds) = struct('z',      {{'data', 'model', 'xhatkj'}}, ...
                                'xhatkj', {{'data', 'model', 'xhat10'}}, ...
                                'mu',     {{'model', 'data', 'xhat10'}}, ...
                                'ehatkj', {{'data', 'model'}}, ...
                                'mubar',  {{'model'}});
        checks.(X).(n) = alwayschecks;

        X = 'KcalPhiTheta';
        allX = [allX, X];
        props.(X).(ds) = catstruct(props.PhiTheta.(ds), ...
                         struct('zbb',      {{'data', 'model', 'xbbhatkj'}}, ...
                                'xbbhatkj', {{'data', 'model', 'xhat10'}}, ...
                                'ebbhatkj', {{'xhatkj', 'xbbhatkj'}}, ...
                                'mubb',     {{'model'}}));
        checks.(X).(n) = alwayschecks;

        %%%%%%%%%%%%%%%%%%%%%% stochtype = 'PhibbThetabb' %%%%%%%%%%%%%%%%%%%%%%

        X = 'KcalPhibbThetabb';
        allX = [allX, X];
        props.(X).(ds) = struct('z',        {{'data', 'model', 'xhatkj'}}, ...
                                'xhatkj',   {{'data', 'model', 'xhat10'}}, ...
                                'mu',       {{'model', 'data', 'xhat10'}}, ...
                                'ehatkj',   {{'data', 'model'}}, ...
                                'mubar',    {{'model'}}, ...
                                'zbb',      {{'data', 'model', 'xbbhatkj'}}, ...
                                'xbbhatkj', {{'data', 'model', 'xhat10'}}, ...
                                'ebbhatkj', {{'xhatkj', 'xbbhatkj'}}, ...
                                'mubb',     {{'model'}});
        checks.(X).(n) = alwayschecks;

        %%%%%%%%%%%%%%%%%%% create instances with stateFlag %%%%%%%%%%%%%%%%%%%%

        for Y = allX
            X = [Y{:}, 'stateFlag'];
            allX = [allX, X];
            props.(X).(ds) = catstruct(props.(Y{:}).(ds), ...
                                           struct('Gw', {{'data', 'model'}}, ...
                                                  'Hv', {{'data', 'model'}}));

            % If stochtype is not 'none', we can calculate xhatkj, and from that
            % we can calculate e.
            if ~strcmp(Y{:}, 'Kcalnone')
                props.(X).(ds).e = {'data', 'xhatkj'};
            end%if

            % For Y{:} = 'Kcalnone', 'KcalQRS', 'KcalQRSSig10', 'KcalPhiTheta',
            % and 'KcalPhibbThetabb', we can calculate xbbhatkj, and from that
            % we can calculate ebb.
            if numel(Y{:}) >= 4 && strcmp('Kcal', Y{:}(1:4))
                props.(X).(ds).ebb = {'data', 'xbbhatkj'};
            end%if
            checks.(X).(n) = checks.(Y{:}).(n);
        end%for

        %%%%%%%%%%%%%%%%%%%%%% create instances with Lcal %%%%%%%%%%%%%%%%%%%%%%

        for Y = allX
            % If Kcal is already given, all adding Lcal does is allow one to run
            % the filter to get xbbhatkk.
            if numel(Y{:}) >= 4 && strcmp('Kcal', Y{:}(1:4))
                X = ['Lcal', Y{:}];
                allX = [allX, X];
                props.(X).(ds) = props.(Y{:}).(ds);
                props.(X).(ds).xbbhatkk = {'xbbhatkj', 'model', 'zbb'};
                checks.(X).(n) = checks.(Y{:}).(n);
            end%if
        end%for

        %%%%%%%%%%%%%%%%%%%%%%%%% fill in other fields %%%%%%%%%%%%%%%%%%%%%%%%%

        for A = {'', 'Lcal'}
        for B = {'', 'Kcal'}
        for C = {'none', 'QRS', 'PhiTheta', 'PhibbThetabb'}
        for D = {'', 'Sig10'}
        for E = {'', 'stateFlag'}
            X = [A{:}, B{:}, C{:}, D{:}, E{:}];
            if ~ismember(X, fieldnames(props)')
                props.(X).(ds) = struct();
                checks.(X).(n) = alwayschecks;
            end%if
            props.(X).(b) = base;
            props.(X).(dp) = fieldnames(props.(X).(ds))';
            props.(X).depends = calcdepends(props.(X).(b), props.(X).(dp), ...
                                            props.(X).(ds));
            nd = numel(props.(X).(dp));
            props.(X).current = cell2struct( ...
                                     mat2cell(false(nd, 1), ones(nd, 1), 1), ...
                                     props.(X).(dp));
            props.(X).store = structfun(@(x) [], props.(X).current, ...
                                        'UniformOutput', false());

            checks.(X).depends = struct();
            for cn = fieldnames(allchecks)'
                if ismember(cn{:}, checks.(X).(n))
                    checks.(X).depends.(cn{:}) = allchecks.(cn{:});
                end%if
            end%for
            nc = numel(checks.(X).(n));
            checks.(X).current = cell2struct( ...
                                     mat2cell(false(nc, 1), ones(nc, 1), 1), ...
                                     checks.(X).(n));
        end%for
        end%for
        end%for
        end%for
        end%for

        %%%%%%%%%%%%%%%%%%%%%%%%%%%% save mat file %%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        filename = [ceestimator.installdir(), 'ceestimator_constants.mat'];
        if isoctave()
            save('-7', filename, 'props', 'checks');
        else % Matlab
            save(filename, 'props', 'checks', '-v7');
        end%if
    end%function generate_constants

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function d = installdir()
        mf = mfilename();
        mff = mfilename('fullpath');
        d = mff(1:end-numel(mf));
    end%function installdir

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function unittests_(varargin)
        msg = {};

        doB = ismember('B', varargin);
        doD = ismember('D', varargin);
        doxhat10 = ismember('xhat10', varargin);

        args = {};
        for X = {'B', 'D', 'xhat10'}
            if eval(['do', X{:}])
                args = [args, X{:}];
            else
                args = [args, ['no ' X{:}]];
            end%if
        end%for

        name = strjoin(args, ', ');

        try
            if isoctave()
                pkg('load', 'statistics');
            end%if

            n = 5;
            p = 4;
            A = randn(n);
            A = A / max(abs(eig(A))) * 0.9;
            C = randn(p, n);
            m = 3;
            if doB
                B = randn(n, m);
            end%if
            if doD
                D = randn(p, m);
            end%if
            g = 3;
            G = randn(n, g);
            h = 2;
            H = randn(p, h);
            QRS = randn(g + h);
            QRS = QRS*QRS';
            Q = QRS(1:g, 1:g);
            R = QRS(g+1:end, g+1:end);
            S = QRS(1:g, g+1:end);

            mdl_getprops = cemodel('A', A, 'C', C, 'G', G, 'H', H, 'Q', Q, ...
                                   'R', R, 'S', S);
            for X = {'Phi', 'Theta', 'Sigkj'}
                eval([X{:}, ' = mdl_getprops.', X{:}, ';']);
            end%for

            Nd = 10;

            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

            % Create empty estimator.
            est = ceestimator();
            if est.spec.xhat10 ...
                || (est.spec.Sig10 ...
                || (~strcmp(est.spec.modelspecstring, 'none') ...
                || (~strcmp(est.model.spec.string, 'none') ...
                || (est.spec.datastateFlag ...
                || (est.data.stateFlag ...
                || (~strcmp(est.spec.string, 'none') ...
                || ~est.isempty()))))))
                msg = [msg, ['failed (line ', num2str(getline()), '): ', ...
                             'error with empty estimator']];
            end%if

            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

            % Create empty model and link it to estimator.
            mdl = cemodel();
            est.model = mdl();
            if est.spec.xhat10 ...
                || (est.spec.Sig10 ...
                || (~strcmp(est.spec.modelspecstring, 'none') ...
                || (~strcmp(est.model.spec.string, 'none') ...
                || (est.spec.datastateFlag ...
                || (est.data.stateFlag ...
                || (~strcmp(est.spec.string, 'none') ...
                || ~est.isempty()))))))
                msg = [msg, ['failed (line ', num2str(getline()), '): ', ...
                             'error linking model']];
            end%if

            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

            for X = {'A', 'C'}
                mdl.(X{:}) = eval(X{:});
            end%for

            if doB
                mdl.B = B;
            end%if

            if doD
                mdl.D = D;
            end%if

            % data property has not been set yet.
            if est.spec.xhat10 ...
                || (est.spec.Sig10 ...
                || (~strcmp(est.spec.modelspecstring, 'none') ...
                || (~strcmp(est.model.spec.string, 'none') ...
                || (est.spec.datastateFlag ...
                || (est.data.stateFlag ...
                || (~strcmp(est.spec.string, 'none') ...
                || est.isempty()))))))
                msg = [msg, ['failed (line ', num2str(getline()), '): ', ...
                             'error adding A, C, B, D, to model']];
            end%if

            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

            for X = {'G', 'H', 'Q', 'R', 'S'}
                mdl.(X{:}) = eval(X{:});
            end%for

            if doB || doD
                u = randn(m, Nd);
                kwargs = {'u', u};
            else
                kwargs = {'Nd', Nd};
            end%if

            if doxhat10
                xhat10 = randn(n, 1);
                est.xhat10 = xhat10;
                x1 = mvnrnd(xhat10', Sigkj)';
            else
                x1 = mvnrnd(zeros(n, 1)', Sigkj)';
            end%if

            kwargs = [kwargs, 'x1', x1];

            try
                [dat, Gw, Hv] = mdl.simulate(kwargs{:});
                Gw(:, end) = []; % make dimensions consistent with calcGwHv
                                 % output
            catch err
                msg = [msg, ['failed (line ', num2str(getline()), '): ', ...
                             'mdl.simulate failed with following error: ', ...
                             err.message]];
            end%trycatch
            x = dat.x;
            y = dat.y;

            for X = {'G', 'H', 'Q', 'R', 'S'}
                mdl.(X{:}) = [];
            end%for

            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

            est.data = dat;
            if est.spec.xhat10 ~= doxhat10 ...
                || (est.spec.Sig10 ...
                || (~strcmp(est.spec.modelspecstring, 'none') ...
                || (~strcmp(est.model.spec.string, 'none') ...
                || (~est.spec.datastateFlag ...
                || (~est.data.stateFlag ...
                || (~strcmp(est.spec.string, 'nonestateFlag') ...
                || est.isempty()))))))
                msg = [msg, ['failed (line ', num2str(getline()), '): ', ...
                             'error linking data']];
            end%if

            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

            Q2 = randn(n);
            Q2 = Q2*Q2';
            R2 = randn(p);
            R2 = R2*R2';
            mdl_genKcal = cemodel('A', A, 'C', C, 'Q', Q2, 'R', R2);
            if doB
                mdl_genKcal.B = B;
            end%if
            if doD
                mdl_genKcal.D = D;
            end%if

            Kcal = mdl_genKcal.K;

            mdl.Kcal = Kcal;
            if est.spec.xhat10 ~= doxhat10 ...
                || (est.spec.Sig10 ...
                || (~strcmp(est.spec.modelspecstring, 'Kcalnone') ...
                || (~strcmp(est.model.spec.string, 'Kcalnone') ...
                || (~est.spec.datastateFlag ...
                || (~est.data.stateFlag ...
                || (~strcmp(est.spec.string, 'KcalnonestateFlag') ...
                || est.isempty()))))))
                msg = [msg, ['failed (line ', num2str(getline()), '): ', ...
                             'error setting Kcal in model']];
            end%if

            for X = est.props.dependent
                if exist(X{:}, 'var')
                    if ~comp(eval(X{:}), est.(X{:}))
                        msg = [msg, ['failed (line ', num2str(getline()), ...
                                     '): inconsistent calculation of ', X{:}]];
                    end%if
                else
                    eval([X{:}, ' = est.', X{:}, ';']);
                end%if
            end%for

            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

            Lcal = mdl_genKcal.L;
            mdl.Lcal = Lcal;
            if est.spec.xhat10 ~= doxhat10 ...
                || (est.spec.Sig10 ...
                || (~strcmp(est.spec.modelspecstring, 'LcalKcalnone') ...
                || (~strcmp(est.model.spec.string, 'LcalKcalnone') ...
                || (~est.spec.datastateFlag ...
                || (~est.data.stateFlag ...
                || (~strcmp(est.spec.string, 'LcalKcalnonestateFlag') ...
                || est.isempty()))))))
                msg = [msg, ['failed (line ', num2str(getline()), '): ', ...
                             'error setting Lcal in model']];
            end%if

            for X = est.props.dependent
                if exist(X{:}, 'var')
                    if ~comp(eval(X{:}), est.(X{:}))
                        msg = [msg, ['failed (line ', num2str(getline()), ...
                                     '): inconsistent calculation of ', X{:}]];
                    end%if
                else
                    eval([X{:}, ' = est.', X{:}, ';']);
                end%if
            end%for

            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

            mdl.Kcal = [];
            mdl.Lcal = [];

            for X = {'G', 'H', 'Q', 'R', 'S'}
                mdl.(X{:}) = eval(X{:});
            end%for

            if est.spec.xhat10 ~= doxhat10 ...
                || (est.spec.Sig10 ...
                || (~strcmp(est.spec.modelspecstring, 'QRS') ...
                || (~strcmp(est.model.spec.string, 'QRS') ...
                || (~est.spec.datastateFlag ...
                || (~est.data.stateFlag ...
                || (~strcmp(est.spec.string, 'QRSstateFlag') ...
                || est.isempty()))))))
                msg = [msg, ['failed (line ', num2str(getline()), '): ', ...
                             'error setting G, H, Q, R, S in model']];
            end%if

            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

            % Test smoother without Sig10.

            for X = est.props.dependent
                if exist(X{:}, 'var')
                    if ~comp(eval(X{:}), est.(X{:}))
                        msg = [msg, ['failed (line ', num2str(getline()), ...
                                     '): inconsistent calculation of ', X{:}]];
                    end%if
                else
                    eval([X{:}, ' = est.', X{:}, ';']);
                end%if
            end%for

            [xhatkN_test, SigkN_test, SiglkN_test] = est.test_smoother();

%            stuff = ...
%            [diag((dat.x - xhatkj)' * (dat.x - xhatkj)), ...
%             diag((dat.x - xhatkk)' * (dat.x - xhatkk)), ...
%             diag((dat.x - xhatkN_test)' * (dat.x - xhatkN_test))];
%            stuff = [stuff, stuff(:, 2) < stuff(:, 1), ...
%                            stuff(:, 3) < stuff(:, 2)];
%            stuff = [stuff, diag((dat.x - xhatkN)' * (dat.x - xhatkN))]

            if ~comp(xhatkN, xhatkN_test, 1e-3)
                msg = [msg, ['failed (line ', num2str(getline()), '): ', ...
                             'xhatkN from calculate is different than ', ...
                             'xhatkN calculated from test_smoother']];
            end%if

            if ~comp(SigkN, SigkN_test, 1e-3)
                msg = [msg, ['failed (line ', num2str(getline()), '): ', ...
                             'SigkN from calculate is different than ', ...
                             'SigkN calculated from test_smoother']];
            end%if

            if ~comp(SiglkN, SiglkN_test, 1e-3)
                msg = [msg, ['failed (line ', num2str(getline()), '): ', ...
                             'SiglkN from calculate is different than ', ...
                             'SiglkN calculated from test_smoother']];
            end%if

            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

            mdl.Kcal = Kcal;
            if est.spec.xhat10 ~= doxhat10 ...
                || (est.spec.Sig10 ...
                || (~strcmp(est.spec.modelspecstring, 'KcalQRS') ...
                || (~strcmp(est.model.spec.string, 'KcalQRS') ...
                || (~est.spec.datastateFlag ...
                || (~est.data.stateFlag ...
                || (~strcmp(est.spec.string, 'KcalQRSstateFlag') ...
                || est.isempty()))))))
                msg = [msg, ['failed (line ', num2str(getline()), '): ', ...
                             'error setting Kcal in model']];
            end%if

            for X = {'Phibb', 'Thetabb'}
                eval([X{:}, ' = mdl.', X{:}, ';']);
            end%for

            for X = est.props.dependent
                if exist(X{:}, 'var')
                    if ~comp(eval(X{:}), est.(X{:}))
                        msg = [msg, ['failed (line ', num2str(getline()), ...
                                     '): inconsistent calculation of ', X{:}]];
                    end%if
                else
                    eval([X{:}, ' = est.', X{:}, ';']);
                end%if
            end%for

            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

            mdl.Lcal = Lcal;
            if est.spec.xhat10 ~= doxhat10 ...
                || (est.spec.Sig10 ...
                || (~strcmp(est.spec.modelspecstring, 'LcalKcalQRS') ...
                || (~strcmp(est.model.spec.string, 'LcalKcalQRS') ...
                || (~est.spec.datastateFlag ...
                || (~est.data.stateFlag ...
                || (~strcmp(est.spec.string, 'LcalKcalQRSstateFlag') ...
                || est.isempty()))))))
                msg = [msg, ['failed (line ', num2str(getline()), '): ', ...
                             'error setting Kcal in model']];
            end%if

            for X = est.props.dependent
                if exist(X{:}, 'var')
                    if ~comp(eval(X{:}), est.(X{:}))
                        msg = [msg, ['failed (line ', num2str(getline()), ...
                                     '): inconsistent calculation of ', X{:}]];
                    end%if
                else
                    eval([X{:}, ' = est.', X{:}, ';']);
                end%if
            end%for

            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

            for X = {'G', 'H', 'Q', 'R', 'S', 'Lcal', 'Kcal'}
                mdl.(X{:}) = [];
            end%for

            for X = {'Phi', 'Theta'}
                mdl.(X{:}) = eval(X{:});
            end%for

            if est.spec.xhat10 ~= doxhat10 ...
                || (est.spec.Sig10 ...
                || (~strcmp(est.spec.modelspecstring, 'PhiTheta') ...
                || (~strcmp(est.model.spec.string, 'PhiTheta') ...
                || (~est.spec.datastateFlag ...
                || (~est.data.stateFlag ...
                || (~strcmp(est.spec.string, 'PhiThetastateFlag') ...
                || est.isempty()))))))
                msg = [msg, ['failed (line ', num2str(getline()), '): ', ...
                             'error changing model stochtype to PhiTheta']];
            end%if

            for X = est.props.dependent
                if exist(X{:}, 'var')
                    if ~comp(eval(X{:}), est.(X{:}))
                        msg = [msg, ['failed (line ', num2str(getline()), ...
                                     '): inconsistent calculation of ', X{:}]];
                    end%if
                else
                    eval([X{:}, ' = est.', X{:}, ';']);
                end%if
            end%for

            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

            for X = {'Phi', 'Theta'}
                mdl.(X{:}) = [];
            end%for

            for X = {'Kcal', 'Phibb', 'Thetabb'}
                mdl.(X{:}) = eval(X{:});
            end%for

            if est.spec.xhat10 ~= doxhat10 ...
                || (est.spec.Sig10 ...
                || (~strcmp(est.spec.modelspecstring, 'KcalPhibbThetabb') ...
                || (~strcmp(est.model.spec.string, 'KcalPhibbThetabb') ...
                || (~est.spec.datastateFlag ...
                || (~est.data.stateFlag ...
                || (~strcmp(est.spec.string, 'KcalPhibbThetabbstateFlag') ...
                || est.isempty()))))))
                msg = [msg, ['failed (line ', num2str(getline()), '): ', ...
                             'error changing model stochtype to PhibbThetabb']];
            end%if

            for X = est.props.dependent
                if exist(X{:}, 'var')
                    if ~comp(eval(X{:}), est.(X{:}))
                        msg = [msg, ['failed (line ', num2str(getline()), ...
                                     '): inconsistent calculation of ', X{:}]];
                    end%if
                else
                    eval([X{:}, ' = est.', X{:}, ';']);
                end%if
            end%for

            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

            % Test functionality of Kcal and Lcal.

            est_testcal = ceestimator('model', mdl_genKcal, 'data', dat);
            if doxhat10
                est_testcal.xhat10 = xhat10;
            end%if

            if ~comp(est_testcal.xhatkj, xbbhatkj)
                msg = [msg, ['failed (line ', num2str(getline()), '): ', ...
                             'inconsistent calculation of xbbhatkj']];
            end%if

            if ~comp(est_testcal.xhatkk, xbbhatkk)
                msg = [msg, ['failed (line ', num2str(getline()), '): ', ...
                             'inconsistent calculation of xbbhatkk']];
            end%if

            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

            % Test smoother with Sig10.

            for X = {'Kcal', 'Phibb', 'Thetabb'}
                mdl.(X{:}) = [];
            end%for

            for X = {'G', 'H', 'Q', 'R', 'S'}
                mdl.(X{:}) = eval(X{:});
            end%for

            Sig10 = randn(n);
            Sig10 = Sig10 * Sig10';
            est.Sig10 = Sig10;

            if est.spec.xhat10 ~= doxhat10 ...
                || (~est.spec.Sig10 ...
                || (~strcmp(est.spec.modelspecstring, 'QRS') ...
                || (~strcmp(est.model.spec.string, 'QRS') ...
                || (~est.spec.datastateFlag ...
                || (~est.data.stateFlag ...
                || (~strcmp(est.spec.string, 'QRSSig10stateFlag') ...
                || est.isempty()))))))
                msg = [msg, ['failed (line ', num2str(getline()), '): ', ...
                             'error changing model stochtype to PhibbThetabb']];
            end%if

            % make sure all properties evaluate without errors
            for X = est.props.dependent
                eval(['est.', X{:}, ';']);
            end%for

            [xhatkN_test, SigkN_test, SiglkN_test] = est.test_smoother();

            if ~comp(est.xhatkN, xhatkN_test, 1e-3)
                msg = [msg, ['failed (line ', num2str(getline()), '): ', ...
                             'xhatkN from calculate is different than ', ...
                             'xhatkN calculated from test_smoother']];
            end%if

            if ~comp(est.SigkN, SigkN_test, 1e-3)
                msg = [msg, ['failed (line ', num2str(getline()), '): ', ...
                             'SigkN from calculate is different than ', ...
                             'SigkN calculated from test_smoother']];
            end%if

            if ~comp(est.SiglkN, SiglkN_test, 1e-3)
                msg = [msg, ['failed (line ', num2str(getline()), '): ', ...
                             'SiglkN from calculate is different than ', ...
                             'SiglkN calculated from test_smoother']];
            end%if

            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

            for X = {'G', 'H', 'Q', 'R', 'S'}
                mdl.(X{:}) = [];
            end%for

            mdl.C = [C, ones(p, 1)];

            caughterr = false();
            try
                est.check('compatible');
            catch
                caughterr = true();
            end%trycatch
            if ~caughterr
                msg = [msg, ['failed (line ', num2str(getline()), '): ', ...
                             'should get an error from check compatible']];
            end%if

            mdl.C = C;

            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

            old_xhat10 = est.xhat10;
            est.xhat10 = ones(n + 1, 1);

            caughterr = false();
            try
                est.check('xhat10');
            catch
                caughterr = true();
            end%trycatch
            if ~caughterr
                msg = [msg, ['failed (line ', num2str(getline()), '): ', ...
                             'should get an error from check xhat10']];
            end%if

            est.xhat10 = old_xhat10;

            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

            est.Sig10 = eye(n + 1);

            caughterr = false();
            try
                est.check('Sig10size');
            catch
                caughterr = true();
            end%trycatch
            if ~caughterr
                msg = [msg, ['failed (line ', num2str(getline()), '): ', ...
                             'should get an error from check Sig10size']];
            end%if

            est.Sig10 = Sig10;

            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

            caughterr = false();
            try
                est.check('Sig10spec');
            catch
                caughterr = true();
            end%trycatch
            if ~caughterr
                msg = [msg, ['failed (line ', num2str(getline()), '): ', ...
                             'should get an error from check Sig10spec']];
            end%if

            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

            for X = {'Phi', 'Theta'}
                mdl.(X{:}) = eval(X{:});
            end%for

            caughterr = false();
            try
                est.check('Sig10spec');
            catch
                caughterr = true();
            end%trycatch
            if ~caughterr
                msg = [msg, ['failed (line ', num2str(getline()), '): ', ...
                             'should get an error from check Sig10spec']];
            end%if

            for X = {'Phi', 'Theta'}
                mdl.(X{:}) = [];
            end%for

            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

            for X = {'Kcal', 'Phibb', 'Thetabb'}
                mdl.(X{:}) = eval(X{:});
            end%for

            caughterr = false();
            try
                est.check('Sig10spec');
            catch
                caughterr = true();
            end%trycatch
            if ~caughterr
                msg = [msg, ['failed (line ', num2str(getline()), '): ', ...
                             'should get an error from check Sig10spec']];
            end%if

            for X = {'Kcal', 'Phibb', 'Thetabb'}
                mdl.(X{:}) = [];
            end%for

            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

            mdl.Lcal = Lcal;

            caughterr = false();
            try
                est.check('Lcalspec');
            catch
                caughterr = true();
            end%trycatch
            if ~caughterr
                msg = [msg, ['failed (line ', num2str(getline()), '): ', ...
                             'should get an error from check Lcalspec']];
            end%if

            mdl.Lcal = [];

            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

            caughterr = false();
            try
                est.flags = 1;
            catch
                caughterr = true();
            end%trycatch
            if ~caughterr
                msg = [msg, ['failed (line ', num2str(getline()), '): ', ...
                             'should get an error from set.flags']];
            end%if

            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

            est = ceestimator();
            est_same = est;
            est_different = ceestimator();
            if ~(est == est_same) ...
               || (~(est ~= est_different) ...
               || ((est ~= est_same) ...
               || (est == est_different)))
                msg = [msg, ['failed (line ', num2str(getline()), '): ', ...
                             '== and/or ~= are not working as expected']];
            end%if

        catch err
            lns = cellfun(@num2str, {err.stack.line}, 'UniformOutput', false());
            msg = [msg, ['UNEXPECTED ERROR: ', err.message, char(10), ...
                         'look at line(s) ', strjoin(lns, ', ')]];
        end%trycatch

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        if ~isempty(msg)
            disp(['ceestimator: ', name, ': ', char(10), ...
                  strjoin(msg, char(10))]);
        else
            disp(['ceestimator: ', name, ': all unittests passed']);
        end%if
    end%function unittests_

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function unittests()
        for b = {'B', ''}
            for d = {'D', ''}
                for x = {'xhat10', ''}
                    ceestimator.unittests_(b{:}, d{:}, x{:});
                end%for
            end%for
        end%for
    end%function unittests
end%methods (Static)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

methods (Access = private)
    function [xhatkN, SigkN, SiglkN] = test_smoother(self)
        self.check();

        n = self.model.n;
        p = self.model.p;
        m = self.model.m;
        Tf = self.data.Tf;
        Nd = self.data.Nd; % Nd = Tf + 1

        Bexist = ismember('B', self.model.spec.BD);
        Dexist = ismember('D', self.model.spec.BD);

        A = cell(Nd, 1);
        A{1} = eye(n);
        for i = 2:Nd
            A{i} = A{i-1} * self.model.A;
        end%for

        if Bexist
            B1 = blktoep([zeros(n); A(1:end-1)], 'tril') ...
                  * kron(eye(Nd), self.model.B);
            B2 = kron(eye(Nd), self.model.C) * B1;
            if Dexist
                B2 = B2 + kron(eye(Nd), self.model.D);
            end%if
        end%if

        if Dexist && ~Bexist
            B2 = kron(eye(Nd), self.model.D);
        end%if

        A1 = blktoep(cellfun(@(X) [X, zeros(n, p)], A, 'UniformOutput', ...
                                                              false()), 'tril');
        A2 = kron(eye(Nd), self.model.C) * A1 ...
              + kron(eye(Nd), [zeros(p, n), eye(p)]);

        if self.spec.xhat10
            xhat10 = self.xhat10;
        else
            xhat10 = zeros(n, 1);
        end%if
        mz = [xhat10; zeros(Tf * n + Nd * p, 1)];

        if self.spec.Sig10
            Sig10 = self.Sig10;
        else
            Sig10 = self.model.Sigkj;
        end%if
        RSQblock = [self.model.HRH, self.model.GSH'; ...
                    self.model.GSH, self.model.GQG];
        Pz = blkdiag(Sig10, kron(eye(Tf), RSQblock), self.model.HRH);

        tmp = ApinvB(A1 * Pz * A2', forceherm(A2 * Pz * A2'));

        z1 = self.data.y(:) - A2 * mz;
        if Bexist || Dexist
            z1 = z1 - B2 * self.data.u(:);
        end
        xhatkN = A1 * mz + tmp * z1;
        if Bexist
            xhatkN = B1 * self.data.u(:) + xhatkN;
        end%if
        xhatkN = reshape(xhatkN, n, Nd);

        Pxy = forceherm(A1 * Pz * A1' - tmp * A2 * Pz * A1');
        SigkN = celldiag(mat2cell(Pxy, repmat(n, Nd, 1), repmat(n, Nd, 1)))';
        SiglkN = celldiag(mat2cell(Pxy((n + 1):end, 1:(end - n)), ...
                 repmat(n, Nd - 1, 1), repmat(n, Nd - 1, 1)))';
    end%function test_smoother
end%methods (Access = private)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

end%classdef ceestimator

