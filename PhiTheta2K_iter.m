%% Copyright (C) 2018 Travis Arnold
%%
%% This file is part of Octave.
%%
%% Octave is free software; you can redistribute it and/or modify it
%% under the terms of the GNU General Public License as published by
%% the Free Software Foundation; either version 3 of the License, or (at
%% your option) any later version.
%%
%% Octave is distributed in the hope that it will be useful, but
%% WITHOUT ANY WARRANTY; without even the implied warranty of
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
%% General Public License for more details.
%%
%% You should have received a copy of the GNU General Public License
%% along with Octave; see the file COPYING.  If not, see
%% <http://www.gnu.org/licenses/>.

%% [K, T, E, isconv, rnorm, rsnorm] = PhiTheta2K_iter(A, C, Phi, Theta, kwargs)
%%
%% Find the optimal linear predictor (steady-state Kalman predictor) by
%% iterating the discrete algebraic Riccati equation. The documentation for
%% dare_iter is recommended reading, as PhiTheta2K_iter is simply a wrapper for
%% dare_iter.
%%
%% * SYSTEM *
%%
%% x[k+1] = A*x[k] + B*u[k] + w[k]   (state transition)
%%   y[k] = C*x[k] + D*u[k] + v[k]     (measurement)
%%
%% E[w] = [0]      E[ww' wv'] = [Q  S]
%%  [v]   [0]       [vw' vv']   [S' R]
%%
%% P = A*P*A' + Q
%%
%% Phi = C*P*C' + R
%%
%% Theta = A*P*C' + S
%%
%% * INPUTS *
%%
%% A : state transition matrix (n x n)
%%
%% C : measurement matrix (p x n)
%%
%% Phi : matrix (p x p)
%%
%% Theta : matrix (n x p)
%%
%% * OPTIONAL INPUTS *
%%
%% kwargs may be passed as either a struct or as key-value pairs. Optional
%% inputs are the same as those for dare_iter, with the exception that optional
%% argument S may not be passed.
%%
%% * OUTPUTS *
%% 
%% K : steady-state Kalman predictor gain (n x p)
%% K = -(A*T*C' - Theta) * pinv(Phi - C*T*C')
%%
%% T : matrix (n x n)
%% T = E[ (x[k|k-1] - E[x[k]]) * (x[k|k-1] - E[x[k]])' ]
%% T = A*T*A' + (A*T*C' - Theta) * pinv(Phi - C*T*C') * (A*T*C' - Theta)'
%%
%% E : steady-state closed loop poles (n x 1)
%% E = eig(A - K*C)
%%
%% isconv : True if the iterations converge to a solution.
%%
%% rnorm : Frobenius norm of the residual.
%%
%% rsnorm : Scaled Frobenius norm of the residual.

%% Author: Travis Arnold <tjarnold@wisc.edu>
%% July 2018.

function [K, T, E, isconv, rnorm, rsnorm] = PhiTheta2K_iter(A, C, Phi, Theta, varargin)

kwargs = args2kwargs(varargin{:});

if isfield(kwargs, 'S')
    error('cannot pass option S here');
end%if

if ~isnumericmatrix(A)
    error('A must be a numeric matrix');
end%if

if ~myissquare(A)
    error('A must be square');
end%if

if ~isnumericmatrix(Theta)
    error('Theta must be a numeric matrix');
end%if

n = size(A, 1);
kwargs.S = -Theta;

[T, E, K, ~, ~, ~, isconv, rnorm, rsnorm] = dare_iter(A', C', zeros(n, n), ...
                                                   -Phi, kwargs);
% Transpose K.
K = cellctrans(K);

end%function PhiTheta2K_iter

