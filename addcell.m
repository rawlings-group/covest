%% Copyright (C) 2018 Travis Arnold
%%
%% This file is part of Octave.
%%
%% Octave is free software; you can redistribute it and/or modify it
%% under the terms of the GNU General Public License as published by
%% the Free Software Foundation; either version 3 of the License, or (at
%% your option) any later version.
%%
%% Octave is distributed in the hope that it will be useful, but
%% WITHOUT ANY WARRANTY; without even the implied warranty of
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
%% General Public License for more details.
%%
%% You should have received a copy of the GNU General Public License
%% along with Octave; see the file COPYING.  If not, see
%% <http://www.gnu.org/licenses/>.

%% S = addcell(C)
%%
%% Adds all elements of cell array C together, assuming all elements are
%% numerical arrays of the same size. An error is issued if any of the elements
%% of C are sparse.

%% Author: Travis Arnold <tjarnold@wisc.edu>
%% June 2018.

function S = addcell(C)
    if nargin() ~= 1
        error('addcell: exactly one input argument required')
    end%if

    if ~iscell(C)
        error('addcell: C must be a cell array');
    end%if

    C = C(:);
    n = numel(C);

    Cisnumeric = cellfun(@isnumeric, C);
    if ~all(Cisnumeric)
        error('addcell: all elements of C must be numeric arrays');
    end%if

    Cissparse = cellfun(@issparse, C);
    if any(Cissparse)
        error('addcell: elements of C may not be sparse');
    end%if

    Csizes = cellfun(@size, C, 'UniformOutput', false);
    if n > 1
        if ~isequal(Csizes{:})
            error('addcell: all elements of C must have same dimensions');
        end%if
    end%if

    d = numel(Csizes{1}); % number of dimensions
    S = sum(cat(d+1, C{:}), d+1);
end%function addcell

%!error <exactly one input argument required> addcell();
%!error <exactly one input argument required> addcell(1, 2);
%!error <C must be a cell array> addcell(1);
%!error <C must be a cell array> addcell('hello');
%!error <all elements of C must be numeric arrays> addcell({'a'});
%!error <all elements of C must be numeric arrays> addcell({{1}});
%!error <elements of C may not be sparse> addcell({speye(2)});
%!error <elements of C may not be sparse> addcell({speye(2), speye(2)});
%!error <all elements of C must have same dimensions> addcell({1, ones(2,2,2)});

%!test
%!
%! for i = 1:5
%! for j = 1:5
%! for k = 1:5
%!
%! A = randn(i, j, k);
%! B = randn(i, j, k);
%! C = randn(i, j, k);
%! D = randn(i, j, k);
%! E = randn(i, j, k);
%! F = randn(i, j, k);
%! G = randn(i, j, k);
%! H = randn(i, j, k);
%! I = randn(i, j, k);
%!
%! assert(A + B, addcell({A, B}));
%! assert(C + D + E, addcell({C, D, E}));
%! assert(F + G + H + I, addcell({F, G, H, I}));
%!
%! end%for
%! end%for
%! end%for

