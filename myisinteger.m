%% Copyright (C) 2018 Travis Arnold
%%
%% This file is part of Octave.
%%
%% Octave is free software; you can redistribute it and/or modify it
%% under the terms of the GNU General Public License as published by
%% the Free Software Foundation; either version 3 of the License, or (at
%% your option) any later version.
%%
%% Octave is distributed in the hope that it will be useful, but
%% WITHOUT ANY WARRANTY; without even the implied warranty of
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
%% General Public License for more details.
%%
%% You should have received a copy of the GNU General Public License
%% along with Octave; see the file COPYING.  If not, see
%% <http://www.gnu.org/licenses/>.

%% [c, a] = myisinteger(A)
%%
%% Return c = true if A is an integer. Unlike Octave's built in isinteger
%% function, this will work on floating point values, but is only accurate to
%% machine precision. Optionally, also return a = floor(A). Note that
%%
%% [true, Inf] = myisinteger(Inf());
%%
%% [false, NaN] = myisinteger(NaN());
%%
%% See also: isinteger.

%% Author: Travis Arnold <tjarnold@wisc.edu>
%% July 2018.

function [c, a] = myisinteger(A)

if ~isnumericscalar(A)
    error('A must be a numeric scalar');
end%if

a = floor(A);
c = (A == a);
end%function myisinteger

