%% Copyright (C) 2003-2017 Gabriele Pannocchia
%% Copyright (C) 2018 Travis Arnold
%%
%% This file is part of Octave.
%%
%% Octave is free software; you can redistribute it and/or modify it
%% under the terms of the GNU General Public License as published by
%% the Free Software Foundation; either version 3 of the License, or (at
%% your option) any later version.
%%
%% Octave is distributed in the hope that it will be useful, but
%% WITHOUT ANY WARRANTY; without even the implied warranty of
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
%% General Public License for more details.
%%
%% You should have received a copy of the GNU General Public License
%% along with Octave; see the file COPYING.  If not, see
%% <http://www.gnu.org/licenses/>.

%% retval = myisdefinite(A, tol)
%%
%% Return 1 if A is symmetric positive definite within the tolerance specified
%% by tol or 0 if A is symmetric positive semidefinite. Otherwise, return -1.
%%
%% If tol is omitted, a default value of 100 * eps() is used. Note that
%% internally A is scaled to have Frobenius norm 1. The only exception to this
%% is if norm(A, 'fro') is zero to machine precision. In this case, 0 is
%% automatically returned.
%%
%% This function is very similar (and in fact was adapted from) Octave's built
%% in isdefinite function, but requires the tolerance to be strictly positive,
%% as using a tolerance of exactly zero can produce unexpected results.
%%
%% See also: issymmetric, ishermitian, isdefinite.

%% Author: Travis Arnold <tjarnold@wisc.edu>
%% July 2018.
%% Adapted from Octave's built in isdefinite function written by Gabriele
%% Pannocchia.

function retval = myisdefinite(A, tol)

% Check number of input arguments.
if ~ismember(nargin(), [1, 2])
    error('exactly one or two input arguments required');
end%if

% Make sure A is a numeric matrix.
if ~isnumericmatrix(A)
    error('A must be a numeric matrix');
end%if

% Make sure A is Hermitian.
if ~ishermitian(A)
    error('A must be a Hermitian matrix');
end%if

% If it is provided, perform checks on tol.
if (nargin() == 2)
    % Make sure tol is a numeric scalar.
    if ~isnumericscalar(tol)
        error('tol must be a numeric scalar');
    end%if

    % Make sure tol is positive.
    if ~(tol > 0)
        error('tol must be positive');
    end%if
end%if

n = size(A, 1);

% If A is zero to machine precision, return that it is positive semidefinite.
% Otherwise, use Cholesky factorizations to determine definiteness.

% Extract Frobenius norm of A.
Anorm = norm(A, 'fro');

if (Anorm / n < 100 * eps())
    retval = 0;
else
    % Normalize A to have Frobenius norm of 1.
    A = A / Anorm; 

    % If it is not provided, set tol to default value.
    if (nargin == 1)
        tol = 100 * eps(class(A));
    end%if

    % Cholesky factorizations.
    ee = tol * eye(size(A, 1));
    [r, p] = chol(A - ee);
    if (p == 0)
        retval = 1; % Positive definite.
    else
        [r, p] = chol(A + ee);
        if (p == 0)
            retval = 0; % Positive semidefinite.
        else
            retval = -1;
        end%if
    end%if
end%if

end%function myisdefinite

%!test
%! A = [-1 0; 0 -1];
%! assert(myisdefinite(A), -1);

%!test
%! A = [1 0; 0 1];
%! assert(myisdefinite(A), 1);

%!test
%! A = [2 -1 0; -1 2 -1; 0 -1 2];
%! assert(myisdefinite(A), 1);

%!test
%! A = [1 0; 0 0];
%! assert(myisdefinite(A), 0);

%!error <exactly one or two input arguments required> myisdefinite()
%!error <exactly one or two input arguments required> myisdefinite(1,2,3)
%!error <tol must be a numeric scalar> myisdefinite(0, 'abcd')
%!error <tol must be a numeric scalar> myisdefinite(0, eye(2))
%!error <tol must be positive> myisdefinite(0, 0)
%!error <tol must be positive> myisdefinite(0, -1)

%!error <A must be a Hermitian matrix> myisdefinite([1 2; 3 4])
%!error <A must be a numeric matrix> myisdefinite('abcd')

