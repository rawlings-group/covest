%% Copyright (C) 2019 Travis Arnold
%%
%% This file is part of Octave.
%%
%% Octave is free software; you can redistribute it and/or modify it
%% under the terms of the GNU General Public License as published by
%% the Free Software Foundation; either version 3 of the License, or (at
%% your option) any later version.
%%
%% Octave is distributed in the hope that it will be useful, but
%% WITHOUT ANY WARRANTY; without even the implied warranty of
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
%% General Public License for more details.
%%
%% You should have received a copy of the GNU General Public License
%% along with Octave; see the file COPYING.  If not, see
%% <http://www.gnu.org/licenses/>.

%% bool = comp(A, B, tol)
%%
%% Returns bool = true if norm(A(:) - B(:), 'fro') <= tol * norm(A(:), 'fro'). A
%% and B must be arrays or cell arrays. In the case of cell arrays, cell2mat is
%% applied. A and B (or the elements thereof, in the case of cell arrays) do not
%% have to be numeric, but if they are not they will be converted using double.
%% The default value for tol is 1e-6.
%%
%% See also: isequal, eq.

%% Author: Travis Arnold <tjarnold@wisc.edu>
%% July 2019.

function bool = comp(A, B, tol)

if nargin() == 3
    if ~isnumericscalar(tol) || tol < 0
        error('tol must be a nonnegative numeric scalar');
    end%if
elseif nargin() == 2
    tol = 1e-6;
else
    error('exactly 2 or 3 input arguments required');
end%if

for X = {'A', 'B'}
    if iscell(eval(X{:}))
        if ~all(cellfun(@isnumeric, eval(X{:})))
            eval([X{:}, ' = cellfun(@double, ', X{:}, '''UniformOutput'', ', ...
                  'false());']);
        end%if
        eval([X{:}, ' = cell2mat(', X{:} ,');']);
    elseif ~isnumeric(eval(X{:}))
        eval([X{:}, ' = double(', X{:}, ');']);
    end%if
end%for

if numel(A) ~= numel(B)
    error('A and B must have an equal number of elements');
end%if

bool = norm(A(:) - B(:), 'fro') <= tol * norm(A(:), 'fro');

end%function comp

