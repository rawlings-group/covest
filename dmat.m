%% Copyright (C) 2017 Travis Arnold
%%
%% This file is part of Octave.
%%
%% Octave is free software; you can redistribute it and/or modify it
%% under the terms of the GNU General Public License as published by
%% the Free Software Foundation; either version 3 of the License, or (at
%% your option) any later version.
%%
%% Octave is distributed in the hope that it will be useful, but
%% WITHOUT ANY WARRANTY; without even the implied warranty of
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
%% General Public License for more details.
%%
%% You should have received a copy of the GNU General Public License
%% along with Octave; see the file COPYING.  If not, see
%% <http://www.gnu.org/licenses/>.

%% -*- texinfo -*-
%% @deftypefn {Function File} {@var{D} =} dmat (@var{m}, @var{n})
%% Return the matrix
%% @tex
%%  $D_{m,n}$
%% @end tex
%% @ifnottex
%%  @math{D(m,n)}
%% @end ifnottex
%% which is the unique
%% @tex
%%  $mn \times mn - n(n-1)/2$
%% @end tex
%% @ifnottex
%%  @math{@var{m}*@var{n}} by @math{@var{m}*@var{n}-@var{n}*(@var{n}-1)/2}
%% @end ifnottex
%% matrix such that
%% @tex
%%  $D_{m,n} {\rm myvech} \, A = {\rm vec} \, A$
%% @end tex
%% @ifnottex
%%  @math{D(m,n)*myvech (A) = vec (A)}
%% @end ifnottex
%% for all
%% @tex
%%  $m \times n$
%% @end tex
%% @ifnottex
%%  @math{@var{m}} by @math{@var{n}}
%% @end ifnottex
%% matrices
%% @tex
%%  $A$
%% @end tex
%% @ifnottex
%%  @math{A}
%% @end ifnottex
%% with
%% @tex
%%  $m \geq n$
%% @end tex
%% @ifnottex
%%  @math{@var{m} >= @var{n}}
%% @end ifnottex
%% and the upper
%% @tex
%%  $n \times n$
%% @end tex
%% @ifnottex
%%  @math{@var{n} by @var{n}}
%% @end ifnottex
%% block of 
%% @tex
%%  $A$
%% @end tex
%% @ifnottex
%%  @math{A}
%% @end ifnottex
%% symmetric.
%%
%% If only one argument @var{m} is given,
%% @tex
%%  $D_{m,m}$
%% @end tex
%% @ifnottex
%%  @nospell{@math{D(m,m)}}
%% @end ifnottex
%% is returned.
%%
%% Returns a sparse matrix.
%%
%% This is a generalization of the duplication matrix. See Magnus and Neudecker
%% (1988), @cite{Matrix Differential Calculus with Applications in Statistics
%% and Econometrics}.
%% @seealso{duplication_matrix, dupmat, dupmatpinv, dmatpinv, vec, vech, myvech}
%% @end deftypefn

%% Author: Travis Arnold <tjarnold@wisc.edu>, December 2017.
%% Adapted to be Matlab compatible, May 2018.

function D = dmat(m, n)
    if (nargin() < 1 || nargin() > 2)
        error('dmat: one or two input arguments is required');
    end%if

    try
        [misposint, m] = isposinteger(m);
    catch
        error('dmat: M must be a positive integer');
    end%trycatch

    if ~misposint
        error('dmat: M must be a positive integer');
    end%if

    if (nargin() == 1)
        D = dupmat(m);
        return;
    end%if

    try
        [nisposint, n] = isposinteger(n);
    catch
        error('dmat: N must be a positive integer');
    end%trycatch

    if ~nisposint
        error('dmat: N must be a positive integer');
    end%if

    if (~(m >= n))
        error('dmat: M must be greater than or equal to N');
    end%if

    count = [0, arrayfun(@(k) sum(m-1:-1:k), m-1:-1:m-n+1)];
    j = 1:n;
    row1 = (j-1)*m + j;
    col1 = count + j;

    col23 = arrayfun(@(k) count(k) + (k+1:n), 1:n-1, 'UniformOutput', false);
    col23 = horzcat(col23{:});

    row2 = arrayfun(@(k) (k-1)*m + (k+1:n) , 1:n-1, 'UniformOutput', false);
    row2 = horzcat(row2{:});

    row3 = arrayfun(@(k) ((k+1:n)-1)*m + k , 1:n-1, 'UniformOutput', false);
    row3 = horzcat(row3{:});

    col4 = arrayfun(@(k) (k*m-m+n+1-k*(k-1)/2):(k*m-k*(k-1)/2), 1:n, ...
                    'UniformOutput', false);
    col4 = horzcat(col4{:});

    row4 = arrayfun(@(k) k*m-m+n+1:k*m, 1:n, 'UniformOutput', false);
    row4 = horzcat(row4{:});

    D = sparse([row1 row2 row3 row4], [col1 col23 col23 col4], 1);

    % Below is an easier way to calculate this matrix, but it is slower.
    % D = dupmat(m)(1:m*n, 1:(m*n - n*(n-1)/2));
end%function dmat

%!assert(full(dmat(1)), 1);
%!assert(full(dmat(4)), duplication_matrix(4));
%!assert(full(dmat(7)), duplication_matrix(7));
%!assert(dmat(2), dmat(2, 2));

%!test
%! N = 2;
%! A = rand(N);
%! B = A * A';
%! C = A + A';
%! D = dmat(N);
%! assert(D * vech(B), vec(B), 1e-6);
%! assert(D * vech(C), vec(C), 1e-6);

%!test
%! N = 3;
%! A = rand(N);
%! B = A * A';
%! C = A + A';
%! D = dmat(N);
%! assert(D * vech(B), vec(B), 1e-6);
%! assert(D * vech(C), vec(C), 1e-6);

%!test
%! N = 4;
%! A = rand(N);
%! B = A * A';
%! C = A + A';
%! D = dmat(N);
%! assert(D * vech(B), vec(B), 1e-6);
%! assert(D * vech(C), vec(C), 1e-6);

%!test
%! M = 2;
%! N = 1;
%! A = rand(M);
%! B = A * A'; B = B(:, 1:N);
%! C = A + A'; C = C(:, 1:N);
%! D = dmat(M, N);
%! assert(D * myvech(B), vec(B), 1e-6);
%! assert(D * myvech(C), vec(C), 1e-6);

%!test
%! M = 3;
%! N = 2;
%! A = rand(M);
%! B = A * A'; B = B(:, 1:N);
%! C = A + A'; C = C(:, 1:N);
%! D = dmat(M, N);
%! assert(D * myvech(B), vec(B), 1e-6);
%! assert(D * myvech(C), vec(C), 1e-6);

%!test
%! M = 4;
%! N = 3;
%! A = rand(M);
%! B = A * A'; B = B(:, 1:N);
%! C = A + A'; C = C(:, 1:N);
%! D = dmat(M, N);
%! assert(D * myvech(B), vec(B), 1e-6);
%! assert(D * myvech(C), vec(C), 1e-6);

%!test
%! M = 4;
%! N = 2;
%! A = rand(M);
%! B = A * A'; B = B(:, 1:N);
%! C = A + A'; C = C(:, 1:N);
%! D = dmat(M, N);
%! assert(D * myvech(B), vec(B), 1e-6);
%! assert(D * myvech(C), vec(C), 1e-6);

%!test
%! M = 10;
%! N = 7;
%! A = rand(M);
%! B = A * A'; B = B(:, 1:N);
%! C = A + A'; C = C(:, 1:N);
%! D = dmat(M, N);
%! assert(D * myvech(B), vec(B), 1e-6);
%! assert(D * myvech(C), vec(C), 1e-6);

%!error <one or two input arguments is required> dmat()
%!error <one or two input arguments is required> dmat(1, 2, 3)
%!error <M must be a positive integer> dmat(0.5)
%!error <M must be a positive integer> dmat(-1)
%!error <M must be a positive integer> dmat(ones(1,4))
%!error <N must be a positive integer> dmat(1, 0.5)
%!error <N must be a positive integer> dmat(1, -1)
%!error <N must be a positive integer> dmat(1, ones(1,4))
%!error <M must be greater than or equal to N> dmat(1, 2)

