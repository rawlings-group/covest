%% Copyright (C) 2019 Travis Arnold
%%
%% This file is part of Octave.
%%
%% Octave is free software; you can redistribute it and/or modify it
%% under the terms of the GNU General Public License as published by
%% the Free Software Foundation; either version 3 of the License, or (at
%% your option) any later version.
%%
%% Octave is distributed in the hope that it will be useful, but
%% WITHOUT ANY WARRANTY; without even the implied warranty of
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
%% General Public License for more details.
%%
%% You should have received a copy of the GNU General Public License
%% along with Octave; see the file COPYING.  If not, see
%% <http://www.gnu.org/licenses/>.

%% depends = calcdepends(baseprops, dependentprops, dependsspec)
%%
%% Calculate struct depends, which shows which baseprops each dependentprop
%% depends on. baseprops and dependentprops are cell arrays of character
%% vectors. dependsspec is a struct that shows how each dependentprop depends on
%% both baseprops and dependentprops.

%% Author: Travis Arnold <tjarnold@wisc.edu>
%% January 2019.

function depends = calcdepends(baseprops, dependentprops, dependsspec)

% Check number of arguments.
if nargin() ~= 3
    error('Exactly three input arguments required');
end%if

% Make sure baseprops is a cell.
if ~iscell(baseprops)
    error('baseprops must be a cell array');
end%if

% Make sure all elements of baseprops are character vectors.
if ~all(cellfun(@ischarrow, baseprops))
    error('all elements of baseprops must be character vectors');
end%if

% Make sure dependentprops is a cell.
if ~iscell(dependentprops)
    error('dependentprops must be a cell array');
end%if

% Make sure all elements of dependentprops are character vectors.
if ~all(cellfun(@ischarrow, dependentprops))
    error('all elements of dependentprops must be character vectors');
end%if

% Make sure dependsspec is a struct.
if ~isstruct(dependsspec)
    error('dependsspec must be a struct');
end%if

% Make sure fields of dependsspec coincide exactly with dependentprops.
a = sort(dependentprops);
a = a(:);
b = sort(fieldnames(dependsspec));
b = b(:);
if ~isequal(a, b)
    error('fields of dependsspec must coincide exactly with dependentprops');
end%if

% Check for unrecognized property names in dependsspec.
u = {};
for X = dependentprops
    u = union(u, dependsspec.(X{:}));
end%for
sd = setdiff(u, union(baseprops, dependentprops));
if ~isempty(sd)
    error(['all property names in dependsspec must be either baseprops or ', ...
           'dependentprops (unrecognized property name(s): ', ...
           strjoin(sd, ', '), ')']);
end%if

depends = struct();

for X = dependentprops
    % Starting property list.
    proplist = dependsspec.(X{:});

    % Extract base properties.
    base = intersect(proplist, baseprops);

    % Extract dependent properties.
    dependent = intersect(proplist, dependentprops);

    while ~isempty(dependent)
        proplist = base;
        for Y = dependent
            proplist = unique([proplist, dependsspec.(Y{:})]);

            % Extract base properties.
            base = intersect(proplist, baseprops);

            % Extract dependent properties.
            dependent = intersect(proplist, dependentprops);
        end%for
    end%while

    depends.(X{:}) = base;
end%for

end%function calcdepends

