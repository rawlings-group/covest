%% Copyright (C) 2018 Travis Arnold
%%
%% This file is part of Octave.
%%
%% Octave is free software; you can redistribute it and/or modify it
%% under the terms of the GNU General Public License as published by
%% the Free Software Foundation; either version 3 of the License, or (at
%% your option) any later version.
%%
%% Octave is distributed in the hope that it will be useful, but
%% WITHOUT ANY WARRANTY; without even the implied warranty of
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
%% General Public License for more details.
%%
%% You should have received a copy of the GNU General Public License
%% along with Octave; see the file COPYING.  If not, see
%% <http://www.gnu.org/licenses/>.

%% [X, L, G, Z, H, M, isconv, rnorm, rsnorm, iters] = dare_iter(A, B, Q, R, kwargs)
%%
%% Attempt to solve the generalized discrete time algebraic Riccati equation
%% (DARE) by iterating.
%%
%% * INPUTS *
%%
%% A : matrix (n x n)
%%
%% B : matrix (n x m)
%%
%% Q : matrix (n x n)
%%
%% R : matrix (m x m).
%%
%% * OPTIONAL INPUTS *
%%
%% kwargs may be passed as either a struct or as key-value pairs with the
%% following optional inputs:
%%
%% S : matrix (n x m). Default value: zeros(n, m).
%%
%% X0 : matrix (n x n). Inital value of X to start the iterations. Default
%%      value: eye(n). Remark: X0 = Q is also a reasonable initial value, but in
%%      practice this can lead to inaccurate results for low rank systems. An
%%      identity matrix gives more consistent results in these cases.
%%
%% forceherm : Nonnegative integer, or Inf(). Specifies the initial number of
%%             iterations of the Riccati equation that are not forced to be
%%             Hermitian. For example if forceherm = 0, then Hermitian-ness will
%%             always be enforced. If forceherm = Inf(), then Hermitian-ness
%%             will never be enforced. If forceherm = 10, then Hermitian-ness
%%             will only be enforced after the first 10 iterations. This option
%%             is included because when starting with X0 = zeros(n, n) it is
%%             possible that the Riccati iterations can get stuck cycling
%%             between small values. Default value: 0.
%%
%% maxiters : Maximum number of iterations. Default value: 10000.
%%
%% forcealliters : Force maximum number of iterations to occur, regardless of
%%                convergence status. Default value: false().
%%
%% returnalliters : Return results of all iterations. There are three options:
%%                     'all' : Return results from every iteration, with matrix
%%                             quantities in cell arrays and scalar quantities
%%                             in arrays.
%%                     'scalars' : Return results from every iteration for only
%%                                 the scalar quantities (rnorm, rsnorm, and
%%                                 isconv).
%%                     'none' : Only return the final results. Default.
%%
%% reltol : Relative solution tolerance. Default value: 1e-12.
%%
%% abstol : Absolute solution tolerance. Default value: 1e-100.
%%
%% pinvfun : Character vector specifying which pseudoinverse function to use.
%%           The options are 'pinv', 'rpinv', 'pinvAB', and 'lsqminnorm' 
%%           (Matlab only). Default value: 'pinvAB'.
%%
%% pinvtol: Tolerance passed to pinvfun function. Optional; if not specified,
%%          the default is used (note that the default values for 'pinv' and
%%          'rpinv' are different). To avoid ambiguity, when pinvtol is user
%%          specified, pinvfun must also be user specified.
%%
%% checksys : If true(), the stabilizability and detectability properties of the
%%            system are checked, and warnings are issued if they indicate that
%%            the DARE may not have a solution. Default value: true() in Octave,
%%            false() in Matlab (as of now I cannot find a reliable way to
%%            determine stabilizability in Matlab).
%%
%% warnifnotcnv : If true(), if the DARE iterations do not converge, a warning
%%                is issued. Default value: true().
%%
%% * OUTPUTS *
%%
%% X : Solution to the Riccati equation (n x n matrix). Returns a cell array of
%%     Riccati iterates if returnalliters = 'all'. Note that X (or X{end}) may
%%     not be a solution to the the DARE! You must check the isconv output to
%%     see if the iterations converged. Corresponds to Kalman predictor error.
%%
%% L : Closed loop poles (n x 1). Returns a matrix if returnalliters = 'all',
%%     where each column contains the closed loop poles for each iteration.
%%
%% G : Gain matrix (m x n). Corresponds to Kalman predictor gain. Returns a cell
%%     array if returnalliters = 'all'.
%%
%% Z : Corresponds to the Kalman filter error (n x n matrix). Returns a cell
%%     array if returnalliters = 'all'.
%%
%% H : Gain matrix (m x n). Corresponds to Kalman filter gain. Returns a cell
%%     array if returnalliters = 'all'.
%%
%% M : Matrix (m x n). Storing this matrix can be useful for the Kalman
%%     smoothing algorithm.
%%
%% isconv : True if the iterations converge to a solution. The solution is
%%          considered converged if
%%              norm(Xnew - Xold, 'fro') < reltol * norm(Xnew, 'fro')
%%                  or
%%              norm(Xnew - Xold, 'fro') < abstol
%%          Returns a one row matrix if returnalliters = 'all' or 'scalars'.
%%
%% rnorm : Frobenius norm of the residual:
%%             rnorm = norm(Xnew - Xold, 'fro')
%%         Returns a one row matrix if returnalliters = 'all' or 'scalars'.
%%
%% rsnorm : Scaled Frobenius norm of the residual:
%%              rsnorm = rnorm / norm(Xnew, 'fro')
%%          Returns a one row matrix if returnalliters = 'all' or 'scalars'.
%%
%% iters : Number of Riccat iterations performed.
%%
%% timespent : Amount of time (in seconds) that it takes problem to solve. Pre
%%             and post processing time is not included; timespent only includes
%%             the time spent on actually calculating Riccati iterates.
%%             Evaluated using tic and toc.
%%
%% * EQUATIONS *
%%
%% X = A'*X*A - (A'*X*B + S) * pinv(B'*X*B + R) * (A'*X*B + S)' + Q
%%
%% G = pinv(B'*X*B + R) * (A'*X*B + S)'
%%
%% M = pinv(B'*X*B + R) * B'
%%
%% H = pinv(B'*X*B + R) * (X*B)' = M * X'
%%
%% Z = X - X*B*H
%%
%% L = eig(A - B*G)
%%
%% See also: dare, pinv, rpinv.

%% Author: Travis Arnold <tjarnold@wisc.edu>
%% January 2019.

function [X, L, G, Z, H, M, isconv, rnorm, rsnorm, iters, timespent] = dare_iter(A, B, Q, R, varargin)

% Get kwargs struct.
kwargs = args2kwargs(varargin{:});

% Make sure kwargs are valid; throw an error if not.
checkkwargs(kwargs, {'S', 'X0', 'forceherm', 'maxiters', 'reltol', 'abstol', ...
                     'pinvfun', 'pinvtol', 'forcealliters', ...
                     'returnalliters', 'checksys', 'warnifnotcnv'});

% Check number of input arguments.
if nargin() < 4
    error('at least four input arguments required');
end%if

% Make sure A, B, Q, and R are numeric matrices.
if ~isnumericmatrix(A)
    error('A must be a numeric matrix');
end%if

if ~isnumericmatrix(B)
    error('B must be a numeric matrix');
end%if

if ~isnumericmatrix(Q)
    error('Q must be a numeric matrix');
end%if

if ~isnumericmatrix(R)
    error('R must be a numeric matrix');
end%if

% Make sure A, B, Q, and R have consistent sizes.
if ~myissquare(A)
    error('A must be square');
end%if

if ~myissquare(Q)
    error('Q must be square');
end%if

if ~myissquare(R)
    error('R must be square');
end%if

if (size(A, 1) ~= size(B, 1))
    error('sizes of A and B are inconsistent');
end%if

if (size(A, 1) ~= size(Q, 1))
    error('sizes of A and Q are inconsistent');
end%if

if (size(B, 2) ~= size(R, 1))
    error('sizes of B and R are inconsistent');
end%if

% Extract sizes of matrices.
n = size(A, 1);
m = size(B, 2);

% Disable divide by zero warnings.
if isoctave()
    warning('off', 'Octave:nearly-singular-matrix');
    warning('off', 'Octave:divide-by-zero');
    warning('off', 'Octave:singular-matrix');
else
    warning('off', 'MATLAB:nearlySingularMatrix');
    warning('off', 'MATLAB:singularMatrix');
    warning('off', 'MATLAB:divideByZero');
end%if

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% parse kwawrgs %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Set default values.
fh = 0; % Don't use forceherm as a variable name, since it is a function name.
S = zeros(n, m);
X0 = eye(n);
maxiters = 10000;
reltol = 1e-12;
abstol = 1e-100;
pinvfunspec = 'pinvAB';
forcealliters = false();
returnalliters = 'none';
checksys = isoctave();
warnifnotcnv = true();

% For each possible option, check if it is included in kwargs. If so, perform
% appropriate checks on the passed value.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% forceherm %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if isfield(kwargs, 'forceherm')
    [isnonnegint, fh] = isnonneginteger(kwargs.forceherm);
    if ~isnonnegint
        error('forceherm must be a positive integer or Inf');
    end%if
end%if

% Throw an error or warning if Q or R is not Hermitian.
if ~ishermitian(Q)
    if fh < Inf()
        % If Hermitian-ness will be enforced at some point, throw an error if
        % Q is not Hermitian.
        error('Q must be Hermitian when forceherm < Inf()');
    else
        warning(['Q is not Hermitian; proceeding as normal, but this is ', ...
                 'not a typical scenario and you may get unexpected results']);
    end%if
end%if

if ~ishermitian(R)
    if fh < Inf()
        % If Hermitian-ness will be enforced at some point, throw an error
        % if R is not Hermitian.
        error('R must be Hermitian when forceherm < Inf()');
    else
        warning(['R is not Hermitian; proceeding as normal, but this is', ...
                 'not a typical scenario and you may get unexpected results']);
    end%if
end%if

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% S %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if isfield(kwargs, 'S')
    % Make sure S is a numeric matrix.
    if ~isnumericmatrix(kwargs.S)
        error('S must be a numeric matrix');
    end%if

    % Make sure size of S is consistent with A and Q.
    if (size(kwargs.S, 1) ~= size(A, 1))
        error('size of S is inconsistent with A and Q');
    end%if

    % Make sure size of S is consistent with B.
    if (size(kwargs.S, 2) ~= size(B, 2))
        error('size of S is inconsistent with B');
    end%if

    S = kwargs.S;
end%if

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% X0 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if isfield(kwargs, 'X0')
    % Make sure X0 is a numeric matrix.
    if ~isnumericmatrix(kwargs.X0)
        error('X0 must be a numeric matrix');
    end%if

    % Make sure X0 is square.
    if ~myissquare(kwargs.X0)
        error('X0 must be square');
    end%if

    % Make sure size of X0 is consistent with A.
    if (size(kwargs.X0, 1) ~= n)
        error('size of X0 is inconsistent with A');
    end%if
    
    % Throw an error or warning if X0 is not Hermitian.
    if ~ishermitian(kwargs.X0)
        if fh < Inf()
            % If Hermitian-ness will be enforced at some point, throw an error
            % if X0 is not Hermitian.
            error('X0 must be Hermitian when forceherm < Inf()');
        else
            warning(['X0 is not Hermitian; proceeding as normal, but this ', ...
                     'is not a typical scenario and you may get ', ...
                     'unexpected results']);
        end%if
    end%if

    X0 = kwargs.X0;
end%if

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% maxiters %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if isfield(kwargs, 'maxiters')
    [maxitersnonnegint, maxiters] = isnonneginteger(kwargs.maxiters);
    if ~maxitersnonnegint
        error('maxiters must be a nonnegative integer');
    end%if
    maxiters = kwargs.maxiters;
end%if

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% reltol %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if isfield(kwargs, 'reltol')
    % Make sure reltol is a numeric scalar.
    if ~isnumericscalar(kwargs.reltol)
        error('reltol must be a numeric scalar');
    end%if

    % Make sure reltol is positive.
    if ~(kwargs.reltol > 0)
        error('reltol must be positive');
    end%if

    reltol = kwargs.reltol;
end%if

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% abstol %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if isfield(kwargs, 'abstol')
    % Make sure abstol is a numeric scalar.
    if ~isnumericscalar(kwargs.abstol)
        error('abstol must be a numeric scalar');
    end%if

    % Make sure abstol is positive.
    if ~(kwargs.abstol >= 0)
        error('abstol must be nonnegative');
    end%if

    abstol = kwargs.abstol;
end%if

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% pinvtol %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if isfield(kwargs, 'pinvtol')
    % Make sure pinvtol is a numeric scalar.
    if ~isnumericscalar(kwargs.pinvtol)
        error('pinvtol must be a numeric scalar');
    end%if

    % Make sure pinvtol is nonnegative.
    if ~(kwargs.pinvtol >= 0)
        error('pinvtol must be nonnegative');
    end%if

    % Make sure pinvfun has been specified.
    if ~isfield(kwargs, 'pinvfun')
        error(['when pinvtol is user specified, pinvfun must also be user ', ...
               'specified']);
    end%if

    pinvtol = kwargs.pinvtol;
    pinvtolgiven = true();
else
    pinvtolgiven = false();
end%if

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% pinvfun %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Set function handle pinvfun to the appropriate function to calculate A^+ * B.

if isfield(kwargs, 'pinvfun')
    % Make sure pinvfun is a character vector.
    if ~ischarrow(kwargs.pinvfun)
        error('pinvfun must be a character vector');
    end%if
    pinvfunspec = kwargs.pinvfun;
end%if

switch pinvfunspec
    case 'pinv'
        if pinvtolgiven
            pinvfun = @(a, b) pinv(a, pinvtol) * b;
        else
            pinvfun = @(a, b) pinv(a) * b;
        end%if
    case 'rpinv'
        if pinvtolgiven
            pinvfun = @(a, b) rpinv(a, pinvtol) * b;
        else
            pinvfun = @(a, b) rpinv(a) * b;
        end%if
    case 'pinvAB'
        if pinvtolgiven
            warning(['pinvAB does not accept a tolerance parameter; ', ...
                     'ignoring the given pinvtol']);
        end%if
        pinvfun = @pinvAB;
    case 'lsqminnorm'
        if isoctave()
            error('lsqminnorm may only be used in Matlab');
        end%if
        if pinvtolgiven
            pinvfun = @(a, b) lsqminnorm(a, b, pinvtol);
        else
            pinvfun = @lsqminnorm;
        end%if
    otherwise
        error(['pinvfun must be ''pinv'', ''rpinv'', ''pinvAB'', or ', ...
               '''lsqminnorm'' (Matlab only)']);
end%switch

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% returnalliters %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if isfield(kwargs, 'returnalliters')
    % Make sure returnalliters is a character vector.
    if ~ischarrow(kwargs.returnalliters)
        error('returnalliters must be a character vector');
    end%if
    if ~ismember(kwargs.returnalliters, {'all', 'scalars', 'none'})
        error('returnalliters must be ''all'', ''scalars'', or ''none''');
    end%if
    returnalliters = kwargs.returnalliters;
end%if
rai_none = strcmp(returnalliters, 'none');
rai_all = ~rai_none && strcmp(returnalliters, 'all');

%%%%%%%%%%%%%%%%%%%% forcealliters, checksys, warnifnotcnv %%%%%%%%%%%%%%%%%%%%%

for X = {'forcealliters', 'checksys', 'warnifnotcnv'}
    if isfield(kwargs, X{:})
        % Make sure given value is a logical scalar.
        if ~islogicalscalar(kwargs.(X{:}))
            error([X{:}, ' must be a logical scalar']);
        end%if
        eval([X{:}, ' = kwargs.', X{:}, ';']);
    end%if
end%for

%%%%%%%%%%%%%%%%%%% check detectability and stabilizability %%%%%%%%%%%%%%%%%%%%

if checksys
    try
        [~, ~, isstab] = myisctrb(A, B);
        if ~isstab
            warning(['(A, B) is not stabilizable; a bounded solution to ', ...
                     'the DARE may not exist']);
        end%if
    catch err
        warning(['unable to determine stability of (A, B); ', ...
                 'myisctrb gave the following error: ', err.message]);
    end%trycatch

    RS = pinvfun(R, S'); % pinv(R) * S'
    try
        [~, ~, isdetec] = myisobsv(A - B * RS, forceherm(Q - S * RS));
        if ~isdetec
            warning(['(A - B*pinv(R)*S'', Q - S*pinv(R)*S'') is not ', ...
                     'detectable; for certain initial conditions, ', ...
                     'iterating the DARE may reach different steady state ', ...
                     'solutions, or it may not reach a steady state ', ...
                     'solution at all (oscillatory behavior is possible); ', ...
                     'furthermore, the corresponding LQR may not ', ...
                     'be asymptotically stable']);
        end%if
    catch err
        warning(['unable to determine stability of A - B*pinv(R)*S'', ', ...
                 'Q - S*pinv(R)*S''); myisobsv gave the following error: ', ...
                 err.message]);
    end%trycatch
end%if

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%% iterate the Riccati equation %%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Functions to calculate G, H, and L.
X2G = @(X) pinvfun(B' * X * B + R, (A' * X * B + S)');
X2M = @(X) pinvfun(B' * X * B + R, B');
if isoctave()
    G2L = @(G) eig(A - B*G);
else % Matlab eig only works on sparse matrices if they are symmetric.
    G2L = @(G) eig(full(A - B*G));
end%if

% If we are returning all iterations in some capacity, preallocate and
% initialize.
if ~rai_none % returnalliters = 'scalars' or 'all'
    isconv = false(1, maxiters + 1);
    rnorm = NaN(1, maxiters + 1);
    rsnorm = NaN(1, maxiters + 1);

    if rai_all % returnalliters = 'all'
        X = cell(1, maxiters + 1);
        M = cell(1, maxiters + 1);
        H = cell(1, maxiters + 1);
        Z = cell(1, maxiters + 1);
        G = cell(1, maxiters + 1);
        L = NaN(n, maxiters + 1);

        tic;
        X{1} = X0;
        M{1} = X2M(X0);
        H{1} = M{1} * X0';
        Z{1} = X0 - X0*B*H{1};
        % Recall that if Hermitian-ness will be enforced at some point, then X0
        % is forced to be Hermitian. Therefore, in this case we also force Z{1}
        % to be Hermitian.
        if fh < Inf()
            Z{1} = forceherm(Z{1});
        end%if
    else % returnalliters = 'scalars'
        tic();
    end%if
else % returnalliters = 'none'
    tic;
end%if

% Iterate the Riccati equation.
Xold = X0;
for i = 1:maxiters
    % A'*X
    p0 = A' * Xold;

    % A'*X*A
    p1 = p0 * A;

    % A'*X*B + S
    p2 = p0 * B + S;

    % B'*X*B + R
    if i <= fh
        p3 = B' * Xold * B + R;
    else % if i > fh, force Hermitian-ness
        p3 = forceherm(B' * Xold * B) + R;
    end%if

    % pinv(B' * X * B + R) * (A' * X * B + S)'
    Gold = pinvfun(p3, p2'); % Note that this is G{i}, not G{i+1}!

    % A' * X * A - (A' * X * B + S) * pinv(B' * X * B + R) * (A' * X * B + S)'
    Xnew = p1 - p2 * Gold;

    % Enforce Hermitian-ness, if necessary.
    if i > fh
        Xnew = forceherm(Xnew);
    end%if

    % A' * X * A - (A' * X * B + S) * pinv(B' * X * B + R) * (A' * X * B + S)'
    %                                                                        + Q
    Xnew = Xnew + Q;

    % Check for convergence.
    Xnewnorm = norm(Xnew, 'fro');
    normdiff = norm(Xnew - Xold, 'fro');
    cnv = normdiff < reltol * Xnewnorm || normdiff < abstol;

    % If we are returning all iterations, put results into appropriate places.
    if ~rai_none % returnalliters = 'scalars' or 'all'
        isconv(i+1) = cnv;
        rnorm(i+1) = normdiff;
        if Xnewnorm == 0
            rsnorm(i+1) = 0;
        else
            rsnorm(i+1) = normdiff / Xnewnorm;
        end%if

        if rai_all % returnalliters = 'all'
            X{i+1} = Xnew;
            M{i+1} = X2M(Xnew);
            H{i+1} = M{i+1} * Xnew';
            Z{i+1} = X{i+1} - X{i+1} * B * H{i+1};
            if i > fh
                Z{i+1} = forceherm(Z{i+1});
            end%if
            G{i} = Gold;
            L(:, i) = G2L(G{i});
        end%if
    end%if

    % Detect if this is the last iteration. This is true if we are not forcing
    % all iterations and convergence is detected, or if we have reached the max
    % number of iterations.
    lastiter = (~forcealliters && cnv) || (i == maxiters);

    if lastiter
        if rai_all % returnalliters = 'all'
            % If this is the last iteration, we need to calculate the final
            % values of G and L.
            G{i+1} = X2G(Xnew);
            L(:, i+1) = G2L(G{i+1});

            timespent = toc;

            % If we are returning all iterations, remove empty cells at the end
            % of each cell array. This is only necessary if we have not reached
            % the maximum number of iterations.
            if i < maxiters
                X(i+2:end) = [];
                M(i+2:end) = [];
                H(i+2:end) = [];
                Z(i+2:end) = [];
                G(i+2:end) = [];
                L(i+2:end) = [];
                isconv(i+2:end) = [];
                rnorm(i+2:end) = [];
                rsnorm(i+2:end) = [];
            end%if
        elseif rai_none % returnalliters = 'none'
            timespent = toc;
        else % returnalliters = 'scalars'
            timespent = toc;
            if i < maxiters
                isconv(i+2:end) = [];
                rnorm(i+2:end) = [];
                rsnorm(i+2:end) = [];
            end%if
        end%if

        % If warnifnotcnv option is on and iterations haven't converged, throw a
        % warning.
        if warnifnotcnv && ~cnv
            warning('DARE iterations did not converge');
        end%if

        break;
    else
        Xold = Xnew;
    end%if
end%for

% If we aren't returning all iterations, only return the final (not necessarily
% converged!) values.
if ~rai_all % returnalliters = 'scalars' or 'none'
    X = Xnew;
    M = X2M(X);
    H = M * X';
    Z = X - X*B*H;
    if i > fh
        Z = forceherm(Z);
    end%if
    G = X2G(X);
    L = G2L(G);

    if rai_none % returnalliters = 'none'
        isconv = cnv;
        rnorm = normdiff;
        if Xnewnorm == 0
            rsnorm = 0;
        else
            rsnorm = normdiff / Xnewnorm;
        end%if
    end%if
end%if

iters = i;

% Reenable divide by zero warnings.
if isoctave()
    warning('on', 'Octave:nearly-singular-matrix');
    warning('on', 'Octave:divide-by-zero');
    warning('on', 'Octave:singular-matrix');
else
    warning('on', 'MATLAB:nearlySingularMatrix');
    warning('on', 'MATLAB:singularMatrix');
    warning('on', 'MATLAB:divideByZero');
end%if

end%function dare_iter

