%% Copyright (C) 2018 Travis Arnold
%%
%% This file is part of Octave.
%%
%% Octave is free software; you can redistribute it and/or modify it
%% under the terms of the GNU General Public License as published by
%% the Free Software Foundation; either version 3 of the License, or (at
%% your option) any later version.
%%
%% Octave is distributed in the hope that it will be useful, but
%% WITHOUT ANY WARRANTY; without even the implied warranty of
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
%% General Public License for more details.
%%
%% You should have received a copy of the GNU General Public License
%% along with Octave; see the file COPYING.  If not, see
%% <http://www.gnu.org/licenses/>.

%% a = myissquare(A)
%%
%% Return true if A is a square matrix. Matlab compatible.
%%
%% See also: issquare, ismatrix.

%% Author: Travis Arnold <tjarnold@wisc.edu>
%% July 2018.

function a = myissquare(A)

if ~ismatrix(A)
    error('A must be a matrix');
end%if

a = (size(A, 1) == size(A, 2));

end%function myissquare

