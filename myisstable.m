%% Copyright (C) 2018 Travis Arnold
%%
%% This file is part of Octave.
%%
%% Octave is free software; you can redistribute it and/or modify it
%% under the terms of the GNU General Public License as published by
%% the Free Software Foundation; either version 3 of the License, or (at
%% your option) any later version.
%%
%% Octave is distributed in the hope that it will be useful, but
%% WITHOUT ANY WARRANTY; without even the implied warranty of
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
%% General Public License for more details.
%%
%% You should have received a copy of the GNU General Public License
%% along with Octave; see the file COPYING.  If not, see
%% <http://www.gnu.org/licenses/>.

%% [bool, lammax] = myisstable(A, tol)
%%
%% Check the stability of square matrix A. bool = 1 if all of the eigenvalues of
%% A are inside the unit circle and bool = 0 otherwise. Optionally, also return
%% lammax = max(abs(eig(A))). Returns bool = false and lammax = [] if A is
%% empty.

%% Author: Travis Arnold <tjarnold@wisc.edu>
%% July 2018.

function [bool, lammax] = myisstable(A)

% Check number of input arguments.
if nargin() ~= 1
    error('exactly one input argument required');
end%if

% Make sure A is a numeric matrix.
if ~isnumericmatrix(A)
    error('A must be a numeric matrix');
end%if

% Make sure A is square.
if ~myissquare(A)
    error('A must be square');
end%if

if isempty(A)
    bool = false();
    lammax = [];
else
    % Calculate magnitude of largest eigenvalue.
    lammax = max(abs(eig(A)));

    % Check stability of A.
    bool = (lammax < 1);
end%if

end%function myisstable

