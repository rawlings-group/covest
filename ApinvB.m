%% Copyright (C) 2020 Travis Arnold
%%
%% This file is part of Octave.
%%
%% Octave is free software; you can redistribute it and/or modify it
%% under the terms of the GNU General Public License as published by
%% the Free Software Foundation; either version 3 of the License, or (at
%% your option) any later version.
%%
%% Octave is distributed in the hope that it will be useful, but
%% WITHOUT ANY WARRANTY; without even the implied warranty of
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
%% General Public License for more details.
%%
%% You should have received a copy of the GNU General Public License
%% along with Octave; see the file COPYING.  If not, see
%% <http://www.gnu.org/licenses/>.

%% C = ApinvB(A, B)
%%
%% Calculate A * pinv(B). If Octave, C = A / B. If Matlab, 
%% C = lsqminnorm(B', A')'. If this returns a matrix with NaNs or Infs, then
%% instead C = A * rpinv(B).
%%
%% See also: mrdivide, rpinv, pinv, lsqminnorm (Matlab only).

%% Author: Travis Arnold <tjarnold@wisc.edu>
%% January 2020.

function C = ApinvB(A, B)
    if isoctave()
        C = A / B;
    else % Matlab
        C = lsqminnorm(B', A')';
    end%if
    if any(vec(isnan(C))) || any(vec(isinf(C)))
        C = A * rpinv(B);
    end
end%function ApinvB

