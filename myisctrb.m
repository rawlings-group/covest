%% Copyright (C) 2020 Travis Arnold
%%
%% This file is part of Octave.
%%
%% Octave is free software; you can redistribute it and/or modify it
%% under the terms of the GNU General Public License as published by
%% the Free Software Foundation; either version 3 of the License, or (at
%% your option) any later version.
%%
%% Octave is distributed in the hope that it will be useful, but
%% WITHOUT ANY WARRANTY; without even the implied warranty of
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
%% General Public License for more details.
%%
%% You should have received a copy of the GNU General Public License
%% along with Octave; see the file COPYING.  If not, see
%% <http://www.gnu.org/licenses/>.

%% [isctrb, ncont, isstab, nstab, Ac, Bc, T] = myisctrb(A, B, tol)
%%
%% Perform controllability and stabilizability analysis on discrete time linear
%% system (A, B) by putting the system into controllability canonical form. In
%% Octave, this is done using the SLICOT routine that is used in the isobsv and
%% isctrb functions from the control package. In Matlab, this is done using the
%% ctrbf function.
%%
%% * Inputs *
%%
%% A : state transition matrix (n x n)
%%
%% B : measurement matrix (n x m) 
%%
%% tol : Tolerance parameter. Optional. Default value: 1e-8. This is the
%%       tolerance parameter passed to the SLICOT factorization routine (Octave)
%%       or to ctrbf (Matlab). It is also used to determine how many states are
%%       stabilizable, in that an uncontrollable state is deemed stabilizable if
%%       the corresponding eigenvalue has magnitude < 1 - tol.
%%
%% * Outputs *
%%
%% isctrb : Logical. 1 if system is controllable, 0 if system is uncontrollable.
%%
%% ncont : Number of controllable states. Equal to the rank of the
%%         controllability matrix.
%%
%% isstab : Logical. 1 if system is stabilizable, 0 if system is not
%%          stabilizable.
%%
%% nstab : Number of stabilizable states.
%%
%% Ac : State transition matrix of controllability canonical form.
%%
%% Bc : Input shaping matrix of controllability canonical form.
%%
%% T : Transformation matrix.
%%
%% Ac = T * A * T'              Bc = T * B
%%    = | A11   A12 |              = | B1 |
%%      |  0    A22 |                | 0  |
%%
%% See also: obsv, ctrb, isobsv (Octave only), isctrb (Octave only), myisobsv.

%% Author: Travis Arnold <tjarnold@wisc.edu>
%% Februaury 2020.

function [isctrb, ncont, isstab, nstab, Ac, Bc, T] = myisctrb(A, B, tol)

% Check number of input arguments.
if ~ismember(nargin(), [2, 3])
    error('exactly two or three input arguments required');
end%if

% Make sure A and B are numeric matrices.
if ~isnumericmatrix(A)
    error('A must be a numeric matrix');
end%if
if ~isnumericmatrix(B)
    error('B must be a numeric matrix');
end%if

% Make sure A and B have consistent sizes.
if ~myissquare(A)
    error('A must be square');
end%if
n = size(A, 1);
if (n ~= size(B, 1))
    error('sizes of A and B are inconsistent');
end%if

% Default value of tol.
if nargin() <= 2
    tol = 1e-8;
else % User provided value of tol.
    % Make sure tol is a numeric scalar.
    if ~isnumericscalar(tol)
        error('tol must be a numeric scalar');
    end%if
    % Make sure tol is 0 < tol < 1.
    if ~((tol > 0) || (tol < 1))
        error('tol must be 0 < tol < 1');
    end%if
end%if

if isoctave()
    % Use slicot routine to get controllability canonical form.
    % Have to do this line in eval, or Matlab parser will complain about the
    % slicot function name beginning with an underscore, even though this line
    % will never get run in Matlab.
    eval('[Ac, Bc, T, ncont] = __sl_ab01od__(A, B, tol);');
    T = T';
else % Matlab
    % Use ctrbf routine to get controllability canonical form.
    [Ac, Bc, ~, T, ncont] = ctrbf(A, B, zeros(0, n), tol);
    ncont = sum(ncont);
    Ubar = fliplr(speye(n));
    Ac = Ubar * Ac * Ubar';
    Bc = Ubar * Bc;
    T = Ubar * T;
end%if

isctrb = (n == ncont);
A22ind = (ncont + 1):n;
A22 = Ac(A22ind, A22ind);
nstab = sum(abs(eig(A22)) < (1 - tol)) + ncont;
isstab = (n == nstab);

end%function myisctrb

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%% Make sure the transformations do what they should %%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%!test
%! n = 3;
%! m = 2;
%! A = [1, 2, 3; 4, 5, 6; 7, 8, 9] / 3;
%! B = [1, 3; 5, 7; 8, 10] / 4;
%! [~, ~, ~, ~, Ac, Bc, T] = myisctrb(A, B, 1e-8);
%! assert(norm(Ac - T * A * T', 'fro') < 1e-8);
%! assert(norm(Bc - T * B, 'fro') < 1e-8);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%% Make sure controllability detection works---nondefective A %%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%!test
%! for n = 1:5
%! for m = 1:n
%! for ncont = 0:n
%!
%! isctrb = (n == ncont);
%! nuncont = n - ncont;
%! cont_eigs = 1./(1:ncont);
%! uncont_eigs = 1./(1:nuncont);
%!
%! % Create model.
%! T11 = magic(ncont) + eye(ncont) / (ncont + 1)^2;
%! A11 = T11 * diag_eig_real(cont_eigs) / T11;
%! B1 = reshape(1:(ncont * m), ncont, m) / (ncont * m + 1);
%! if rank_dyntol(ctrb(A11, B1), 1e-8) < ncont
%!     error('(A11, B1) is not controllable');
%! end%if
%! T22 = magic(nuncont) + eye(nuncont) / (nuncont + 1)^2;
%! A22 = T22 * diag_eig_real(uncont_eigs) / T22;
%! A12 = flipud(reshape(1:(ncont * nuncont), ncont, nuncont) / (n + 1));
%! T = magic(n) + eye(n) / (n + 1)^2;
%! A = T * [A11, A12; zeros(nuncont, ncont), A22] / T;
%! B = T * [B1; zeros(nuncont, m)];
%!
%! % Check controllability by the rank of the controllability matrix.
%! ncont_rank = rank_dyntol(ctrb(A, B), 1e-8);
%! isctrb_rank = (ncont_rank == n);
%!
%! % Check controllability with myisctrb.
%! [isctrb_myisctrb, ncont_myisctrb] = myisctrb(A, B);
%!
%! % Make sure results are consistent.
%! assert(ncont == ncont_myisctrb);
%! assert(isctrb == isctrb_myisctrb);
%! assert(ncont == ncont_rank);
%! assert(isctrb == isctrb_rank);
%!
%! end, end, end % end for loops

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%% Make sure controllability detection works---defective A %%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%!test
%! A11 = [0.7, 1; 0, 0.7];
%! A22 = [0.8, 1; 0, 0.8];
%! m = 2;
%! ncont = size(A11, 1);
%! nuncont = size(A22, 1);
%! n = ncont + nuncont;
%! isctrb = (n == ncont);
%! T11 = magic(ncont) + eye(ncont) / (ncont + 1)^2;
%! T22 = magic(nuncont) + eye(nuncont) / (nuncont + 1)^2;
%! T = magic(n) + eye(n) / (n + 1)^2;
%! A11 = T11 * A11 / T11;
%! A22 = T22 * A22 / T22;
%! A12 = flipud(reshape(1:(ncont * nuncont), ncont, nuncont) / (n + 1));
%! A = T * [A11, A12; zeros(nuncont, ncont), A22] / T;
%! B1 = reshape(1:(ncont * m), ncont, m) / (ncont * m + 1);
%! B = T * [B1; zeros(nuncont, m)];
%! if rank_dyntol(ctrb(A11, B1), 1e-8) < ncont
%!     error('(A11, B1) is not controllable');
%! end%if
%!
%! % Check controllablity with myisctrb.
%! [isctrb_myisctrb, ncont_myisctrb] = myisctrb(A, B);
%!
%! % Check controllablity by the rank of the controllablity matrix.
%! ncont_rank = rank_dyntol(ctrb(A, B), 1e-8);
%! isctrb_rank = (ncont_rank == n);
%!
%! % Make sure results are consistent.
%! assert(ncont == ncont_myisctrb);
%! assert(isctrb == isctrb_myisctrb);
%! assert(ncont == ncont_rank);
%! assert(isctrb == isctrb_rank);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%% Make sure stabilizability detection works---nondefective A %%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%!test
%! for n = 1:5
%! for m = 1:n
%! for ncont = 0:n
%! for stabmult = [0.7, 1.2] % ensure we get both stabilizable and unstabilizable cases
%!
%! isctrb = (n == ncont);
%! nuncont = n - ncont;
%! cont_eigs = 1./(1:ncont);
%! uncont_eigs = 1./(1:nuncont);
%!
%! % Create model.
%! T11 = magic(ncont) + eye(ncont) / (ncont + 1)^2;
%! A11 = T11 * diag_eig_real(cont_eigs) / T11;
%! B1 = reshape(1:(ncont * m), ncont, m) / (ncont * m + 1);
%! if rank_dyntol(ctrb(A11, B1), 1e-8) < ncont
%!     error('(A11, B1) is not controllable');
%! end%if
%! T22 = magic(nuncont) + eye(nuncont) / (nuncont + 1)^2;
%! A22 = T22 * diag_eig_real(uncont_eigs) / T22;
%! A12 = flipud(reshape(1:(ncont * nuncont), ncont, nuncont) / (n + 1));
%! T = magic(n) + eye(n) / (n + 1)^2;
%! if ~isempty(A22)
%!    A22 = (A22 / max(abs(eig(A22)))) * stabmult;
%! end%if
%! nstab = sum(abs(eig(A22)) < 1) + ncont;
%! isstab = (nstab == n);
%! A = T * [A11, A12; zeros(nuncont, ncont), A22] / T;
%! B = T * [B1; zeros(nuncont, m)];
%!
%! % Check controllability by the rank of the controllability matrix.
%! ncont_rank = rank_dyntol(ctrb(A, B), 1e-8);
%! isctrb_rank = (ncont_rank == n);
%!
%! % Check controllability with myisctrb.
%! [isctrb_myisctrb, ncont_myisctrb, isstab_myisctrb, nstab_myisctrb] = myisctrb(A, B);
%!
%! % Make sure results are consistent.
%! assert(ncont == ncont_myisctrb);
%! assert(isctrb == isctrb_myisctrb);
%! assert(ncont == ncont_rank);
%! assert(isctrb == isctrb_rank);
%! assert(isstab == isstab_myisctrb);
%! assert(nstab == nstab_myisctrb);
%!
%! end, end, end, end % end for loops

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%% Make sure stabilizability detection works---defective A %%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%!test
%! for N = [0.8, 1.2]
%!
%! A11 = [0.7, 1; 0, 0.7];
%! A22 = [N, 1; 0, N];
%! isstab = N < 1;
%! m = 2;
%! ncont = size(A11, 1);
%! nstab = sum(abs(eig(A22)) < 1) + ncont;
%! nuncont = size(A22, 1);
%! n = ncont + nuncont;
%! isctrb = (n == ncont);
%! T11 = magic(ncont) + eye(ncont) / (ncont + 1)^2;
%! T22 = magic(nuncont) + eye(nuncont) / (nuncont + 1)^2;
%! T = magic(n) + eye(n) / (n + 1)^2;
%! A11 = T11 * A11 / T11;
%! A22 = T22 * A22 / T22;
%! A12 = flipud(reshape(1:(ncont * nuncont), ncont, nuncont) / (n + 1));
%! A = T * [A11, A12; zeros(nuncont, ncont), A22] / T;
%! B1 = reshape(1:(ncont * m), ncont, m) / (ncont * m + 1);
%! B = T * [B1; zeros(nuncont, m)];
%! if rank_dyntol(ctrb(A11, B1), 1e-8) < ncont
%!     error('(A11, B1) is not controllable');
%! end%if
%!
%! % Check controllablity with myisctrb.
%! [isctrb_myisctrb, ncont_myisctrb, isstab_myisctrb, nstab_myisctrb] = myisctrb(A, B);
%!
%! % Check controllablity by the rank of the controllablity matrix.
%! ncont_rank = rank_dyntol(ctrb(A, B), 1e-8);
%! isctrb_rank = (ncont_rank == n);
%!
%! % Make sure results are consistent.
%! assert(ncont == ncont_myisctrb);
%! assert(isctrb == isctrb_myisctrb);
%! assert(ncont == ncont_rank);
%! assert(isctrb == isctrb_rank);
%! assert(isstab == isstab_myisctrb);
%! assert(nstab == nstab_myisctrb);
%! end%for

