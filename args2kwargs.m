%% Copyright (C) 2018 Travis Arnold
%%
%% This file is part of Octave.
%%
%% Octave is free software; you can redistribute it and/or modify it
%% under the terms of the GNU General Public License as published by
%% the Free Software Foundation; either version 3 of the License, or (at
%% your option) any later version.
%%
%% Octave is distributed in the hope that it will be useful, but
%% WITHOUT ANY WARRANTY; without even the implied warranty of
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
%% General Public License for more details.
%%
%% You should have received a copy of the GNU General Public License
%% along with Octave; see the file COPYING.  If not, see
%% <http://www.gnu.org/licenses/>.

%% kwargs = args2kwargs(varargin)
%%
%% Check the input arguments that are passed to a function. There are two
%% syntaxes that are supported. First, varargin may be one struct containing all
%% of the keyword arguments for the function. In this case, the variable kwargs
%% returned is simply this struct. Second, key-value pairs may be passed. In
%% this case, args2kwargs parses the key-value pairs, and returns them as the
%% struct kwargs.

%% Author: Travis Arnold <tjarnold@wisc.edu>
%% November 2018.

function kwargs = args2kwargs(varargin)

switch nargin()
    case 0
        kwargs = struct();
    case 1 % If varargin has only one element, it must be a struct.
        if ~isstruct(varargin{:})
            error(['when only one additional argument is passed, it must ', ...
                   'be a struct']);
        end%error
        kwargs = varargin{:};
    otherwise % Assume key-value pairs are passed.
        if ~iseven(nargin())
            error(['key-value pairs are incorrectly specified; odd number ', ...
                   'of arguments detected']);
        else
            kwargs = struct();
            for i = 1:2:(length(varargin) - 1)
                if ~ischarrow(varargin{i})
                    error('kwarg names must be character vectors');
                end%if
                kwargs.(varargin{i}) = varargin{i + 1};
            end%for
        end%if
end%switch

end%function args2kwargs

