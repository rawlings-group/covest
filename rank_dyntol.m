%% Copyright (C) 1993-2019 John W. Eaton
%%
%% This file is part of Octave.
%%
%% Octave is free software: you can redistribute it and/or modify it
%% under the terms of the GNU General Public License as published by
%% the Free Software Foundation, either version 3 of the License, or
%% (at your option) any later version.
%%
%% Octave is distributed in the hope that it will be useful, but
%% WITHOUT ANY WARRANTY; without even the implied warranty of
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%% GNU General Public License for more details.
%%
%% You should have received a copy of the GNU General Public License
%% along with Octave; see the file COPYING.  If not, see
%% <https://www.gnu.org/licenses/>.

%% retval = rank_dyntol(A, tol)
%% 
%% Compute the rank of matrix A using the singular value decomposition.
%%
%% rank_dyntol is identical to Octave's built in rank function except that it
%% handles the tol argument differently. The rank is taken to be the number of
%% singular values of A that are greater than the calculated tolerance. If the
%% second argument is included, the tolerance it is taken to be
%%
%%     tolerance = max (size (A)) * sigma(1) * tol;
%%
%% If the second argument is omitted, then rank_dyntol behaves identically to
%% rank: the tolerance is taken to be 
%%
%%     tolerance = max (size (A)) * sigma(1) * eps;
%%
%% where 'eps' is machine precision and 'sigma(1)' is the largest singular value
%% of A.
%%
%% See also: rank, myrank, null, orth, sprank, svd, eps.

%% Author: jwe
%% Author: Travis Arnold, <tjarnold@wisc.edu>, February 2020
%% Adapted from Octave's rank function.

function retval = rank_dyntol(A, tol)
    if (nargin == 1)
        s = svd(A);
        if (isempty(s))
            tolerance = 0;
        else
            if (isa(A, "single"))
                tolerance = max(size(A)) * s(1) * eps("single");
            else
                tolerance = max(size(A)) * s(1) * eps();
            end%if
        end%if
    elseif (nargin == 2)
        s = svd(A);
        if (isempty(s))
            tolerance = 0;
        else
            tolerance = max(size(A)) * s(1) * tol;
        end%if
    else
        error('rank_dyntol: exactly two or three input arguments required');
    end%if

    retval = sum(s > tolerance);
end%function

%!test
%! A = [1 2 3 4 5 6 7;
%!      4 5 6 7 8 9 12;
%!      1 2 3.1 4 5 6 7;
%!      2 3 4 5 6 7 8;
%!      3 4 5 6 7 8 9;
%!      4 5 6 7 8 9 10;
%!      5 6 7 8 9 10 11];
%! assert(rank_dyntol(A), 4);

%!test
%! A = [1 2 3 4 5 6 7;
%!      4 5 6 7 8 9 12;
%!      1 2 3.0000001 4 5 6 7;
%!      4 5 6 7 8 9 12.00001;
%!      3 4 5 6 7 8 9;
%!      4 5 6 7 8 9 10;
%!      5 6 7 8 9 10 11];
%! assert(rank_dyntol(A), 4);

%!test
%! A = [1 2 3 4 5 6 7;
%!      4 5 6 7 8 9 12;
%!      1 2 3 4 5 6 7;
%!      4 5 6 7 8 9 12.00001;
%!      3 4 5 6 7 8 9;
%!      4 5 6 7 8 9 10;
%!      5 6 7 8 9 10 11];
%! assert(rank_dyntol(A), 3);

%!test
%! A = [1 2 3 4 5 6 7;
%!      4 5 6 7 8 9 12;
%!      1 2 3 4 5 6 7;
%!      4 5 6 7 8 9 12;
%!      3 4 5 6 7 8 9;
%!      4 5 6 7 8 9 10;
%!      5 6 7 8 9 10 11];
%! assert(rank_dyntol(A), 3);

%!test
%! A = eye(100);
%! assert(rank_dyntol(A), 100);

%!assert(rank_dyntol([]), 0)
%!assert(rank_dyntol([1:9]), 1)
%!assert(rank_dyntol([1:9]'), 1)

%!test
%! A = [1, 2, 3; 1, 2.001, 3; 1, 2, 3.0000001];
%! assert(rank_dyntol(A), 3);
%! assert(rank_dyntol(A, 0.001), 1);
%! assert(rank_dyntol(A, 0.000001), 2);
%! assert(rank_dyntol(A, 0.0000000001), 3);

