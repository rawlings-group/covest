%% Copyright (C) 2020 Travis Arnold
%%
%% This file is part of Octave.
%%
%% Octave is free software; you can redistribute it and/or modify it
%% under the terms of the GNU General Public License as published by
%% the Free Software Foundation; either version 3 of the License, or (at
%% your option) any later version.
%%
%% Octave is distributed in the hope that it will be useful, but
%% WITHOUT ANY WARRANTY; without even the implied warranty of
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
%% General Public License for more details.
%%
%% You should have received a copy of the GNU General Public License
%% along with Octave; see the file COPYING.  If not, see
%% <http://www.gnu.org/licenses/>.

%% C = pinvAB(A, B)
%%
%% Calculate pinv(A) * B. If Octave, C = A \ B. If Matlab, C = lsqminnorm(A, B).
%% If this returns a matrix with NaNs or Infs, then instead C = rpinv(A) * B.
%%
%% See also: mldivide, rpinv, pinv, lsqminnorm (Matlab only).

%% Author: Travis Arnold <tjarnold@wisc.edu>
%% January 2020.

function C = pinvAB(A, B)
    if isoctave()
        C = A \ B;
    else % Matlab
        C = lsqminnorm(A, B);
    end%if
    if any(vec(isnan(C))) || any(vec(isinf(C)))
        C = rpinv(A) * B;
    end
end%function pinvAB

