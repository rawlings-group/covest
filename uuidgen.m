%% Copyright (C) 2019 Travis Arnold
%%
%% This file is part of Octave.
%%
%% Octave is free software; you can redistribute it and/or modify it
%% under the terms of the GNU General Public License as published by
%% the Free Software Foundation; either version 3 of the License, or (at
%% your option) any later version.
%%
%% Octave is distributed in the hope that it will be useful, but
%% WITHOUT ANY WARRANTY; without even the implied warranty of
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
%% General Public License for more details.
%%
%% You should have received a copy of the GNU General Public License
%% along with Octave; see the file COPYING.  If not, see
%% <http://www.gnu.org/licenses/>.

%% id = uuidgen()
%%
%% Generate a universally unique identifier (UUID) using a system call to
%% 'uuidgen'. This utility should be installed by default on Mac and Linux based
%% systems. For Windows, it can be obtained by installing the Windows SDK, and
%% adding the install directory to the system path. If the call to uuidgen
%% fails, now() is called as a stand-in. The returned id is formatted such that
%% it is a valid struct field name in Matlab.

%% Author: Travis Arnold <tjarnold@wisc.edu>
%% January 2019.

function id = uuidgen()
    [status, id] = system('uuidgen');
    if status ~= 0
        warning(['system call to ''uuidgen'' failed with following ', ...
                 'message: ''', id, '''; using ''now()'' function ', ...
                 'instead as a stand-in']);
        id = num2str(now(), 100);
        id(id == '.') = ''; % remove decimal point
        id = id(randperm(length(id))); % randomize order of numbers so different
                                       % ids will be easier to distinguish to
                                       % the human eye
    else
        id(end) = ''; % get rid of the newline at the end
        id(id == '-') = ''; % remove hyphens
    end%if
    id = ['t', id]; % prepend with a letter so that it is a valid struct field
                    % name in Matlab
end%function uuidgen

