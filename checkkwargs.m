%% Copyright (C) 2018 Travis Arnold
%%
%% This file is part of Octave.
%%
%% Octave is free software; you can redistribute it and/or modify it
%% under the terms of the GNU General Public License as published by
%% the Free Software Foundation; either version 3 of the License, or (at
%% your option) any later version.
%%
%% Octave is distributed in the hope that it will be useful, but
%% WITHOUT ANY WARRANTY; without even the implied warranty of
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
%% General Public License for more details.
%%
%% You should have received a copy of the GNU General Public License
%% along with Octave; see the file COPYING.  If not, see
%% <http://www.gnu.org/licenses/>.

%% [bool, msg] = checkkwargs(kwargs, okfields, [okcombs])
%%
%% Check if struct kwargs contains any fieldnames that are not included in cell
%% array okayfields. If the optional third argument okcombs is given, also check
%% that the combination of fields given in kwargs matches one of the
%% combinations given in okcombs. If any tests fail, an error is thrown with an
%% explanatory message.

%% Author: Travis Arnold <tjarnold@wisc.edu>
%% November 2018.

function checkkwargs(kwargs, okfields, okcombs)

% Check number of input arguments.
if ~ismember(nargin(), 2:3)
    error('either two or three input arguments required');
end%if

% Make sure kwargs is a struct.
if ~isstruct(kwargs)
    error('kwargs must be a struct');
end%if

% Make sure okfields is a cell.
if ~iscell(okfields)
    error('okfields must be a cell array');
end%if

% Make sure all elements of okfields are character vectors.
if ~all(cellfun(@ischarrow, okfields))
    error('all elements of okfields must be character vectors');
end%if

% Check for unrecognized fieldnames in kwargs (that is, fieldnames in kwargs
% that are not in okfields).
unrec = setdiff(fieldnames(kwargs)', okfields);

% If there are unrecognized fieldnames, throw an error.
if ~isempty(unrec)
    error(['unrecognized fieldname(s) in kwargs: ', strjoin(unrec, ', ')]);
end%if

% Error checking on okcombs, if it is given.
if nargin() == 3
    % Make sure okcombs is a cell.
    if ~iscell(okcombs)
        error('okcombs must be a cell array');
    end%if

    % Make sure okcombs is not empty.
    if isempty(okcombs)
        error('okcombs is empty');
    end%if

    % Ensure okcombs is a row cell vector, so we can iterate over it.
    okcombs = okcombs(:)'; 

    % Error checking on the individual elements of okcombs.
    unrec = {};
    for c = okcombs
        % Make sure elements of okcombs are cell arrays.
        if ~iscell(c{:})
            error('all elements of okcombs must themselves be cell arrays');
        end%if

        % Make sure elements of okcombs are cell arrays of character vectors.
        if isempty(c{:})
            error(['all elements of okcombs must be cell arrays of ', ...
                   'character vectors']);
        end%if

        if ~all(cellfun(@ischarrow, c{:}))
            error(['all elements of okcombs must be cell arrays of ', ...
                   'character vectors']);
        end%if

        % Make sure elements of okcombs have unique entries.
        if numel(unique(c{:})) ~= numel(c{:})
            error('all elements of okcombs must have unique entires')
        end%if

        % Find any elements in c{:} that are not in okfields.
        unrec = [unrec, setdiff(c{:}, okfields)];
    end%for

    % If we found any elements in okcombs that are not in okfields, throw an
    % error.
    if ~isempty(unrec)
        error(['okcombs contains field names that are not given in ', ...
               'okfields: ', strjoin(unique(unrec), ', ')]);
    end%if

    % Check if one of the correct combinations is passed.
    sortkwargfields = strjoin(sort(fieldnames(kwargs)), '');
    sortokcombs = unique(cellfun(@(X) strjoin(sort(X), ''), okcombs, ...
                                 'UniformOutput', false()));
    if ~any(vec(strcmp(sortkwargfields, sortokcombs)))
        formatokcombs = strjoin(cellfun(@(X) ['(', strjoin(X, ', '), ')'], ...
                                        okcombs, 'UniformOutput', false), ', ');
        error(['invalid combination of kwargs passed; acceptable ', ...
               'combinations are: ', formatokcombs]);
    end%if
end%if
end%function checkkwargs

%!error <either two or three input arguments required> checkkwargs();
%!error <either two or three input arguments required> checkkwargs(1);
%!error <either two or three input arguments required> checkkwargs(1, 2, 3, 4);
%!error <kwargs must be a struct> checkkwargs(1, 2);
%!error <okfields must be a cell array> checkkwargs(struct(), 2);
%!error <all elements of okfields must be character vectors> checkkwargs(struct(), {1});

%!error <okfields must be a cell>
%! kwargs.apple = 1;
%! okfields = 1;
%! checkkwargs(kwargs, okfields);

%!error <all elements of okfields must be character vectors>
%! kwargs.apple = 1;
%! okfields = {1};
%! checkkwargs(kwargs, okfields);

%!error <all elements of okfields must be character vectors>
%! kwargs.apple = 1;
%! okfields = {'apple', 1};
%! checkkwargs(kwargs, okfields);

%!error <unrecognized fieldname\(s\) in kwargs: banana, date, elderberry>
%! kwargs.apple = 1;
%! kwargs.banana = 1;
%! kwargs.cantaloupe = 1;
%! kwargs.date = 1;
%! kwargs.elderberry = 1;
%! okfields = {'apple', 'cantaloupe'};
%! checkkwargs(kwargs, okfields);

%!error <okcombs must be a cell array>
%! kwargs.a = 1;
%! kwargs.b = 1;
%! kwargs.c = 1;
%! okfields = {'a', 'b', 'c', 'd', 'e'};
%! okcombs = 1;
%! checkkwargs(kwargs, okfields, okcombs);

%!error <okcombs is empty>
%! kwargs.a = 1;
%! kwargs.b = 1;
%! kwargs.c = 1;
%! okfields = {'a', 'b', 'c', 'd', 'e'};
%! okcombs = {};
%! checkkwargs(kwargs, okfields, okcombs);

%!error <all elements of okcombs must themselves be cell arrays>
%! kwargs.a = 1;
%! kwargs.b = 1;
%! kwargs.c = 1;
%! okfields = {'a', 'b', 'c', 'd', 'e'};
%! okcombs = {1};
%! checkkwargs(kwargs, okfields, okcombs);

%!error <all elements of okcombs must themselves be cell arrays>
%! kwargs.a = 1;
%! kwargs.b = 1;
%! kwargs.c = 1;
%! okfields = {'a', 'b', 'c', 'd', 'e'};
%! okcombs = {{'a'}, 1};
%! checkkwargs(kwargs, okfields, okcombs);

%!error <all elements of okcombs must be cell arrays of character vectors>
%! kwargs.a = 1;
%! kwargs.b = 1;
%! kwargs.c = 1;
%! okfields = {'a', 'b', 'c', 'd', 'e'};
%! okcombs = {{}};
%! checkkwargs(kwargs, okfields, okcombs);

%!error <all elements of okcombs must be cell arrays of character vectors>
%! kwargs.a = 1;
%! kwargs.b = 1;
%! kwargs.c = 1;
%! okfields = {'a', 'b', 'c', 'd', 'e'};
%! okcombs = {{1}};
%! checkkwargs(kwargs, okfields, okcombs);

%!error <all elements of okcombs must be cell arrays of character vectors>
%! kwargs.a = 1;
%! kwargs.b = 1;
%! kwargs.c = 1;
%! okfields = {'a', 'b', 'c', 'd', 'e'};
%! okcombs = {{1}, {'a'}};
%! checkkwargs(kwargs, okfields, okcombs);

%!error <all elements of okcombs must be cell arrays of character vectors>
%! kwargs.a = 1;
%! kwargs.b = 1;
%! kwargs.c = 1;
%! okfields = {'a', 'b', 'c', 'd', 'e'};
%! okcombs = {{'a'}, {1}};
%! checkkwargs(kwargs, okfields, okcombs);

%!error <all elements of okcombs must be cell arrays of character vectors>
%! kwargs.a = 1;
%! kwargs.b = 1;
%! kwargs.c = 1;
%! okfields = {'a', 'b', 'c', 'd', 'e'};
%! okcombs = {{'a', 'b'}, {1}};
%! checkkwargs(kwargs, okfields, okcombs);

%!error <all elements of okcombs must have unique entires>
%! kwargs.a = 1;
%! kwargs.b = 1;
%! kwargs.c = 1;
%! okfields = {'a', 'b', 'c', 'd', 'e'};
%! okcombs = {{'a', 'a'}};
%! checkkwargs(kwargs, okfields, okcombs);

%!error <all elements of okcombs must have unique entires>
%! kwargs.a = 1;
%! kwargs.b = 1;
%! kwargs.c = 1;
%! okfields = {'a', 'b', 'c', 'd', 'e'};
%! okcombs = {{'a', 'b', 'a'}};
%! checkkwargs(kwargs, okfields, okcombs);

%!error <all elements of okcombs must have unique entires>
%! kwargs.a = 1;
%! kwargs.b = 1;
%! kwargs.c = 1;
%! okfields = {'a', 'b', 'c', 'd', 'e'};
%! okcombs = {{'a', 'b'}, {'c', 'd', 'c'}};
%! checkkwargs(kwargs, okfields, okcombs);

%!error <okcombs contains field names that are not given in okfields: f>
%! kwargs.a = 1;
%! kwargs.b = 1;
%! kwargs.c = 1;
%! okfields = {'a', 'b', 'c', 'd', 'e'};
%! okcombs = {{'a', 'b', 'f'}};
%! checkkwargs(kwargs, okfields, okcombs);

%!error <okcombs contains field names that are not given in okfields: f, grapefruit>
%! kwargs.a = 1;
%! kwargs.b = 1;
%! kwargs.c = 1;
%! okfields = {'a', 'b', 'c', 'd', 'e'};
%! okcombs = {{'a', 'b', 'grapefruit', 'f'}};
%! checkkwargs(kwargs, okfields, okcombs);

%!error <okcombs contains field names that are not given in okfields: f>
%! kwargs.a = 1;
%! kwargs.b = 1;
%! kwargs.c = 1;
%! okfields = {'a', 'b', 'c', 'd', 'e'};
%! okcombs = {{'a', 'b'}, {'f', 'c'}};
%! checkkwargs(kwargs, okfields, okcombs);

%!error <okcombs contains field names that are not given in okfields: x, y, z>
%! kwargs.a = 1;
%! kwargs.b = 1;
%! kwargs.c = 1;
%! okfields = {'a', 'b', 'c', 'd', 'e'};
%! okcombs = {{'a', 'b', 'z'}, {'c', 'd', 'y'}, {'d', 'e', 'x'}};
%! checkkwargs(kwargs, okfields, okcombs);

%!error <invalid combination of kwargs passed; acceptable combinations are: \(a, b\), \(a, c\)>
%! kwargs.b = 1;
%! kwargs.c = 1;
%! okfields = {'a', 'b', 'c', 'd', 'e'};
%! okcombs = {{'a', 'b'}, {'a', 'c'}};
%! checkkwargs(kwargs, okfields, okcombs);

%!error <invalid combination of kwargs passed; acceptable combinations are: \(a, c\), \(a, b\)>
%! kwargs.b = 1;
%! kwargs.c = 1;
%! okfields = {'a', 'b', 'c', 'd', 'e'};
%! okcombs = {{'a', 'c'}, {'a', 'b'}};
%! checkkwargs(kwargs, okfields, okcombs);

