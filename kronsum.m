%% Copyright (C) 2018 Travis Arnold
%%
%% This file is part of Octave.
%%
%% Octave is free software; you can redistribute it and/or modify it
%% under the terms of the GNU General Public License as published by
%% the Free Software Foundation; either version 3 of the License, or (at
%% your option) any later version.
%%
%% Octave is distributed in the hope that it will be useful, but
%% WITHOUT ANY WARRANTY; without even the implied warranty of
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
%% General Public License for more details.
%%
%% You should have received a copy of the GNU General Public License
%% along with Octave; see the file COPYING.  If not, see
%% <http://www.gnu.org/licenses/>.

%% C = kronsum(A, B)
%%
%% Returns the Kronecker sum of square matrices A and B. If A is m by m and B is
%% n by n, this quantity is calculated as
%%
%% C = kron(A, eye(n)) + kron(eye(m), B)
%%
%% See also: kron, kronplus.

%% Author: Travis Arnold <tjarnold@wisc.edu>
%% May 2018.

function C = kronsum(A, B)
    
    if ~isnumericmatrix(A)
        error('kronsum: A must be a numeric 2-D array')
    end%if

    if ~isnumericmatrix(B)
        error('kronsum: B must be a numeric 2-D array')
    end%if
    
    [m, n] = size(A);
    [p, q] = size(B);

    if m ~= n
        error('kronsum: A must be square')
    end%if

    if p ~= q
        error('kronsum: B must be square')
    end%if

    C = kron(A, eye(p)) + kron(eye(m), B);
end%function kronsum

