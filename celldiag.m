%% Copyright (C) 2019 Travis Arnold
%%
%% This file is part of Octave.
%%
%% Octave is free software; you can redistribute it and/or modify it
%% under the terms of the GNU General Public License as published by
%% the Free Software Foundation; either version 3 of the License, or (at
%% your option) any later version.
%%
%% Octave is distributed in the hope that it will be useful, but
%% WITHOUT ANY WARRANTY; without even the implied warranty of
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
%% General Public License for more details.
%%
%% You should have received a copy of the GNU General Public License
%% along with Octave; see the file COPYING.  If not, see
%% <http://www.gnu.org/licenses/>.

%% M = celldiag(V)
%% M = celldiag(V, K)
%% M = celldiag(V, M, N)
%% V = celldiag(M)
%% V = celldiag(M, K)
%%
%% celldiag is a Matlab compatible version of diag that allows some of the extra
%% functionality that Octave's diag allows. Namely, cell array inputs are
%% allowed, and a three input version of the function is implemented.
%%
%% If called from Octave or if the first argument is not a cell array, then
%% celldiag is simply a wrapper for diag.
%%
%% See also: diag, celldiag_.

%% Author: Travis Arnold <tjarnold@wisc.edu>
%% August 2019. 

function D = celldiag(varargin)
    if nargin() < 1 || nargin() > 3
        error('between one and three input arguments required');
    end%if

    if isoctave() || ~iscell(varargin{1})
        D = diag(varargin{:});
    else % Matlab, and first argument is a cell array
        D = celldiag_(varargin{:});
    end%if
end%function celldiag

