%% Copyright (C) 2019 Travis Arnold
%%
%% This file is part of Octave.
%%
%% Octave is free software; you can redistribute it and/or modify it
%% under the terms of the GNU General Public License as published by
%% the Free Software Foundation; either version 3 of the License, or (at
%% your option) any later version.
%%
%% Octave is distributed in the hope that it will be useful, but
%% WITHOUT ANY WARRANTY; without even the implied warranty of
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
%% General Public License for more details.
%%
%% You should have received a copy of the GNU General Public License
%% along with Octave; see the file COPYING.  If not, see
%% <http://www.gnu.org/licenses/>.

%% M = celldiag_(V)
%% M = celldiag_(V, K)
%% M = celldiag_(V, M, N)
%% V = celldiag_(M)
%% V = celldiag_(M, K)
%%
%% Function with the logic for celldiag. This is a separate function in a
%% separate m-file because that makes it easier to test the function in Octave.
%%
%% See also: diag, celldiag.

%% Author: Travis Arnold <tjarnold@wisc.edu>
%% August 2019. 

function D = celldiag_(varargin)
    C = varargin{1}; % celldiag ensures that this is a cell array.
    s = size(C);
    if numel(s) > 2
        error('matrix must be 2-dimensional');
    end%if

    if ismember(0, s)
        warning('first argument is an empty cell array');
        D = {};
        return;
    end%if

    if ismember(1, s) % Vector input
        C = C(:); % stack into a column
        numelC = numel(C);

        if nargin() <= 2
            if nargin() == 2
                [bool, K] = myisinteger(varargin{2});
                if ~bool
                    error('K must be an integer');
                end%if
            else % nargin() == 1
                K = 0;
            end%if

            M = numelC + abs(K);
            N = M;

        else % nargin() == 3
            [bool, M] = isnonneginteger(varargin{2});
            if ~bool
                error('M must be a nonnegative integer');
            end%if

            [bool, N] = isnonneginteger(varargin{3});
            if ~bool
                error('N must be a nonnegative integer');
            end%if

            if M < numelC || N < numelC
                error(['M and N must be greater than or equal to the ', ...
                       'number of elements in V']);
            end%if

            K = 0;
        end%if

        D = cell(M, N);

        if K >= 0
            rwstrt = 1;
            clstrt = K + 1;
        else % K < 0
            rwstrt = -K + 1;
            clstrt = 1;
        end%if

        rws = (0:numelC-1) + rwstrt;
        cls = (0:numelC-1) + clstrt;

        eval(['[D{', strjoin(cellfun(@num2str, ...
                                     num2cell(sub2ind([M, M], rws, cls)), ...
                                     'UniformOutput', false()), ...
                             '}, D{'), '}] = C{:};']);

    else % Matrix input
        if nargin() >= 2
            if nargin() > 2
                warning('ignoring all arguments past the second');
            end%if

            [bool, K] = myisinteger(varargin{2});
            if ~bool
                error('K must be an integer');
            end%if
        else
            K = 0;
        end%if

        if (K < 0 && -K > s(1) - 1) || (K > 0 && K > s(2) - 1)
            D = cell(0, 1);
        else
            if K >= 0
                rwstrt = 1;
                clstrt = K + 1;
            else % K < 0
                rwstrt = -K + 1;
                clstrt = 1;
            end%if

            n = min(s(1) - rwstrt + 1, s(2) - clstrt + 1);
            rws = (0:n-1) + rwstrt;
            cls = (0:n-1) + clstrt;
            D = C(sub2ind(s, rws, cls))';
        end%if
    end%if
end%function celldiag_

%!test
%! for i = 2:5
%!     for j = 2:5
%!         C = cell(i, j);
%!         C = cellfun(@(X) randn(2), C, 'UniformOutput', false());
%!         assert(diag(C), celldiag_(C));
%!         for K = -6:6
%!             C = cell(i, j);
%!             C = cellfun(@(X) randn(2), C, 'UniformOutput', false());
%!             assert(diag(C, K), celldiag_(C, K));
%!         end
%!     end
%! end

%!test
%! for num = 1:5
%!     C = cell(num, 1);
%!     C = cellfun(@(X) randn(2), C, 'UniformOutput', false());
%!     assert(diag(C), celldiag_(C));
%!     for K = -5:5
%!         assert(diag(C, K), celldiag_(C, K));
%!     end
%! end

%!test
%! for num = 1:5
%!     C = cell(num, 1);
%!     C = cellfun(@(X) randn(2), C, 'UniformOutput', false());
%!     for M = num:num+5
%!         for N = num:num+5
%!             assert(diag(C, M, N), celldiag_(C, M, N));
%!         end
%!     end
%! end

