%% Copyright (C) 2018 Travis Arnold
%%
%% This file is part of Octave.
%%
%% Octave is free software; you can redistribute it and/or modify it
%% under the terms of the GNU General Public License as published by
%% the Free Software Foundation; either version 3 of the License, or (at
%% your option) any later version.
%%
%% Octave is distributed in the hope that it will be useful, but
%% WITHOUT ANY WARRANTY; without even the implied warranty of
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
%% General Public License for more details.
%%
%% You should have received a copy of the GNU General Public License
%% along with Octave; see the file COPYING.  If not, see
%% <http://www.gnu.org/licenses/>.

%% T = blktoep(C, R)
%%
%% Return a block Toeplitz matrix where the elements of C form the first block
%% column and (optionally) the elements of R form the first block row:
%%
%% C{1}   R{2}   R{3}  ...  R{m}
%% C{2}   C{1}   R{2}  ...  R{m-1}
%% C{3}   C{2}   C{1}  ...  R{m-2}
%%  .      .       .   .     .
%%  .      .       .    .    .
%%  .      .       .     .   .
%% C{n}  C{n-1}  C{n-2} ... C{1}
%%
%% If the first element of R is not the same as the first element of C, the
%% first element of C is used. If the second argument is omitted, the first row
%% is taken to be the same as the first column.
%%
%% Alternatively, if R is 'tril', then the block lower triangular matrix is
%% formed:
%%
%% C{1}    0      0    ...   0
%% C{2}   C{1}    0    ...   0
%% C{3}   C{2}   C{1}  ...   0
%%  .      .       .   .     .
%%  .      .       .    .    .
%%  .      .       .     .   .
%% C{n}  C{n-1}  C{n-2} ... C{1}
%%
%% See also: toeplitz, hermblktoep.

%% Author: Travis Arnold <tjarnold@wisc.edu>
%% May 2018.

function T = blktoep(C, R)
    if  (nargin() < 1 || nargin() > 2)
        error('blktoep: one or two input arguments is required');
    end%if

    if ~iscell(C)
        error('blktoep: C must be a cell array');
    end%if
    C = C(:);
    n = numel(C);

    if isempty(C)
        error('blktoep: C is an empty cell array');
    end%if

    Csizes = cellfun(@size, C, 'UniformOutput', false);
    Cismat = cellfun(@ismatrix, C);

    if ~all(Cismat)
        error('blktoep: all elements of C must be matrices');
    end%if

    if n > 1
        if ~isequal(Csizes{:})
            error('blktoep: all elements of C must have same dimensions');
        end%if
    end%if

    if nargin() == 1
        T = cell2mat(C(toeplitz(1:n)));
    elseif nargin() == 2
        if ischarrow(R) && strcmp(R, 'tril')
            [rws, cls] = size(C{1});
            R = mat2cell([C{1}; repmat(zeros(size(C{1})), n-1, 1)], ...
                         repmat(rws, 1, n), cls);
            m = n;
        elseif ~iscell(R)
            error('blktoep: R must be a cell array or ''tril''');
        else
            R = R(:);
            m = numel(R);

            if isempty(R)
                error('blktoep: R is an empty cell array');
            end%if

            Rsizes = cellfun(@size, R, 'UniformOutput', false);
            Rismat = cellfun(@ismatrix, R);

            if ~all(Rismat)
                error('blktoep: all elements of R must be matrices');
            end%if

            if m > 1
                if ~isequal(Rsizes{:})
                    error(['blktoep: all elements of R must have same ', ...
                           'dimensions']);
                end%if
            end%if

            if ~isequal(Csizes{1}, Rsizes{1})
                error('blktoep: elements of C and R must have same sizes');
            end%if

            if ~isequal(C{1}, R{1})
                warning(['blktoep: first element of C does not match ', ...
                         'first element of R; C wins diagonal conflict']);
            end%if
        end%if

        CR = [C; R(2:end)];
        if n == 1
            T = cell2mat(CR(toeplitz(1:n, [1, (n+1):(n+m-1)]))');
        else
            T = cell2mat(CR(toeplitz(1:n, [1, (n+1):(n+m-1)])));
        end%if
    end%if
end%function blktoep

%!error <one or two input arguments is required> blktoep();
%!error <one or two input arguments is required> blktoep(1, 2, 3);
%!error <C must be a cell array> blktoep([1 2 3]);
%!error <C is an empty cell array> blktoep({});
%!error <all elements of C must be matrices> blktoep({ones(2,2,2)});
%!error <all elements of C must have same dimensions> blktoep({[1], [2, 3]});
%!error <R must be a cell array or 'tril'> blktoep({1}, [1]);
%!error <R is an empty cell array> blktoep({1}, {});
%!error <all elements of R must be matrices> blktoep({1}, {ones(2,2,2)});
%!error <all elements of R must have same dimensions> blktoep({1},{[1],[2, 3]});
%!error <elements of C and R must have same sizes> blktoep({1}, {[1, 2]});
%!warning <first element of C does not match first element of R; C wins diagonal conflict> blktoep({1}, {2});

%!test
%! for i = 1:5
%! for j = 1:5
%!
%! A = randn(i, j);
%! B = randn(i, j);
%! C = randn(i, j);
%! D = randn(i, j);
%! E = randn(i, j);
%! F = randn(i, j);
%! G = randn(i, j);
%! Z = zeros(i, j);
%!
%! assert(A, blktoep({A}));
%! assert(A, blktoep({A}, 'tril'))
%! assert([A B; B A], blktoep({A, B}));
%! assert([A Z; B A], blktoep({A, B}, 'tril'));
%! assert([A B C; B A B; C B A], blktoep({A, B, C}));
%! assert([A Z Z; B A Z; C B A], blktoep({A, B, C}, 'tril'));
%! assert([A B C D; B A B C; C B A B; D C B A], blktoep({A, B, C, D}));
%! assert([A Z Z Z; B A Z Z; C B A Z; D C B A], blktoep({A, B, C, D}, 'tril'));
%!
%! assert([A; B; C; D], blktoep({A, B, C, D}, {A}));
%! assert([A E; B A; C B; D C], blktoep({A, B, C, D}, {A, E}));
%! assert([A E F; B A E; C B A; D C B], blktoep({A, B, C, D}, {A, E, F}));
%! assert([A E F G; B A E F; C B A E; D C B A], ...
%!        blktoep({A, B, C, D}, {A, E, F, G}));
%!
%! assert([A B C D], blktoep({A}, {A, B, C, D}));
%! assert([A E F G; B A E F], blktoep({A, B}, {A, E, F, G}));
%! assert([A E F G; B A E F; C B A E], blktoep({A, B, C}, {A, E, F, G}));
%!
%! end%for
%! end%for

