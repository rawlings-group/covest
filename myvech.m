%% Copyright (C) 1995-2015 Kurt Hornik
%% Copyright (C) 2009 VZLU Prague
%% Copyright (C) 2017 Travis Arnold
%%
%% This file is part of Octave.
%%
%% Octave is free software; you can redistribute it and/or modify it
%% under the terms of the GNU General Public License as published by
%% the Free Software Foundation; either version 3 of the License, or (at
%% your option) any later version.
%%
%% Octave is distributed in the hope that it will be useful, but
%% WITHOUT ANY WARRANTY; without even the implied warranty of
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
%% General Public License for more details.
%%
%% You should have received a copy of the GNU General Public License
%% along with Octave; see the file COPYING.  If not, see
%% <http://www.gnu.org/licenses/>.

%% -*- texinfo -*-
%% @deftypefn {Function File} {} myvech (@var{x})
%% Return the vector obtained by eliminating all superdiagonal elements of
%% the matrix @var{x} and stacking the result one column above the
%% other.  Unlike Octave's vech function, @var{x} does not have to be square.
%%
%% This has uses in matrix calculus where the underlying matrix is symmetric
%% and it would be redundant to keep values above the main diagonal.
%%
%% See Magnus and Neudecker (1988), @cite{Matrix Differential Calculus with
%% Applications in Statistics and Econometrics}.
%% @seealso{vec, vech}
%% @end deftypefn

%% Author: Travis Arnold, <tjarnold@wisc.edu>, January 2017
%% Adapted from Octave's vech function.
%% Adapted to be Matlab compatible, May 2018.

function v = myvech(x)
    if (nargin() ~= 1)
        error('myvech: exactly one input argument is required');
    end%if

    [m, n] = size(x);
%    slices = cellslices(x(:), (1:n) + m*(0:n-1), m*(1:n)); % Matlab incompatible
    slices = arrayfun(@(k) x(k:m, k), 1:min(m, n), 'UniformOutput', false);
    v = vertcat(slices{:});
end%function myvech

%!assert(myvech([1, 2, 3; 4, 5, 6; 7, 8, 9]), [1; 4; 7; 5; 8; 9])

%!test
%! A = randn(7);
%! assert(vech(A), myvech(A));

%!assert(myvech([1, 2, 3, 4; 5, 6, 7, 8]), [1; 5; 6]);

%!assert(myvech([1, 2, 3, 4; 5, 6, 7, 8]'), [1; 2; 3; 4; 6; 7; 8]);

%!error <exactly one input argument is required> myvech()
%!error <exactly one input argument is required> myvech(1, 2)

