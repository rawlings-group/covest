%% Copyright (C) 2018 Travis Arnold
%%
%% This file is part of Octave.
%%
%% Octave is free software; you can redistribute it and/or modify it
%% under the terms of the GNU General Public License as published by
%% the Free Software Foundation; either version 3 of the License, or (at
%% your option) any later version.
%%
%% Octave is distributed in the hope that it will be useful, but
%% WITHOUT ANY WARRANTY; without even the implied warranty of
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
%% General Public License for more details.
%%
%% You should have received a copy of the GNU General Public License
%% along with Octave; see the file COPYING.  If not, see
%% <http://www.gnu.org/licenses/>.

%% retval = rpinv(A, tol)
%%
%% Calculate the pseudoinverse of matrix A. Optional parameter tol must satisfy
%% 0 < tol < 1. Singular values less than sigma(1) * tol are ignored, where
%% sigma(1) is the largest singular value. If tol is not provided, the default
%% value of 1e-6 is used.
%%
%% This is similar to Octave's built-in pinv function; the difference is that
%% here, the tol parameter that may be passed is relative instead of absolute
%% (that is, pinv ignores singular values less than tol).
%%
%% See also: pinv, svd.

%% Author: Travis Arnold <tjarnold@wisc.edu>
%% July 2018.

function retval = rpinv(A, tol)

% Check number of input arguments.
if ~ismember(nargin(), [1, 2])
    error('exactly one or two input arguments required');
end%if

% Make sure A is a numeric matrix.
if ~isnumericmatrix(A)
    error('A must be a numeric matrix');
end%if

% If it is provided, perform checks on tol.
if (nargin() == 2)
    % Make sure tol is a numeric scalar.
    if ~isnumericscalar(tol)
        error('tol must be a numeric scalar');
    end%if

    % Make sure 0 < tol < 1.
    if (tol <= 0) || (tol >= 1)
        error('tol must satisfy 0 < tol < 1');
    end%if
else
    % Set default value of tol.
    tol = 1e-6;
end%if

% Compute SVD of A.
[U, S, V] = svd(A);

% Calculate rank as the number of singular values greater than the specified
% tolerance.
r = sum(diag(S) > S(1, 1)*tol);

% Calculate the pseudoinverse based on the SVD.
retval = ApinvB(V(:, 1:r), S(1:r, 1:r)) * U(:, 1:r)';

end%function rpinv

