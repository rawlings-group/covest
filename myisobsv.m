%% Copyright (C) 2020 Travis Arnold
%%
%% This file is part of Octave.
%%
%% Octave is free software; you can redistribute it and/or modify it
%% under the terms of the GNU General Public License as published by
%% the Free Software Foundation; either version 3 of the License, or (at
%% your option) any later version.
%%
%% Octave is distributed in the hope that it will be useful, but
%% WITHOUT ANY WARRANTY; without even the implied warranty of
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
%% General Public License for more details.
%%
%% You should have received a copy of the GNU General Public License
%% along with Octave; see the file COPYING.  If not, see
%% <http://www.gnu.org/licenses/>.

%% [isobsv, nobsv, isdetec, ndetec, Ac, Cc, T] = myisobsv(A, C, tol)
%%
%% Perform observability and detectability analysis on discrete time linear
%% system (A, C) by putting the system into observability canonical form.
%% myisobsv is a wrapper of myisctrb; see the documentation of myisctrb for more
%% information.
%%
%% * Inputs *
%%
%% A : state transition matrix (n x n)
%%
%% C : measurement matrix (p x n) 
%%
%% tol : Tolerance parameter. Optional.
%%
%% * Outputs *
%%
%% isobsv : Logical. 1 if system is observable, 0 if system is unobservable.
%%
%% nobsv : Number of observable states. Equal to the rank of the observability
%%         matrix.
%%
%% isdetec : Logical. 1 if system is stabilizable, 0 if system is stabilizable.
%%
%% ndetec : Number of detectability states.
%%
%% Ac : State transition matrix of observability canonical form.
%%
%% Cc : Measurement matrix of observability canonical form.
%%
%% T : Transformation matrix.
%%
%% Ac = T * A * T'             Cc =  C * T'
%%    = | A11     0  |            = | C1    0 |
%%      | A21    A22 |
%%
%% See also: obsv, ctrb, isobsv (Octave only), isctrb (Octave only), myisctrb.

%% Author: Travis Arnold <tjarnold@wisc.edu>
%% February 2020.

function [isobsv, nobsv, isdetec, ndetec, Ac, Cc, T] = myisobsv(A, C, tol)

% Check number of input arguments.
if ~ismember(nargin(), [2, 3])
    error('exactly two or three input arguments required');
end%if

% Make sure A and C are numeric matrices, to ensure they can be transposed and
% passed to myisctrb.
if ~isnumericmatrix(A)
    error('A must be a numeric matrix');
end%if
if ~isnumericmatrix(C)
    error('C must be a numeric matrix');
end%if

switch nargin()
    case 2
        [isobsv, nobsv, isdetec, ndetec, Ac, Cc, T] = myisctrb(A', C');
    case 3
        [isobsv, nobsv, isdetec, ndetec, Ac, Cc, T] = myisctrb(A', C', tol);
end%switch

Ac = Ac';
Cc = Cc';

end%function myisobsv

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%% Make sure the transformations do what they should %%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%!test
%! n = 3;
%! m = 2;
%! A = [1, 2, 3; 4, 5, 6; 7, 8, 9] / 3;
%! C = ([1, 3; 5, 7; 8, 10] / 4)';
%! [~, ~, ~, ~, Ac, Cc, T] = myisobsv(A, C, 1e-8);
%! assert(norm(Ac - T * A * T', 'fro') < 1e-8);
%! assert(norm(Cc - C * T', 'fro') < 1e-8);

