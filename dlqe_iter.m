%% Copyright (C) 2018 Travis Arnold
%%
%% This file is part of Octave.
%%
%% Octave is free software; you can redistribute it and/or modify it
%% under the terms of the GNU General Public License as published by
%% the Free Software Foundation; either version 3 of the License, or (at
%% your option) any later version.
%%
%% Octave is distributed in the hope that it will be useful, but
%% WITHOUT ANY WARRANTY; without even the implied warranty of
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
%% General Public License for more details.
%%
%% You should have received a copy of the GNU General Public License
%% along with Octave; see the file COPYING.  If not, see
%% <http://www.gnu.org/licenses/>.

%% [L, K, P, Z, E, M, isconv, rnorm, rsnorm] = dlqe_iter(A, C, Q, R, kwargs)
%%
%% Find the optimal linear estimator (steady-state Kalman filter) by iterating
%% the discrete algebraic Riccati equation. The documentation for dare_iter is
%% recommended reading, as dlqe_iter is simply a wrapper for dare_iter.
%%
%% * SYSTEM *
%%
%% x[k+1] = A*x[k] + B*u[k] + w[k]   (state transition)
%%   y[k] = C*x[k] + D*u[k] + v[k]     (measurement)
%%
%% E[w] = [0]      E[ww' wv'] = [Q  S]
%%  [v]   [0]       [vw' vv']   [S' R]
%%
%% * INPUTS *
%%
%% A : state transition matrix (n x n)
%%
%% C : measurement matrix (p x n)
%%
%% Q : process noise covariance (n x n)
%%
%% R : measurement noise covariance (p x p)
%%
%% * OPTIONAL INPUTS *
%%
%% kwargs may be passed as either a struct or as key-value pairs. Optional
%% inputs are the same as those for dare_iter.
%%
%% * OUTPUTS *
%%
%% L : steady-state Kalman filter gain (n x p)
%% L = P*C' * pinv(C*P*C' + R)
%% 
%% K : steady-state Kalman predictor gain (n x p)
%% K = (A*P*C' + S) * pinv(C*P*C' + R)
%%
%% P : steady-state Kalman predictor error (n x n)
%% P = cov(x[k|k-1] - x[k])
%% P = A*P*A' - (A*P*C' + S) * pinv(C*P*C' + R) * (A*P*C' + S)' + Q
%%
%% Z : steady-state Kalman filter error (n x n)
%% Z = cov(x[k|k] - x[k])
%% Z = P - L*C*P
%%
%% E : steady-state closed loop poles (n x 1)
%% E = eig(A - K*C)
%%
%% M : useful to store for Kalman smoothing algorithm (n x p)
%% M = C' * pinv(C*P*C' + R)
%%
%% isconv : True if the iterations converge to a solution.
%%
%% rnorm : Frobenius norm of the residual.
%%
%% rsnorm : Scaled Frobenius norm of the residual.
%%
%% * KALMAN FILTER EQUATIONS *
%%
%% Predictor form:
%%
%% x[k+1|k] = A*x[k|k-1] + B*u[k] + K*(y[k] - C*x[k|k-1] - D*u[k])
%%
%% Filtering form, time update:
%%
%% x[k+1|k] = A*x[k|k] + B*u[k] + S*pinv(R)*(y[k] - C*x[k|k] - D*u[k])
%%          = A*x[k|k] + B*u[k] + S*pinv(R)*(eye(p) - C*L)
%%                                           *(y[k] - C*x[k|k-1] - D*u[k])
%% 
%% Filtering form, measurement update
%% 
%% x[x+1|k+1] = x[k+1|k] + L*(y[k+1] - C*x[k+1|k] - D*u[k+1])
%% 
%% See also: dare, dare_iter, dlqe.

%% Author: Travis Arnold <tjarnold@wisc.edu>
%% July 2018.

function [L, K, P, Z, E, M, isconv, rnorm, rsnorm] = dlqe_iter(A, C, Q, R, varargin)

[P, E, K, Z, L, M, isconv, rnorm, rsnorm] = dare_iter(A', C', Q, R, ...
                                                      varargin{:});

% Transpose K, L, and M.
K = cellctrans(K);
L = cellctrans(L);
M = cellctrans(M);

end%function dlqe_iter

