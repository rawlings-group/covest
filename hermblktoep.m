%% Copyright (C) 2018 Travis Arnold
%%
%% This file is part of Octave.
%%
%% Octave is free software; you can redistribute it and/or modify it
%% under the terms of the GNU General Public License as published by
%% the Free Software Foundation; either version 3 of the License, or (at
%% your option) any later version.
%%
%% Octave is distributed in the hope that it will be useful, but
%% WITHOUT ANY WARRANTY; without even the implied warranty of
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
%% General Public License for more details.
%%
%% You should have received a copy of the GNU General Public License
%% along with Octave; see the file COPYING.  If not, see
%% <http://www.gnu.org/licenses/>.

%% T = hermblktoep(C, dims)
%%
%% Return a Hermitian block Toeplitz matrix where the elements of C form the
%% first block column. dims is an optional 2-D argument specifying the number of
%% block columns and rows to include. If dims = [n, m], the returned matrix is:
%%
%% C{1}    C{2}'   C{3}'  ...  C{m}'
%% C{2}    C{1}    C{2}'  ...  C{m-1}'
%% C{3}    C{2}    C{1}   ...  C{m-2}'
%%  .       .        .    .      .
%%  .       .        .     .     .
%%  .       .        .      .    .
%% C{n}   C{n-1}   C{n-2}  ...  C{1}
%%
%% A warning is issued if C{1} is not Hermitian. Sometimes this happens when
%% C{1} is nearly Hermitian, but not quite due to numerical roundoff. In this
%% case consider making use of the forceherm function.
%%
%% See also: toeplitz, blktoep, forceherm.

%% Author: Travis Arnold <tjarnold@wisc.edu>
%% May 2018.

function T = hermblktoep(C, dims)
    if  (nargin() < 1 || nargin() > 2)
        error('hermblktoep: one or two input arguments is required');
    end%if

    if ~iscell(C)
        error('hermblktoep: C must be a cell array');
    end%if
    C = C(:);
    n = numel(C);

    if isempty(C)
        error('hermblktoep: C is an empty cell array');
    end%if

    Csizes = cellfun(@size, C, 'UniformOutput', false);
    Cismat = cellfun(@ismatrix, C);

    if ~all(Cismat)
        error('hermblktoep: all elements of C must be matrices');
    end%if

    if n > 1
        if ~isequal(Csizes{:})
            error('hermblktoep: all elements of C must have same dimensions');
        end%if
    end%if

    if Csizes{1}(1) ~= Csizes{1}(2)
        error('hermblktoep: all elements of C must be square');
    end%if

    if ~isequal(C{1}, C{1}')
        warning(['hermblktoep: diagonal block is not Hermitian; if this ', ...
                 'is due to numerical roundoff error you might want to ', ...
                 'use the forceherm function']);
    end%if

    if nargin() == 1
        nrows = n;
        ncols = n;
    elseif nargin() == 2
        if ~isnumericmatrix(dims)
            error('hermblktoep: dims must be a numeric 2-D array');
        end%if

        if numel(dims) < 2
            warning(['hermblktoep: number of block columns not specified; ', ...
                      'using all blocks in C']);
            dims(2) = n;
        end%if

        if numel(dims) > 2
            warning(['hermblktoep: more than two elements detected in ', ...
                     'dims; ignoring all but the first two elements']);
        end%if

        nrows = dims(1);
        ncols = dims(2);

        if (nrows < 1 || mod(nrows, 1) ~= 0 || ncols < 1 || mod(ncols, 1) ~= 0)
            error('hermblktoep: dims must have positive integer values');
        end%if

        if (nrows > n)
            warning(['hermblktoep: more block rows specified than number ', ...
                     'of elements of C; using all elements of C']);
            nrows = n;
        end%if

        if (ncols > n)
            warning(['hermblktoep: more block columns specified than ', ...
                     'number of elements of C; using all elements of C']);
            ncols = n;
        end%if
    end%if

    Cherm = [C{1}; cellctrans(C(2:end))];

    if (nrows == 1 && ncols == 1)
        T = C{1};
    elseif (nrows == 1 && ncols ~= 1)
        T = horzcat(C{1}, Cherm{2:ncols});
    elseif (nrows ~= 1 && ncols == 1)
        T = vertcat(C{1:nrows});
    else
        T = blktoep(C(1:nrows), Cherm(1:ncols));
    end%if
end%function hermblktoep

%!error <one or two input arguments is required> hermblktoep();
%!error <one or two input arguments is required> hermblktoep(1, 2, 3);
%!error <C must be a cell array> hermblktoep([1 2 3]);
%!error <C is an empty cell array> blktoep({});
%!error <all elements of C must be matrices> hermblktoep({ones(2,2,2)});
%!error <all elements of C must have same dimensions> hermblktoep({[1], [2, 3]});
%!error <all elements of C must be square> hermblktoep({[1 2], [2 3]});
%!error <all elements of C must be square> hermblktoep({[1; 2], [2; 3]});
%!warning <diagonal block is not Hermitian; if this is due to numerical roundoff error you might want to use the forceherm function> hermblktoep({[1 2; 3 4]});
%!warning <diagonal block is not Hermitian; if this is due to numerical roundoff error you might want to use the forceherm function> hermblktoep({[5 6; 7 8], [1 2; 3 4]});
%!error <dims must be a numeric 2-D array> hermblktoep({1}, 'abcd');
%!error <dims must be a numeric 2-D array> hermblktoep({1}, ones(2,2,2));
%!warning <number of block columns not specified; using all blocks in C> hermblktoep({1}, 1);
%!warning <more than two elements detected in dims; ignoring all but the first two elements> hermblktoep({1}, [1 1 1]); 
%!error <dims must have positive integer values> hermblktoep({1}, [1 0]);
%!error <dims must have positive integer values> hermblktoep({1}, [0 1]);
%!error <dims must have positive integer values> hermblktoep({1}, [0 0]);
%!error <dims must have positive integer values> hermblktoep({1}, [-1 0]);
%!error <dims must have positive integer values> hermblktoep({1}, [0 -1]);
%!error <dims must have positive integer values> hermblktoep({1}, [-1 -1]);
%!error <dims must have positive integer values> hermblktoep({1}, [1 0.5]);
%!warning <more block rows specified than number of elements of C; using all elements of C> hermblktoep({1}, [2 1]);
%!warning <more block columns specified than number of elements of C; using all elements of C> hermblktoep({1}, [1 2]);

%!test
%! for i = 1:5
%! A = randn(i); A = A*A';
%! B = randn(i);
%! C = randn(i);
%! D = randn(i);
%%%%! E = randn(i);
%%%%! F = randn(i);
%%%%! G = randn(i);
%! 
%! assert(A, hermblktoep({A}));
%! assert([A B'; B A] , hermblktoep({A, B}));
%! assert([A B' C'; B A B'; C B A] , hermblktoep({A, B, C}));
%! assert([A B' C' D'; B A B' C'; C B A B'; D C B A] , hermblktoep({A, B, C, D}));
%! 
%! assert(A, hermblktoep({A}, [1, 1]))
%!
%! assert(A , hermblktoep({A, B}, [1 1]));
%! assert([A B'] , hermblktoep({A, B}, [1 2]));
%! assert([A; B] , hermblktoep({A, B}, [2 1]));
%! assert([A B'; B A] , hermblktoep({A, B}, [2 2]));
%!
%! assert(A , hermblktoep({A, B, C}, [1 1]));
%! assert([A; B], hermblktoep({A, B, C}, [2 1]));
%! assert([A B'], hermblktoep({A, B, C}, [1 2]));
%! assert([A B'; B A], hermblktoep({A, B, C}, [2 2]));
%! assert([A; B; C], hermblktoep({A, B, C}, [3 1]));
%! assert([A B' C'], hermblktoep({A, B, C}, [1 3]));
%! assert([A B'; B A; C B], hermblktoep({A, B, C}, [3 2]));
%! assert([A B' C'; B A B'], hermblktoep({A, B, C}, [2 3]));
%! assert([A B' C'; B A B'; C B A], hermblktoep({A, B, C}, [3 3]));
%! end%for

