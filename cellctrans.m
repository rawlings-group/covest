%% Copyright (C) 2019 Travis Arnold
%%
%% This file is part of Octave.
%%
%% Octave is free software; you can redistribute it and/or modify it
%% under the terms of the GNU General Public License as published by
%% the Free Software Foundation; either version 3 of the License, or (at
%% your option) any later version.
%%
%% Octave is distributed in the hope that it will be useful, but
%% WITHOUT ANY WARRANTY; without even the implied warranty of
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
%% General Public License for more details.
%%
%% You should have received a copy of the GNU General Public License
%% along with Octave; see the file COPYING.  If not, see
%% <http://www.gnu.org/licenses/>.

%% ct = cellctrans(c)
%%
%% For a cell array of matrices c, return a cell array ct that contains the
%% conjugate transpose of every matrix of c. If c is a matrix, then return
%% ct = c'.

%% Author: Travis Arnold <tjarnold@wisc.edu>
%% January 2019.

function ct = cellctrans(c)

if nargin() ~= 1
    error('exactly one input argument required');
end%if

if ~iscell(c) && ismatrix(c)
    ct = c';
elseif iscell(c)
    if ~all(cellfun(@ismatrix, c))
        error('c must be a matrix or a cell array of matrices');
    end%if
    ct = cellfun(@ctranspose, c, 'UniformOutput', false());
else
    error('c must be a matrix or a cell array of matrices');
end%if

end%function cellctrans

