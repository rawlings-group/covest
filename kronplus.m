%% Copyright (C) 2018 Travis Arnold
%%
%% This file is part of Octave.
%%
%% Octave is free software; you can redistribute it and/or modify it
%% under the terms of the GNU General Public License as published by
%% the Free Software Foundation; either version 3 of the License, or (at
%% your option) any later version.
%%
%% Octave is distributed in the hope that it will be useful, but
%% WITHOUT ANY WARRANTY; without even the implied warranty of
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
%% General Public License for more details.
%%
%% You should have received a copy of the GNU General Public License
%% along with Octave; see the file COPYING.  If not, see
%% <http://www.gnu.org/licenses/>.

%% C = kronplus(A, B)
%%
%% Similar to the Kronecker product, but instead of all of the pairwise products
%% of the elements of A and B, C contains all the pairwise sums. If A is m by n
%% and B is p by q, this is calculated as
%%
%% C = kron(A, ones(p, q)) + kron(ones(m, n), B)
%%
%% Note that this is different than the Kronecker sum.
%%
%% See also: kron, kronsum.

%% Author: Travis Arnold <tjarnold@wisc.edu>
%% May 2018.

function C = kronplus(A, B)
    if ~isnumericmatrix(A)
        error('kronplus: A must be a numeric 2-D array')
    end%if

    if ~isnumericmatrix(B)
        error('kronplus: B must be a numeric 2-D array')
    end%if
    
    [m, n] = size(A);
    [p, q] = size(B);

    C = kron(A, ones(p, q)) + kron(ones(m, n), B);
end%function kronplus

