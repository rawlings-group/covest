%% Copyright (C) 2017 Travis Arnold
%%
%% This file is part of Octave.
%%
%% Octave is free software; you can redistribute it and/or modify it
%% under the terms of the GNU General Public License as published by
%% the Free Software Foundation; either version 3 of the License, or (at
%% your option) any later version.
%%
%% Octave is distributed in the hope that it will be useful, but
%% WITHOUT ANY WARRANTY; without even the implied warranty of
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
%% General Public License for more details.
%%
%% You should have received a copy of the GNU General Public License
%% along with Octave; see the file COPYING.  If not, see
%% <http://www.gnu.org/licenses/>.

%% -*- texinfo -*-
%% @deftypefn {Function File} {@var{D} =} dupmat (@var{n})
%% Return the duplication matrix
%% @tex
%%  $D_n$
%% @end tex
%% @ifnottex
%%  @math{D(n)}
%% @end ifnottex
%% which is the unique
%% @tex
%%  $n^2 \times n(n+1)/2$
%% @end tex
%% @ifnottex
%%  @math{@var{n}^2} by @math{@var{n}*(@var{n}+1)/2}
%% @end ifnottex
%% matrix such that
%% @tex
%%  $D_n {\rm vech} \, A = {\rm vec} \, A$
%% @end tex
%% @ifnottex
%%  @math{D(n)*vech (A) = vec (A)}
%% @end ifnottex
%% for all symmetric
%% @tex
%%  $n \times n$
%% @end tex
%% @ifnottex
%%  @math{@var{n}} by @math{@var{n}}
%% @end ifnottex
%% matrices
%% @tex
%%  $A$.
%% @end tex
%% @ifnottex
%%  @math{A}.
%% @end ifnottex
%%
%% Unlike Octave's duplication_matrix function, dupmat returns a sparse
%% matrix, but it is MUCH faster than duplication_matrix. For a
%% demonstration of this, type 'demo dupmat'.
%%
%% See Magnus and Neudecker (1988), @cite{Matrix Differential Calculus with
%% Applications in Statistics and Econometrics}.
%% @seealso{duplication_matrix, vec, vech, dupmatpinv}
%% @end deftypefn

%% Author: Travis Arnold <tjarnold@wisc.edu>, January 2017.
%% Adapted from Octave's duplication_matrix function.
%% Adapted to be Matlab compatible, May 2018.

function D = dupmat(n)
    if (nargin() ~= 1)
        error('dupmat: exactly one input argument is required');
    end%if

    try
        [nisposint, n] = isposinteger(n);
    catch
        error('dupmat: N must be a positive integer');
    end%trycatch

    if ~nisposint
        error('dupmat: N must be a positive integer');
    end%if

    count = [0, arrayfun(@(k) sum(n-1:-1:k), n-1:-1:1)];
    j = 1:n;
    row1 = (j-1)*n + j;
    col1 = count + j;

    col23 = arrayfun(@(k) count(k) + (k+1:n), 1:n-1, 'UniformOutput', false);
    col23 = horzcat(col23{:});

    row2 = arrayfun(@(k) (k-1)*n + (k+1:n) , 1:n-1, 'UniformOutput', false);
    row2 = horzcat(row2{:});

    row3 = arrayfun(@(k) ((k+1:n)-1)*n + k , 1:n-1, 'UniformOutput', false);
    row3 = horzcat(row3{:});

    D = sparse([row1 row2 row3], [col1 col23 col23], 1);
end%function dupmat

%!assert(full(dupmat(1)), 1);
%!assert(full(dupmat(4)), duplication_matrix(4));
%!assert(full(dupmat(7)), duplication_matrix(7));

%!test
%! N = 2;
%! A = rand(N);
%! B = A * A';
%! C = A + A';
%! D = dupmat(N);
%! assert(D * vech(B), vec(B), 1e-6);
%! assert(D * vech(C), vec(C), 1e-6);

%!test
%! N = 3;
%! A = rand(N);
%! B = A * A';
%! C = A + A';
%! D = dupmat(N);
%! assert(D * vech(B), vec(B), 1e-6);
%! assert(D * vech(C), vec(C), 1e-6);

%!test
%! N = 4;
%! A = rand(N);
%! B = A * A';
%! C = A + A';
%! D = dupmat(N);
%! assert(D * vech(B), vec(B), 1e-6);
%! assert(D * vech(C), vec(C), 1e-6);

%!error <exactly one input argument is required> dupmat()
%!error <exactly one input argument is required> dupmat(1, 2)
%!error <N must be a positive integer> dupmat(0.5)
%!error <N must be a positive integer> dupmat(-1)
%!error <N must be a positive integer> dupmat(ones(1,4))

%!demo
%! n1 = 5000;
%! n2 = 180;
%! timer = tic;
%! dupmat(n1);
%! t1 = toc(timer);
%! duplication_matrix(n2);
%! t2 = toc(timer);
%! fprintf('dupmat(%i) : %f seconds\n', n1, t1);
%! fprintf('duplication_matrix(%i) : %f seconds\n', n2, t2-t1);

