classdef cedata < handle

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% properties %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

properties (Constant)
    props = struct( ...
                 'base', {{'x', 'y', 'u'}}, ...
                 'dependent', {{'Nd', 'Tf', 'n', 'p', 'm', 'stateFlag', 'inputFlag'}})
end%properties

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

properties (GetAccess = public, SetAccess = public)
    x
    y
    u
end%properties

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

properties (GetAccess = public, SetAccess = private)
    Nd
    Tf
    n
    p
    m
    inputFlag = false()
    stateFlag = false()
end%properties

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

properties (Hidden, GetAccess = public, SetAccess = private)
    id
end%properties

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

properties (Hidden, GetAccess = public, SetAccess = public)
    flags = struct('ceestimator', struct(), ...
                   'als', struct())
end%properties

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% methods %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

methods (Access = public)
    function self = cedata(varargin)
        % Constructor.

        % Generate uuid.
        self.id = uuidgen();

        % Get kwargs struct.
        kwargs = args2kwargs(varargin{:});

        % Make sure kwargs are valid; throw an error if not.
        checkkwargs(kwargs, self.props.base);

        % Set fields of self.
        for X = fieldnames(kwargs)'
            self.(X{:}) = kwargs.(X{:});
        end%for
    end%function cedata

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function bool = eq(self, b)
        % Determine if two cedata instances refer to the same object by
        % comparing their id property. This functionality is built into Matlab,
        % but not Octave.
        if nargin() ~= 2
            error('exactly two input arguments required');
        end%if
        if ~isa(b, 'cedata')
            error('second argument is not a cedata object');
        end%if
        bool = strcmp(self.id, b.id);
    end%function eq

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function bool = ne(self, b)
        % Determine if two cedata instances do not refer to the same object by
        % comparing their id property. This functionality is built into Matlab,
        % but not Octave.
        if nargin() ~= 2
            error('exactly two input arguments required');
        end%if
        if ~isa(b, 'cedata')
            error('second argument is not a cedata object');
        end%if
        bool = ~(self == b);
    end%function ne

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function [bool, msg] = compatible(self, mdl)
        % Check if cedata object is compatible with given cemodel object. Throw
        % an error with an explanatory message if they are incompatible. Note
        % that this is just a wrapper around the compatible function provided by
        % the cemodel class.

        % Check number of input arguments.
        if nargin() ~= 2
            error('exactly two input arguments required');
        end%if

        % Make sure mdl is a cemodel object.
        if ~isa(mdl, 'cemodel')
            error('model must be a cemodel object');
        end%if

        [bool, msg] = mdl.compatible(self);
    end%function compatible

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function [bool, msg] = isempty(self)
        % Check if everything in cedata object is empty.

        if nargin() ~= 1
            error('exactly one input argument required');
        end%if
        msg = '';
        bool = isempty(self.y) && isempty(self.u);
        if bool
            msg = 'cedata object is empty';
        end%if
    end%function isempty

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function disp(self)
        if isoctave()
            disp('<object cedata>');
        else % Matlab
            disp(['<object cedata>', char(10)]);
        end%if
    end%function
end%methods (Access = public)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% set methods %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

methods (Access = private)
    function need2set = helpsetX(self, X, val)
        % Determine if X needs to be set.
        need2set = ~isequal(val, self.(X));

        % If cedata object changes, raise flags.
        if need2set
            for s = {'ceestimator', 'als'}
                self.flags.(s{:}) = structfun(@(x) true(), ...
                                              self.flags.(s{:}), ...
                                              'UniformOutput', false());
            end%for
        end%if
    end%function helpsetX
end%methods

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

methods
    function set.x(self, val)
        if ~isnumericmatrix(val)
            error('x must be a numeric matrix');
        end%if
        need2set = self.helpsetX('x', val);
        if need2set
            self.x = val;
            self.stateFlag = ~isempty(val);
            if self.stateFlag
                self.n = size(val, 1);
            else
                self.n = [];
            end%if
        end%if
    end%function set.x

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function set.y(self, val)
        if ~isnumericmatrix(val)
            error('y must be a numeric matrix');
        end%if
        need2set = self.helpsetX('y', val);
        if need2set
            self.y = val;
            self.p = size(val, 1);
            self.Nd = size(val, 2);
            self.Tf = self.Nd - 1;
        end%if
    end%function set.y

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function set.u(self, val)
        if ~isnumericmatrix(val)
            error('u must be a numeric matrix');
        end%if
        need2set = self.helpsetX('u', val);
        if need2set
            self.u = val;
            self.inputFlag = ~isempty(val);
            if self.inputFlag
                self.m = size(self.u, 1);
            else
                self.m = [];
            end%if
        end%if
    end%function set.u

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function set.flags(self, val)
        % The flags property has public SetAccess, because attached ceestimator
        % and als objects need to be able to change it. However, the user should
        % not be able to change this property, so check to make sure it is being
        % changed from an appropriate place, and if not, throw an error.

        d = dbstack();
        dnames = {d.name};

        if isoctave()
            cmpfunc = @strcmp;
        else % Matlab
            cmpfunc = @endsWith;
        end%if

        % Check if flags is being changed from changing a baseprop of the
        % cedata object.
        tests(1) = numel(dnames) >= 2 && cmpfunc(dnames{2}, 'helpsetX');

        % Check if flags are being lowered by linked ceestimator or als object.
        tests(2) = numel(dnames) >= 2 ...
                                  && (cmpfunc(dnames{2}, 'seeifmdchanged') ...
                                      || cmpfunc(dnames{2}, 'seeifmdechanged'));

        % Check if flags is being changed by set.data --> link in ceestimator
        % or als.
        tests(3) = numel(dnames) >= 2 && cmpfunc(dnames{2}, 'link');

        if ~any(tests(:))
            error('cannot set property ''flags'' in this context');
        end%if

        self.flags = val;
    end%function set.flags
end%methods

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

end%classdef cedata

