%% Copyright (C) 2017 Travis Arnold
%%
%% This file is part of Octave.
%%
%% Octave is free software; you can redistribute it and/or modify it
%% under the terms of the GNU General Public License as published by
%% the Free Software Foundation; either version 3 of the License, or (at
%% your option) any later version.
%%
%% Octave is distributed in the hope that it will be useful, but
%% WITHOUT ANY WARRANTY; without even the implied warranty of
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
%% General Public License for more details.
%%
%% You should have received a copy of the GNU General Public License
%% along with Octave; see the file COPYING.  If not, see
%% <http://www.gnu.org/licenses/>.

%% -*- texinfo -*-
%% @deftypefn {Function File} {@var{P} =} dupmatpinv (@var{n})
%% Return the Moore-Penrose pseudoinverse
%% @tex
%%  $P_n$
%% @end tex
%% @ifnottex
%%  @math{P(n)}
%% @end ifnottex
%% of the duplication matrix
%% @tex
%%  $D_n$.
%% @end tex
%% @ifnottex
%%  @math{D(n)}.
%% @end ifnottex
%% This is a
%% @tex
%%  $n(n+1)/2 \times n^2$
%% @end tex
%% @ifnottex
%%  @math{@var{n}*(@var{n}+1)/2} by @math{@var{n}^2}
%% @end ifnottex
%% matrix that "undoes" the effect of
%% @tex
%%  $D_n$.
%% @end tex
%% @ifnottex
%%  @math{D(n)}.
%% @end ifnottex
%% That is,
%% @tex
%%  $P_n {\rm vec} \, A = {\rm vech} \, A$
%% @end tex
%% @ifnottex
%%  @math{P(n)*vec (A) = vech (A)}
%% @end ifnottex
%% for all symmetric
%% @tex
%%  $n \times n$
%% @end tex
%% @ifnottex
%%  @math{@var{n}} by @math{@var{n}}
%% @end ifnottex
%% matrices
%% @tex
%%  $A$.
%% @end tex
%% @ifnottex
%%  @math{A}.
%% @end ifnottex
%%
%% Returns a sparse matrix. Equivalent to pinv (dupmat (@var{n})),
%% but MUCH faster.  For a demonstration of this, type 'demo dupmatpinv'.
%%
%% See Magnus and Neudecker (1988), @cite{Matrix Differential Calculus with
%% Applications in Statistics and Econometrics}.
%% @seealso{duplication_matrix, vec, vech, dupmat}
%% @end deftypefn

%% Author: Travis Arnold <tjarnold@wisc.edu>, January 2017.
%% Adapted to be Matlab compatible, May 2018.

function D = dupmatpinv(n)
    if (nargin() ~= 1)
        error('dupmatpinv: exactly one input argument is required');
    end%if

    try
        [nisposint, n] = isposinteger(n);
    catch
        error('dupmatpinv: N must be a positive integer');
    end%trycatch

    if ~nisposint
        error('dupmatpinv: N must be a positive integer');
    end%if

    count = [0, arrayfun(@(k) sum(n-1:-1:k), n-1:-1:1)];
    j = 1:n;

    col1 = (j-1)*n + j;
    row1 = count + j;

    row23 = arrayfun(@(k) count(k) + (k+1:n), 1:n-1, 'UniformOutput', false);
    row23 = horzcat(row23{:});

    col2 = arrayfun(@(k) (k-1)*n + (k+1:n) , 1:n-1, 'UniformOutput', false);
    col2 = horzcat(col2{:});

    col3 = arrayfun(@(k) ((k+1:n)-1)*n + k , 1:n-1, 'UniformOutput', false);
    col3 = horzcat(col3{:});

    D = sparse(row1, col1, 1) ...
        + sparse([row23 row23], [col2 col3], 0.5, n*(n+1)/2, n^2);
end%function dupmatpinv

%!assert(full(dupmatpinv(1)), 1);
%!assert(full(dupmatpinv(2)), [1 0 0 0; 0 0.5 0.5 0; 0 0 0 1]);
%!assert(full(dupmatpinv(4)), pinv(duplication_matrix(4)), 1e-6)
%!assert(full(dupmatpinv(7)), pinv(duplication_matrix(7)), 1e-6)

%!test
%! n = 10;
%! A = randn(n);
%! A = A + A';
%! assert(dupmatpinv(n) * vec(A), vech(A));

%!test
%! n = 17;
%! A = randn(n);
%! A = A + A';
%! assert(dupmatpinv(n) * vec(A), vech(A));

%!error <exactly one input argument is required> dupmatpinv()
%!error <exactly one input argument is required> dupmatpinv(1, 2)
%!error <N must be a positive integer> dupmatpinv(0.5)
%!error <N must be a positive integer> dupmatpinv(-1)
%!error <N must be a positive integer> dupmatpinv(ones(1,4))

%!demo
%! n1 = 5000;
%! n2 = 50;
%! timer = tic();
%! dupmatpinv(n1);
%! t1 = toc(timer);
%! pinv(dupmat(n2));
%! t2 = toc(timer);
%! fprintf('dupmatpinv(%i) : %f seconds\n', n1, t1);
%! fprintf('pinv(dupmat(%i)) : %f seconds\n', n2, t2-t1);

