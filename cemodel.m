classdef cemodel < handle

% TODO add an error or warning somewhere if G or H has linearly dependent
% columns? (maybe do this in als or ceestimator instead of here)

% TODO add the individual U matrices as properties; i.e., can pull UPhiQ,
% UThetaQ, UPhiR, and UThetaS directly out of U (same for bb equivalents)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% properties %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

properties (GetAccess = public, SetAccess = private)
    props
        % Struct. Fields:
        %   base
        %        {'A', 'B', 'C', 'D', 'G', 'H', 'Kcal', 'Lcal', 'Q', 'R', ...
        %         'S', 'Phi', 'Theta', 'Phibb', 'Thetabb'}
        %   stoch
        %        {'Kcal', 'Lcal', 'Q', 'R', 'S', 'Phi', 'Theta', ...
        %         'Phibar', 'Thetabar'}
        %   dependent
        %        Cell array of strings givng the current dependent properties of
        %        the cemodel object.
        %   dependsspec
        %        Struct. Field names are the dependent properties of the cemodel
        %        object. Field values are cell arrays of strings of the
        %        corresponding base and dependent properties that are used to
        %        calculate each dependent property.
        %   depends
        %        Struct. Similar to dependsspec, but shows the dependencies in
        %        terms of base properties only.
        %   current
        %        Struct. Field names are the dependent properties of the cemodel
        %        object. Field values are true for dependent properties that are
        %        up to date and false for dependent properties that are out of
        %        date (or that have not been calculated).
        %   store
        %        Struct. Field names are the dependent properties of the cemodel
        %        object. Field values are the cached calculated values of the
        %        dependent properties.
    checks
        % Struct. Fields:
        %   names
        %        Cell array of strings with the names of the checks that are
        %        relevant to the cemodel object.
        %   depends
        %        Struct. Field names are the names of the checks that are
        %        relevant to the cemodel object. Field values are cell arrays of
        %        strings with the base properties that each check depends on.
        %   current
        %        Struct. Field names are the names of the checks that are
        %        relevant to the cemodel object. Field values are true for
        %        checks that are up to date and false for those that are out of
        %        date/have not passed.
    spec = struct('BD', '', ...
                  'GH', '', ...
                  'stoch', {{}}, ...
                  'stochtype', 'none', ...
                  'Kcal', false(), ...
                  'Lcal', false(), ...
                  'string', 'none');
        % Struct. Fields:
        %   BD
        %        String. One of '', 'B', 'D', or 'BD'.
        %   GH
        %        String. One of '', 'G', 'H', or 'GH'.
        %   stoch
        %        Cell array with possible members 'Q', 'R', 'S', 'Phi', 'Theta',
        %        'Phibb', 'Thetabb', 'Kcal', and 'Lcal'.
        %   stochtype
        %        String. One of 'none', 'QRS', 'PhiTheta', or 'PhibbThetabb'.
        %   Kcal
        %        True if user has provided Kcal and false otherwise.
        %   Lcal
        %        True if user has provided Lcal and false otherwise.
        %   string
        %        String. One of '', 'Kcal', 'Lcal', or 'LcalKcal' concatenated
        %        with spec.stochtype (one of 'none', 'QRS', 'PhiTheta', or
        %        'PhibbThetabb'). Used internally to load self.props and
        %        self.checks.
end%properties

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% base props %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

properties (GetAccess = public, SetAccess = public)
    A
    B
    C
    D
    Q
    R
    S
    G
    H
    Kcal
    Lcal
end%properties

%%%%%%%%%%%%%%% sometimes base props, sometimes dependent props %%%%%%%%%%%%%%%%

properties (GetAccess = public, SetAccess = public)
    Phi % C * P * C' + H * R * H'
    Theta % A * P * C' + G * S * H'
    Phibb
    Thetabb
end%properties

%%%%%%%%%%%%%%%%%%%%%%%%%%%% always dependent props %%%%%%%%%%%%%%%%%%%%%%%%%%%%

properties (GetAccess = public, SetAccess = private)
    % kalman filter properties:
    %
    % L : steady-state Kalman filter gain (n x p)
    % L = Sigkj*C' * pinv(C*Sigkj*C' + H*R*H')
    %
    % K : steady-state Kalman predictor gain (n x p)
    % K = (A*Sigkj*C' + G*S*H') * pinv(C*Sigkj*C' + H*R*H')
    %
    % Sigkj : steady-state Kalman predictor error (n x n)
    % Sigkj = cov(x[k|k-1] - x[k])
    % Sigkj = A*Sigkj*A' - (A*Sigkj*C' + G*S*H')
    %         * pinv(C*Sigkj*C' + H*R*H') * (A*Sigkj*C' + G*S*H')' + G*Q*G'
    %
    % Sigkk : steady-state Kalman filter error (n x n)
    % Sigkk = cov(x[k|k] - x[k])
    % Sigkk = Sigkj - L*C*Sigkj
    %
    % T = E[(x[k|k-1] - E[x[k]]) * (x[k|k-1] - E[x[k]])']
    %   = P - Sigkj
    %
    % Abar : innovations form state transition matrix
    % Abar = A - K*C
    %
    % E : steady-state closed loop poles (n x 1)
    % E = eig(Abar)
    %
    % Abarstable : true if all eigenvalues of Abar are inside the unit circle
    n
    p
    m
    g
    h
    Aeigs
    Astable % True if A is stable and false otherwise.
    GQG % G * Q * G'
    HRH % H * R * H'
    GSH % G * S * H'
    P
    L
    K
    M
    Sigkj
    Sigkk
    E
    T
    U
    Utl
    Abar
    Abareigs
    Abarstable % True if Abar is stable and false otherwise.
    Gbar
    Qbar
    Sbar
    GQGbar % Gbar * Qbar * Gbar'
    GSHbar % Gbar * Sbar * H'
    Pbar
    Lbar
    Kbar
    Mbar
    Sigbarkj
    Sigbarkk
    Ebar
    Tbar
    Phibar
    Thetabar
    Abb
    Abbeigs
    Abbstable
    Gbb
    GQGbb % Gbb * Qbar * Gbb'
    GSHbb % Gbb * Sbar * H'
    Pbb
    Lbb
    Kbb
    Mbb
    Sigbbkj
    Sigbbkk
    Ebb
    Tbb
    Ubb
    Ubbtl
end%properties

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

properties (Hidden, GetAccess = public, SetAccess = private)
    id
end%properties

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

properties (Hidden, GetAccess = public, SetAccess = public)
    flags = struct('ceestimator', struct(), ...
                   'als', struct());
end%properties

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% methods %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

methods (Access = public)
    function self = cemodel(varargin)
        % Constructor.

        % Generate uuid.
        self.id = uuidgen();

        % Set initial values for props and checks.
        self.load_constants();

        % Get kwargs struct.
        kwargs = args2kwargs(varargin{:});

        % Make sure kwargs are valid; throw an error if not.
        checkkwargs(kwargs, self.props.base);

        % Set fields of self.
        for X = fieldnames(kwargs)'
            self.(X{:}) = kwargs.(X{:});
        end%for
    end%function cemodel

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function bool = eq(self, b)
        % Determine if two cemodel instances refer to the same object by
        % comparing their id property. This functionality is built into Matlab,
        % but not Octave.
        if nargin() ~= 2
            error('exactly two input arguments required');
        end%if
        if ~isa(b, 'cemodel')
            error('second argument is not a cemodel object');
        end%if
        bool = strcmp(self.id, b.id);
    end%function eq

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function bool = ne(self, b)
        % Determine if two cemodel instances do not refer to the same object by
        % comparing their id property. This functionality is built into Matlab,
        % but not Octave.
        if nargin() ~= 2
            error('exactly two input arguments required');
        end%if
        if ~isa(b, 'cemodel')
            error('second argument is not a cemodel object');
        end%if
        bool = ~(self == b);
    end%function ne

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function bool = isempty(self)
        % Check if everything in cemodel object is empty.
        if nargin() ~= 1
            error('exactly one input argument required');
        end%if
        bool = all(cellfun('isempty', cellfun(@(X) self.(X), ...
              {'A', 'B', 'C', 'D', 'G', 'H'}, 'UniformOutput', false()))) && ...
              strcmp(self.spec.string, 'none');
    end%function isempty

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function check(self, varargin)
        % Perform checks on cemodel object.

        % Make sure input arguments are character vectors.
        if ~all(cellfun(@ischarrow, varargin))
            error('all arguments (other than self) must be character vectors');
        end%if

        % Check for unrecoginized check names.
        unrec = setdiff(varargin, [self.checks.names]);
        if ~isempty(unrec)
            error(['unrecognized input argument(s): ', ...
                   strjoin(unrec, ', '), ' (acceptable arguments are ''', ...
                   strjoin(self.checks.names, ''', '''), ''')']);
        end%if

        % If no additional arguments are passed, perform all checks.
        checkall = isempty(varargin);

        % Function that tells if each check needs to be performed.
        checkX = @(X) ismember(X, self.checks.names) ...
                      && (~self.checks.current.(X) ...
                      && (checkall || ismember(X, varargin)));

        msg = {}; % initialize cell to hold error messages

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% empty %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        if checkX('empty')
            % Check if everything in cemodel object is empty.
            bool = self.isempty();
            if bool
                msg = [msg, 'cemodel object is empty'];
            end%if
            self.checks.current.empty = ~bool;
        end%if checkX('empty')

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% ABsize %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        if checkX('ABsize')
            % If B is in the model, compare sizes of A and B.
            if ismember('B', self.spec.BD)
                bool = size(self.A, 1) == size(self.B, 1);
            else
                bool = true();
            end%if
            if ~bool
                msg = [msg, 'A and B have inconsistent dimensions'];
            end%if
            self.checks.current.ABsize = bool;
        end%if checkX('ABsize')

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% ACsize %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        if checkX('ACsize')
            % Compare sizes of A and C.
            bool = size(self.A, 2) == size(self.C, 2);
            if ~bool
                msg = [msg, 'A and C have inconsistent dimensions'];
            end%if
            self.checks.current.ACsize = bool;
        end%if checkX('ACsize')

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% CDsize %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        if checkX('CDsize')
            % If D is in the model, compare sizes of C and D.
            if ismember('D', self.spec.BD)
                bool = size(self.C, 1) == size(self.D, 1);
            else
                bool = true();
            end%if
            if ~bool
                msg = [msg, 'C and D have inconsistent dimensions'];
            end%if
            self.checks.current.CDsize = bool;
        end%if checkX('CDsize')

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% BDsize %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        if checkX('BDsize')
            % If both B and D are in the model, compare sizes of B and D.
            if strcmp(self.spec.BD, 'BD')
                bool = size(self.B, 2) == size(self.D, 2);
            else
                bool = true();
            end%if
            if ~bool
                msg = [msg, 'B and D have inconsistent dimensions'];
            end%if
            self.checks.current.BDsize = bool;
        end%if checkX('BDsize')

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Kcalsize %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        if checkX('Kcalsize')
            % If Kcal is in the model, compare its size with A and C.
            bool = true();
            if size(self.Kcal, 1) ~= size(self.A, 1)
                bool = false();
                msg = [msg, 'Kcal and A have inconsistent dimensions'];
            end%if
            if size(self.Kcal, 2) ~= size(self.C, 1)
                bool = false();
                msg = [msg, 'Kcal and C have inconsistent dimensions'];
            end%if
            self.checks.current.Kcalsize = bool;
        end%if checkX('Kcalsize')

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Lcalsize %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        if checkX('Lcalsize')
            % If Lcal is in the model, compare its size with A and C.
            bool = true();
            if size(self.Lcal, 1) ~= size(self.A, 1)
                bool = false();
                msg = [msg, 'Lcal and A have inconsistent dimensions'];
            end%if
            if size(self.Lcal, 2) ~= size(self.C, 1)
                bool = false();
                msg = [msg, 'Lcal and C have inconsistent dimensions'];
            end%if
            self.checks.current.Lcalsize = bool;
        end%if checkX('Lcalsize')

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% QRSspec %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        if checkX('QRSspec')
            % Check that both Q and R are given.
            bool = ismember('Q', self.spec.stoch) ...
                   && ismember('R', self.spec.stoch);
            if ~bool
                msg = [msg, 'Q and R must both be given'];
            end%if
            self.checks.current.QRSspec = bool;
        end%if checkX('QRSspec')

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% GQsize %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        if checkX('GQsize')
            % Check sizes of G and Q.
            bool = true();
            self.check('QRSspec'); % check that Q and R are given
            if ismember('G', self.spec.GH);
                if size(self.G, 2) ~= size(self.Q, 1);
                    bool = false();
                    msg = [msg, 'G and Q have inconsistent dimensions'];
                end%if
                if size(self.G, 1) ~= size(self.A, 1)
                    bool = false();
                    msg = [msg, 'G and A have inconsistent dimensions'];
                end%if
            else
                if size(self.A, 1) ~= size(self.Q, 1);
                    bool = false();
                    msg = [msg, 'A and Q have inconsistent dimensions'];
                end%if
            end%if
            self.checks.current.GQsize = bool;
        end%if checkX('GQsize')

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% HRsize %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        if checkX('HRsize')
            % Check sizes of H and R.
            bool = true();
            self.check('QRSspec'); % check that Q and R are given
            if ismember('H', self.spec.GH)
                if size(self.H, 2) ~= size(self.R, 1);
                    bool = false();
                    msg = [msg, 'H and R have inconsistent dimensions'];
                end%if
                if size(self.H, 1) ~= size(self.C, 1)
                    bool = false();
                    msg = [msg, 'H and C have inconsistent dimensions'];
                end%if
            else
                if size(self.C, 1) ~= size(self.R, 1);
                    bool = false();
                    msg = [msg, 'C and R have inconsistent dimensions'];
                end%if
            end%if
            self.checks.current.HRsize = bool;
        end%if checkX('HRsize')

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Ssize %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        if checkX('Ssize')
            % If S is in the model, compare its size with G, H, Q, and R.
            bool = true();
            self.check('QRSspec'); % check that Q and R are given
            if ismember('S', self.spec.stoch)
                if ismember('G', self.spec.GH) ...
                   && size(self.S, 1) ~= size(self.G, 2)
                    bool = false();
                    msg = [msg, 'S and G have inconsistent dimensions'];
                end%if
                if ismember('H', self.spec.GH) ...
                   && size(self.S, 2) ~= size(self.H, 2)
                    bool = false();
                    msg = [msg, 'S and H have inconsistent dimensions'];
                end%if
                if size(self.S, 1) ~= size(self.Q, 1)
                    bool = false();
                    msg = [msg, 'Q and S have inconsistent dimensions'];
                end%if
                if size(self.S, 2) ~= size(self.R, 1)
                    bool = false();
                    msg = [msg, 'R and S have inconsistent dimensions'];
                end%if
            end%if
            self.checks.current.Ssize = bool;
        end%if checkX('Ssize')

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%% QRSdefinite %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        if checkX('QRSdefinite')
            % Check positive semidefiniteness of whatever combimation of Q, R,
            % and/or [Q S; S' R] is relevant.
            bool = true();
            if ismember('S', self.spec.stoch)
                % check that Q and R are given, and dimensions of Q, R, S
                self.check('QRSspec', 'Ssize');
                if myisdefinite([self.Q, self.S; self.S', self.R]) == -1
                    bool = false();
                    msg = [msg, '[Q S; S'' R] is not positive semidefinite'];
                end%if
            end%if
            self.checks.current.QRSdefinite = bool;
        end%if checkX('QRSdefinite')

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%% PhiThetaspec %%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        if checkX('PhiThetaspec')
            % Check that both Phi and Theta are provided. If they are, also
            % check that A is stable.
            bool = true();
            if ismember('Phi', self.spec.stoch) ...
               && ismember('Theta', self.spec.stoch)
                self.props.store.Aeigs = eig(self.A);
                self.props.current.Aeigs = true();
                self.props.store.Astable = ...
                                         (max(abs(self.props.store.Aeigs)) < 1);
                self.props.current.Astable = true();
                if ~self.props.store.Astable
                    bool = false();
                    msg = [msg, ['A is unstable; cannot specify Phi and ', ...
                                 'Theta (because steady state Phi and ', ...
                                 'Theta do not exist when A is unstable)']];
                end%if
            else
                bool = false();
                msg = [msg, ['invalid specification of Phi, and Theta---', ...
                             'they both must be given']];
            end%if
            self.checks.current.PhiThetaspec = bool;
        end%if checkX('PhiThetaspec')

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Phisize %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        if checkX('Phisize')
            % If Phi is in the model, compare its size with C and H.
            bool = true();
            self.check('PhiThetaspec'); % check that Phi and Theta are given
            if size(self.Phi, 1) ~= size(self.C, 1)
                bool = false();
                msg = [msg, 'Phi and C have inconsistent dimensions'];
            end%if
            if ismember('H', self.spec.GH)
                if size(self.Phi, 1) ~= size(self.H, 1)
                    bool = false();
                    msg = [msg, 'Phi and H have inconsistent dimensions'];
                end%if
            end%if
            self.checks.current.Phisize = bool;
        end%if checkX('Phisize')

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Thetasize %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        if checkX('Thetasize')
            % If Theta is in the model, compare its size with A, C, G, and H.
            bool = true();
            self.check('PhiThetaspec'); % check that Phi and Theta are given
            if size(self.Theta, 1) ~= size(self.A, 1)
                bool = false();
                msg = [msg, 'Theta and A have inconsistent dimensions'];
            end%if
            if size(self.Theta, 2) ~= size(self.C, 1)
                bool = false();
                msg = [msg, 'Theta and C have inconsistent dimensions'];
            end%if
            if ismember('G', self.spec.GH)
                if size(self.Theta, 1) ~= size(self.G, 1)
                    bool = false();
                    msg = [msg, 'Theta and G have inconsistent dimensions'];
                end%if
            end%if
            if ismember('H', self.spec.GH)
                if size(self.Theta, 2) ~= size(self.H, 1)
                    bool = false();
                    msg = [msg, 'Theta and H have inconsistent dimensions'];
                end%if
            end%if
            self.checks.current.Thetasize = bool;
        end%if checkX('Thetasize')

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%% PhiThetasize %%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        if checkX('PhiThetasize')
            % If Phi and Theta are both in the model, compare their sizes.
            bool = true();
            self.check('PhiThetaspec'); % check that Phi and Theta are given
            if size(self.Theta, 2) ~= size(self.Phi, 1)
                bool = false();
                msg = [msg, 'Phi and Theta have inconsistent dimensions'];
            end%if
            self.checks.current.PhiThetasize = bool;
        end%if checkX('PhiThetasize')

        %%%%%%%%%%%%%%%%%%%%%%%%%%% PhiThetadefinite %%%%%%%%%%%%%%%%%%%%%%%%%%%

        if checkX('PhiThetadefinite')
            % Check that Phi and Theta satisfy the semidefinite requirements.
            % TODO add a test instead of passing automatically. Make sure test
            % accounts for G and H if they are given.
            % TODO when I add a test, make sure that the dependencies for this
            % test are specified correctly in generate_constants fuction below.
            % Also, add a unittest for it.
            self.check('PhiThetaspec'); % check that Phi and Theta are given
            bool = true();
            % add checks on Phi, Theta size if necessary
            % Make sure G and H are accounted for if they are given
            self.checks.current.PhiThetadefinite = bool;
        end%if checkX('PhiThetadefinite')

        %%%%%%%%%%%%%%%%%%%%%%%%%%% PhibbThetabbspec %%%%%%%%%%%%%%%%%%%%%%%%%%%

        if checkX('PhibbThetabbspec')
            % Check that both Phibb and Thetabb are provided and that the system
            % is detectable. If Kcal is also provided, check that
            % Abb = A - Kcal * C is stable.
            self.check('ACsize');
            bool = true();
            if ~ismember('Phibb', self.spec.stoch) ...
               || ~ismember('Thetabb', self.spec.stoch)
                bool = false();
                msg = [msg, ['invalid specification of Phibb and ', ...
                             'Thetabb---they both must be given']];
            end%if
            [~, ~, isdetec] = myisobsv(self.A, self.C);
            if ~isdetec
                bool = false();
                msg = [msg, ['(A, C) is not detectable; cannot specify ', ...
                             'Phibb and Thetabb in this case because ', ...
                             'Abb = A - Kcal * C is unstable for all ', ...
                             'Kcal, and steady state Phibb and Thetabb do ', ...
                             'not exist when Abb is unstable']];
            elseif self.spec.Kcal
                self.check('Kcalsize');
                self.props.store.Abbeigs = eig(self.A - self.Kcal * self.C);
                self.props.current.Abbeigs = true();
                self.props.store.Abbstable = ...
                                       (max(abs(self.props.store.Abbeigs)) < 1);
                self.props.current.Abbstable = true();
                if ~self.props.store.Abbstable
                    bool = false();
                    msg = [msg, ['Abb = A - Kcal*C is unstable; cannot ', ...
                                 'specify Phibb and Thetabb in this case ', ...
                                 'because steady state Phibb and Thetabb ', ...
                                 'do not exist when Abb is unstable']];
                end%if
            end%if
            self.checks.current.PhibbThetabbspec = bool;
        end%if checkX('PhibbThetabbspec')

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Phibbsize %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        if checkX('Phibbsize')
            % If Phibb is in the model, compare its size with C and H.
            bool = true();
            self.check('PhibbThetabbspec'); % check that Kcal, Phibb, and
                                            % Thetabb are given
            if size(self.Phibb, 1) ~= size(self.C, 1)
                bool = false();
                msg = [msg, 'Phibb and C have inconsistent dimensions'];
            end%if
            if ismember('H', self.spec.GH)
                if size(self.Phibb, 1) ~= size(self.H, 1)
                    bool = false();
                    msg = [msg, 'Phibb and H have inconsistent dimensions'];
                end%if
            end%if
            self.checks.current.Phibbsize = bool;
        end%if checkX('Phibbsize')

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%% Thetabbsize %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        if checkX('Thetabbsize')
            % If Thetabb is in the model, compare its size with A, C, G, and H.
            bool = true();
            self.check('PhibbThetabbspec'); % check that Kcal, Phibb, and 
                                            % Thetabb are given
            if size(self.Thetabb, 1) ~= size(self.A, 1)
                bool = false();
                msg = [msg, 'Thetabb and A have inconsistent dimensions'];
            end%if
            if size(self.Thetabb, 2) ~= size(self.C, 1)
                bool = false();
                msg = [msg, 'Thetabb and C have inconsistent dimensions'];
            end%if
            if ismember('G', self.spec.GH)
                if size(self.Thetabb, 1) ~= size(self.G, 1)
                    bool = false();
                    msg = [msg, 'Thetabb and G have inconsistent dimensions'];
                end%if
            end%if
            if ismember('H', self.spec.GH)
                if size(self.Thetabb, 2) ~= size(self.H, 1)
                    bool = false();
                    msg = [msg, 'Thetabb and H have inconsistent dimensions'];
                end%if
            end%if
            self.checks.current.Thetabbsize = bool;
        end%if checkX('Thetabbsize')

        %%%%%%%%%%%%%%%%%%%%%%%%%%% PhibbThetabbsize %%%%%%%%%%%%%%%%%%%%%%%%%%%

        if checkX('PhibbThetabbsize')
            % If Phibb and Thetabb are both in the model, compare their sizes.
            bool = true();
            self.check('PhibbThetabbspec'); % check that Kcal, Phibb, and
                                              % Thetabb are given
            if size(self.Thetabb, 2) ~= size(self.Phibb, 1)
                bool = false();
                msg = [msg, 'Phibb and Thetabb have inconsistent dimensions'];
            end%if
            self.checks.current.PhibbThetabbsize = bool;
        end%if checkX('PhibbThetabbsize')

        %%%%%%%%%%%%%%%%%%%%%%%%% PhibbThetabbdefinite %%%%%%%%%%%%%%%%%%%%%%%%%

        if checkX('PhibbThetabbdefinite')
            % Check that Kcal, Phibb and Thetabb satisfy the semidefinite
            % requirements.
            % TODO add a test instead of passing automatically. Make sure test
            % accounts for G and H if they are given.
            % TODO when I add a test, make sure that the dependencies for this
            % test are specified correctly in generate_constants fuction below.
            % Also, add a unittest for it.
            self.check('PhibbThetabbspec'); % check that Kcal, Phibb, and
                                              % Thetabb are given
            bool = true();
            % add checks on Kcal, Phibb, Thetabb size if necessary
            % Make sure G and H are accounted for if they are given
            self.checks.current.PhibbThetabbdefinite = bool;
        end%if checkX('PhibbThetabbdefinite')

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        % If any checks did not pass, throw an error.
        if ~isempty(msg)
            msg = strjoin(msg, char(10));
            error(msg);
        end%if
    end%function check

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function [bool, msg] = compatible(self, data)
        % Check if cemodel object is compatible with given cedata object.

        % Check number of input arguments.
        if nargin() ~= 2
            error('exactly two input arguments required');
        end%if

        % Make sure data is a cedata object.
        if ~isa(data, 'cedata')
            error('data must be a cedata object');
        end%if

        % Check the model before checking compatibility.
        self.check();

        % Container for messages.
        msg = {};

        % Check if number of outputs match. 
        if self.p ~= data.p
            msg = [msg, ['data and model have different numbers of ', ...
                         'outputs (mismatched p''s)']];
        end%if

        % Commpare input properties in data and model.
        a = data.inputFlag;
        b = ~strcmp(self.spec.BD, '');

        if a && b % Data has inputs, model accepts inputs.
            % Compare m's.
            if self.m ~= data.m
                msg = [msg, ['data and model have different numbers of ', ...
                             'inputs (mismatched m''s)']];
            end%if
            % Compare numpber of inputs and outputs.
            if data.Nd ~= size(data.u, 2)
                msg = [msg, ['inconsistent number of data points: there ', ...
                             'must be the same number of inputs and outputs']];
            end%if
        end%if
        if a && ~b % Data has inputs, model doesn't accept them. Incompatible.
            msg = [msg, 'data has inputs, but model does not accept inputs'];
        end%if
        if ~a && b % Data doesn't have inputs, model accepts them. Incompatible.
            msg = [msg, 'data does not have inputs, but model accepts inputs'];
        end%if

        % Commpare state properties in data and model.
        if data.stateFlag % Data has states.
            % Compare n's.
            if self.n ~= data.n
                msg = [msg, ['data and model have different numbers of ', ...
                             'states (mismatched n''s)']];
            end%if
            % Compare number of states and outputs.
            if data.Nd ~= size(data.x, 2)
                msg = [msg, ['inconsistent number of data points: there ', ...
                             'must be the same number of states and outputs']];
            end%if
        end%if

        bool = isempty(msg);

        % Combine messages into string.
        if ~bool
            msg = strjoin(msg, '; ');
        end%if
    end%function compatible

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function [data, Gw, Hv] = simulate(self, varargin)
        % Simulate model. Acceptable additional arguments are u, Nd, and x1.

        if strcmp(self.spec.stochtype, 'PhiTheta')
            error('cannot simulate model when stochtype = ''PhiTheta''');
        end%if
        if strcmp(self.spec.stochtype, 'PhibbThetabb')
            error('cannot simulate model when stochtype = ''PhibbThetabb''');
        end%if

        % Get kwargs struct.
        kwargs = args2kwargs(varargin{:});

        % Check if B and D exist.
        Bexist = ismember('B', self.spec.BD);
        Dexist = ismember('D', self.spec.BD);

        %%%%%%%%%%%%%%%%%%%%%%%%%%%% Error checking %%%%%%%%%%%%%%%%%%%%%%%%%%%%

        % Make sure kwargs are valid; throw an error if not.
        checkkwargs(kwargs, {'u', 'Nd', 'x1'});

        ugiven = isfield(kwargs, 'u');
        Ndgiven = isfield(kwargs, 'Nd');
        x1given = isfield(kwargs, 'x1');

        % Check that model is valid; throw an error if it is not.
        self.check();

        % If system takes inputs and none are given, use zeros, but throw a
        % warning.
        if ~ugiven && (Bexist || Dexist)
            warning(['model has inputs, but no inputs are given; ', ...
                     'simulating with zero vectors for all inputs']);
        end%if

        % If inputs are given but model does not have inputs, throw an error.
        if ugiven && ~(Bexist || Dexist)
            error('inputs are given, but model does not accept inputs');
        end%if

        % Error checking on Nd.
        if Ndgiven
            [Ndisposint, Ndround] = isposinteger(kwargs.Nd);
            if ~Ndisposint
                error('Nd must be a positive integer');
            end%if
        end%if

        % Error checking on u, and set the Nd that will be used for the
        % simulation.
        if ugiven
            if ~isnumericmatrix(kwargs.u)
                error('u must be a numeric matrix');
            end%if
            if size(kwargs.u, 1) ~= self.m
                error(['size of given u is inconsistent with model ', ...
                       ' (mismatched m''s)']);
            end%if

            Nd = size(kwargs.u, 2);

            % If user has given Nd and u and they have inconsistent sizes, give
            % a warning.
            if Ndgiven && Nd ~= Ndround
                warning(['given Nd is inconsistent with size of given u; ', ...
                         'ignoring given Nd and simulating over all given ', ...
                         'u''s']);
            end%if
        % If there are no inputs and no Nd provided, throw an error.
        else
            if ~Ndgiven
                error('Nd must be provided when no inputs are provided');
            end%if
            Nd = Ndround;
        end%if

        % Error checking on x1.
        if x1given
            if ~isnumeric(kwargs.x1)
                error('x1 must be a numeric column vector');
            end%if
            if ~isempty(kwargs.x1)
                if ~iscolumn(kwargs.x1)
                    error('x1 must be a numeric column vector');
                end%if
            end%if
            if size(kwargs.x1, 1) ~= self.n
                error(['size of given x1 is inconsistent with model ', ...
                       '(mismatched n''s)']);
            end%if

            % Preallocate.
            x = [kwargs.x1, NaN(self.n, Nd-1)];
        else
            % Preallocate.
            x = [zeros(self.n, 1), NaN(self.n, Nd-1)];
        end%if

        %%%%%%%%%%%%%%%%%%%%%%%%%%% Generate noises %%%%%%%%%%%%%%%%%%%%%%%%%%%%

        wexist = ismember('Q', self.spec.stoch);
        vexist = ismember('R', self.spec.stoch);

        % Load statistics package in Octave if necessary.
        if wexist || vexist
            if isoctave()
                pkg('load', 'statistics');
            end%if
        end%if

        % Generate noises. Note that if S is given, Q and R are also given (if
        % not, it would have been caught by the above call to self.check()).
        if ismember('S', self.spec.stoch)
            GwHv = mvnrnd(zeros(1, self.n + self.p), ...
                          [self.GQG , self.GSH; ...
                           self.GSH', self.HRH], Nd)';
            Gw = GwHv(1:self.n, :);
            Hv = GwHv(self.n+1:end, :);
        else
            if wexist
                Gw = mvnrnd(zeros(1, self.n), self.GQG, Nd)';
            end%if
            if vexist
                Hv = mvnrnd(zeros(1, self.p), self.HRH, Nd)';
            end%if
        end%if

        %%%%%%%%%%%%%%%%%%%%%% Do simulation calculations %%%%%%%%%%%%%%%%%%%%%%

        % Advance state.
        doBu = Bexist && ugiven;
        for i = 2:Nd
            x(:, i) = self.A * x(:, i-1);
            if doBu
                x(:, i) = x(:, i) + self.B * kwargs.u(:, i-1);
            end%if
            if wexist
                x(:, i) = x(:, i) + Gw(:, i-1);
            end%if
        end%for

        % Calculate outputs.
        y = self.C * x;
        if Dexist && ugiven
            y = y + self.D * kwargs.u;
        end%if
        if vexist
            y = y + Hv;
        end%if

        %%%%%%%%%%%%%%%%%%%% Return data as a cedata object %%%%%%%%%%%%%%%%%%%%

        if ugiven
            data = cedata('y', y, 'x', x, 'u', kwargs.u);
        else
            data = cedata('y', y, 'x', x);
        end%if

        %%%%%%%%%%%%%%%%%%% If no noises, return zero arrays %%%%%%%%%%%%%%%%%%%

        if ~wexist
            Gw = zeros(self.n, Nd);
        end%if

        if ~vexist
            Hv = zeros(self.p, Nd);
        end%if
    end%function simulate

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function [Gw, Hv] = calcGwHv(self, data)
        % If data object has states, use this to calculate the (shaped) noises,
        % G*w and H*v.

        % Check number of input arguments.
        if nargin() ~= 2
            error('exactly two input arguments required');
        end%if

        % Make sure data is a cedata object.
        if ~isa(data, 'cedata')
            error('data must be a cedata object');
        end%if

        % Check that data has states.
        if ~data.stateFlag
            error('cedata object does not have states');
        end%if

        % Check that model and data are compatible.
        [bool, msg] = self.compatible(data);
        if ~bool
            error(['model and data are not compatible: ', msg]);
        end%if

        % Store number of data points.
        Nd = data.Nd;

        % Calculate Gw.
        Gw = data.x(:, 2:Nd) - self.A * data.x(:, 1:Nd-1);
        if ismember('B', self.spec.BD)
            Gw = Gw - self.B * data.u(:, 1:Nd-1);
        end%if

        % Calculate Hv.
        Hv = data.y - self.C * data.x;
        if ismember('D', self.spec.BD)
            Hv = Hv - self.D * data.u;
        end%if
    end%function calcGwHv

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function disp(self)
        if isoctave()
            disp('<object cemodel>');
        else % Matlab
            disp(['<object cemodel>', char(10)]);
        end%if
    end%function
end%methods (Access = public)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

methods (Access = private)
    function calculate(self, X)
        % Calculate dependent props. Updates values in self.props.store.

        % Check that model is valid; throw an error if it is not.
        self.check();

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Phi %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        if strcmp(X, 'Phi')
            if ~self.Astable
                warning(['A is unstable; therefore steady state Phi does ', ...
                         'not exist']);
                self.props.store.Phi = [];
            else
                switch self.spec.stochtype
                    case 'QRS'
                        self.props.store.Phi = forceherm(self.C * self.P ...
                                                          * self.C' + self.HRH);
                    case 'PhibbThetabb'
                        self.props.store.Phi = forceherm(self.Phibb ...
                                      + self.C * (self.T - self.Tbb) * self.C');
                end%switch
            end%if
            self.props.current.Phi = true();
            return;
        end%if strcmp(X, 'Phi')

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Theta %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        if strcmp(X, 'Theta')
            if ~self.Astable
                warning(['A is unstable; therefore steady state Theta ', ...
                         'does not exist']);
                self.props.store.Theta = [];
            else
                switch self.spec.stochtype
                    case 'QRS'
                        self.props.store.Theta = self.A * self.P * self.C' ...
                                                 + self.GSH;
                    case 'PhibbThetabb'
                        self.props.store.Theta = self.Thetabb ...
                                   + self.A * (self.T - self.Tbb) * self.C' ...
                                   + self.Kcal * self.Phibb;
                end%switch
            end%if
            self.props.current.Theta = true();
            return;
        end%if strcmp(X, 'Theta')

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Phibb %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        if strcmp(X, 'Phibb')
            if ~self.Abbstable
                warning(['Abb is unstable; therefore steady state Phibb ', ...
                         'does not exist']);
                self.props.store.Phibb = [];
            else
                switch self.spec.stochtype
                    case 'QRS'
                        self.props.store.Phibb = ...
                             forceherm(self.C * self.Pbb * self.C' + self.HRH);
                    case 'PhiTheta'
                        self.props.store.Phibb = forceherm(self.Phi ...
                                      + self.C * (self.Tbb - self.T) * self.C');
                end%switch
            end%if
            self.props.current.Phibb = true();
            return;
        end%if strcmp(X, 'Phibb')

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Thetabb %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        if strcmp(X, 'Thetabb')
            if ~self.Abbstable
                warning(['Abb is unstable; therefore steady state ', ...
                         'Thetabb does not exist']);
                self.props.store.Thetabb = [];
            else
                switch self.spec.stochtype
                    case 'QRS'
                        self.props.store.Thetabb = self.Abb * self.Pbb ...
                                                    * self.C' + self.GSHbb;
                    case 'PhiTheta'
                        self.props.store.Thetabb = self.Theta + self.Abb ...
                         * (self.Tbb - self.T) * self.C' - self.Kcal * self.Phi;
                end%switch
            end%if
            self.props.current.Thetabb = true();
            return;
        end%if strcmp(X, 'Thetabb')

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% n %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        if strcmp(X, 'n')
            self.props.store.n = size(self.A, 1);
            self.props.current.n = true();
            return;
        end%if strcmp(X, 'n')

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% p %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        if strcmp(X, 'p')
            self.props.store.p = size(self.C, 1);
            self.props.current.p = true();
            return;
        end%if strcmp(X, 'p')

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% m %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        if strcmp(X, 'm')
            switch self.spec.BD
                case {'B', 'BD'}
                    self.props.store.m = size(self.B, 2);
                case 'D'
                    self.props.store.m = size(self.D, 2);
                case ''
                    self.props.store.m = [];
            end%switch
            self.props.current.m = true();
            return;
        end%if strcmp(X, 'm')

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% g %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        if strcmp(X, 'g')
            if ismember('G', self.spec.GH)
                self.props.store.g = size(self.G, 2);
            elseif ismember('Q', self.spec.stoch)
                self.props.store.g = self.n;
            else
                self.props.store.g = [];
            end%if
            self.props.current.g = true();
            return;
        end%if strcmp(X, 'g')

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% h %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        if strcmp(X, 'h')
            if ismember('H', self.spec.GH)
                self.props.store.h = size(self.H, 2);
            elseif ismember('R', self.spec.stoch)
                self.props.store.h = self.p;
            else
                self.props.store.h = [];
            end%if
            self.props.current.h = true();
            return;
        end%if strcmp(X, 'h')

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Aeigs %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        if strcmp(X, 'Aeigs')
            self.props.store.Aeigs = eig(self.A);
            self.props.current.Aeigs = true();
            return;
        end%if strcmp(X, 'Aeigs')

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Astable %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        if strcmp(X, 'Astable')
            self.props.store.Astable = (max(abs(self.Aeigs)) < 1);
            self.props.current.Astable = true();
            return;
        end%if strcmp(X, 'Astable')

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% GQG %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        if strcmp(X, 'GQG')
            self.props.store.GQG = self.Q;
            if ismember('G', self.spec.GH)
                self.props.store.GQG = forceherm(self.G ...
                                              * self.props.store.GQG * self.G');
            end%if
            self.props.current.GQG = true();
            return;
        end%if strcmp(X, 'GQG')

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% HRH %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        if strcmp(X, 'HRH')
            self.props.store.HRH = self.R;
            if ismember('H', self.spec.GH)
                self.props.store.HRH = forceherm(self.H * ...
                                                self.props.store.HRH * self.H');
            end%if
            self.props.current.HRH = true();
            return;
        end%if strcmp(X, 'HRH')

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% GSH %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        if strcmp(X, 'GSH')
            if ismember('S', self.spec.stoch)
                self.props.store.GSH = self.S;
                if ismember('G', self.spec.GH)
                    self.props.store.GSH = self.G * self.props.store.GSH;
                end%if
                if ismember('H', self.spec.GH)
                    self.props.store.GSH = self.props.store.GSH * self.H';
                end%if
            else
                self.props.store.GSH = zeros(self.n, self.p);
            end%if
            self.props.current.GSH = true();
            return;
        end%if strcmp(X, 'GSH')

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% P %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        if strcmp(X, 'P')
            if ~self.Astable
                warning(['A is unstable; therefore steady state P does ', ...
                         'not exist']);
                self.props.store.P = [];
            else % If A is stable, solve discrete Lyapunov equation for P.
                self.props.store.P = forceherm(dlyap(self.A, self.GQG));
            end%if
            self.props.current.P = true();
            return;
        end%if strcmp(X, 'P')

        %%%%%%%%%%%%%%%%%%%%%%% L, K, Sigkj, Sigkk, E, T %%%%%%%%%%%%%%%%%%%%%%%

        if strcmp(X, 'L') ...
           || (strcmp(X, 'K') ...
           || (strcmp(X, 'Sigkj') ...
           || (strcmp(X, 'Sigkk') ...
           || (strcmp(X, 'E') ...
           || strcmp(X, 'T')))))
            switch self.spec.stochtype
                case 'QRS'
                    [self.props.store.L, ...
                     self.props.store.K, ...
                     self.props.store.Sigkj, ...
                     self.props.store.Sigkk, ...
                     self.props.store.E, ...
                     self.props.store.M] ...
                     = dlqe_iter(self.A, self.C, self.GQG, self.HRH, ...
                                 'S', self.GSH);
                    if ~self.Astable
                        warning(['A is unstable; therefore steady state T ', ...
                                 'does not exist']);
                        self.props.store.T = [];
                    else
                        self.props.store.T = self.P - self.props.store.Sigkj;
                    end%if
                    self.props.current.L = true();
                    self.props.current.K = true();
                    self.props.current.Sigkj = true();
                    self.props.current.Sigkk = true();
                    self.props.current.E = true();
                    self.props.current.M = true();
                    self.props.current.T = true();

                case 'PhiTheta'
                    % A must be stable (otherwise it would have been caught by
                    % checkX('PhiThetaspec')).
                    [self.props.store.K, ...
                     self.props.store.T, ...
                     self.props.store.E] ...
                     = PhiTheta2K_iter(self.A, self.C, self.Phi, self.Theta);
                    self.props.current.K = true();
                    self.props.current.T = true();
                    self.props.current.E = true();

                case 'PhibbThetabb'
                    % Abb must be stable. If not, it would have been caught by
                    % checkX('PhibbThetabbspec').
                    tm = forceherm(self.Phibb - self.C * self.Tbb * self.C');
                    self.props.store.K = self.Kbb + self.Kcal * tm * rpinv(tm);
                    if ~self.Astable
                        warning(['A is unstable; therefore steady state T ', ...
                                 'does not exist']);
                        self.props.store.T = [];
                    else
                        self.props.store.T = forceherm(dlyap(self.A, ...
                            forceherm(-self.props.store.K * (self.A ...
                             * self.Tbb * self.C' - self.Thetabb - self.Kcal ...
                             * self.Phibb)')));
                    end%if
                    self.props.store.E = eig(self.A ...
                                                 - self.props.store.K * self.C);
                    self.props.current.K = true();
                    self.props.current.T = true();
                    self.props.current.E = true();
            end%switch
            return;
        end%if strcmp(X, L, K, Sigkj, Sigkk, E, T)

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% M %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        if strcmp(X, 'M')
            switch self.spec.stochtype
                case 'QRS'
                    % In this case, M is calculated by dlqe_iter. Call get.Sigkj
                    % and it will do the calculation if necessary.
                    self.Sigkj;

                otherwise % 'PhiTheta' or 'PhibbThetabb'
                    switch self.spec.stochtype
                        case 'PhiTheta'
                            X = forceherm(self.Phi - self.C * self.T * self.C');
                        case 'PhibbThetabb'
                            X = forceherm(self.Phibb ...
                                           - self.C * self.Tbb * self.C');
                    end%switch

                    self.props.store.M = ApinvB(self.C', X);
                    self.props.current.M = true();
            end%switch
            return;
        end%if strcmp(X, 'M')

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% U %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        if strcmp(X, 'U')
            if ~self.Astable
                warning(['A is unstable; therefore steady state U does ', ...
                         'not exist']);
                self.props.store.U = [];
                self.props.current.U = true();
                return;
            end%if

            if ismember('G', self.spec.GH)
                G = self.G;
                g = self.g;
            else
%                G = eye(self.n);
                G = speye(self.n);
                g = self.n;
            end%if

            if ismember('H', self.spec.GH)
                H = self.H;
                h = self.h;
            else
%                H = eye(self.p);
                H = speye(self.p);
                h = self.p;
            end%if

            self.props.store.U = [self.Utl, ...
                       [zeros((self.p * (self.p + 1) / 2), g * h); kron(H, G)]];
            self.props.current.U = true();
            return;
        end%if strcmp(X, 'U')

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Utl %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        if strcmp(X, 'Utl')
            if ~self.Astable
                warning(['A is unstable; therefore steady state Utl does ', ...
                         'not exist']);
                self.props.store.Utl = [];
                self.props.current.Utl = true();
                return;
            end%if

            if ismember('G', self.spec.GH)
                G = self.G;
                g = self.g;
            else
%                G = eye(self.n);
                G = speye(self.n);
                g = self.n;
            end%if

            if ismember('H', self.spec.GH)
                H = self.H;
                h = self.h;
            else
%                H = eye(self.p);
                H = speye(self.p);
                h = self.p;
            end%if

            tmp = pinvAB(eye(self.n^2) - kron(self.A, self.A), ...
                                                        kron(G, G) * dupmat(g));

            self.props.store.Utl = [ ...
[dupmatpinv(self.p) * [kron(self.C, self.C) * tmp, kron(H, H) * dupmat(h)]]; ...
[kron(self.C, self.A) * tmp, zeros(self.n * self.p, (h * (h + 1) / 2))]];
            self.props.current.Utl = true();
            return;
        end%if strcmp(X, 'Utl')

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Abar %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        if strcmp(X, 'Abar')
            self.props.store.Abar = self.A - self.K * self.C;
            self.props.current.Abar = true();
            return;
        end%if strcmp(X, 'Abar')

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Abarstable %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        if strcmp(X, 'Abarstable')
            self.props.store.Abarstable = (max(abs(self.E)) < 1);
            self.props.current.Abarstable = true();
            return;
        end%if strcmp(X, 'Abarstable')

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Gbar %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        if strcmp(X, 'Gbar')
            if ismember('G', self.spec.GH)
                G = self.G;
            else
                G = eye(self.n);
            end%if
            if ismember('H', self.spec.GH)
                H = self.H;
            else
                H = eye(self.p);
            end%if
            self.props.store.Gbar = [G, -self.K * H];
            self.props.current.Gbar = true();
            return;
        end%if strcmp(X, 'Gbar')

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Qbar %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        if strcmp(X, 'Qbar')
            if ismember('S', self.spec.stoch)
                self.props.store.Qbar = [self.Q self.S; self.S' self.R];
            else
                self.props.store.Qbar = blkdiag(self.Q, self.R);
            end%if
            self.props.current.Qbar = true();
            return;
        end%if strcmp(X, 'Qbar')

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Sbar %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        if strcmp(X, 'Sbar')
            if ismember('S', self.spec.stoch)
                self.props.store.Sbar = [self.S; self.R];
            else
                % if we get here, Q and R must be given (if not it will be
                % caught by checkX('QRSspec')); therefore g and h will not be
                % empty
                self.props.store.Sbar = [zeros(self.g, self.h); self.R];
            end%if
            self.props.current.Sbar = true();
            return;
        end%if strcmp(X, 'Sbar')

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% GQGbar %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        if strcmp(X, 'GQGbar')
            self.props.store.GQGbar = forceherm(self.Gbar * self.Qbar ...
                                                 * self.Gbar');
            self.props.current.GQGbar = true();
            return;
        end%if strcmp(X, 'GQGbar')

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% GSHbar %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        if strcmp(X, 'GSHbar')
            if ismember('H', self.spec.GH)
                H = self.H;
            else
                H = eye(self.p);
            end%if
            self.props.store.GSHbar = self.Gbar * self.Sbar * H';
            self.props.current.GSHbar = true();
            return;
        end%if strcmp(X, 'GSHbar')

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Kbar %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        if strcmp(X, 'Kbar')
            self.props.store.Kbar = zeros(self.n, self.p);
            self.props.current.Kbar = true();
            return;
        end%if strcmp(X, 'Kbar')

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Tbar %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        if strcmp(X, 'Tbar')
            self.props.store.Tbar = zeros(self.n);
            self.props.current.Tbar = true();
            return;
        end%if strcmp(X, 'Tbar')

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Phibar %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        if strcmp(X, 'Phibar')
            switch self.spec.stochtype
                case 'QRS'
                    self.props.store.Phibar = ...
                             forceherm(self.C * self.Pbar * self.C' + self.HRH);
                case 'PhiTheta'
                    self.props.store.Phibar = forceherm(self.Phi ...
                                                   - self.C * self.T * self.C');
                case 'PhibbThetabb'
                    self.props.store.Phibar = forceherm(self.Phibb ...
                                                 - self.C * self.Tbb * self.C');
            end%switch
            self.props.current.Phibar = true();
            return;
        end%if strcmp(X, 'Phibar')

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Thetabar %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        if strcmp(X, 'Thetabar')
            self.props.store.Thetabar = zeros(self.n, self.p);
            self.props.current.Thetabar = true();
            return;
        end%if strcmp(X, 'Thetabar')

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Abb %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        if strcmp(X, 'Abb')
            self.props.store.Abb = self.A - self.Kcal * self.C;
            self.props.current.Abb = true();
            return;
        end%if strcmp(X, 'Abb')

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Abbeigs %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        if strcmp(X, 'Abbeigs')
            self.props.store.Abbeigs = eig(self.Abb);
            self.props.current.Abbeigs = true();
            return;
        end%if strcmp(X, 'Abbeigs')

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Abbstable %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        if strcmp(X, 'Abbstable')
            self.props.store.Abbstable = (max(abs(self.Abbeigs)) < 1);
            self.props.current.Abbstable = true();
            return;
        end%if strcmp(X, 'Abbstable')

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Gbb %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        if strcmp(X, 'Gbb')
            if ismember('G', self.spec.GH)
                G = self.G;
            else
                G = eye(self.n);
            end%if
            if ismember('H', self.spec.GH)
                H = self.H;
            else
                H = eye(self.p);
            end%if
            self.props.store.Gbb = [G, -self.Kcal * H];
            self.props.current.Gbb = true();
            return;
        end%if strcmp(X, 'Gbb')

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% GQGbb %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        if strcmp(X, 'GQGbb')
            self.props.store.GQGbb = forceherm(self.Gbb * self.Qbar ...
                                                * self.Gbb');
            self.props.current.GQGbb = true();
            return;
        end%if strcmp(X, 'GQGbb')

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% GSHbb %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        if strcmp(X, 'GSHbb')
            if ismember('H', self.spec.GH)
                H = self.H;
            else
                H = eye(self.p);
            end%if
            self.props.store.GSHbb = self.Gbb * self.Sbar * H';
            self.props.current.GSHbb = true();
            return;
        end%if strcmp(X, 'GSHbb')

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Pbb %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        if strcmp(X, 'Pbb')
            % If Abb is not stable, steady state Pbb doesn't exist, so
            % throw a warning.
            if ~self.Abbstable
                warning(['Abb is unstable; therefore steady state Pbb ', ...
                         'does not exist']);
                self.props.store.Pbb = [];
            else % If Abb is stable, solve discrete Lyapunov equation for Pbb.
                self.props.store.Pbb = forceherm(dlyap(self.Abb, self.GQGbb));
            end%if
            self.props.current.Pbb = true();
            return;
        end%if strcmp(X, 'Pbb')

        %%%%%%%%%%%%%%%%%%%%%%%%%%%% Kbb, Ebb, Tbb %%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        if strcmp(X, 'Kbb') || (strcmp(X, 'Ebb') || strcmp(X, 'Tbb'))
            switch self.spec.stochtype
                case 'QRS'
                    tm = forceherm(self.C * self.Sigkj * self.C' + self.HRH);
                    self.props.store.Kbb = self.K - self.Kcal * tm * rpinv(tm);
                    self.props.store.Ebb = eig(self.Abb ...
                                               - self.props.store.Kbb * self.C);
                    if ~self.Abbstable
                        warning(['Abb is unstable; therefore steady state ', ...
                                 'Tbb does not exist']);
                        self.props.store.Tbb = [];
                    else
                        self.props.store.Tbb = self.Pbb - self.Sigbbkj;
                    end%if
                    self.props.current.Kbb = true();
                    self.props.current.Ebb = true();
                    self.props.current.Tbb = true();

                case 'PhiTheta'
                    % A must be stable (otherwise it would have been caught by
                    % checkX('PhiThetaspec')).
                    tm = forceherm(self.Phi - self.C * self.T * self.C');
                    self.props.store.Kbb = self.K - self.Kcal * tm * rpinv(tm);
                    self.props.store.Ebb = eig(self.Abb ...
                                               - self.props.store.Kbb * self.C);
                    if ~self.Abbstable
                        warning(['Abb is unstable; therefore steady state ', ...
                                 'Tbb does not exist']);
                        self.props.store.Tbb = [];
                    else
                        self.props.store.Tbb = forceherm(dlyap(self.Abb, ...
                            forceherm(-self.props.store.Kbb * (self.Abb ...
                             * self.T * self.C' - self.Theta + self.Kcal ...
                             * self.Phi)')));
                    end%if
                    self.props.current.Kbb = true();
                    self.props.current.Ebb = true();
                    self.props.current.Tbb = true();

                case 'PhibbThetabb'
                    % Abb must be stable. If not, it would have been caught by
                    % checkX('PhibbThetabbspec').
                    [self.props.store.Kbb, ...
                     self.props.store.Tbb, ...
                     self.props.store.Ebb] ...
                     = PhiTheta2K_iter(self.Abb, self.C, self.Phibb, ...
                                       self.Thetabb);
                    self.props.current.Kbb = true();
                    self.props.current.Tbb = true();
                    self.props.current.Ebb = true();
            end%switch
            return;
        end%if strcmp(X, Kbb, Ebb, Tbb)

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Ubb %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        if strcmp(X, 'Ubb')
            if ~self.Abbstable
                warning(['Abb is unstable; therefore steady state Ubb ', ...
                         'does not exist']);
                self.props.store.Ubb = [];
                self.props.current.Ubb = true();
                return;
            end%if

            if ismember('G', self.spec.GH)
                G = self.G;
                g = self.g;
            else
%                G = eye(self.n);
                G = speye(self.n);
                g = self.n;
            end%if

            if ismember('H', self.spec.GH)
                H = self.H;
                h = self.h;
            else
%                H = eye(self.p);
                H = speye(self.p);
                h = self.p;
            end%if

            Fbbinv = eye(self.n^2) - kron(self.Abb, self.Abb);
            CCFbb = ApinvB(kron(self.C, self.C), Fbbinv);
            CAbbFbb = ApinvB(kron(self.C, self.Abb), Fbbinv);

            KcalH = self.Kcal * H;
            KcalHKcalH = kron(KcalH, KcalH);
            KcalHG = kron(KcalH, G);
            GG = kron(G, G);
            IK = speye(self.n^2) + commat(self.n);

            self.props.store.Ubb = [ ...
        dupmatpinv(self.p) * [CCFbb * GG * dupmat(g), ...
                              (CCFbb * KcalHKcalH + kron(H, H)) * dupmat(h), ...
                              -CCFbb * IK * KcalHG]; ...
        [CAbbFbb * GG * dupmat(g), ...
         (CAbbFbb * KcalHKcalH - kron(H, KcalH)) * dupmat(h), ...
         -CAbbFbb * IK * KcalHG + kron(H, G)] ...
                                   ];
            self.props.current.Ubb = true();
            return;
        end%if strcmp(X, 'Ubb')

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Ubbtl %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        if strcmp(X, 'Ubbtl')
            if ~self.Abbstable
                warning(['Abb is unstable; therefore steady state Ubbtl ', ...
                         'does not exist']);
                self.props.store.Ubbtl = [];
                self.props.current.Ubbtl = true();
                return;
            end%if

            if ismember('G', self.spec.GH)
                g = self.g;
            else
                g = self.n;
            end%if

            if ismember('H', self.spec.GH)
                h = self.h;
            else
                h = self.p;
            end%if

            self.props.store.Ubbtl = ...
                               self.Ubb(:, 1:((g * (g + 1) + h * (h + 1)) / 2));
            self.props.current.Ubbtl = true();
            return;
        end%if strcmp(X, 'Ubbtl')
    end%function calculate

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function load_constants(self)
        filename = [cemodel.installdir(), 'cemodel_constants.mat'];
        if exist(filename, 'file') ~= 2
            try
                cemodel.generate_constants();
            catch err
                error(['unable to generate file ', filename, ' from ', ...
                       'function cemodel.generate_constants(); got ', ...
                       'the following error message: ', err.message]);
            end%trycatch
        end%if
        try
            if isoctave()
                S = load('-7', filename);
            else % Matlab
                S = load(filename);
            end%if
            self.props = S.props.(self.spec.string);
            self.checks = S.checks.(self.spec.string);
        catch err
            error(['unable to load props.', self.spec.string, ' and ', ...
                   'checks.', self.spec.string, ' from file ', filename, ...
                   '; got the following error message: ', err.message]);
        end%trycatch
    end%function load_constants

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% get methods %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = getdependentprop(self, X)
        % Helper method used to get properties in self.props.dependent.

        % Make sure input argument is a recognized value.
        if ~ismember(X, self.props.dependent)
            error(['cannot access property ''', X, '''; props.dependent ', ...
                   'of cemodel object is currently: ''', ...
                   strjoin(self.props.dependent, ''', '''), '''']);
        end%if

        % If self.X isn't current, update self.props.store.X. Remember,
        % self.calculate will update self.props.current.
        if ~self.props.current.(X)
            self.calculate(X);
        end%if

        % Return self.props.store.X.
        val = self.props.store.(X);
    end%function getdependentprop
end%methods (Access = private)

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

methods
    function val = get.Phi(self)
        if strcmp(self.spec.stochtype, 'QRS') ...
           || strcmp(self.spec.stochtype, 'PhibbThetabb')
            val = self.getdependentprop('Phi');
        else
            val = self.Phi;
        end%if
    end%function get.Phi

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.Theta(self)
        if strcmp(self.spec.stochtype, 'QRS') ...
           || strcmp(self.spec.stochtype, 'PhibbThetabb')
            val = self.getdependentprop('Theta');
        else
            val = self.Theta;
        end%if
    end%function get.Theta

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.Phibb(self)
        if strcmp(self.spec.stochtype, 'QRS') ...
           || strcmp(self.spec.stochtype, 'PhiTheta')
            val = self.getdependentprop('Phibb');
        else
            val = self.Phibb;
        end%if
    end%function get.Phibb

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.Thetabb(self)
        if strcmp(self.spec.stochtype, 'QRS') ...
           || strcmp(self.spec.stochtype, 'PhiTheta')
            val = self.getdependentprop('Thetabb');
        else
            val = self.Thetabb;
        end%if
    end%function get.Thetabb

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.n(self)
        val = self.getdependentprop('n');
    end%function get.n

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.p(self)
        val = self.getdependentprop('p');
    end%function get.p

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.m(self)
        val = self.getdependentprop('m');
    end%function get.m

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.g(self)
        val = self.getdependentprop('g');
    end%function get.g

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.h(self)
        val = self.getdependentprop('h');
    end%function get.h

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.Aeigs(self)
        val = self.getdependentprop('Aeigs');
    end%function get.Aeigs

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.Astable(self)
        val = self.getdependentprop('Astable');
    end%function get.Astable

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.GQG(self)
        val = self.getdependentprop('GQG');
    end%function get.GQG

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.HRH(self)
        val = self.getdependentprop('HRH');
    end%function get.HRH

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.GSH(self)
        val = self.getdependentprop('GSH');
    end%function get.GSH

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.P(self)
        val = self.getdependentprop('P');
    end%function get.P

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.L(self)
        val = self.getdependentprop('L');
    end%function get.L

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.K(self)
        val = self.getdependentprop('K');
    end%function get.K

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.M(self)
        val = self.getdependentprop('M');
    end%function get.M

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.Sigkj(self)
        val = self.getdependentprop('Sigkj');
    end%function get.Sigkj

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.Sigkk(self)
        val = self.getdependentprop('Sigkk');
    end%function get.Sigkk

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.E(self)
        val = self.getdependentprop('E');
    end%function get.E

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.T(self)
        val = self.getdependentprop('T');
    end%function get.T

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.U(self)
        val = self.getdependentprop('U');
    end%function get.U

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.Utl(self)
        val = self.getdependentprop('Utl');
    end%function get.Utl

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.Abar(self)
        val = self.getdependentprop('Abar');
    end%function get.Abar

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.Abareigs(self)
        val = self.E;
    end%function get.Abareigs

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.Abarstable(self)
        val = self.getdependentprop('Abarstable');
    end%function get.Abarstable

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.Gbar(self)
        val = self.getdependentprop('Gbar');
    end%function get.Gbar

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.Qbar(self)
        val = self.getdependentprop('Qbar');
    end%function get.Qbar

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.Sbar(self)
        val = self.getdependentprop('Sbar');
    end%function get.Sbar

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.GQGbar(self)
        val = self.getdependentprop('GQGbar');
    end%function get.GQGbar

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.GSHbar(self)
        val = self.getdependentprop('GSHbar');
    end%function get.GSHbar

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.Pbar(self)
        val = self.Sigkj;
    end%function get.Pbar

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.Lbar(self)
        val = self.L;
    end%function get.Lbar

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.Kbar(self)
        val = self.getdependentprop('Kbar');
    end%function get.Kbar

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.Mbar(self)
        val = self.M;
    end%function get.Mbar

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.Sigbarkj(self)
        val = self.Sigkj;
    end%function get.Sigbarkj

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.Sigbarkk(self)
        val = self.Sigkk;
    end%function get.Sigbarkk

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.Ebar(self)
        val = self.E;
    end%function get.Ebar

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.Tbar(self)
        val = self.getdependentprop('Tbar');
    end%function get.Tbar

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.Phibar(self)
        val = self.getdependentprop('Phibar');
    end%function get.Phibar

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.Thetabar(self)
        val = self.getdependentprop('Thetabar');
    end%function get.Thetabar

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.Abb(self)
        val = self.getdependentprop('Abb');
    end%function get.Abb

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.Abbeigs(self)
        val = self.getdependentprop('Abbeigs');
    end%function get.Abbeigs

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.Abbstable(self)
        val = self.getdependentprop('Abbstable');
    end%function get.Abbstable

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.Gbb(self)
        val = self.getdependentprop('Gbb');
    end%function get.Gbb

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.GQGbb(self)
        val = self.getdependentprop('GQGbb');
    end%function get.GQGbb

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.GSHbb(self)
        val = self.getdependentprop('GSHbb');
    end%function get.GSHbb

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.Pbb(self)
        val = self.getdependentprop('Pbb');
    end%function get.Pbb

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.Lbb(self)
        if self.spec.Kcal
            val = self.L;
        else
            error('Lbb does not exist because Kcal is not provided');
        end%if
    end%function get.Lbb

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.Kbb(self)
        val = self.getdependentprop('Kbb');
    end%function get.Kbb

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.Mbb(self)
        if self.spec.Kcal
            val = self.M;
        else
            error('Mbb does not exist because Kcal is not provided');
        end%if
    end%function get.Mbb

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.Sigbbkj(self)
        if self.spec.Kcal
            val = self.Sigkj;
        else
            error('Sigbbkj does not exist because Kcal is not provided');
        end%if
    end%function get.Sigbbkj

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.Sigbbkk(self)
        if self.spec.Kcal
            val = self.Sigkk;
        else
            error('Sigbbkk does not exist because Kcal is not provided');
        end%if
    end%function get.Sigbbkk

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.Ebb(self)
        val = self.getdependentprop('Ebb');
    end%function get.Ebb

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.Tbb(self)
        val = self.getdependentprop('Tbb');
    end%function get.Tbb

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.Ubb(self)
        val = self.getdependentprop('Ubb');
    end%function get.Ubb

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.Ubbtl(self)
        val = self.getdependentprop('Ubbtl');
    end%function get.Ubbtl
end%methods

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% set methods %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

methods (Access = private)
    function need2set = helpsetX(self, X, val)
        % Helper method that is used to update self.props, self.checks, and
        % self.spec when base props are set.

        % Determine if X needs to be set.
        need2set = ~isequal(val, self.(X));
        need2updatestoch = need2set ...
                           && (ismember(X, self.props.stoch) ...
                           && (isempty(self.(X)) ~= isempty(val)));
            % The last condition is because we only need to run self.updatestoch
            % when stochastic parameters are being changed between empty and
            % nonempty. If a stochastic parameter changes from one numerical
            % value to another, nothing in self.spec chages, so we don't need to
            % run self.updatestoch.
        if need2updatestoch
            self.updatestoch(X, val);
        end%if

        % If cemodel object changes, update self.checks.current and
        % self.props.current and raise flags.
        if need2set
            self.updatecurrent(X);
            self.raiseflags();
        end%if
    end%function helpsetX

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function updatecurrent(self, X)
        % For base property X, mark all dependent checks and dependent
        % properties as out of date.

        for Y = self.checks.names
            if ismember(X, self.checks.depends.(Y{:}))
                self.checks.current.(Y{:}) = false();
            end%if
        end%for
        for Y = self.props.dependent
            if ismember(X, self.props.depends.(Y{:}))
                self.props.current.(Y{:}) = false();
            end%if
        end%for
    end%function updatecurrent

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function raiseflags(self)
        for s = {'ceestimator', 'als'}
            self.flags.(s{:}) = structfun(@(x) true(), self.flags.(s{:}), ...
                                          'UniformOutput', false());
        end%for
    end%function raiseflags

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function updatestoch(self, X, val)
        old_stochtype = self.spec.stochtype;
        need2reload = false();

        if strcmp(X, 'Kcal')
            % If Kcal was just changed either from empty to nonempty or vice
            % versa, update value of self.spec.Kcal and self.spec.stoch.
            self.spec.Kcal = ~isempty(val);
            if self.spec.Kcal % add 'Kcal' to self.spec.stoch
                self.spec.stoch = [self.spec.stoch, 'Kcal'];
            else % remove 'Kcal' from self.spec.stoch
                [bool, idx] = ismember('Kcal', self.spec.stoch);
                if bool
                    self.spec.stoch(idx) = [];
                end%if
                % If stoch is now completely empty, make sure it has 0 x 0
                % dimensions.
                if isempty(self.spec.stoch)
                    self.spec.stoch = {};
                end%if
            end%if
            need2reload = true();
        elseif strcmp(X, 'Lcal')
            % If Lcal was just changed either from empty to nonempty or vice
            % versa, update value of self.spec.Lcal and self.spec.stoch.
            self.spec.Lcal = ~isempty(val);
            if self.spec.Lcal % add 'Lcal' to self.spec.stoch
                self.spec.stoch = [self.spec.stoch, 'Lcal'];
            else % remove 'Lcal' from self.spec.stoch
                [bool, idx] = ismember('Lcal', self.spec.stoch);
                if bool
                    self.spec.stoch(idx) = [];
                end%if
                if isempty(self.spec.stoch)
                    % If stoch is now completely empty, make sure it has 0 x 0
                    % dimensions.
                    self.spec.stoch = {};
                end%if
            end%if
            need2reload = true();
        else
            need2break = false();
            for Z = {{'Q', 'R', 'S'}, {'Phi', 'Theta'}, {'Phibb', 'Thetabb'}}
                ZZ = Z{:}; % {'Q', 'R', 'S'}, {'Phi', 'Theta'}, or
                           % {'Phibb', 'Thetabb'}
                for Y = ZZ
                    % Y{:} = 'Q', 'R', 'S', 'Phi', 'Theta', 'Phibb', or
                    % 'Thetabb'
                    if strcmp(X, Y{:})
                        % if property Y{:} was set, update self.spec.stoch
                        % and self.spec.stochtype
                        possible_stochtype = horzcat(ZZ{:});

                        if ~isempty(val) % adding property X
                            self.spec.stoch = [self.spec.stoch, X];
                            self.spec.stochtype = possible_stochtype;
                        else % removing property X
                            [bool, idx] = ismember(X, self.spec.stoch);
                            if bool
                                self.spec.stoch(idx) = [];
                            end%if
                            if any(ismember(ZZ, self.spec.stoch))
                                self.spec.stochtype = possible_stochtype;
                            else
                                self.spec.stochtype = 'none';
                            end%if
                        end%if

                        % If stoch is now completely empty, make sure it has
                        % 0 x 0 dimensions.
                        if isempty(self.spec.stoch)
                            self.spec.stoch = {};
                        end%if

                        need2reload = ~strcmp(old_stochtype, ...
                                              self.spec.stochtype);
                        need2break = true();
                        break;
                    end%if
                end%for
                if need2break
                    break;
                end%if
            end%for
        end%if

        % If self.spec.stochtype, self.spec.Kcal, or self.spec.Lcal changed,
        % set self.spec.string and use it to load self.props and self.checks.
        if need2reload
            self.spec.string = self.spec.stochtype;
            if self.spec.Kcal
                self.spec.string = ['Kcal', self.spec.string];
            end%if
            if self.spec.Lcal
                self.spec.string = ['Lcal', self.spec.string];
            end%if
            self.load_constants(); % load self.props and self.checks
        end%if
    end%function updatestoch

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function setBD(self)
        b = ~isempty(self.B);
        d = ~isempty(self.D);
        if b && d
            self.spec.BD = 'BD';
        elseif b && ~d
            self.spec.BD = 'B';
        elseif ~b && d
            self.spec.BD = 'D';
        else
            self.spec.BD = '';
        end%if
    end%function setBD

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function setGH(self)
        g = ~isempty(self.G);
        h = ~isempty(self.H);
        if g && h
            self.spec.GH = 'GH';
        elseif g && ~h
            self.spec.GH = 'G';
        elseif ~g && h
            self.spec.GH = 'H';
        else
            self.spec.GH = '';
        end%if
    end%function setGH
end%methods (Access = private)

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

methods
    function set.A(self, val)
        if ~isnumericmatrix(val)
            error('A must be a numeric matrix');
        end%if
        if ~myissquare(val) && ~isempty(val)
            error('A must be square');
        end%if
        need2set = self.helpsetX('A', val);
        if need2set
            self.A = val;
        end%if
    end%function set.A

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function set.C(self, val)
        if ~isnumericmatrix(val)
            error('C must be a numeric matrix');
        end%if
        need2set = self.helpsetX('C', val);
        if need2set
            self.C = val;
        end%if
    end%function set.C

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function set.B(self, val)
        if ~isnumericmatrix(val)
            error('B must be a numeric matrix');
        end%if
        need2set = self.helpsetX('B', val);
        if need2set
            self.B = val;
            self.setBD();
        end%if
    end%function set.B

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function set.D(self, val)
        if ~isnumericmatrix(val)
            error('D must be a numeric matrix');
        end%if
        need2set = self.helpsetX('D', val);
        if need2set
            self.D = val;
            self.setBD();
        end%if
    end%function set.D

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function set.G(self, val)
        if ~isnumericmatrix(val)
            error('G must be a numeric matrix');
        end%if
        need2set = self.helpsetX('G', val);
        if need2set
            self.G = val;
            self.setGH();
        end%if
    end%function set.G

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function set.H(self, val)
        if ~isnumericmatrix(val)
            error('H must be a numeric matrix');
        end%if
        need2set = self.helpsetX('H', val);
        if need2set
            self.H = val;
            self.setGH();
        end%if
    end%function set.H

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function set.Q(self, val)
        if strcmp(self.spec.stochtype, 'PhiTheta') ...
           || strcmp(self.spec.stochtype, 'PhibbThetabb')
            error(['cannot set Q when Phi/Theta or Phibb/Thetabb are user ', ...
                   'specified']);
        end%if
        if ~isnumericmatrix(val)
            error('Q must be a numeric matrix');
        end%if
        if ~myissquare(val) && ~isempty(val)
            error('Q must be square');
        end%if
        if ~ishermitian(val)
            error(['Q must be Hermitian (if Q is only slightly ', ...
                   'non-Hermitian due to round-off error, use of the ', ...
                   'forceherm function is recommended)']);
        end%if
        if ~isempty(val) && myisdefinite(val) == -1
            error('Q must be positive semidefinite');
        end%if
        X = 'Q';
        need2set = self.helpsetX(X, val);
        if need2set
            self.(X) = val;
        end%if
    end%function set.Q

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function set.R(self, val)
        if strcmp(self.spec.stochtype, 'PhiTheta') ...
           || strcmp(self.spec.stochtype, 'PhibbThetabb')
            error(['cannot set R when Phi/Theta or Phibb/Thetabb are user ', ...
                   'specified']);
        end%if
        if ~isnumericmatrix(val)
            error('R must be a numeric matrix');
        end%if
        if ~myissquare(val) && ~isempty(val)
            error('R must be square');
        end%if
        if ~ishermitian(val)
            error(['R must be Hermitian (if R is only slightly ', ...
                   'non-Hermitian due to round-off error, use of the ', ...
                   'forceherm function is recommended)']);
        end%if
        if ~isempty(val) && myisdefinite(val) == -1
            error('R must be positive semidefinite');
        end%if
        X = 'R';
        need2set = self.helpsetX(X, val);
        if need2set
            self.(X) = val;
        end%if
    end%function set.R

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function set.S(self, val)
        if strcmp(self.spec.stochtype, 'PhiTheta') ...
           || strcmp(self.spec.stochtype, 'PhibbThetabb')
            error(['cannot set S when Phi/Theta or Phibb/Thetabb are user ', ...
                   'specified']);
        end%if
        if ~isnumericmatrix(val)
            error('S must be a numeric matrix');
        end%if
        X = 'S';
        need2set = self.helpsetX(X, val);
        if need2set
            self.(X) = val;
        end%if
    end%function set.S

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function set.Phi(self, val)
        if strcmp(self.spec.stochtype, 'QRS') ...
           || strcmp(self.spec.stochtype, 'PhibbThetabb')
            error(['cannot set Phi when Q/R/S or Phibb/Thetabb are user ', ...
                   'specified']);
        end%if
        if ~isnumericmatrix(val)
            error('Phi must be a numeric matrix');
        end%if
        if ~myissquare(val) && ~isempty(val)
            error('Phi must be square');
        end%if
        if ~ishermitian(val)
            error(['Phi must be Hermitian (if Phi is only slightly ', ...
                   'non-Hermitian due to round-off error, use of the ', ...
                   'forceherm function is recommended)']);
        end%if
        if ~isempty(val) && myisdefinite(val) == -1
            error('Phi must be positive semidefinite');
        end%if
        X = 'Phi';
        need2set = self.helpsetX(X, val);
        if need2set
            self.(X) = val;
        end%if
    end%function set.Phi

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function set.Theta(self, val)
        if strcmp(self.spec.stochtype, 'QRS') ...
           || strcmp(self.spec.stochtype, 'PhibbThetabb')
            error(['cannot set Theta when Q/R/S or Phibb/Thetabb are user ', ...
                   'specified']);
        end%if
        if ~isnumericmatrix(val)
            error('Theta must be a numeric matrix');
        end%if
        X = 'Theta';
        need2set = self.helpsetX(X, val);
        if need2set
            self.(X) = val;
        end%if
    end%function set.Theta

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function set.Kcal(self, val)
        if ~isnumericmatrix(val)
            error('Kcal must be a numeric matrix');
        end%if
        X = 'Kcal';
        need2set = self.helpsetX(X, val);
        if need2set
            self.(X) = val;
        end%if
    end%function set.Kcal

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function set.Lcal(self, val)
        if ~isnumericmatrix(val)
            error('Lcal must be a numeric matrix');
        end%if
        X = 'Lcal';
        need2set = self.helpsetX(X, val);
        if need2set
            self.(X) = val;
        end%if
    end%function set.Lcal

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function set.Phibb(self, val)
        if strcmp(self.spec.stochtype, 'QRS') ...
           || strcmp(self.spec.stochtype, 'PhiTheta')
            error(['cannot set Phibb when Q/R/S or Phi/Theta are user ', ...
                   'specified']);
        end%if
        if ~isnumericmatrix(val)
            error('Phibb must be a numeric matrix');
        end%if
        if ~myissquare(val) && ~isempty(val)
            error('Phibb must be square');
        end%if
        if ~ishermitian(val)
            error(['Phibb must be Hermitian (if Phibb is only slightly ', ...
                   'non-Hermitian due to round-off error, use of the ', ...
                   'forceherm function is recommended)']);
        end%if
        if ~isempty(val) && myisdefinite(val) == -1
            error('Phibb must be positive semidefinite');
        end%if
        X = 'Phibb';
        need2set = self.helpsetX(X, val);
        if need2set
            self.(X) = val;
        end%if
    end%function set.Phibb

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function set.Thetabb(self, val)
        if strcmp(self.spec.stochtype, 'QRS') ...
           || strcmp(self.spec.stochtype, 'PhiTheta')
            error(['cannot set Thetabb when Q/R/S or Phi/Theta are user', ...
                   'specified']);
        end%if
        if ~isnumericmatrix(val)
            error('Thetabb must be a numeric matrix');
        end%if
        X = 'Thetabb';
        need2set = self.helpsetX(X, val);
        if need2set
            self.(X) = val;
        end%if
    end%function set.Thetabb

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function set.flags(self, val)
        % The flags property has public SetAccess, because attached ceestimator
        % and als objects need to be able to change it. However, the user should
        % not be able to change this property, so check to make sure it is being
        % changed from an appropriate place, and if not, throw an error.

        d = dbstack();
        dnames = {d.name};

        if isoctave()
            cmpfunc = @strcmp;
        else % Matlab
            cmpfunc = @endsWith;
        end%if

        % Check if flags is being changed from changing a base prop of the
        % cemodel object.
        tests(1) = numel(dnames) >= 2 && cmpfunc(dnames{2}, 'raiseflags');

        % Check if flags are being lowered by linked ceestimator or als object.
        tests(2) = numel(dnames) >= 2 ...
                                  && (cmpfunc(dnames{2}, 'seeifmdchanged') ...
                                      || cmpfunc(dnames{2}, 'seeifmdechanged'));

        % Check if flags is being changed by set.model --> link in ceestimator
        % or als.
        tests(3) = numel(dnames) >= 2 && cmpfunc(dnames{2}, 'link');

        if ~any(tests(:))
            error('cannot set property ''flags'' in this context');
        end%if

        self.flags = val;
    end%function set.flags
end%methods

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

methods (Static)
    function generate_constants()
        props = struct();
        checks = struct();
        base = {'A', ...
                'B', ...
                'C', ...
                'D', ...
                'G', ...
                'H', ...
                'Kcal', ...
                'Lcal', ...
                'Q', ...
                'R', ...
                'S', ...
                'Phi', ...
                'Theta', ...
                'Phibb', ...
                'Thetabb'};
        stoch = {'Kcal', ...
                 'Lcal', ...
                 'Q', ...
                 'R', ...
                 'S', ...
                 'Phi', ...
                 'Theta', ...
                 'Phibb', ...
                 'Thetabb'};
        allchecks = struct( ...
        'empty',                {base}, ...
        'ABsize',               {{'A', 'B'}}, ...
        'ACsize',               {{'A', 'C'}}, ...
        'CDsize',               {{'C', 'D'}}, ...
        'BDsize',               {{'B', 'D'}}, ...
        'Kcalsize',             {{'Kcal', 'A', 'C'}}, ...
        'Lcalsize',             {{'Lcal', 'A', 'C'}}, ...
        'QRSspec',              {{'Q', 'R', 'A', 'C'}}, ...
        'GQsize',               {{'G', 'Q', 'A'}}, ...
        'HRsize',               {{'H', 'R', 'C'}}, ...
        'Ssize',                {{'S', 'G', 'H', 'Q', 'R'}}, ...
        'QRSdefinite',          {{'Q', 'R', 'S'}}, ...
        'PhiThetaspec',         {{'Phi', 'Theta', 'A', 'C'}}, ...
        'Phisize',              {{'Phi', 'C', 'H'}}, ...
        'Thetasize',            {{'Theta', 'A', 'C', 'G', 'H'}}, ...
        'PhiThetasize',         {{'Phi', 'Theta'}}, ...
        'PhiThetadefinite',     {{'Phi', 'Theta'}}, ...
        'PhibbThetabbspec',     {{'Kcal', 'Phibb', 'Thetabb', 'A', 'C'}}, ...
        'Phibbsize',            {{'Phibb', 'C', 'H'}}, ...
        'Thetabbsize',          {{'Thetabb', 'A', 'C', 'G', 'H'}}, ...
        'PhibbThetabbsize',     {{'Phibb', 'Thetabb'}}, ...
        'PhibbThetabbdefinite', {{'Phibb', 'Thetabb'}});
        alwayschecks =           {'empty', ...
                                  'ABsize', ...
                                  'ACsize', ...
                                  'CDsize', ...
                                  'BDsize'};
        QRSchecks =              {'QRSspec', ...
                                  'GQsize', ...
                                  'HRsize', ...
                                  'Ssize', ...
                                  'QRSdefinite'};
        PhiThetachecks =         {'PhiThetaspec', ...
                                  'Phisize', ...
                                  'Thetasize', ...
                                  'PhiThetasize', ...
                                  'PhiThetadefinite'};
        PhibbThetabbchecks =     {'PhibbThetabbspec', ...
                                  'Phibbsize', ...
                                  'Thetabbsize', ...
                                  'PhibbThetabbsize'};
        KcalPhibbThetabbchecks = [PhibbThetabbchecks, ...
                                  'PhibbThetabbdefinite', ...
                                  'Kcalsize'];

        allX = {};
        ds = 'dependsspec';
        dp = 'dependent';
        b = 'base';
        n = 'names';

        %%%%%%%%%%%%%%%%%%%%%%%%%% stochtype = 'none' %%%%%%%%%%%%%%%%%%%%%%%%%%

        X = 'none';
        allX = [allX, X];
        props.(X).(ds) = ...
       struct('n',       {{'A'}}, ...
              'p',       {{'C'}}, ...
              'm',       {{'B', 'D'}}, ...
              'g',       {{'G', 'Q', 'n'}}, ...
              'h',       {{'H', 'R', 'p'}}, ...
              'Aeigs',   {{'A'}}, ...
              'Astable', {{'Aeigs'}}, ...
              'U',       {{'Astable', 'G', 'g', 'H', 'h', 'n', 'p', 'Utl'}}, ...
              'Utl',     {{'Astable', 'G', 'g', 'H', 'h', 'n', 'p', 'A', 'C'}});
        checks.(X).(n) = alwayschecks;

        X = 'Kcalnone';
        allX = [allX, X];
        props.(X).(ds) = catstruct(props.none.(ds), ...
struct('Abb',       {{'A', 'C', 'Kcal'}}, ...
       'Abbeigs',   {{'Abb'}}, ...
       'Abbstable', {{'Abbeigs'}}, ...
       'Gbb',       {{'G', 'n', 'H', 'p', 'Kcal'}}, ...
       'Ubb',       {{'Abbstable', 'G', 'g', 'H', 'h', 'n', 'p', 'C', 'Abb', ...
                      'Kcal'}}, ...
       'Ubbtl',     {{'Abbstable', 'g', 'n', 'h', 'p', 'Ubb'}}));
        checks.(X).(n) = [alwayschecks, 'Kcalsize'];

        %%%%%%%%%%%%%%%%%%%%%%%%%% stochtype = 'QRS' %%%%%%%%%%%%%%%%%%%%%%%%%%%

        X = 'QRS';
        allX = [allX, X];
        props.(X).(ds) = ...
 struct('Phi',        {{'C', 'P', 'HRH', 'Astable'}}, ...
        'Theta',      {{'A', 'P', 'C', 'GSH', 'Astable'}}, ...
        'n',          {{'A'}}, ...
        'p',          {{'C'}}, ...
        'm',          {{'B', 'D'}}, ...
        'g',          {{'G', 'Q', 'n'}}, ...
        'h',          {{'H', 'R', 'p'}}, ...
        'Aeigs',      {{'A'}}, ...
        'Astable',    {{'Aeigs'}}, ...
        'GQG',        {{'G', 'Q'}}, ...
        'HRH',        {{'H', 'R'}}, ...
        'GSH',        {{'G', 'S', 'H', 'n', 'p'}}, ...
        'P',          {{'A', 'GQG', 'Astable'}}, ...
        'L',          {{'A', 'C', 'GQG', 'HRH', 'GSH'}}, ...
        'K',          {{'A', 'C', 'GQG', 'HRH', 'GSH'}}, ...
        'M',          {{'C', 'Sigkj', 'H', 'R'}}, ...
        'Sigkj',      {{'A', 'C', 'GQG', 'HRH', 'GSH'}}, ...
        'Sigkk',      {{'A', 'C', 'GQG', 'HRH', 'GSH'}}, ...
        'E',          {{'A', 'C', 'GQG', 'HRH', 'GSH'}}, ...
        'T',          {{'P', 'Sigkj', 'Astable'}}, ...
        'U',          {{'Astable', 'G', 'g', 'H', 'h', 'n', 'p', 'Utl'}}, ...
        'Utl',        {{'Astable', 'G', 'g', 'H', 'h', 'n', 'p', 'A', 'C'}}, ...
        'Abar',       {{'A', 'K', 'C'}}, ...
        'Abarstable', {{'E'}}, ...
        'Gbar',       {{'G', 'H', 'n', 'p', 'K'}}, ...
        'Qbar',       {{'Q', 'S', 'R'}}, ...
        'Sbar',       {{'S', 'R', 'g', 'h'}}, ...
        'GQGbar',     {{'Gbar', 'Qbar'}}, ...
        'GSHbar',     {{'H', 'p', 'Gbar', 'Sbar'}}, ...
        'Kbar',       {{'n', 'p'}}, ...
        'Tbar',       {{'n'}}, ...
        'Phibar',     {{'Abarstable', 'C', 'Sigkj', 'HRH'}}, ...
        'Thetabar',   {{'n', 'p'}});
        checks.(X).(n) = [alwayschecks, QRSchecks];

        X = 'KcalQRS';
        allX = [allX, X];
        props.(X).(ds) = catstruct(props.QRS.(ds), ...
struct('Phibb',     {{'Abbstable', 'C', 'Pbb', 'HRH'}}, ...
       'Thetabb',   {{'Abbstable', 'Abb', 'Pbb', 'C', 'GSHbb'}}, ...
       'Abb',       {{'A', 'C', 'Kcal'}}, ...
       'Abbeigs',   {{'Abb'}}, ...
       'Abbstable', {{'Abbeigs'}}, ...
       'Gbb',       {{'G', 'n', 'H', 'p', 'Kcal'}}, ...
       'GQGbb',     {{'Gbb', 'Qbar'}}, ...
       'GSHbb',     {{'H', 'p', 'Gbb', 'Sbar'}}, ...
       'Pbb',       {{'Abb', 'GQGbb', 'Abbstable'}}, ...
       'Kbb',       {{'K', 'Kcal', 'C', 'Sigkj', 'HRH'}}, ...
       'Ebb',       {{'Abb', 'Kbb', 'C'}}, ...
       'Tbb',       {{'Pbb', 'Sigkj', 'Abbstable'}}, ...
       'Ubb',       {{'Abbstable', 'G', 'g', 'H', 'h', 'n', 'p', 'C', 'Abb', ...
                      'Kcal'}}, ...
       'Ubbtl',     {{'Abbstable', 'g', 'n', 'h', 'p', 'Ubb'}}));

        checks.(X).(n) = [alwayschecks, QRSchecks, 'Kcalsize'];

        %%%%%%%%%%%%%%%%%%%%%%%% stochtype = 'PhiTheta' %%%%%%%%%%%%%%%%%%%%%%%%

        X = 'PhiTheta';
        allX = [allX, X];
    props.(X).(ds) = ...
 struct('n',          {{'A'}}, ...
        'p',          {{'C'}}, ...
        'm',          {{'B', 'D'}}, ...
        'g',          {{'G'}}, ...
        'h',          {{'H'}}, ...
        'Aeigs',      {{'A'}}, ...
        'Astable',    {{'Aeigs'}}, ...
        'K',          {{'A', 'C', 'Phi', 'Theta'}}, ...
        'M',          {{'Phi', 'C', 'T'}}, ...
        'E',          {{'A', 'C', 'Phi', 'Theta'}}, ...
        'T',          {{'A', 'C', 'Phi', 'Theta'}}, ...
        'U',          {{'Astable', 'G', 'g', 'H', 'h', 'n', 'p', 'Utl'}}, ...
        'Utl',        {{'Astable', 'G', 'g', 'H', 'h', 'n', 'p', 'A', 'C'}}, ...
        'Abar',       {{'A', 'K', 'C'}}, ...
        'Abarstable', {{'Abar'}}, ...
        'Gbar',       {{'G', 'H', 'n', 'p', 'K'}}, ...
        'Kbar',       {{'n', 'p'}}, ...
        'Tbar',       {{'n'}}, ...
        'Phibar',     {{'Abarstable', 'Phi', 'C', 'T'}}, ...
        'Thetabar',   {{'n', 'p'}});
        checks.(X).(n) = [alwayschecks, PhiThetachecks];

        X = 'KcalPhiTheta';
        allX = [allX, X];
        props.(X).(ds) = catstruct(props.PhiTheta.(ds), struct( ...
'Phibb',     {{'Abbstable', 'Phi', 'C', 'T', 'Tbb'}}, ...
'Thetabb',   {{'Abbstable', 'Theta', 'Abb', 'T', 'Tbb', 'C', 'Kcal', ...
               'Phi'}}, ...
'Abb',       {{'A', 'Kcal', 'C'}}, ...
'Abbeigs',   {{'Abb'}}, ...
'Abbstable', {{'Abbeigs'}}, ...
'Gbb',       {{'G', 'H', 'n', 'p', 'Kcal'}}, ...
'Kbb',       {{'K', 'Kcal', 'Phi', 'C', 'T'}}, ...
'Ebb',       {{'Abb', 'Kbb', 'C'}}, ...
'Tbb',       {{'Abbstable', 'Abb', 'Kbb', 'T', 'C', 'Theta', 'Kcal', ...
               'Phi'}}, ...
'Ubb',       {{'Abbstable', 'G', 'g', 'H', 'h', 'n', 'p', 'C', 'Abb', ...
               'Kcal'}}, ...
'Ubbtl',     {{'Abbstable', 'g', 'n', 'h', 'p', 'Ubb'}}));
        checks.(X).(n) = [alwayschecks, PhiThetachecks, 'Kcalsize'];

        %%%%%%%%%%%%%%%%%%%%%% stochtype = 'PhibbThetabb' %%%%%%%%%%%%%%%%%%%%%%

        X = 'PhibbThetabb';
        allX = [allX, X];
        props.(X).(ds) = ...
       struct('n',       {{'A'}}, ...
              'p',       {{'C'}}, ...
              'm',       {{'B', 'D'}}, ...
              'g',       {{'G'}}, ...
              'h',       {{'H'}}, ...
              'Aeigs',   {{'A'}}, ...
              'Astable', {{'Aeigs'}}, ...
              'U',       {{'Astable', 'G', 'g', 'H', 'h', 'n', 'p', 'Utl'}}, ...
              'Utl',     {{'Astable', 'G', 'g', 'H', 'h', 'n', 'p', 'A', 'C'}});
        checks.(X).(n) = [alwayschecks, PhibbThetabbchecks];

        X = 'KcalPhibbThetabb';
        allX = [allX, X];
        props.(X).(ds) = catstruct(props.PhibbThetabb.(ds), struct( ...
'Phi',        {{'Astable', 'Phibb', 'C', 'T', 'Tbb'}}, ...
'Theta',      {{'Astable', 'Thetabb', 'A', 'T', 'Tbb', 'C', 'Kcal', ...
                'Phibb'}}, ...
'K',          {{'Kbb', 'Kcal', 'Phibb', 'C', 'Tbb'}}, ...
'M',          {{'Phibb', 'C', 'Tbb'}}, ...
'E',          {{'A', 'K', 'C'}}, ...
'T',          {{'Astable', 'A', 'K', 'Tbb' 'C', 'Thetabb', 'Kcal', ...
                'Phibb'}}, ...
'Abar',       {{'A', 'K', 'C'}}, ...
'Abarstable', {{'Abar'}}, ...
'Gbar',       {{'G', 'H', 'n', 'p', 'K'}}, ...
'Kbar',       {{'n', 'p'}}, ...
'Tbar',       {{'n'}}, ...
'Phibar',     {{'Abarstable', 'Phibb', 'C', 'Tbb'}}, ...
'Thetabar',   {{'n', 'p'}}, ...
'Abb',        {{'A', 'Kcal', 'C'}}, ...
'Abbeigs',    {{'Abb'}}, ...
'Abbstable',  {{'Abbeigs'}}, ...
'Gbb',        {{'G', 'H', 'n', 'p', 'Kcal'}}, ...
'Kbb',        {{'Abb', 'C', 'Phibb', 'Thetabb'}}, ...
'Tbb',        {{'Abb', 'C', 'Phibb', 'Thetabb'}}, ...
'Ebb',        {{'Abb', 'C', 'Phibb', 'Thetabb'}}, ...
'Ubb',        {{'Abbstable', 'G', 'g', 'H', 'h', 'n', 'p', 'C', 'Abb', ...
                 'Kcal'}}, ...
'Ubbtl',      {{'Abbstable', 'g', 'n', 'h', 'p', 'Ubb'}}));
        checks.(X).(n) = [alwayschecks, KcalPhibbThetabbchecks];

        %%%%%%%%%%%%%%%%%%%%%% create instances with Lcal %%%%%%%%%%%%%%%%%%%%%%

        for X = allX
            LcalX = ['Lcal', X{:}];
            allX = [allX, LcalX];
            props.(LcalX).(ds) = props.(X{:}).(ds);
            checks.(LcalX).(n) = [checks.(X{:}).(n), 'Lcalsize'];
        end%for

        %%%%%%%%%%%%%%%%%%%%%%%%% fill in other fields %%%%%%%%%%%%%%%%%%%%%%%%%

        for X = allX
            props.(X{:}).(b) = base;
            props.(X{:}).stoch = stoch;
            props.(X{:}).(dp) = fieldnames(props.(X{:}).(ds))';
            props.(X{:}).depends = calcdepends(props.(X{:}).(b), ...
                                               props.(X{:}).(dp), ...
                                               props.(X{:}).(ds));
            nd = numel(props.(X{:}).(dp));
            props.(X{:}).current = cell2struct( ...
                                     mat2cell(false(nd, 1), ones(nd, 1), 1), ...
                                     props.(X{:}).(dp));
            props.(X{:}).store = structfun(@(x) [], props.(X{:}).current, ...
                                           'UniformOutput', false());
            checks.(X{:}).depends = struct();
            for cn = fieldnames(allchecks)'
                if ismember(cn{:}, checks.(X{:}).(n))
                    checks.(X{:}).depends.(cn{:}) = allchecks.(cn{:});
                end%if
            end%for
            nc = numel(checks.(X{:}).(n));
            checks.(X{:}).current = cell2struct( ...
                                     mat2cell(false(nc, 1), ones(nc, 1), 1), ...
                                     checks.(X{:}).(n));
        end%for

        %%%%%%%%%%%%%%%%%%%%%%%%%%%% save mat file %%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        filename = [cemodel.installdir(), 'cemodel_constants.mat'];
        if isoctave()
            save('-7', filename, 'props', 'checks');
        else % Matlab
            save(filename, 'props', 'checks', '-v7');
        end%if
    end%function generate_constants

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function d = installdir()
        mf = mfilename();
        mff = mfilename('fullpath');
        d = mff(1:end-numel(mf));
    end%function installdir

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function unittests()
        msg = {};

        try
            if isoctave()
                pkg('load', 'statistics');
            end%if

            n = 5;
            p = 4;
            A = randn(n);
            A = A / max(abs(eig(A))) * 0.9;
            C = randn(p, n);
            m = 3;
            B = randn(n, m);
            D = randn(p, m);
            g = 2;
            G = randn(n, g);
            h = p;
            H = randn(p, h);
            QRS = randn(g + h);
            QRS = QRS*QRS';
            Q = QRS(1:g, 1:g);
            R = QRS(g+1:end, g+1:end);
            S = QRS(1:g, g+1:end);

            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

            mdl = cemodel();
            if ~strcmp(mdl.spec.BD, '') ...
               || (~strcmp(mdl.spec.GH, '') ...
               || (~isequal(mdl.spec.stoch, {}) ...
               || (~strcmp(mdl.spec.stochtype, 'none') ...
               || (~mdl.isempty() ...
               || (~strcmp(mdl.spec.string, 'none') ...
               || (mdl.spec.Lcal ...
               || mdl.spec.Kcal))))))
                msg = [msg, ['failed (line ', num2str(getline()), '): ', ...
                             'error with empty model']];
            end%if

            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

            mdl.A = A;
            mdl.C = C;
            if ~strcmp(mdl.spec.BD, '') ...
               || (~strcmp(mdl.spec.GH, '') ...
               || (~isequal(mdl.spec.stoch, {}) ...
               || (~strcmp(mdl.spec.stochtype, 'none') ...
               || (mdl.isempty() ...
               || (~strcmp(mdl.spec.string, 'none') ...
               || (mdl.spec.Lcal ...
               || mdl.spec.Kcal))))))
                msg = [msg, ['failed (line ', num2str(getline()), '): ', ...
                             'error adding A and C']];
            end%if

            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

            mdl.B = B;
            if ~strcmp(mdl.spec.BD, 'B') ...
               || (~strcmp(mdl.spec.GH, '') ...
               || (~isequal(mdl.spec.stoch, {}) ...
               || (~strcmp(mdl.spec.stochtype, 'none') ...
               || (mdl.isempty() ...
               || (~strcmp(mdl.spec.string, 'none') ...
               || (mdl.spec.Lcal ...
               || mdl.spec.Kcal))))))
                msg = [msg, ['failed (line ', num2str(getline()), '): ', ...
                             'error adding B']];
            end%if

            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

            mdl.D = D;
            if ~strcmp(mdl.spec.BD, 'BD') ...
               || (~strcmp(mdl.spec.GH, '') ...
               || (~isequal(mdl.spec.stoch, {}) ...
               || (~strcmp(mdl.spec.stochtype, 'none') ...
               || (mdl.isempty() ...
               || (~strcmp(mdl.spec.string, 'none') ...
               || (mdl.spec.Lcal ...
               || mdl.spec.Kcal))))))
                msg = [msg, ['failed (line ', num2str(getline()), '): ', ...
                             'error adding D']];
            end%if

            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

            mdl.B = [];
            if ~strcmp(mdl.spec.BD, 'D') ...
               || (~strcmp(mdl.spec.GH, '') ...
               || (~isequal(mdl.spec.stoch, {}) ...
               || (~strcmp(mdl.spec.stochtype, 'none') ...
               || (mdl.isempty() ...
               || (~strcmp(mdl.spec.string, 'none') ...
               || (mdl.spec.Lcal ...
               || mdl.spec.Kcal))))))
                msg = [msg, ['failed (line ', num2str(getline()), '): ', ...
                             'error removing B']];
            end%if

            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

            mdl.D = [];
            if ~strcmp(mdl.spec.BD, '') ...
               || (~strcmp(mdl.spec.GH, '') ...
               || (~isequal(mdl.spec.stoch, {}) ...
               || (~strcmp(mdl.spec.stochtype, 'none') ...
               || (mdl.isempty() ...
               || (~strcmp(mdl.spec.string, 'none') ...
               || (mdl.spec.Lcal ...
               || mdl.spec.Kcal))))))
                msg = [msg, ['failed (line ', num2str(getline()), '): ', ...
                             'error removing D']];
            end%if

            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

            mdl.G = G;
            if ~strcmp(mdl.spec.BD, '') ...
               || (~strcmp(mdl.spec.GH, 'G') ...
               || (~isequal(mdl.spec.stoch, {}) ...
               || (~strcmp(mdl.spec.stochtype, 'none') ...
               || (mdl.isempty() ...
               || (~strcmp(mdl.spec.string, 'none') ...
               || (mdl.spec.Lcal ...
               || mdl.spec.Kcal))))))
                msg = [msg, ['failed (line ', num2str(getline()), '): ', ...
                             'error adding G']];
            end%if

            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

            mdl.H = H;
            if ~strcmp(mdl.spec.BD, '') ...
               || (~strcmp(mdl.spec.GH, 'GH') ...
               || (~isequal(mdl.spec.stoch, {}) ...
               || (~strcmp(mdl.spec.stochtype, 'none') ...
               || (mdl.isempty() ...
               || (~strcmp(mdl.spec.string, 'none') ...
               || (mdl.spec.Lcal ...
               || mdl.spec.Kcal))))))
                msg = [msg, ['failed (line ', num2str(getline()), '): ', ...
                             'error adding H']];
            end%if

            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

            mdl.G = [];
            if ~strcmp(mdl.spec.BD, '') ...
               || (~strcmp(mdl.spec.GH, 'H') ...
               || (~isequal(mdl.spec.stoch, {}) ...
               || (~strcmp(mdl.spec.stochtype, 'none') ...
               || (mdl.isempty() ...
               || (~strcmp(mdl.spec.string, 'none') ...
               || (mdl.spec.Lcal ...
               || mdl.spec.Kcal))))))
                msg = [msg, ['failed (line ', num2str(getline()), '): ', ...
                             'error removing G']];
            end%if

            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

            mdl.H = [];
            if ~strcmp(mdl.spec.BD, '') ...
               || (~strcmp(mdl.spec.GH, '') ...
               || (~isequal(mdl.spec.stoch, {}) ...
               || (~strcmp(mdl.spec.stochtype, 'none') ...
               || (mdl.isempty() ...
               || (~strcmp(mdl.spec.string, 'none') ...
               || (mdl.spec.Lcal ...
               || mdl.spec.Kcal))))))
                msg = [msg, ['failed (line ', num2str(getline()), '): ', ...
                             'error removing H']];
            end%if

            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

            mdl.G = G;
            if ~strcmp(mdl.spec.BD, '') ...
               || (~strcmp(mdl.spec.GH, 'G') ...
               || (~isequal(mdl.spec.stoch, {}) ...
               || (~strcmp(mdl.spec.stochtype, 'none') ...
               || (mdl.isempty() ...
               || (~strcmp(mdl.spec.string, 'none') ...
               || (mdl.spec.Lcal ...
               || mdl.spec.Kcal))))))
                msg = [msg, ['failed (line ', num2str(getline()), '): ', ...
                             'error adding G']];
            end%if

            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

            mdl.H = H;
            if ~strcmp(mdl.spec.BD, '') ...
               || (~strcmp(mdl.spec.GH, 'GH') ...
               || (~isequal(mdl.spec.stoch, {}) ...
               || (~strcmp(mdl.spec.stochtype, 'none') ...
               || (mdl.isempty() ...
               || (~strcmp(mdl.spec.string, 'none') ...
               || (mdl.spec.Lcal ...
               || mdl.spec.Kcal))))))
                msg = [msg, ['failed (line ', num2str(getline()), '): ', ...
                             'error adding H']];
            end%if

            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

            for X = {'U', 'Utl'}
                eval([X{:}, ' = mdl.', X{:}, ';']);
            end%for

            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

            mdl.Q = Q;
            if ~strcmp(mdl.spec.BD, '') ...
               || (~strcmp(mdl.spec.GH, 'GH') ...
               || (~isequal(mdl.spec.stoch, {'Q'}) ...
               || (~strcmp(mdl.spec.stochtype, 'QRS') ...
               || (mdl.isempty() ...
               || (~strcmp(mdl.spec.string, 'QRS') ...
               || (mdl.spec.Lcal ...
               || mdl.spec.Kcal))))))
                msg = [msg, ['failed (line ', num2str(getline()), '): ', ...
                             'error adding Q']];
            end%if

            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

            mdl.R = R;
            if ~strcmp(mdl.spec.BD, '') ...
               || (~strcmp(mdl.spec.GH, 'GH') ...
               || (~isequal(mdl.spec.stoch, {'Q', 'R'}) ...
               || (~strcmp(mdl.spec.stochtype, 'QRS') ...
               || (mdl.isempty() ...
               || (~strcmp(mdl.spec.string, 'QRS') ...
               || (mdl.spec.Lcal ...
               || mdl.spec.Kcal))))))
                msg = [msg, ['failed (line ', num2str(getline()), '): ', ...
                             'error adding R']];
            end%if

            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

            mdl.S = S;
            mdl.check();
            if ~strcmp(mdl.spec.BD, '') ...
               || (~strcmp(mdl.spec.GH, 'GH') ...
               || (~isequal(mdl.spec.stoch, {'Q', 'R', 'S'}) ...
               || (~strcmp(mdl.spec.stochtype, 'QRS') ...
               || (mdl.isempty() ...
               || (~strcmp(mdl.spec.string, 'QRS') ...
               || (mdl.spec.Lcal ...
               || mdl.spec.Kcal))))))
                msg = [msg, ['failed (line ', num2str(getline()), '): ', ...
                             'error adding S']];
            end%if

            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

            for X = {'n', 'p', 'm', 'g', 'h'}
                if eval(X{:}) ~= mdl.(X{:})
                    msg = [msg, ['failed (line ', num2str(getline()), '): ', ...
                                 'mdl.', X{:}, ' ~= ', X{:}]];
                end%if
            end%for

            for X = {'Phi', 'Theta', 'Aeigs', 'Astable', 'GQG', 'HRH', ...
                     'GSH', 'P', 'L', 'K', 'M', 'Sigkj', 'Sigkk', 'E', 'T', ...
                     'Abar', 'Abareigs', 'Abarstable', 'Gbar', 'Qbar', ...
                     'Sbar', 'GQGbar', 'GSHbar', 'Pbar', 'Lbar', 'Kbar', ...
                     'Mbar', 'Sigbarkj', 'Sigbarkk', 'Ebar', 'Tbar', ...
                     'Phibar', 'Thetabar'}
                eval([X{:}, ' = mdl.', X{:}, ';']);
            end%for

            dl = struct();

            if isoctave
                [dl.L, dl.Sigkj, dl.Sigkk, ~] = dlqe(A, [], C, GQG, HRH, GSH);
            else % Matlab
                [dl.L, dl.Sigkj, dl.Sigkk, ~] = dlqe(A - GSH * (HRH \ C), ...
                           eye(n), C, forceherm(GQG - GSH * (HRH \ GSH')), HRH);
            end%if

            for X = {'L', 'Sigkj', 'Sigkk'}
                if ~comp(eval(X{:}), dl.(X{:}))
                    msg = [msg, ['failed (line ', num2str(getline()), '): ', ...
                                 X{:}, ' calcuated from dlqe is different ', ...
                                 'than ', X{:}, ' calculated from cemodel']];
                end%if
            end%for

            for X = {'U', 'Utl'}
                if ~comp(eval(X{:}), mdl.(X{:}))
                    msg = [msg, ['failed (line ', num2str(getline()), '): ', ...
                                 'inconsistent calculation of ' X{:}]];
                end%if
            end%for

            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

            try
                mdl.simulate('Nd', 10);
            catch
                msg = [msg, ['failed (line ', num2str(getline()), '): ', ...
                             'error using simulate function']];
            end%trycatch

            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

            try
                mdl.simulate('Nd', 10, 'x1', randn(n, 1));
            catch
                msg = [msg, ['failed (line ', num2str(getline()), '): ', ...
                             'error using simulate function']];
            end%trycatch

            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

            mdl.Q = [];
            mdl.R = [];
            mdl.S = [];

            Q2 = randn(n);
            Q2 = Q2*Q2';
            R2 = randn(p);
            R2 = R2*R2';
            mdl_genKcal = cemodel('A', A, 'C', C, 'Q', Q2, 'R', R2);
            Kcal = mdl_genKcal.K;

            mdl.Kcal = Kcal;
            if ~strcmp(mdl.spec.BD, '') ...
               || (~strcmp(mdl.spec.GH, 'GH') ...
               || (~isequal(mdl.spec.stoch, {'Kcal'}) ...
               || (~strcmp(mdl.spec.stochtype, 'none') ...
               || (mdl.isempty() ...
               || (~strcmp(mdl.spec.string, 'Kcalnone') ...
               || (mdl.spec.Lcal ...
               || ~mdl.spec.Kcal))))))
                msg = [msg, ['failed (line ', num2str(getline()), '): ', ...
                             'error adding Kcal']];
            end%if

            for X = {'Ubb', 'Ubbtl'}
                eval([X{:}, ' = mdl.', X{:}, ';']);
            end%for

            for X = {'U', 'Utl'}
                if ~comp(eval(X{:}), mdl.(X{:}))
                    msg = [msg, ['failed (line ', num2str(getline()), '): ', ...
                                 'inconsistent calculation of ' X{:}]];
                end%if
            end%for

            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

            mdl.Q = Q;
            mdl.R = R;
            mdl.S = S;

            if ~strcmp(mdl.spec.BD, '') ...
               || (~strcmp(mdl.spec.GH, 'GH') ...
               || (~isequal(mdl.spec.stoch, {'Kcal', 'Q', 'R', 'S'}) ...
               || (~strcmp(mdl.spec.stochtype, 'QRS') ...
               || (mdl.isempty() ...
               || (~strcmp(mdl.spec.string, 'KcalQRS') ...
               || (mdl.spec.Lcal ...
               || ~mdl.spec.Kcal))))))
                msg = [msg, ['failed (line ', num2str(getline()), '): ', ...
                             'error adding Q, R, S back']];
            end%if

            for X = {'Phibb', 'Thetabb', 'Abb', 'Abbeigs', 'Abbstable', ...
                     'Gbb', 'GQGbb', 'GSHbb', 'Pbb', 'Lbb', 'Kbb', 'Mbb', ...
                     'Sigbbkj', 'Sigbbkk', 'Ebb', 'Tbb'}
                eval([X{:}, ' = mdl.', X{:}, ';']);
            end%for

            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

            Lcal = ones(n, p);
            mdl.Lcal = Lcal;
            if ~strcmp(mdl.spec.BD, '') ...
               || (~strcmp(mdl.spec.GH, 'GH') ...
               || (~isequal(mdl.spec.stoch, {'Kcal', 'Q', 'R', 'S', 'Lcal'}) ...
               || (~strcmp(mdl.spec.stochtype, 'QRS') ...
               || (mdl.isempty() ...
               || (~strcmp(mdl.spec.string, 'LcalKcalQRS') ...
               || (~mdl.spec.Lcal ...
               || ~mdl.spec.Kcal))))))
                msg = [msg, ['failed (line ', num2str(getline()), '): ', ...
                             'error adding Lcal']];
            end%if

            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

            caughterr = false();
            try
                mdl.Phi = randn(n, n);
            catch
                caughterr = true();
            end%trycatch
            if ~caughterr
                msg = [msg, ['failed (line ', num2str(getline()), '): ', ...
                             'adding Phi should cause an error, but does not']];
            end%if

            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

            caughterr = false();
            try
                mdl.Theta = randn(n, p);
            catch
                caughterr = true();
            end%trycatch
            if ~caughterr
                msg = [msg, ['failed (line ', num2str(getline()), '): ', ...
                             'adding Theta should cause an error, but ', ...
                             'does not']];
            end%if

            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

            caughterr = false();
            try
                mdl.Phibb = randn(n, n);
            catch
                caughterr = true();
            end%trycatch
            if ~caughterr
                msg = [msg, ['failed (line ', num2str(getline()), '): ', ...
                             'adding Phibb should cause an error, but ', ...
                             'does not']];
            end%if

            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

            caughterr = false();
            try
                mdl.Thetabb = randn(n, p);
            catch
                caughterr = true();
            end%trycatch
            if ~caughterr
                msg = [msg, ['failed (line ', num2str(getline()), '): ', ...
                             'adding Thetabb should cause an error, but ', ...
                             'does not']];
            end%if

            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

            mdl.R = [];
            if ~strcmp(mdl.spec.BD, '') ...
               || (~strcmp(mdl.spec.GH, 'GH') ...
               || (~isequal(mdl.spec.stoch, {'Kcal', 'Q', 'S', 'Lcal'}) ...
               || (~strcmp(mdl.spec.stochtype, 'QRS') ...
               || (mdl.isempty() ...
               || (~strcmp(mdl.spec.string, 'LcalKcalQRS') ...
               || (~mdl.spec.Lcal ...
               || ~mdl.spec.Kcal))))))
                msg = [msg, ['failed (line ', num2str(getline()), '): ', ...
                             'error removing R']];
            end%if

            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

            mdl.S = [];
            if ~strcmp(mdl.spec.BD, '') ...
               || (~strcmp(mdl.spec.GH, 'GH') ...
               || (~isequal(mdl.spec.stoch, {'Kcal', 'Q', 'Lcal'}) ...
               || (~strcmp(mdl.spec.stochtype, 'QRS') ...
               || (mdl.isempty() ...
               || (~strcmp(mdl.spec.string, 'LcalKcalQRS') ...
               || (~mdl.spec.Lcal ...
               || ~mdl.spec.Kcal))))))
                msg = [msg, ['failed (line ', num2str(getline()), '): ', ...
                             'error removing S']];
            end%if

            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

            mdl.Q = [];
            if ~strcmp(mdl.spec.BD, '') ...
               || (~strcmp(mdl.spec.GH, 'GH') ...
               || (~isequal(mdl.spec.stoch, {'Kcal', 'Lcal'}) ...
               || (~strcmp(mdl.spec.stochtype, 'none') ...
               || (mdl.isempty() ...
               || (~strcmp(mdl.spec.string, 'LcalKcalnone') ...
               || (~mdl.spec.Lcal ...
               || ~mdl.spec.Kcal))))))
                msg = [msg, ['failed (line ', num2str(getline()), '): ', ...
                             'error removing Q']];
            end%if

            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

            mdl.Lcal = [];
            if ~strcmp(mdl.spec.BD, '') ...
               || (~strcmp(mdl.spec.GH, 'GH') ...
               || (~isequal(mdl.spec.stoch, {'Kcal'}) ...
               || (~strcmp(mdl.spec.stochtype, 'none') ...
               || (mdl.isempty() ...
               || (~strcmp(mdl.spec.string, 'Kcalnone') ...
               || (mdl.spec.Lcal ...
               || ~mdl.spec.Kcal))))))
                msg = [msg, ['failed (line ', num2str(getline()), '): ', ...
                             'error removing Lcal']];
            end%if

            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

            mdl.Kcal = [];
            if ~strcmp(mdl.spec.BD, '') ...
               || (~strcmp(mdl.spec.GH, 'GH') ...
               || (~isequal(mdl.spec.stoch, {}) ...
               || (~strcmp(mdl.spec.stochtype, 'none') ...
               || (mdl.isempty() ...
               || (~strcmp(mdl.spec.string, 'none') ...
               || (mdl.spec.Lcal ...
               || mdl.spec.Kcal))))))
                msg = [msg, ['failed (line ', num2str(getline()), '): ', ...
                             'error removing Kcal']];
            end%if

            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

            mdl.Phi = Phi;
            if ~strcmp(mdl.spec.BD, '') ...
               || (~strcmp(mdl.spec.GH, 'GH') ...
               || (~isequal(mdl.spec.stoch, {'Phi'}) ...
               || (~strcmp(mdl.spec.stochtype, 'PhiTheta') ...
               || (mdl.isempty() ...
               || (~strcmp(mdl.spec.string, 'PhiTheta') ...
               || (mdl.spec.Lcal ...
               || mdl.spec.Kcal))))))
                msg = [msg, ['failed (line ', num2str(getline()), '): ', ...
                             'error adding Phi']];
            end%if

            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

            mdl.Theta = Theta;
            if ~strcmp(mdl.spec.BD, '') ...
               || (~strcmp(mdl.spec.GH, 'GH') ...
               || (~isequal(mdl.spec.stoch, {'Phi', 'Theta'}) ...
               || (~strcmp(mdl.spec.stochtype, 'PhiTheta') ...
               || (mdl.isempty() ...
               || (~strcmp(mdl.spec.string, 'PhiTheta') ...
               || (mdl.spec.Lcal ...
               || mdl.spec.Kcal))))))
                msg = [msg, ['failed (line ', num2str(getline()), '): ', ...
                             'error addin Theta']];
            end%if

            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

            for X = {'Astable', 'K', 'M', 'T', 'Abar', 'Abarstable', 'Gbar', ...
                     'Kbar', 'Mbar', 'Tbar', 'Phibar', 'Thetabar', 'U', 'Utl'}
                if ~comp(eval(X{:}), mdl.(X{:}))
                    msg = [msg, ['failed (line ', num2str(getline()), '): ', ...
                                 'inconsistent calculation of ' X{:}]];
                end%if
            end%for

            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

            mdl.Kcal = Kcal;
            if ~strcmp(mdl.spec.BD, '') ...
               || (~strcmp(mdl.spec.GH, 'GH') ...
               || (~isequal(mdl.spec.stoch, {'Phi', 'Theta', 'Kcal'}) ...
               || (~strcmp(mdl.spec.stochtype, 'PhiTheta') ...
               || (mdl.isempty() ...
               || (~strcmp(mdl.spec.string, 'KcalPhiTheta') ...
               || (mdl.spec.Lcal ...
               || ~mdl.spec.Kcal))))))
                msg = [msg, ['failed (line ', num2str(getline()), '): ', ...
                             'error adding Kcal']];
            end%if

            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

            for X = {'Astable', 'K', 'M', 'T', 'Abar', 'Abarstable', 'Gbar', ...
                     'Kbar', 'Mbar', 'Tbar', 'Phibar', 'Thetabar', 'Phibb', ...
                     'Thetabb', 'Abb', 'Abbstable', 'Gbb', 'Kbb', 'Mbb', ...
                     'Tbb', 'U', 'Utl', 'Ubb', 'Ubbtl'}
                if ~comp(eval(X{:}), mdl.(X{:}))
                    msg = [msg, ['failed (line ', num2str(getline()), '): ', ...
                                 'inconsistent calculation of ' X{:}]];
                end%if
            end%for

            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

            caughterr = false();
            try
                mdl.Q = randn(g, g);
            catch
                caughterr = true();
            end%trycatch
            if ~caughterr
                msg = [msg, ['failed (line ', num2str(getline()), '): ', ...
                             'adding Q should cause an error, but does not']];
            end%if

            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

            caughterr = false();
            try
                mdl.R = randn(h, h);
            catch
                caughterr = true();
            end%trycatch
            if ~caughterr
                msg = [msg, ['failed (line ', num2str(getline()), '): ', ...
                             'adding R should cause an error, but does not']];
            end%if

            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

            caughterr = false();
            try
                mdl.S = randn(g, h);
            catch
                caughterr = true();
            end%trycatch
            if ~caughterr
                msg = [msg, ['failed (line ', num2str(getline()), '): ', ...
                             'adding S should cause an error, but does not']];
            end%if

            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

            caughterr = false();
            try
                mdl.Phibb = randn(n, n);
            catch
                caughterr = true();
            end%trycatch
            if ~caughterr
                msg = [msg, ['failed (line ', num2str(getline()), '): ', ...
                             'adding Phibb should cause an error, but ', ...
                             'does not']];
            end%if

            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

            caughterr = false();
            try
                mdl.Thetabb = randn(n, p);
            catch
                caughterr = true();
            end%trycatch
            if ~caughterr
                msg = [msg, ['failed (line ', num2str(getline()), '): ', ...
                             'adding Thetabb should cause an error, but ', ...
                             'does not']];
            end%if

            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

            mdl.Phi = [];
            mdl.Theta = [];
            if ~strcmp(mdl.spec.BD, '') ...
               || (~strcmp(mdl.spec.GH, 'GH') ...
               || (~isequal(mdl.spec.stoch, {'Kcal'}) ...
               || (~strcmp(mdl.spec.stochtype, 'none') ...
               || (mdl.isempty() ...
               || (~strcmp(mdl.spec.string, 'Kcalnone') ...
               || (mdl.spec.Lcal ...
               || ~mdl.spec.Kcal))))))
                msg = [msg, ['failed (line ', num2str(getline()), '): ', ...
                             'error removing Phi and Theta']];
            end%if

            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

            mdl.Kcal = [];
            if ~strcmp(mdl.spec.BD, '') ...
               || (~strcmp(mdl.spec.GH, 'GH') ...
               || (~isequal(mdl.spec.stoch, {}) ...
               || (~strcmp(mdl.spec.stochtype, 'none') ...
               || (mdl.isempty() ...
               || (~strcmp(mdl.spec.string, 'none') ...
               || (mdl.spec.Lcal ...
               || mdl.spec.Kcal))))))
                msg = [msg, ['failed (line ', num2str(getline()), '): ', ...
                             'error removing Kcal']];
            end%if

            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

            mdl.Thetabb = Thetabb;
            if ~strcmp(mdl.spec.BD, '') ...
               || (~strcmp(mdl.spec.GH, 'GH') ...
               || (~isequal(mdl.spec.stoch, {'Thetabb'}) ...
               || (~strcmp(mdl.spec.stochtype, 'PhibbThetabb') ...
               || (mdl.isempty() ...
               || (~strcmp(mdl.spec.string, 'PhibbThetabb') ...
               || (mdl.spec.Lcal ...
               || mdl.spec.Kcal))))))
                msg = [msg, ['failed (line ', num2str(getline()), '): ', ...
                             'error adding Thetabb']];
            end%if

            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

            mdl.Phibb = Phibb;
            if ~strcmp(mdl.spec.BD, '') ...
               || (~strcmp(mdl.spec.GH, 'GH') ...
               || (~isequal(mdl.spec.stoch, {'Thetabb', 'Phibb'}) ...
               || (~strcmp(mdl.spec.stochtype, 'PhibbThetabb') ...
               || (mdl.isempty() ...
               || (~strcmp(mdl.spec.string, 'PhibbThetabb') ...
               || (mdl.spec.Lcal ...
               || mdl.spec.Kcal))))))
                msg = [msg, ['failed (line ', num2str(getline()), '): ', ...
                             'error adding Phibb']];
            end%if

            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

            for X = {'U', 'Utl'}
                if ~comp(eval(X{:}), mdl.(X{:}))
                    msg = [msg, ['failed (line ', num2str(getline()), '): ', ...
                                 'inconsistent calculation of ' X{:}]];
                end%if
            end%for

            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

            mdl.Kcal = Kcal;
            if ~strcmp(mdl.spec.BD, '') ...
               || (~strcmp(mdl.spec.GH, 'GH') ...
               || (~isequal(mdl.spec.stoch, {'Thetabb', 'Phibb', 'Kcal'}) ...
               || (~strcmp(mdl.spec.stochtype, 'PhibbThetabb') ...
               || (mdl.isempty() ...
               || (~strcmp(mdl.spec.string, 'KcalPhibbThetabb') ...
               || (mdl.spec.Lcal ...
               || ~mdl.spec.Kcal))))))
                msg = [msg, ['failed (line ', num2str(getline()), '): ', ...
                             'error adding Kcal']];
            end%if

            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

            for X = {'Phi', 'Theta', 'K', 'M', 'T', 'Abar', 'Abarstable', ...
                     'Gbar', 'Kbar', 'Mbar', 'Tbar', 'Phibar', 'Thetabar', ...
                     'Abb', 'Abbstable', 'Gbb', 'Kbb', 'Mbb', 'Tbb', 'U', ...
                     'Utl', 'Ubb', 'Ubbtl'}
                if ~comp(eval(X{:}), mdl.(X{:}))
                    msg = [msg, ['failed (line ', num2str(getline()), '): ', ...
                                 'inconsistent calculation of ' X{:}]];
                end%if
            end%for

            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

            caughterr = false();
            try
                mdl.Q = randn(g, g);
            catch
                caughterr = true();
            end%trycatch
            if ~caughterr
                msg = [msg, ['failed (line ', num2str(getline()), '): ', ...
                             'adding Q should cause an error but does not']];
            end%if

            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

            caughterr = false();
            try
                mdl.R = randn(h, h);
            catch
                caughterr = true();
            end%trycatch
            if ~caughterr
                msg = [msg, ['failed (line ', num2str(getline()), '): ', ...
                             'adding R should cause an error but does not']];
            end%if

            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

            caughterr = false();
            try
                mdl.S = randn(g, h);
            catch
                caughterr = true();
            end%trycatch
            if ~caughterr
                msg = [msg, ['failed (line ', num2str(getline()), '): ', ...
                             'adding S should cause an error but does not']];
            end%if

            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

            caughterr = false();
            try
                mdl.Phi = randn(n, n);
            catch
                caughterr = true();
            end%trycatch
            if ~caughterr
                msg = [msg, ['failed (line ', num2str(getline()), '): ', ...
                             'adding Phi should cause an error but does not']];
            end%if

            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

            caughterr = false();
            try
                mdl.Theta = randn(n, p);
            catch
                caughterr = true();
            end%trycatch
            if ~caughterr
                msg = [msg, ['failed (line ', num2str(getline()), '): ', ...
                             'adding Theta should cause an error but does ', ...
                             'not']];
            end%if

            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

            mdl.Kcal = [];
            if ~strcmp(mdl.spec.BD, '') ...
               || (~strcmp(mdl.spec.GH, 'GH') ...
               || (~isequal(mdl.spec.stoch, {'Thetabb', 'Phibb'}) ...
               || (~strcmp(mdl.spec.stochtype, 'PhibbThetabb') ...
               || (mdl.isempty() ...
               || (~strcmp(mdl.spec.string, 'PhibbThetabb') ...
               || (mdl.spec.Lcal ...
               || mdl.spec.Kcal))))))
                msg = [msg, ['failed (line ', num2str(getline()), '): ', ...
                             'error removing Kcal']];
            end%if

            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

            mdl.Phibb = [];
            mdl.Thetabb = [];
            if ~strcmp(mdl.spec.BD, '') ...
               || (~strcmp(mdl.spec.GH, 'GH') ...
               || (~isequal(mdl.spec.stoch, {}) ...
               || (~strcmp(mdl.spec.stochtype, 'none') ...
               || (mdl.isempty() ...
               || (~strcmp(mdl.spec.string, 'none') ...
               || (mdl.spec.Lcal ...
               || mdl.spec.Kcal))))))
                msg = [msg, ['failed (line ', num2str(getline()), '): ', ...
                             'error removing Phibb and Thetabb']];
            end%if

            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

            mdl.G = [];
            mdl.H = [];
            if ~strcmp(mdl.spec.BD, '') ...
               || (~strcmp(mdl.spec.GH, '') ...
               || (~isequal(mdl.spec.stoch, {}) ...
               || (~strcmp(mdl.spec.stochtype, 'none') ...
               || (mdl.isempty() ...
               || (~strcmp(mdl.spec.string, 'none') ...
               || (mdl.spec.Lcal ...
               || mdl.spec.Kcal))))))
                msg = [msg, ['failed (line ', num2str(getline()), '): ', ...
                             'error removing G and H']];
            end%if

            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

            mdl.A = [];
            mdl.C = [];
            if ~strcmp(mdl.spec.BD, '') ...
               || (~strcmp(mdl.spec.GH, '') ...
               || (~isequal(mdl.spec.stoch, {}) ...
               || (~strcmp(mdl.spec.stochtype, 'none') ...
               || (~mdl.isempty() ...
               || (~strcmp(mdl.spec.string, 'none') ...
               || (mdl.spec.Lcal ...
               || mdl.spec.Kcal))))))
                msg = [msg, ['failed (line ', num2str(getline()), '): ', ...
                             'error removing A and C']];
            end%if

            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

            mdl = cemodel('A', A, 'C', C, 'B', ones(n+1));
            caughterr = false();
            try
                mdl.check('ABsize');
            catch
                caughterr = true();
            end%trycatch
            if ~caughterr
                msg = [msg, ['failed (line ', num2str(getline()), '): ', ...
                             'should get an error from check ABsize']];
            end%if

            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

            mdl = cemodel('A', A, 'C', ones(n+1));
            caughterr = false();
            try
                mdl.check('ACsize');
            catch
                caughterr = true();
            end%trycatch
            if ~caughterr
                msg = [msg, ['failed (line ', num2str(getline()), '): ', ...
                             'should get an error from check ACsize']];
            end%if

            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

            mdl = cemodel('A', A, 'C', C, 'D', ones(p+1));
            caughterr = false();
            try
                mdl.check('CDsize');
            catch
                caughterr = true();
            end%trycatch
            if ~caughterr
                msg = [msg, ['failed (line ', num2str(getline()), '): ', ...
                             'should get an error from check CDsize']];
            end%if

            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

            mdl = cemodel('A', A, 'C', C, 'B', ones(n, 1), 'D', ones(p, 2));
            caughterr = false();
            try
                mdl.check('BDsize');
            catch
                caughterr = true();
            end%trycatch
            if ~caughterr
                msg = [msg, ['failed (line ', num2str(getline()), '): ', ...
                             'should get an error from check BDsize']];
            end%if

            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

            mdl = cemodel('A', A, 'C', C, 'Kcal', ones(n+1, p));
            caughterr = false();
            try
                mdl.check('Kcalsize');
            catch
                caughterr = true();
            end%trycatch
            if ~caughterr
                msg = [msg, ['failed (line ', num2str(getline()), '): ', ...
                             'should get an error from check Kcalsize']];
            end%if

            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

            mdl = cemodel('A', A, 'C', C, 'Kcal', ones(n, p+1));
            caughterr = false();
            try
                mdl.check('Kcalsize');
            catch
                caughterr = true();
            end%trycatch
            if ~caughterr
                msg = [msg, ['failed (line ', num2str(getline()), '): ', ...
                             'should get an error from check Kcalsize']];
            end%if

            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

            mdl = cemodel('A', A, 'C', C, 'Q', Q);
            caughterr = false();
            try
                mdl.check('QRSspec');
            catch
                caughterr = true();
            end%trycatch
            if ~caughterr
                msg = [msg, ['failed (line ', num2str(getline()), '): ', ...
                             'should get an error from check QRSspec']];
            end%if

            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

            mdl = cemodel('A', A, 'C', C, 'R', R);
            caughterr = false();
            try
                mdl.check('QRSspec');
            catch
                caughterr = true();
            end%trycatch
            if ~caughterr
                msg = [msg, ['failed (line ', num2str(getline()), '): ', ...
                             'should get an error from check QRSspec']];
            end%if

            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

            mdl = cemodel('A', A, 'C', C, 'S', S);
            caughterr = false();
            try
                mdl.check('QRSspec');
            catch
                caughterr = true();
            end%trycatch
            if ~caughterr
                msg = [msg, ['failed (line ', num2str(getline()), '): ', ...
                             'should get an error from check QRSspec']];
            end%if

            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

            mdl = cemodel('A', A, 'C', C, 'Q', Q, 'G', ones(n+1, g));
            caughterr = false();
            try
                mdl.check('GQsize');
            catch
                caughterr = true();
            end%trycatch
            if ~caughterr
                msg = [msg, ['failed (line ', num2str(getline()), '): ', ...
                             'should get an error from check GQsize']];
            end%if

            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

            mdl = cemodel('A', A, 'C', C, 'Q', Q, 'G', ones(n, g+1));
            caughterr = false();
            try
                mdl.check('GQsize');
            catch
                caughterr = true();
            end%trycatch
            if ~caughterr
                msg = [msg, ['failed (line ', num2str(getline()), '): ', ...
                             'should get an error from check GQsize']];
            end%if

            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

            mdl = cemodel('A', A, 'C', C, 'Q', ones(n+1));
            caughterr = false();
            try
                mdl.check('GQsize');
            catch
                caughterr = true();
            end%trycatch
            if ~caughterr
                msg = [msg, ['failed (line ', num2str(getline()), '): ', ...
                             'should get an error from check GQsize']];
            end%if

            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

            mdl = cemodel('A', A, 'C', C, 'R', R, 'H', ones(p+1, h));
            caughterr = false();
            try
                mdl.check('HRsize');
            catch
                caughterr = true();
            end%trycatch
            if ~caughterr
                msg = [msg, ['failed (line ', num2str(getline()), '): ', ...
                             'should get an error from check HRsize']];
            end%if

            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

            mdl = cemodel('A', A, 'C', C, 'R', R, 'H', ones(p, h+1));
            caughterr = false();
            try
                mdl.check('HRsize');
            catch
                caughterr = true();
            end%trycatch
            if ~caughterr
                msg = [msg, ['failed (line ', num2str(getline()), '): ', ...
                             'should get an error from check HRsize']];
            end%if

            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

            mdl = cemodel('A', A, 'C', C, 'R', ones(p+1));
            caughterr = false();
            try
                mdl.check('HRsize');
            catch
                caughterr = true();
            end%trycatch
            if ~caughterr
                msg = [msg, ['failed (line ', num2str(getline()), '): ', ...
                             'should get an error from check HRsize']];
            end%if

            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

            mdl = cemodel('A', A, 'C', C, 'S', S, 'G', ones(n, g+1));
            caughterr = false();
            try
                mdl.check('Ssize');
            catch
                caughterr = true();
            end%trycatch
            if ~caughterr
                msg = [msg, ['failed (line ', num2str(getline()), '): ', ...
                             'should get an error from check Ssize']];
            end%if

            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

            mdl = cemodel('A', A, 'C', C, 'S', S, 'H', ones(n, h+1));
            caughterr = false();
            try
                mdl.check('Ssize');
            catch
                caughterr = true();
            end%trycatch
            if ~caughterr
                msg = [msg, ['failed (line ', num2str(getline()), '): ', ...
                             'should get an error from check Ssize']];
            end%if

            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

            mdl = cemodel('A', A, 'C', C, 'S', S, 'Q', ones(g+1));
            caughterr = false();
            try
                mdl.check('Ssize');
            catch
                caughterr = true();
            end%trycatch
            if ~caughterr
                msg = [msg, ['failed (line ', num2str(getline()), '): ', ...
                             'should get an error from check Ssize']];
            end%if

            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

            mdl = cemodel('A', A, 'C', C, 'S', S, 'R', ones(h+1));
            caughterr = false();
            try
                mdl.check('Ssize');
            catch
                caughterr = true();
            end%trycatch
            if ~caughterr
                msg = [msg, ['failed (line ', num2str(getline()), '): ', ...
                             'should get an error from check Ssize']];
            end%if

            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

            mdl = cemodel('A', A, 'C', C, 'Q', zeros(g), 'R', zeros(h), ...
                          'S', ones(g, h));
            caughterr = false();
            try
                mdl.check('QRSdefinite');
            catch
                caughterr = true();
            end%trycatch
            if ~caughterr
                msg = [msg, ['failed (line ', num2str(getline()), '): ', ...
                             'should get an error from check QRSdefinite']];
            end%if

            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

            mdl = cemodel('A', A, 'C', C, 'Phi', Phi);
            caughterr = false();
            try
                mdl.check('PhiThetaspec');
            catch
                caughterr = true();
            end%trycatch
            if ~caughterr
                msg = [msg, ['failed (line ', num2str(getline()), '): ', ...
                             'should get an error from check PhiThetaspec']];
            end%if

            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

            mdl = cemodel('A', A, 'C', C, 'Theta', Theta);
            caughterr = false();
            try
                mdl.check('PhiThetaspec');
            catch
                caughterr = true();
            end%trycatch
            if ~caughterr
                msg = [msg, ['failed (line ', num2str(getline()), '): ', ...
                             'should get an error from check PhiThetaspec']];
            end%if

            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

            mdl = cemodel('A', A/max(abs(eig(A)))*1.5, 'C', C, 'Phi', Phi, ...
                          'Theta', Theta);
            caughterr = false();
            try
                mdl.check('PhiThetaspec');
            catch
                caughterr = true();
            end%trycatch
            if ~caughterr
                msg = [msg, ['failed (line ', num2str(getline()), '): ', ...
                             'should get an error from check PhiThetaspec']];
            end%if

            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

            mdl = cemodel('A', A, 'C', C, 'Phi', ones(n+1));
            caughterr = false();
            try
                mdl.check('Phisize');
            catch
                caughterr = true();
            end%trycatch
            if ~caughterr
                msg = [msg, ['failed (line ', num2str(getline()), '): ', ...
                             'should get an error from check Phisize']];
            end%if

            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

            mdl = cemodel('A', A, 'C', C, 'Phi', ones(p), 'H', ones(p+1));
            caughterr = false();
            try
                mdl.check('Phisize');
            catch
                caughterr = true();
            end%trycatch
            if ~caughterr
                msg = [msg, ['failed (line ', num2str(getline()), '): ', ...
                             'should get an error from check Phisize']];
            end%if

            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

            mdl = cemodel('A', A, 'C', C, 'Theta', ones(n+1,p));
            caughterr = false();
            try
                mdl.check('Thetasize');
            catch
                caughterr = true();
            end%trycatch
            if ~caughterr
                msg = [msg, ['failed (line ', num2str(getline()), '): ', ...
                             'should get an error from check Thetasize']];
            end%if

            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

            mdl = cemodel('A', A, 'C', C, 'Theta', ones(n,p+1));
            caughterr = false();
            try
                mdl.check('Thetasize');
            catch
                caughterr = true();
            end%trycatch
            if ~caughterr
                msg = [msg, ['failed (line ', num2str(getline()), '): ', ...
                             'should get an error from check Thetasize']];
            end%if

            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

            mdl = cemodel('A', A, 'C', C, 'Theta', Theta, 'G', ones(n+1, g));
            caughterr = false();
            try
                mdl.check('Thetasize');
            catch
                caughterr = true();
            end%trycatch
            if ~caughterr
                msg = [msg, ['failed (line ', num2str(getline()), '): ', ...
                             'should get an error from check Thetasize']];
            end%if

            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

            mdl = cemodel('A', A, 'C', C, 'Theta', Theta, 'H', ones(p+1, h));
            caughterr = false();
            try
                mdl.check('Thetasize');
            catch
                caughterr = true();
            end%trycatch
            if ~caughterr
                msg = [msg, ['failed (line ', num2str(getline()), '): ', ...
                             'should get an error from check Thetasize']];
            end%if

            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

            mdl = cemodel('A', A, 'C', C, 'Phi', ones(2), 'Theta', ones(3));
            caughterr = false();
            try
                mdl.check('PhiThetasize');
            catch
                caughterr = true();
            end%trycatch
            if ~caughterr
                msg = [msg, ['failed (line ', num2str(getline()), '): ', ...
                             'should get an error from check PhiThetasize']];
            end%if

            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

            mdl = cemodel('A', A, 'C', C, 'Phibb', Phibb);
            caughterr = false();
            try
                mdl.check('PhibbThetabbspec');
            catch
                caughterr = true();
            end%trycatch
            if ~caughterr
                msg = [msg, ['failed (line ', num2str(getline()), '): ', ...
                             'should get an error from check ', ...
                             'PhibbThetabbspec']];
            end%if

            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

            mdl = cemodel('A', A, 'C', C, 'Thetabb', Thetabb);
            caughterr = false();
            try
                mdl.check('PhibbThetabbspec');
            catch
                caughterr = true();
            end%trycatch
            if ~caughterr
                msg = [msg, ['failed (line ', num2str(getline()), '): ', ...
                             'should get an error from check ', ...
                             'PhibbThetabbspec']];
            end%if

            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

            mdl = cemodel('A', A, 'C', C, 'Phibb', Phibb, 'Kcal', Kcal);
            caughterr = false();
            try
                mdl.check('PhibbThetabbspec');
            catch
                caughterr = true();
            end%trycatch
            if ~caughterr
                msg = [msg, ['failed (line ', num2str(getline()), '): ', ...
                             'should get an error from check ', ...
                             'PhibbThetabbspec']];
            end%if

            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

            mdl = cemodel('A', A, 'C', C, 'Thetabb', Thetabb, 'Kcal', Kcal);
            caughterr = false();
            try
                mdl.check('PhibbThetabbspec');
            catch
                caughterr = true();
            end%trycatch
            if ~caughterr
                msg = [msg, ['failed (line ', num2str(getline()), '): ', ...
                             'should get an error from check ', ...
                             'PhibbThetabbspec']];
            end%if

            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

            mdl = cemodel('A', 2, 'C', 0, 'Kcal', 1, 'Phibb', 1, 'Thetabb', 1);
            caughterr = false();
            try
                mdl.check('PhibbThetabbspec');
            catch
                caughterr = true();
            end%trycatch
            if ~caughterr
                msg = [msg, ['failed (line ', num2str(getline()), '): ', ...
                             'should get an error from check ', ...
                             'PhibbThetabbspec']];
            end%if

            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

            mdl = cemodel('A', 2, 'C', 1, 'Kcal', 0, 'Phibb', 1, 'Thetabb', 1);
            caughterr = false();
            try
                mdl.check('PhibbThetabbspec');
            catch
                caughterr = true();
            end%trycatch
            if ~caughterr
                msg = [msg, ['failed (line ', num2str(getline()), '): ', ...
                             'should get an error from check ', ...
                             'PhibbThetabbspec']];
            end%if

            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

            mdl = cemodel('A', A, 'C', C, 'Phibb', ones(n+1));
            caughterr = false();
            try
                mdl.check('Phibbsize');
            catch
                caughterr = true();
            end%trycatch
            if ~caughterr
                msg = [msg, ['failed (line ', num2str(getline()), '): ', ...
                             'should get an error from check Phibbsize']];
            end%if

            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

            mdl = cemodel('A', A, 'C', C, 'Phibb', ones(p), 'H', ones(p+1));
            caughterr = false();
            try
                mdl.check('Phibbsize');
            catch
                caughterr = true();
            end%trycatch
            if ~caughterr
                msg = [msg, ['failed (line ', num2str(getline()), '): ', ...
                             'should get an error from check Phibbsize']];
            end%if

            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

            mdl = cemodel('A', A, 'C', C, 'Thetabb', ones(n+1,p));
            caughterr = false();
            try
                mdl.check('Thetabbsize');
            catch
                caughterr = true();
            end%trycatch
            if ~caughterr
                msg = [msg, ['failed (line ', num2str(getline()), '): ', ...
                             'should get an error from check Thetabbsize']];
            end%if

            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

            mdl = cemodel('A', A, 'C', C, 'Thetabb', ones(n,p+1));
            caughterr = false();
            try
                mdl.check('Thetabbsize');
            catch
                caughterr = true();
            end%trycatch
            if ~caughterr
                msg = [msg, ['failed (line ', num2str(getline()), '): ', ...
                             'should get an error from check Thetabbsize']];
            end%if

            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

            mdl = cemodel('A', A, 'C', C, 'Thetabb', Thetabb, ...
                          'G', ones(n+1, g));
            caughterr = false();
            try
                mdl.check('Thetabbsize');
            catch
                caughterr = true();
            end%trycatch
            if ~caughterr
                msg = [msg, ['failed (line ', num2str(getline()), '): ', ...
                             'should get an error from check Thetabbsize']];
            end%if

            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

            mdl = cemodel('A', A, 'C', C, 'Thetabb', Thetabb, ...
                          'H', ones(p+1, h));
            caughterr = false();
            try
                mdl.check('Thetabbsize');
            catch
                caughterr = true();
            end%trycatch
            if ~caughterr
                msg = [msg, ['failed (line ', num2str(getline()), '): ', ...
                             'should get an error from check Thetabbsize']];
            end%if

            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

            mdl = cemodel('A', A, 'C', C, 'Phibb', ones(2), ...
                          'Thetabb', ones(3));
            caughterr = false();
            try
                mdl.check('PhibbThetabbsize');
            catch
                caughterr = true();
            end%trycatch
            if ~caughterr
                msg = [msg, ['failed (line ', num2str(getline()), '): ', ...
                             'should get an error from check ', ...
                             'PhibbThetabbsize']];
            end%if

            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

            mdl = cemodel('A', A, 'C', C);
            caughterr = false();
            try
                mdl.flags = 1;
            catch
                caughterr = true();
            end%trycatch
            if ~caughterr
                msg = [msg, ['failed (line ', num2str(getline()), '): ', ...
                             'should get an error from set.flags']];
            end%if

            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

            caughterr = false();
            try
                compatible(cemodel('A', A, 'C', C));
            catch
                caughterr = true();
            end%trycatch
            if ~caughterr
                msg = [msg, ['failed (line ', num2str(getline()), '): ', ...
                             'should get an error from compatible']];
            end%if

            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

            caughterr = false();
            try
                compatible(cemodel('A', A, 'C', C), 1);
            catch
                caughterr = true();
            end%trycatch
            if ~caughterr
                msg = [msg, ['failed (line ', num2str(getline()), '): ', ...
                             'should get an error from compatible']];
            end%if

            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

            caughterr = false();
            try
                compatible(cemodel('A', A, 'C', C), 1, 1);
            catch
                caughterr = true();
            end%trycatch
            if ~caughterr
                msg = [msg, ['failed (line ', num2str(getline()), '): ', ...
                             'should get an error from compatible']];
            end%if

            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

            mdl = cemodel('A', ones(2), 'C', ones(2));
            dat = cedata('y', ones(3, 10));

            if mdl.compatible(dat)
                msg = [msg, ['failed (line ', num2str(getline()), '): ', ...
                             'mdl and dat should not be compatible']];
            end%if

            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

            mdl = cemodel('A', ones(2), 'C', ones(2), 'B', ones(2));
            dat = cedata('y', ones(2, 10), 'u', ones(3, 10));

            if mdl.compatible(dat)
                msg = [msg, ['failed (line ', num2str(getline()), '): ', ...
                             'mdl and dat should not be compatible']];
            end%if

            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

            dat = cedata('y', ones(2, 10), 'u', ones(2, 5));

            if mdl.compatible(dat)
                msg = [msg, ['failed (line ', num2str(getline()), '): ', ...
                             'mdl and dat should not be compatible']];
            end%if

            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

            mdl = cemodel('A', ones(2), 'C', ones(2));
            dat = cedata('y', ones(2, 10), 'u', ones(2, 10));

            if mdl.compatible(dat)
                msg = [msg, ['failed (line ', num2str(getline()), '): ', ...
                             'mdl and dat should not be compatible']];
            end%if

            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

            mdl = cemodel('A', ones(2), 'C', ones(2), 'B', ones(2));
            dat = cedata('y', ones(2, 10));

            if mdl.compatible(dat)
                msg = [msg, ['failed (line ', num2str(getline()), '): ', ...
                             'mdl and dat should not be compatible']];
            end%if

            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

            mdl = cemodel('A', ones(2), 'C', ones(2));
            dat = cedata('y', ones(2, 10), 'x', ones(3, 10));

            if mdl.compatible(dat)
                msg = [msg, ['failed (line ', num2str(getline()), '): ', ...
                             'mdl and dat should not be compatible']];
            end%if

            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

            dat = cedata('y', ones(2, 10), 'x', ones(2, 5));

            if mdl.compatible(dat)
                msg = [msg, ['failed (line ', num2str(getline()), '): ', ...
                             'mdl and dat should not be compatible']];
            end%if

            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

            mdl = cemodel('A', ones(2), 'C', ones(2), 'Phi', Phi, ...
                          'Theta', Theta);

            caughterr = false();
            try
                mdl.simulate();
            catch
                caughterr = true();
            end%trycatch
            if ~caughterr
                msg = [msg, ['failed (line ', num2str(getline()), '): ', ...
                             'should get an error from simulate']];
            end%if

            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

            mdl = cemodel('A', ones(2), 'C', ones(2), 'Kcal', Kcal, ...
                          'Phibb', Phibb, 'Thetabb', Thetabb);

            caughterr = false();
            try
                mdl.simulate();
            catch
                caughterr = true();
            end%trycatch
            if ~caughterr
                msg = [msg, ['failed (line ', num2str(getline()), '): ', ...
                             'should get an error from simulate']];
            end%if

            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

            mdl = cemodel('A', ones(2), 'C', ones(2));

            caughterr = false();
            try
                mdl.simulate('Nd', -2.5);
            catch
                caughterr = true();
            end%trycatch
            if ~caughterr
                msg = [msg, ['failed (line ', num2str(getline()), '): ', ...
                             'should get an error from simulate']];
            end%if

            caughterr = false();
            try
                mdl.simulate('Nd', -2);
            catch
                caughterr = true();
            end%trycatch
            if ~caughterr
                msg = [msg, ['failed (line ', num2str(getline()), '): ', ...
                             'should get an error from simulate']];
            end%if

            caughterr = false();
            try
                mdl.simulate('Nd', 0);
            catch
                caughterr = true();
            end%trycatch
            if ~caughterr
                msg = [msg, ['failed (line ', num2str(getline()), '): ', ...
                             'should get an error from simulate']];
            end%if

            caughterr = false();
            try
                mdl.simulate('Nd', 0.5);
            catch
                caughterr = true();
            end%trycatch
            if ~caughterr
                msg = [msg, ['failed (line ', num2str(getline()), '): ', ...
                             'should get an error from simulate']];
            end%if

            caughterr = false();
            try
                mdl.simulate('Nd', 1.5);
            catch
                caughterr = true();
            end%trycatch
            if ~caughterr
                msg = [msg, ['failed (line ', num2str(getline()), '): ', ...
                             'should get an error from simulate']];
            end%if

            caughterr = false();
            try
                mdl.simulate('u', ones(2, 10));
            catch
                caughterr = true();
            end%trycatch
            if ~caughterr
                msg = [msg, ['failed (line ', num2str(getline()), '): ', ...
                             'should get an error from simulate']];
            end%if

            mdl = cemodel('A', ones(2), 'C', ones(2), 'B', ones(2));

            caughterr = false();
            try
                mdl.simulate('u', 'hi there');
            catch
                caughterr = true();
            end%trycatch
            if ~caughterr
                msg = [msg, ['failed (line ', num2str(getline()), '): ', ...
                             'should get an error from simulate']];
            end%if

            caughterr = false();
            try
                mdl.simulate('u', ones(3, 10));
            catch
                caughterr = true();
            end%trycatch
            if ~caughterr
                msg = [msg, ['failed (line ', num2str(getline()), '): ', ...
                             'should get an error from simulate']];
            end%if

            mdl = cemodel('A', ones(2), 'C', ones(2));

            caughterr = false();
            try
                mdl.simulate();
            catch
                caughterr = true();
            end%trycatch
            if ~caughterr
                msg = [msg, ['failed (line ', num2str(getline()), '): ', ...
                             'should get an error from simulate']];
            end%if

            caughterr = false();
            try
                mdl.simulate('Nd', 10, 'x1', 'hi there');
            catch
                caughterr = true();
            end%trycatch
            if ~caughterr
                msg = [msg, ['failed (line ', num2str(getline()), '): ', ...
                             'should get an error from simulate']];
            end%if

            caughterr = false();
            try
                mdl.simulate('Nd', 10, 'x1', ones(1, 2));
            catch
                caughterr = true();
            end%trycatch
            if ~caughterr
                msg = [msg, ['failed (line ', num2str(getline()), '): ', ...
                             'should get an error from simulate']];
            end%if

            caughterr = false();
            try
                mdl.simulate('Nd', 10, 'x1', ones(3, 1));
            catch
                caughterr = true();
            end%trycatch
            if ~caughterr
                msg = [msg, ['failed (line ', num2str(getline()), '): ', ...
                             'should get an error from simulate']];
            end%if

            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

            mdl = cemodel();
            mdl_same = mdl;
            mdl_different = cemodel();
            if ~(mdl == mdl_same) ...
               || (~(mdl ~= mdl_different) ...
               || ((mdl ~= mdl_same) ...
               || (mdl == mdl_different)))
                msg = [msg, ['failed (line ', num2str(getline()), '): ', ...
                             '== and/or ~= are not working as expected']];
            end%if

        catch err
            lns = cellfun(@num2str, {err.stack.line}, 'UniformOutput', false());
            msg = [msg, ['UNEXPECTED ERROR: ', err.message, char(10), ...
                         'look at line(s) ', strjoin(lns, ', ')]];
        end%trycatch

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        if ~isempty(msg)
            disp(['cemodel: unittests failed:', strjoin(msg, char(10))]);
        else
            disp('cemodel: all unittests passed');
        end%if
    end%function unittests
end%methods (Static)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

end%classdef cemodel

