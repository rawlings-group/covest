%% Copyright (C) 1993-2019 John W. Eaton
%%
%% This file is part of Octave.
%%
%% Octave is free software: you can redistribute it and/or modify it
%% under the terms of the GNU General Public License as published by
%% the Free Software Foundation, either version 3 of the License, or
%% (at your option) any later version.
%%
%% Octave is distributed in the hope that it will be useful, but
%% WITHOUT ANY WARRANTY; without even the implied warranty of
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%% GNU General Public License for more details.
%%
%% You should have received a copy of the GNU General Public License
%% along with Octave; see the file COPYING.  If not, see
%% <https://www.gnu.org/licenses/>.

% TODO 
%% retval = myrank(A, tol)
%% 
%% Compute the rank of matrix A using the singular value decomposition.
%%
%% myrank is similar to Octave's built in rank function, but it uses a different
%% criterion for determining the rank: it takes the singular values of A and
%% looks for spots where they decrease by several orders of magnitude. To be
%% specific:
%%
%% myrank(A) = the minimum index i (if any) such that s(i + 1) / s(i) < tol
%%
%% If no such i is found, then the matrix is determined to be full rank. The
%% only exception is that if all singular values are detected to be zero, then
%% rank 0 is returned.
%%
%% The default value of the tolerance parameter is 1e-6.
%%
%% This approach certainly has its limitations. For example,
%%
%% myrank(diag([1, 1e-5, 1e-10, 1e-15, 1e-20])) = 5
%%
%% For most practical purposes, the rank of this matrix should probably be
%% considered to be something less than 5.
%%
%% See also: rank, rank_dyntol, null, orth, sprank, svd, eps.

%% Author: Travis Arnold, <tjarnold@wisc.edu>, February 2020

function retval = myrank(A, tol)

    switch nargin()
        case 1
            tol = 1e-6; % Default value of tol.

        case 2
            % Make sure tol is a numeric scalar.
            if ~isnumericscalar(tol)
                error('myrank: tol must be a numeric scalar');
            end%if

            % Make sure tol is 0 < tol < 1.
            if ~((tol > 0) || (tol < 1))
                error('myrank: tol must be 0 < tol < 1');
            end%if

        otherwise
            error('myrank: exactly one or two input arguments required');
    end%switch

    s = svd(A);

    % Check for a zero matrix.
    if all(s == 0)
        retval = 0;
        return;
    end%if

    % Find indices at which large jump in singular values happens.
    jump_idx = find((s(2:end) ./ s(1:end-1)) < tol);

    if isempty(jump_idx)
        retval = min(size(A));
    else
        retval = min(jump_idx);
    end%if
end%function

%!test
%! A = [1 2 3 4 5 6 7;
%!      4 5 6 7 8 9 12;
%!      1 2 3.1 4 5 6 7;
%!      2 3 4 5 6 7 8;
%!      3 4 5 6 7 8 9;
%!      4 5 6 7 8 9 10;
%!      5 6 7 8 9 10 11];
%! assert(myrank(A), 4);

%!test
%! A = [1 2 3 4 5 6 7;
%!      4 5 6 7 8 9 12;
%!      1 2 3.0000001 4 5 6 7;
%!      4 5 6 7 8 9 12.00001;
%!      3 4 5 6 7 8 9;
%!      4 5 6 7 8 9 10;
%!      5 6 7 8 9 10 11];
%! assert(myrank(A), 3);

%!test
%! A = [1 2 3 4 5 6 7;
%!      4 5 6 7 8 9 12;
%!      1 2 3 4 5 6 7;
%!      4 5 6 7 8 9 12.00001;
%!      3 4 5 6 7 8 9;
%!      4 5 6 7 8 9 10;
%!      5 6 7 8 9 10 11];
%! assert(myrank(A), 3);

%!test
%! A = [1 2 3 4 5 6 7;
%!      4 5 6 7 8 9 12;
%!      1 2 3 4 5 6 7;
%!      4 5 6 7 8 9 12;
%!      3 4 5 6 7 8 9;
%!      4 5 6 7 8 9 10;
%!      5 6 7 8 9 10 11];
%! assert(myrank(A), 3);

%!test
%! A = eye(100);
%! assert(myrank(A), 100);

%!assert(myrank([]), 0)
%!assert(myrank([1:9]), 1)
%!assert(myrank([1:9]'), 1)

%!test
%! A = [1, 2, 3; 1, 2.001, 3; 1, 2, 3.0000001];
%! assert(myrank(A), 3);
%! assert(myrank(A, 0.001), 1);
%! assert(myrank(A, 0.0001), 2);
%! assert(myrank(A, 0.0000000001), 3);

