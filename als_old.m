classdef als < handle

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% properties %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% TODO calculate dependsspec offline and load it. maybe instead have props.base, props.dynamic, and props.depends. This would make it easy to load.

% TODO do I even need Gammabar? Gammabar = Gamma - Os2bar * K. Maybe include it, but don't use it to calculate As.

% TODO decide how to implement option to calculate weight given Q, R, S or Phi, Theta

% TODO think about way to calculate number of lags (N) and exclude automatically

% TODO say an estimator is needed (A is unstable), but user only passes raw data. Should I just throw an error, or create an estimator to use by default?

% TODO add diagonal R version (maybe even diagonal Q version?)

% TODO add confidence intervals?

% TODO add an option to rescale outputs for cases where they are different
% orders of magnitude?

% TODO decide what kind of regularization to add. L1, L2, both? Maybe nuclear norm?

% TODO add a way to calculate Rs and b (the acutal ones) if we know them?

% TODO helpsetX knows to update current when a baseprop is changed. Am I sure that this behavior also happens when changing/setting model/data/estimator?

% TODO should I make constant matrices (As, Gamma, etc.) sparse or full?

% copied from als_function:
% TODO think about how to set the default estimator, if an estimator is needed. Or is it better to just throw an error in that case?

% TODO set default solver?

% TODO set default weight setting.

% TODO Need heavy and light options for Megan's methods?

% TODO maybe have a few different plotting options. Show fit to data, but maybe also have an option to show initial estimator, and then also new estimator.

% TODO Add a check for detectability?

% TODO Add error/warning about size of N if it is too small

% TODO Maybe base default value of N on system dimensions

% TODO add diagonal R version (maybe even diagonal Q version?)

% TODO add penalty on trace

% TODO add confidence intervals?

% TODO add an option to rescale outputs for cases where they are different
% orders of magnitude?

% TODO see if doing sparse things makes any difference here

% TODO what should I do when user wants to solve a problem that doesn't have a unique solution? Give an error and ask them to specify a problem that does have a unique solution? Or give a warning, and solve some problem that does have a unique solution? Or just pass the problem to the solver, and see what it returns, while issuing a warning that it is not a unique solution?

% TODO should I check that any Q, R, opts.weight values passed in are Hermitian or semidefinite? Is there a reason I didn't include this check on estimator.Q and estimator.R before?

properties (GetAccess = public, SetAccess = private)
    baseprops = {'data', ...
                 'model', ...
                 'estimator', ...
                 'N', ...
                 'weight', ...
                 'fullweightcalc', ...
                 'constrain', ...
                 'solver', ...
                 'problem', ...
                 'exclude', ...
                 'tracestates'}
    dynamicprops = {'Ndtotal', ...
                    'Nd', ...
                    'Tf', ...
                    'n', ...
                    'p', ...
                    'g', ...
                    'h', ...
                    'nt', ...
                    'pt', ...
                    'A', ...
                    'C', ...
                    'G', ...
                    'H', ...
                    'Abar', ...
                    'K', ...
                    'W', ...
                    'Os1', ...
                    'Os2', ...
                    'Gamma', ...
                    'Gammabar', ...
                    'Os1bar', ...
                    'Os2bar', ...
                    'AsQ', ...
                    'AsR', ...
                    'AsS', ...
                    'AsPhi', ...
                    'AsTheta', ...
                    'AsbarQ', ...
                    'AsbarR', ...
                    'AsbarS', ...
                    'AsbarPhibar', ...
                    'AsbarThetabar', ...
                    'As', ...
                    'y', ...
                    'Cshat', ...
                    'Rshat', ...
                    'bhat', ...
                    'est', ...
                    'constraintsactive'}
    dependsspec = struct('Ndtotal', {{'data', 'estimator'}}, ...
                         'Nd', {{'Ndtotal', 'exclude'}}, ...
                         'Tf', {{'Ndtotal', 'exclude'}}, ...
                         'n', {{'model', 'estimator'}}, ...
                         'p', {{'model', 'estimator'}}, ...
                         'g', {{'model', 'estimator'}}, ...
                         'h', {{'model', 'estimator'}}, ...
                         'nt', {{'Nd', 'N'}}, ...
                         'pt', {{'N', 'p'}}, ...
                         'A', {{'model', 'estimator'}}, ...
                         'C', {{'model', 'estimator'}}, ...
                         'G', {{'model', 'estimator'}}, ...
                         'H', {{'model', 'estimator'}}, ...
                         'Abar', {{'estimator'}}, ...
                         'K', {{'estimator'}}, ...
                         'W', {{'weight', 'N', 'fullweightcalc'}}, ... % TODO figure out what else I need here
                         'Os1', {{'A', 'C', 'N'}}, ...
                         'Os2', {{'Os1', 'p', 'n'}}, ...
                         'Gamma', {{'p', 'N'}}, ...
                         'Os1bar', {{'Abar', 'C', 'N'}}, ...
                         'Os2bar', {{'Os1bar', 'p', 'n'}}, ...
                         'Gammabar', {{'p', 'Abar', 'C', 'K'}}, ...
                         'AsQ', {{'C', 'Os1', 'n', 'A', 'G', 'g'}}, ...
                         'AsR', {{'H', 'Gamma', 'h'}}, ...
                         'AsS', {{'H', 'Os2', 'G'}}, ...
                         'AsPhi', {{'p', 'Gamma'}}, ...
                         'AsTheta', {{'p', 'Os2'}}, ...
                         'AsbarQ', {{'C', 'Os1bar', 'n', 'Abar', 'G', 'g'}}, ...
                         'AsbarR', {{'C', 'Os1bar', 'n', 'Abar', 'K', 'H', ...
                                     'Gamma', 'h'}}, ...
                         'AsbarS', {{'C', 'Os1bar', 'n', 'Abar', 'K', 'H', ...
                                     'G', 'Os2bar'}}, ...
                         'AsbarPhibar', {{'p', 'Gamma'}}, ...
                         'AsbarThetabar', {{'p', 'Os2bar'}}, ...
                         'As', {{'p', 'pt', 'problem', 'AsQ', 'AsR', 'AsS', ...
                                 'AsPhi', 'AsTheta', 'AsbarQ', 'AsbarR', ...
                                 'AsbarS', 'AsbarPhibar', 'AsbarThetabar'}}, ...
                         'y', {{'data', 'estimator'}}, ... % TODO figure out everything that goes here (what needs to be there if data gets transformed, and whatnot)
                         'Cshat', {{'y', 'exclude'}}, ... % TODO
                         'Rshat', {{'y', 'exclude'}}, ... % TODO
                         'bhat', {{'y', 'exclude'}}, ... % TODO
                         'est', {{'As', 'bhat', 'W', 'constrain', 'solver', ...
                                  'problem', 'tracestates'}}, ... % TODO do I need problem in here? what else?
                         'constraintsactive', {{'est'}}) % TODO
% TODO do some of these need to include problem/solver?
% TODO think about defaults and make sure they are intelligently chosen
    depends = struct() % this gets calculated in constructor
    ytype % 'outputs', or 'innovations'
end%properties

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% baseprops %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

properties (GetAccess = public, SetAccess = public)
    data
    model
    estimator
    N = 10 % TODO should this have a fixed default? either way, consider adding a way to calculate it based off of the system
    weight = 'data' % TODO have weight be custom, or specify a way to calculate it, like with KL in ceestimator
    fullweightcalc = false()
    constrain = false()
    solver = 'sdpt3' % other options: 'sedumi', 'mosek' (Matlab only)
    problem = 'QR' % For outputs, can be 'QR', 'QRS, 'PhiTheta'
                  % For innovations, can be 'QR', 'QRS, 'PhibarThetabar'
                  % TODO should the default value depend on outputs/innovations?
    exclude = 0 % TODO should this have a fixed default? either way, consider adding a way to calculate it based off of the system
    tracestates % penalize trace
end%properties

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% dynamicprops %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

properties (GetAccess = public, SetAccess = private)
    Ndtotal
    Nd
    Tf
    n
    p
    g
    h
    nt
    pt
    A
    Abar
    C
    G
    H
    W % TODO let this be where the numerical matrix weight is stored, whether it be calculated, or a custom one given
    K % TODO maybe call this Kbar? It is supposed to be the steady state Kalman predictor gain, which one potentially needs when innovations are analyzed
    Os1
    Os2
    Gamma
    Gammabar
    Os1bar
    Os2bar
    AsQ
    AsR
    AsS
    AsPhi
    AsTheta
    AsbarQ
    AsbarR
    AsbarS
    AsbarPhibar
    AsbarThetabar
    As
    y % outputs (transformed or raw) or innovations
    Cshat
    Rshat
    bhat
    est % container to hold solutions
    constraintsactive % logical telling if constraints are active at solution
end%properties

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

properties (GetAccess = public, SetAccess = private)
    checked = false()
    current % Tells whether each dynamicprop is current or not.
end%properties

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

properties (Hidden, GetAccess = private, SetAccess = private)
    store % Used to cache values of dynamicprops.
end%properties

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

properties (Hidden, GetAccess = public, SetAccess = private)
    id
    linked = struct('cedata', '', ...
                    'cemodel', '', ...
                    'ceestimator', ''); % Hold the ids of the linked cedata,
                                        % cemodel, and ceestimator objects
end%properties

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% methods %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

methods (Access = public)
    function self = als(varargin)
        % Constructor.

        % Generate uuid.
        self.id = uuidgen();

        % Calculate depends.
        self.depends = calcdepends(self.baseprops, self.dynamicprops, ...
                                   self.dependsspec);

        % Set default values of data and model objects. Do this here instead of
        % in property declaration to make sure that set.data/model gets called,
        % so that they are properly linked to self.
        self.data = cedata();
        self.model = cemodel();
        self.estimator = ceestimator();

        % Get kwargs struct.
        kwargs = args2kwargs(varargin{:});

        % Make sure kwargs are valid; throw an error if not.
        checkkwargs(kwargs, self.baseprops);

        % Set fields of self.
        for str = fieldnames(kwargs)'
            self.(str{:}) = kwargs.(str{:});
        end%for

        % Set default values for self.current and self.store.
        self.resetcurrent();
    end%fuction als

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function bool = eq(self, b)
        % Determine if two als instances refer to the same object by
        % comparing their id property. This functionality is built into Matlab,
        % but not Octave.

        if nargin() ~= 2
            error('exactly two input arguments required');
        end%if

        if ~isa(b, 'als')
            error('second argument is not an als object');
        end%if

        bool = strcmp(self.id, b.id);
    end%function eq

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function bool = ne(self, b)
        % Determine if two als instances do not refer to the same object by
        % comparing their id property. This functionality is built into Matlab,
        % but not Octave.

        if nargin() ~= 2
            error('exactly two input arguments required');
        end%if

        if ~isa(b, 'als')
            error('second argument is not an als object');
        end%if

        bool = ~(self == b);
    end%function ne

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function bool = islinked(self, mde)
        % Determine if cedata/cemodel/ceestimator object mde is currently linked
        % with self.

        if nargin() ~= 2
            error('exactly two input arguments required');
        end%if

        x = '';
        for s = {'cedata', 'cemodel', 'ceestimator'}
            if isa(mde, s{:})
                x = s{:}; % 'cedata', 'cemodel', or 'ceestimator'
                break;
            end%if
        end%for

        if strcmp(x, '')
            error(['second argument must be a cedata, cemodel, or ', ...
                   'ceestimator object']);
        end%if

        bool = strcmp(self.linked.(x), mde.id);
    end%function islinked

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function [bool, msg] = checkytype(self)
        % Check if data, model, and estimator are all empty by looking at value
        % of self.ytype.

        if nargin() ~= 1
            error('exactly one input argument required');
        end%if

        msg = '';
        if strcmp(self.ytype, '')
            msg = ['data, model, and estimator properties are all empty; ', ...
                   'either data/model or estimator must be provided'];
        end%if
        bool = isempty(msg);
    end%function checkytype

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function [bool, msg] = checkmodel(self)
        % Check model.

        if nargin() ~= 1
            error('exactly one input argument required');
        end%if

        msg = '';
        if strcmp(self.ytype, 'outputs')
            try
                self.model.check();
            catch err
                msg = ['model check failed with following error message: ', ...
                       err.message];
            end%trycatch
        end%if
        bool = isempty(msg);
    end%function checkmodel

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function [bool, msg] = checkdatamodelcompatible(self)
        % Check data/model compatibility.

        if nargin() ~= 1
            error('exactly one input argument required');
        end%if

        msg = '';
        if strcmp(self.ytype, 'outputs')
            try
                self.model.compatible(self.data);
            catch err
                msg = ['data/model compatibility check failed with ', ...
                       'following error message: ', err.message];
            end%trycatch
        end%if
        bool = isempty(msg);
    end%function checkdatamodelcompatible

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function [bool, msg] = checkestimator(self)
        % Check estimator.

        if nargin() ~= 1
            error('exactly one input argument required');
        end%if

        msg = '';
        if strcmp(self.ytype, 'innovations')
            try
                self.estimator.check();
            catch err
                msg = ['estimator check failed with following error ', ...
                       'message: ', err.message];
            end%trycatch
        end%if
        bool = isempty(msg);
    end%function checkestimator

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function [bool, msg] = checkstability(self)
        % Check that A is stable if data/model are given, or that estimator is
        % stable if it is given.

        if nargin() ~= 1
            error('exactly one input argument required');
        end%if

        msg = '';
        switch self.ytype
            case 'outputs'
                if ~self.model.stable
                    msg = ['A is not stable, so ALS cannot be used on the ', ...
                           'outputs; specify an estimator and use ALS on ', ...
                           'the innovations instead'];
                end%if
            case 'innovations'
                if ~self.estimator.stable
                    msg = ['cannot use ALS because estimator is unstable; ', ...
                           'are you sure the model is detectable?'];
                end%if
        end%switch
        bool = isempty(msg);
    end%function checkstability

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function [bool, msg] = checkNd(self)
        % Check that enough data points are provided.

        switch self.ytype
            case 'outputs'
                Ndtotal = size(self.data.y, 2);
            case 'innovations'
                Ndtotal = size(self.estimator.Ys, 2);
        end%switch

        msg = '';
        Nd = Ndtotal - self.exclude;
        if Nd < self.N
            msg = 'must have number of data points - exclude >= N';
        end%if
        bool = isempty(msg);
    end%function checkNd

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function [bool, msg] = checkproblem(self)
        % Check that the problem type specified is allowed.

        msg = '';
        switch self.ytype
            case 'outputs'
                if ~ismember(self.problem, {'QR', 'QRS', 'PhiTheta'})
                    msg = ['when analyzing outputs, problem must be ', ...
                           '''QR'', ''QRS'', or ''PhiTheta'''];
                end%if
            case 'innovations'
                if ~ismember(self.problem, {'QR', 'QRS', 'PhibarThetabar'})
                    msg = ['when analyzing innovations, problem must be ', ...
                           '''QR'', ''QRS'', or ''PhibarThetabar'''];
                end%if
        end%switch
        bool = isempty(msg);
    end%function checkproblem

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function check(self)
        % Run all checks on entire als object. Throw an error with an
        % explanatory message if estimator is invalid.

        % TODO when I change data/model/estimator, will it know it needs to recheck? I don't think it will. maybe need to load current in here. The same will hold in ceestimator, so look there first. EDIT I think it is fixed, still need to test this however.

        % TODO
        % check that solver is installed, but only if it will actually be needed
        % check tracestates, if given
        % make sure N isn't too small, or too big (do I need this? maybe check for uniqueness)
        % it doesn't really make sense to have an isempty function, since a lot of the properties of als cannot be empty (i.e., N must be a positive integer, and an error is issued if the user tries to set it to anything else). Make sure that the checks that are run on data, model, estimator properties will catch it if they are empty (this shouldn't be difficult).
        % check to make sure custom G/H, if provided, are consistent with model

        % Check number of input arguments.
        if nargin() ~= 1
            error('exactly one input argument required');
        end%if

        % Only run checks if the model has not already been checked.
        if ~self.checked
            bool = struct();
            msg = struct();

            % Run all checks.
            [bool.ytype,     msg.ytype] = self.checkytype();
            [bool.model,     msg.model] = self.checkmodel();
            [bool.datamodel, msg.datamodel] = self.checkdatamodelcompatible();
            [bool.estimator, msg.estimator] = self.checkestimator();
            [bool.stability, msg.stability] = self.checkstability();
            [bool.Nd,        msg.Nd] = self.checkNd();
            [bool.problem,   msg.problem] = self.checkproblem();

            % Determine if all checks passed.
            bool = all(cell2mat(struct2cell(bool)'));

            % If any check failed, process messages and throw error.
            if ~bool
                msg = struct2cell(msg)';
                msg = msg(~cellfun('isempty', msg)); % remove empty cells
                msg = strjoin(msg, '; '); % combine messages
                error(msg);

            % If all checks passed, no error message will have been issued;
            % update checked status.
            else
                self.checked = true();
            end%if
        end%if
    end%function check

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% TODO do I want/need calculate to have public access?
    function calculate(self, varargin)
        % Calculate dynamicprops. Update values in self.store.

        % Make sure input arguments are character vectors.
        if ~all(cellfun(@ischarrow, varargin))
            error('all arguments (other than self) must be character vectors');
        end%if

        % If any unrecognized arguments are passed, throw an error.
        unrec = setdiff(varargin, [self.dynamicprops, 'all']);
        if ~isempty(unrec)
            error(['unrecognized input argument(s): ', ...
                   strjoin(unrec, ', '), ' (acceptable arguments are ''', ...
                   strjoin([self.dynamicprops, 'all'], ''', '''), ''')']);
        end%if

        % Check that model is valid; throw an error if it is not.
        self.check();

        % If argument 'all' is passed or no additional arguments are passed,
        % then update everything.
        updateall = ismember('all', varargin) || isempty(varargin);

        % Function that tells if each individual argument needs to be updated.
        updateX = @(X) updateall || ismember(X, varargin);

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        if updateX('Ndtotal')
            switch self.ytype
                case 'outputs'
                    self.store.Ndtotal = size(self.data.y, 2);
                case 'innovations'
                    self.store.Ndtotal = size(self.estimator.Ys, 2);
            end%switch
            self.current.Ndtotal = true();
        end%if updateX('Ndtotal')

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        if updateX('Nd')
            self.store.Nd = self.Ndtotal - self.exclude;
            self.current.Nd = true();
        end%if updateX('Nd')

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        if updateX('Tf')
            self.store.Tf = self.Nd - 1;
            self.current.Tf = true();
        end%if updateX('Tf')

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        for X = 'npghCGH'
            if updateX(X)
                switch self.ytype
                    case 'outputs'
                        self.store.(X) = self.model.(X);
                    case 'innovations'
                        self.store.(X) = self.estimator.model.(X);
                end%switch
                self.current.(X) = true();
            end%if updateX(X)
        end%for X = 'npghCGH'

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        if updateX('nt')
            self.store.nt = self.Tf - self.N + 1;
            self.current.nt = true();
        end%if updateX('nt')

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        if updateX('pt')
            self.store.pt = self.N * self.p;
            self.current.pt = true();
        end%if updateX('pt')

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        if updateX('A')
            switch self.ytype
                case 'outputs'
                    self.store.A = self.model.A;
                case 'innovations'
                    self.store.A = [];
            end%switch
            self.current.A = true();
        end%if updateX('A')

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        if updateX('Abar')
            switch self.ytype
                case 'outputs'
                    self.store.Abar = [];
                case 'innovations'
                    if self.estimator.dynamic
                        self.store.Abar = self.estimator.Abar{end};
                    else
                        self.store.Abar = self.estimator.Abar;
                    end%if
            end%switch
            self.current.Abar = true();
        end%if updateX('Abar')

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        if updateX('K')
            switch self.ytype
                case 'outputs'
                    self.store.K = [];
                case 'innovations'
                    if self.estimator.dynamic
                        self.store.K = self.estimator.K{end};
                    else
                        self.store.K = self.estimator.K;
                    end%if
            end%switch
            self.current.K = true();
        end%if updateX('K')

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% TODO add Cs that can be calculated from model QRS/PhiTheta if it is provided

% TODO when implementing this with Cshat = 0 for j >= N assumption, need to change the limit on the for loop, and change where Cshat is pulled from when it is supposed to be zero.

%Ebtij = arrayfun(@(k) ...
%                 [kronplus([0:pt:(pt*(pt - k*p - 1))]', ...
%                           [0:(pt - k*p - 1)]'), ... rows
%                  kronplus([(k*p*pt):pt:pt*(pt - 1)]', ...
%                           [(k*p):(pt - 1)]')], ... columns
%                 0:N-1, 'UniformOutput', false);
%Ebtij = vertcat(Ebtij{:});
%Ebt = sparse(Ebtij(:, 1) + 1, Ebtij(:, 2) + 1, 1);
%PYbbars = zeros(pt^2);
%Pkronsum1 = zeros(pt^2);
%Pkronsum2 = zeros(pt^2);
%IK = speye(pt^2) + commat(pt, pt);
%for i = 1:nt+1
%    C = Cshat(i:N-1+i);
%    if i <= N-1
%        R = [Cshat(i:-1:1), ...
%             cellfun(@ctranspose, Cshat(2:N-i+1), 'UniformOutput', false())];
%    else
%        R = Cshat(i:-1:i-N+1);
%    end%if
%    PY = blktoep(C, R);
%    Pkron = IK*kron(PY, PY);
%    if i == 1
%        PYbbars = PYbbars + nt*Pkron + Ebt*Pkron*Ebt'; % TODO need forceherm?
%    else
%        if i < nt + 1
%            Pkronsum1 = Pkronsum1 + (nt-i+1)*Pkron;
%        end%if
%        Pkronsum2 = Pkronsum2 + Pkron;
%    end%if
%end
%PYbbars = PYbbars + forceherm2(Pkronsum1 + Ebt*Pkronsum2);

%% TODO make sure that the final value of PYbbars is Hermitian

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% rajamani method

%KptNd = commat(pt, Nd);
%Nb = kron(speye(pt^2), vec(speye(Nd)))' * kron(kron(speye(pt^2), KptNd), speye(Nd));
%Eb = arrayfun(@(k) sparse([1:(pt-p*k)]', [(p*k+1):pt]', 1, pt, pt), 0:N-1, ...
%              'UniformOutput', false());
%Ob = blkdiag(speye(pt*nt), vertcat(Eb{:}));
%Pb = sparse((1:(nt+1)*pt)', kronplus(p*(0:nt), 1:pt), 1);
%ObPb = Ob*Pb;
%PYp = hermblktoep(Cshat); % TODO this will need to change when we assume some are zero
%PYbs = ObPb*PYp*ObPb'; % TODO forceherm?
%KPYbsK = KptNd * PYbs * KptNd';
%PYbbars = Nb * (speye((Nd*pt)^2) + commat(Nd*p)) * kron(KPYbsK, KPYbsK) * Nb'; % TODO forceherm?

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%Hb = sparse(1:p, 1:p, 1, p, pt);
%Gb = kron(sparse(1:N, 1:N, 1./(Nd:-1:(nt+1))), speye(p));
%Lb = dmatpinv(pt, p) * kron(Hb, Gb);
%Pbhat = Lb*PYbbars*Lb; % TODO need forceherm?

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        if updateX('W')
            Wsize = self.pt*self.p - self.p*(self.p-1)/2;
            if ischarrow(self.weight)
                switch self.weight
                    case 'data'
                        % TODO implement
                        error('data option for weight not implemented yet');
                    case 'datafull'
                        % TODO implement
                        error('datafull option for weight not implemented yet');
                    case 'I'
                        self.store.W = eye(Wsize);
                    case 'Z1'
                        % TODO implement
                        error('Z1 option for weight not implemented yet');
                    case 'Z2'
                        % TODO implement
                        error('Z2 option for weight not implemented yet');
                end%switch
            else % custom weight is given
                if size(self.weight, 1) ~= Wsize
                    error(['given weight has dimensions that are ', ...
                           'inconsistent with p and N']);
                end%if
                self.store.W = self.weight;
            end%if
            self.current.W = true();
        end%if updateX('W')

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        if updateX('Os1')
            switch self.ytype
                case 'outputs'
                    self.store.Os1 = self.calcOs1(self.A);
                case 'innovations'
                    self.store.Os1 = [];
            end%switch
            self.current.Os1 = true();
        end%if updateX('Os1')

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        if updateX('Os1_full')
            switch self.ytype
                case 'outputs'
                    self.store.Os1_full = self.calcOs1_full(self.Os1, self.A);
                case 'innovations'
                    self.store.Os1_full = [];
            end%switch
            self.current.Os1_full = true();
        end%if updateX('Os1_full')

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        if updateX('Os2')
            switch self.ytype
                case 'outputs'
                    self.store.Os2 = self.calcOs2(self.Os1);
                case 'innovations'
                    self.store.Os2 = [];
            end%switch
            self.current.Os2 = true();
        end%if updateX('Os2')

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        if updateX('Os2_full')
            switch self.ytype
                case 'outputs'
                    self.store.Os2_full = self.calcOs2(self.Os1_full);
                case 'innovations'
                    self.store.Os2_full = [];
            end%switch
            self.current.Os2_full = true();
        end%if updateX('Os2_full')

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        if updateX('Gamma')
            self.store.Gamma = [eye(self.p); zeros((self.N-1)*self.p, self.p)];
            self.current.Gamma = true();
        end%if updateX('Gamma')

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        if updateX('Gamma_full')
            self.store.Gamma_full = [eye(self.p); ...
                                     zeros((self.Nd-1)*self.p, self.p)];
            self.current.Gamma_full = true();
        end%if updateX('Gamma_full')

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        if updateX('Gammabar')
            switch self.ytype
                case 'outputs'
                    self.store.Gammabar = [];
                case 'innovations'
                    self.store.Gammabar = self.Gamma - self.Os2bar * self.K;
            end%switch
            self.current.Gammabar = true();
        end%if updateX('Gammabar')

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        if updateX('Gammabar_full')
            switch self.ytype
                case 'outputs'
                    self.store.Gammabar_full = [];
                case 'innovations'
                    self.store.Gammabar_full = self.Gamma_full ...
                                               - self.Os2bar_full * self.K;
            end%switch
            self.current.Gammabar_full = true();
        end%if updateX('Gammabar_full')

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        if updateX('Os1bar')
            switch self.ytype
                case 'outputs'
                    self.store.Os1bar = [];
                case 'innovations'
                    self.store.Os1bar = self.calcOs1(self.Abar);
            end%switch
            self.current.Os1bar = true();
        end%if updateX('Os1bar')

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        if updateX('Os1bar_full')
            switch self.ytype
                case 'outputs'
                    self.store.Os1bar_full = [];
                case 'innovations'
                    self.store.Os1bar_full = self.calcOs1_full(self.Os1bar, ...
                                                               self.Abar);
            end%switch
            self.current.Os1bar_full = true();
        end%if updateX('Os1bar_full')

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        if updateX('Os2bar')
            switch self.ytype
                case 'outputs'
                    self.store.Os2bar = [];
                case 'innovations'
                    self.store.Os2bar = self.calcOs2(self.Os1bar);
            end%switch
            self.current.Os2bar = true();
        end%if updateX('Os2bar')

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        if updateX('Os2bar_full')
            switch self.ytype
                case 'outputs'
                    self.store.Os2bar = [];
                case 'innovations'
                    self.store.Os2bar = self.calcOs2(self.Os1bar_full);
            end%switch
            self.current.Os2bar_full = true();
        end%if updateX('Os2bar_full')

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        if updateX('AsQ')
            switch self.ytype
                case 'outputs'
                    self.store.AsQ = kron(self.C, self.Os1) ...
                                    / (eye(self.n^2) - kron(self.A, self.A)) ...
                                    * kron(self.G, self.G) * dupmat(self.g);
                case 'innovations'
                    self.store.AsQ = [];
            end%switch
            self.current.AsQ = true();
        end%if updateX('AsQ')

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        if updateX('AsbarQ')
            switch self.ytype
                case 'outputs'
                    self.store.AsbarQ = [];
                case 'innovations'
                    self.store.AsbarQ = kron(self.C, self.Os1bar) ...
                              / (eye(self.n^2) - kron(self.Abar, self.Abar)) ...
                              * kron(self.G, self.G) * dupmat(self.g);
            end%switch
            self.current.AsbarQ = true();
        end%if updateX('AsbarQ')

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        if updateX('AsR')
            switch self.ytype
                case 'outputs'
                    self.store.AsR = kron(self.H, self.Gamma * self.H) ...
                                     * dupmat(self.h);
                case 'innovations'
                    self.store.AsR = [];
            end%switch
            self.current.AsR = true();
        end%if updateX('AsR')

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        if updateX('AsbarR')
            switch self.ytype
                case 'outputs'
                    self.store.AsbarR = [];
                case 'innovations'
                    self.store.AsbarR = (kron(self.C, self.Os1bar) ...
                              / (eye(self.n^2) - kron(self.Abar, self.Abar)) ...
                              * kron(self.K * self.H, self.K * self.H) ...
                              + kron(self.H, self.Gamma * self.H)) ...
                              * dupmat(self.h);
            end%switch
            self.current.AsbarR = true();
        end%if updateX('AsbarR')

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        if updateX('AsS')
            switch self.ytype
                case 'outputs'
                    self.store.AsS = kron(self.H, self.Os2 * self.G);
                case 'innovations'
                    self.store.AsS = [];
            end%switch
            self.current.AsS = true();
        end%if updateX('AsS')

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        if updateX('AsbarS')
            switch self.ytype
                case 'outputs'
                    self.store.AsbarS = [];
                case 'innovations'
                    self.store.AsbarS = -kron(self.C, self.Os1bar) ...
                              / (eye(self.n^2) - kron(self.Abar, self.Abar)) ...
                              * (eye(self.n^2) + commat(self.n)) ...
                              * kron(self.K * self.H, self.G) ...
                              + kron(self.H, self.Os2bar * self.G);
            end%switch
            self.current.AsbarS = true();
        end%if updateX('AsbarS')

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        if updateX('AsPhi')
            switch self.ytype
                case 'outputs'
                    self.store.AsPhi = kron(eye(self.p), self.Gamma) ...
                                       * dupmat(self.p);
                case 'innovations'
                    self.store.AsPhi = [];
            end%switch
            self.current.AsPhi = true();
        end%if updateX('AsPhi')

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        if updateX('AsbarPhibar')
            switch self.ytype
                case 'outputs'
                    self.store.AsbarPhibar = [];
                case 'innovations'
                    self.store.AsbarPhibar = kron(eye(self.p), self.Gamma) ...
                                             * dupmat(self.p);
            end%switch
            self.current.AsbarPhibar = true();
        end%if updateX('AsbarPhibar')

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        if updateX('AsTheta')
            switch self.ytype
                case 'outputs'
                    self.store.AsTheta = kron(eye(self.p), self.Os2);
                case 'innovations'
                    self.store.AsTheta = [];
            end%switch
            self.current.AsTheta = true();
        end%if updateX('AsTheta')

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        if updateX('AsbarThetabar')
            switch self.ytype
                case 'outputs'
                    self.store.AsbarThetabar = [];
                case 'innovations'
                    self.store.AsbarThetabar = kron(eye(self.p), self.Os2bar);
            end%switch
            self.current.AsbarThetabar = true();
        end%if updateX('AsbarThetabar')

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        if updateX('As')
            switch self.ytype
                case 'outputs'
                    D = dmatpinv(self.pt, self.p);
                    switch self.problem
                        case 'QR'
                            self.store.As = D * [self.AsQ, self.AsR];
                        case 'QRS'
                            self.store.As = D * [self.AsQ, self.AsR, self.AsS];
                        case 'PhiTheta'
                            self.store.As = D * [self.AsPhi, self.AsTheta];
                    end%switch
                case 'innovations'
                    self.store.As = [];
            end%switch
            self.current.As = true();
        end%if updateX('As')

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        if updateX('Asbar')
            switch self.ytype
                case 'outputs'
                    self.store.Asbar = [];
                case 'innovations'
                    D = dmatpinv(self.pt, self.p);
                    switch self.problem
                        case 'QR'
                            self.store.Asbar = D * [AsbarQ, AsbarR];
                        case 'QRS'
                            self.store.Asbar = D * [self.AsbarQ, ...
                                                    self.AsbarR, self.AsbarS];
                        case 'PhibarThetabar'
                            self.store.Asbar = D * [AsbarPhibar, AsbarThetabar];
                    end%switch
            end%switch
            self.current.Asbar = true();
        end%if updateX('Asbar')

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        if updateX('y')
            switch self.ytype
                case 'outputs'
                    y = self.data.y;
                    if ismember('B', self.model.BD)
                        ut = zeros(self.model.m, self.Ndtotal);
                        for i = 2:self.Ndtotal
                            ut(:, i) = self.A * ut(:, i-1) ...
                                       + self.model.B * self.data.u(:, i-1);
                        end%for
                        y = y - self.C * ut;
                    end%if
                    if ismember('D', self.model.BD)
                        y = y - self.model.D * self.data.u;
                    end%if
                    self.store.y = y(:, exclude+1:end);
                case 'innovations'
                    self.store.y = [];
            end%switch
            self.current.y = true();
        end%if updateX('y')

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        if updateX('Ys')
            switch self.ytype
                case 'outputs'
                    self.store.Ys = [];
                case 'innovations'
                    self.store.Ys = self.estimator.Ys(:, exclude+1:end);
            end%switch
            self.current.Ys = true();
        end%if updateX('Ys')

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        if updateX('Cshat')
            switch self.ytype
                case 'outputs'
                    self.store.Cshat = self.calcCs(self.y);
                case 'innovations'
                    self.store.Cshat = [];
            end%switch
            self.current.Cshat = true();
        end%if updateX('Cshat')

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        if updateX('Csbarhat')
            switch self.ytype
                case 'outputs'
                    self.store.Csbarhat = [];
                case 'innovations'
                    self.store.Csbarhat = self.calcCs(self.Ys);
            end%switch
            self.current.Csbarhat = true();
        end%if updateX('Csbarhat')

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        if updateX('Cshat_full')
            switch self.ytype
                case 'outputs'
                    self.store.Cshat_full = [self.Cshat; ...
                                             self.calcCsrest(self.y)];
                case 'innovations'
                    self.store.Cshat_full = [];
            end%switch
            self.current.Cshat_full = true();
        end%if updateX('Cshat_full')

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        if updateX('Csbarhat_full')
            switch self.ytype
                case 'outputs'
                    self.store.Csbarhat_full = [];
                case 'innovations'
                    self.store.Csbarhat_full = [self.Csbarhat; ...
                                                self.calcCsrest(self.Ys)];
            end%switch
            self.current.Csbarhat_full = true();
        end%if updateX('Csbarhat_full')

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        if updateX('Rshat')
            switch self.ytype
                case 'outputs'
                    self.store.Rshat = vertcat(self.Cshat{:});
                case 'innovations'
                    self.store.Rshat = [];
            end%switch
            self.current.Rshat = true();
        end%if updateX('Rshat')

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        if updateX('Rsbarhat')
            switch self.ytype
                case 'outputs'
                    self.store.Rsbarhat = [];
                case 'innovations'
                    self.store.Rsbarhat = vertcat(self.Csbarhat{:});
            end%switch
            self.current.Rsbarhat = true();
        end%if updateX('Rsbarhat')

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        if updateX('bhat')
            switch self.ytype
                case 'outputs'
                    self.store.bhat = myvech(self.Rshat);
                case 'innovations'
                    self.store.bhat = [];
            end%switch
            self.store.bhat = myvech(self.Rshat);
            self.current.bhat = true();
        end%if updateX('bhat')

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        if updateX('bbarhat')
            switch self.ytype
                case 'outputs'
                    self.store.bbarhat = [];
                case 'innovations'
                    self.store.bbarhat = myvech(self.Rsbarhat);
            end%switch
            self.current.bbarhat = true();
        end%if updateX('bbarhat')

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        if updateX('Cs')
            % TODO
            switch self.ytype
                case 'outputs'
                    switch self.model.stochtype
                        case 'QR'

                        case 'PhiTheta'

                        otherwise

                    end%switch
                case 'innovations'
                    self.store.Cs = [];
            end%switch
            self.current.Cs = true();
        end%if updateX('Cs')

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        if updateX('Csbar')
            % TODO
            switch self.ytype
                case 'outputs'
                    
                case 'innovations'
                    
            end%switch
            self.store.Csbar = 
            self.current.Csbar = true();
        end%if updateX('Csbar')

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        if updateX('Cs_full')
            % TODO
            switch self.ytype
                case 'outputs'
                    
                case 'innovations'
                    
            end%switch
            self.store.Cs_full = 
            self.current.Cs_full = true();
        end%if updateX('Cs_full')

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        if updateX('Csbar_full')
            % TODO
            switch self.ytype
                case 'outputs'
                    
                case 'innovations'
                    
            end%switch
            self.store.Csbar_full = 
            self.current.Csbar_full = true();
        end%if updateX('Csbar_full')

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        if updateX('Rs')
            % TODO
            switch self.ytype
                case 'outputs'
                    if isempty(self.model.stochtype)
                        self.store.Rs = [];
                    else % self.model.stochtype = 'QR' or 'PhiTheta'
                        self.store.Rs = self.Gamma * self.model.Phi ...
                                        + self.Os2 * self.model.Theta;
                    end%if
                case 'innovations'
                    self.store.Rs = [];
            end%switch
            self.current.Rs = true();
        end%if updateX('Rs')

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        if updateX('Rs_full')
            % TODO
            switch self.ytype
                case 'outputs'
                    if isempty(self.model.stochtype)
                        self.store.Rs_full = [];
                    else % self.model.stochtype = 'QR' or 'PhiTheta'
                        self.store.Rs_full = 
                    end%if
                case 'innovations'
                    self.store.Rs_full = [];
            end%switch
            self.current.Rs_full = true();
        end%if updateX('Rs_full')

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        if updateX('Rsbar')
            % TODO
            switch self.ytype
                case 'outputs'
                    self.store.Rsbar = [];
                case 'innovations'
                    if isempty(self.estimator.model.stochtype)
                        self.store.Rsbar = [];
                    else % self.estimator.model.stochtype = 'QR' or 'PhiTheta'
                        % TODO check for dynamic/static filtering
                        if self.estimator.dynamic

                        else

                        end%if
                        % TODO after extracting Phibar/Thetabar, calculate Rsbar
                    end%if
            end%switch
            self.current.Rsbar = true();
        end%if updateX('Rsbar')

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        if updateX('Rsbar_full')
            % TODO

            self.current.Rsbar_full = true();
        end%if updateX('Rsbar_full')

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        if updateX('b')
            switch self.ytype
                case 'outputs'
                    self.store.b = myvech(self.Rs);
                case 'innovations'
                    self.store.b = [];
            end%switch
            self.current.b = true();
        end%if updateX('b')

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        if updateX('bbar')
            switch self.ytype
                case 'outputs'
                    self.store.bbar = [];
                case 'innovations'
                    self.store.bbar = myvech(self.Rsbar);;
            end%switch
            self.current.bbar = true();
        end%if updateX('bbar')

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        if updateX('est')
            % TODO implement
            error('calculate est not implemented yet');
            self.store.est = [];
            self.current.est = true();
        end%if updateX('est')

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        if updateX('constraintsactive')
            % TODO implement
            error('calculate constraintsactive not implemented yet');
            self.store.constraintsactive = [];
            self.current.constraintsactive = true();
        end%if updateX('constraintsactive')
    end%function calculate

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function plot(self)
        % TODO implement

        % TODO think about how I want to do this. should I add similar functionality to ceestimator as well?
        error('plot function not implemented yet');
    end%function
end%methods

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

methods (Access = private)
    function Cs = calcCs(self, y)
        Cs = arrayfun(@(j) y(:, j:end) * y(:, 1:end-j+1)' / (self.Nd-j+1), ...
                      1:self.N, 'UniformOutput', false());
    end%function calcCs

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function Cs = calcCsrest(self, y)
        Cs = arrayfun(@(j) y(:, j:end) * y(:, 1:end-j+1)' / (self.Nd-j+1), ...
                      self.N+1:self.Nd, 'UniformOutput', false());
    end%function calcCsrest

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function Os1 = calcOs1(self, A)
        Os1 = zeros(self.pt, self.n); % preallocate
        CAprod = self.C;
        for i = 1:self.N
            rowidx = ((i-1)*self.p + 1):(i*self.p);
            Os1(rowidx, :) = CAprod;
            if i < self.N
                CAprod = CAprod*A;
            end%if
        end%for
    end%function calcOs1

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function Os1 = calcOs2(self, Os1)
        Os2 = [zeros(self.p, self.n); Os1(1:(end-self.p), :)];
    end%function calcOs2

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function Os1_full = calcOs1_full(self, Os1, A)
        Os1_full = [Os1, zeros(self.p*(self.Nd-self.N), self.n)]; % preallocate
        CAprod = Os1((end - self.p + 1):end, :) * A;
        for i = (self.N+1):self.Nd
            rowidx = ((i-1)*self.p + 1):(i*self.p);
            Os1_full(rowidx, :) = CAprod;
            if i < self.Nd
                CAprod = CAprod*A;
            end%if
        end%for
    end%function calcOs1_full

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function link(self, mdenew)
        % Link self to given cedata, cemodel, or ceestimator object mdenew.

        if nargin() ~= 2
            error('exactly two input arguments required');
        end%if

        x = '';
        for s = {'cedata', 'cemodel', 'ceestimator'}
            if isa(mdenew, s{:})
                x = s{:}; % 'cedata' or 'cemodel'
                y = x;
                y(1:2) = ''; % remove leading 'ce', so y is 'data', 'model', or
                             % 'estimator'
                break;
            end%if
        end%for

        if strcmp(x, '')
            error(['second argument must be a cedata, cemodel, or ', ...
                   'ceestimator object']);
        end%if

        % We only need to take further action if self and mdenew are not already
        % linked.
        if ~self.islinked(mdenew)
            % Create a separate handle first to avoid calling
            % set.model/data/estimator.
            mdeold = self.(y);

            % Remove link in old cedata, cemodel, or ceestimator object. Note
            % that when set.data/model/estimator is called in the constructor,
            % self.data/model/estimator = [], so we do not want to do this in
            % that case.
            inconstructor = isequal(mdeold, []);
            if ~inconstructor
                mdeold.linked.als = rmfield(self.(y).linked.als, self.id);
            end%if

            % Add link in self to new cedata, cemodel, or ceestimator object.
            % This overwrites the old link.
            self.linked.(x) = mdenew.id;

            % Create link in new cedata, cemodel, or ceestimator object.
            mdenew.linked.als.(self.id) = true();
        end%if
    end%function link

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function resetcurrent(self)
        % Set current to false and store to empty for all dynamicprops.

        self.current = struct();
        self.store = struct();
        for str = self.dynamicprops
            self.current.(str{:}) = false();
            self.store.(str{:}) = [];
        end%for
    end%function resetcurrent
end%methods

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% get methods %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

methods (Access = private)
    function val = getdynamicprop(self, X)
        % Helper method that is used to get all properties in
        % self.dynamicprops.

        % Check number of input arguments.
        if nargin() ~= 2
            error('exactly two input arguments required');
        end%if

        % Make sure input argument is a character vector.
        if ~ischarrow(X)
            error('second input argument must be a character vector');
        end%if

        % Make sure input argument is a recognized value.
        if ~ismember(X, self.dynamicprops)
            error(['cannot access property ''', X, '''; dynamicprops of ', ...
                   'als object are currently: ''', ...
                   strjoin(self.dynamicprops, ''', '''), '''']);
        end%if

        % If self.X isn't current, update self.store.X. Remember, self.calculate
        % will update self.current.
        if ~self.current.(X)
            self.calculate(X);
        end%if

        % Return self.store.X.
        val = self.store.(X);
    end%function getdynamicprop
end%methods

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

methods
    function val = get.current(self)
        % Check if model, data, and estimator have changed. If so, update
        % current for all dependent dynamicprops.

% TODO make sure this will detect when self.estimator.model and self.estimator.data have changed, and also that it displays self.checked properly
        switch self.ytype
            case 'outputs' % check self.model and self.data
                X = {'model', 'data'};
            case 'innovations' % check self.estimator
                self.estimator.current; % ceestimator.get.current checks if
                                        % linked data/model objects have
                                        % changed, and raises flags if so
                X = {'estimator'};
            case ''
                X = {}; % data, model, and estimator are all empty, so don't do
                        % anything
        end%switch

        for Y = X
            if self.(Y{:}).linked.als.(self.id)
                % this checks if model/data/estimator has changed

                self.checked = false(); % set checked to false

                % Set dependent dynamicprops to not current.
                for str = self.dynamicprops
                    if ismember(Y{:}, self.depends.(str{:}))
                        self.current.(str{:}) = false();
                    end%if
                end%for

                % Lower the flag in linked cemodel, cedata, and ceestimator
                % objects. Create a separate handle first to avoid calling
                % set.model/data/estimator.
                mde = self.(Y{:});
                mde.linked.als.(self.id) = false();
            end%if
        end%for

        val = self.current;
    end%function get.current

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.Ndtotal(self)
        val = self.getdynamicprop('Ndtotal');
    end%function get.Ndtotal

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.Nd(self)
        val = self.getdynamicprop('Nd');
    end%function get.Nd

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.Tf(self)
        val = self.getdynamicprop('Tf');
    end%function get.Tf

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.n(self)
        val = self.getdynamicprop('n');
    end%function get.n

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.p(self)
        val = self.getdynamicprop('p');
    end%function get.p

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.g(self)
        val = self.getdynamicprop('g');
    end%function get.g

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.h(self)
        val = self.getdynamicprop('h');
    end%function get.h

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.nt(self)
        val = self.getdynamicprop('nt');
    end%function get.nt

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.pt(self)
        val = self.getdynamicprop('pt');
    end%function get.pt

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.A(self)
        val = self.getdynamicprop('A');
    end%function get.A

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.C(self)
        val = self.getdynamicprop('C');
    end%function get.C

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.G(self)
        val = self.getdynamicprop('G');
    end%function get.G

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.H(self)
        val = self.getdynamicprop('H');
    end%function get.H

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.Abar(self)
        val = self.getdynamicprop('Abar');
    end%function get.Abar

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.K(self)
        val = self.getdynamicprop('K');
    end%function get.K

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.W(self)
        val = self.getdynamicprop('W');
    end%function get.W

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.Os1(self)
        val = self.getdynamicprop('Os1');
    end%function get.Os1

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.Os2(self)
        val = self.getdynamicprop('Os2');
    end%function get.Os2

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.Gamma(self)
        val = self.getdynamicprop('Gamma');
    end%function get.Gamma

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.Gammabar(self)
        val = self.getdynamicprop('Gammabar');
    end%function get.Gammabar

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.Os1bar(self)
        val = self.getdynamicprop('Os1bar');
    end%function get.Os1bar

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.Os2bar(self)
        val = self.getdynamicprop('Os2bar');
    end%function get.Os2bar

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.AsQ(self)
        val = self.getdynamicprop('AsQ');
    end%function get.AsQ

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.AsR(self)
        val = self.getdynamicprop('AsR');
    end%function get.AsR

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.AsS(self)
        val = self.getdynamicprop('AsS');
    end%function get.AsS

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.AsPhi(self)
        val = self.getdynamicprop('AsPhi');
    end%function get.AsPhi

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.AsTheta(self)
        val = self.getdynamicprop('AsTheta');
    end%function get.AsTheta

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.AsbarQ(self)
        val = self.getdynamicprop('AsbarQ');
    end%function get.AsbarQ

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.AsbarR(self)
        val = self.getdynamicprop('AsbarR');
    end%function get.AsbarR

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.AsbarS(self)
        val = self.getdynamicprop('AsbarS');
    end%function get.AsbarS

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.AsbarPhibar(self)
        val = self.getdynamicprop('AsbarPhibar');
    end%function get.AsbarPhibar

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.AsbarThetabar(self)
        val = self.getdynamicprop('AsbarThetabar');
    end%function get.AsbarThetabar

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.As(self)
        val = self.getdynamicprop('As');
    end%function get.As

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.y(self)
        val = self.getdynamicprop('y');
    end%function get.y

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.Cshat(self)
        val = self.getdynamicprop('Cshat');
    end%function get.Cshat

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.Rshat(self)
        val = self.getdynamicprop('Rshat');
    end%function get.Rshat

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.bhat(self)
        val = self.getdynamicprop('bhat');
    end%function get.bhat

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.est(self)
        val = self.getdynamicprop('est');
    end%function get.est

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.constraintsactive(self)
        val = self.getdynamicprop('constraintsactive');
    end%function get.constraintsactive

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.checked(self)
        self.current; % this will check if model/estimator have changed, and if
                      % so, set self.checked to false.
        val = self.checked;
    end%function get.checked
end%methods

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% set methods %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

methods (Access = private)
    function need2set = helpsetX(self, X, val)
        % Helper method that is used to update self.checked and self.current
        % when baseprops are set.

        okprops = setdiff(self.baseprops, {'data', 'model', 'estimator'}); % TODO replace this maybe at some point to avoid calling setdiff

        % Error message to print if necessary.
        msg = ['exactly three input arguments required; self, one of the ', ...
               'following character vectors: ''', ...
               strjoin(okprops, ''', '''), ''', ', ...
               'and the value to set the property to'];

        % Check number of input arguments.
        if nargin() ~= 3
            error(msg);
        end%if

        % Make sure input argument is a character vector.
        if ~ischarrow(X)
            error(msg);
        end%if

        % Make sure input argument is a recognized value.
        if ~ismember(X, okprops)
            error(msg);
        end%if

        % Determine if X needs to be set.
        need2set = ~isequal(val, self.(X));

        % If als object changes, update self.checked and self.current.
        if need2set
            self.checked = false();
            for str = self.dynamicprops
                if ismember(X, self.depends.(str{:}))
                    self.current.(str{:}) = false();
                end%if
            end%for
        end%if
    end%function helpsetX

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function helpsetytype(self)
        % Set self.ytype depending on which of self.data, self.model, and
        % self.estimator are empty.

        datmodempty = isempty(self.data) && isempty(self.model);
        estempty = isempty(self.estimator);

        % Now set self.ytype to '', 'outputs', or 'innovations', depending on
        % whether data/model or estimator or neither) has been provided.
        if datmodempty && estempty
            self.ytype = '';
        end%if

        if ~datmodempty && estempty
            self.ytype = 'outputs';
        end%if

        if datmodempty && ~estempty
            self.ytype = 'innovations';
        end%if
    end%function helpsetytype
end%methods

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

methods
    function set.data(self, val)
        if ~isa(val, 'cedata')
            error('data must be a cedata object');
        end%if

        % If user is trying to put in nonempty data, but estimator is also
        % nonempty, throw an error.
        if ~isempty(val) && strcmp(self.ytype, 'innovations')
            error('estimator is already set; cannot add data in this scenario');
        end%if

        self.link(val);
        self.data = val;
        self.helpsetytype();
    end%function set.data

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function set.model(self, val)
        if ~isa(val, 'cemodel')
            error('model must be a cemodel object');
        end%if

        % If user is trying to put in nonempty model, but estimator is also
        % nonempty, throw an error.
        if ~isempty(val) && strcmp(self.ytype, 'innovations')
            error(['estimator is already set; cannot add model in this ', ...
                   'scenario']);
        end%if

        self.link(val);
        self.model = val;
        self.helpsetytype();
    end%function set.model

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function set.estimator(self, val)
        if ~isa(val, 'ceestimator')
            error('estimator must be a ceestimator object');
        end%if

        % If user is trying to put in nonempty estimator, and data or model is
        % also nonempty, throw an error.
        if ~isempty(val) && strcmp(self.ytype, 'outputs')
            error(['data and/or model is already set; cannot add ', ...
                   'estimator in this scenario']);
        end%if

        self.link(val);
        self.estimator = val; % TODO could I move this line and the next one into link function?
        self.helpsetytype();
    end%function set.estimator

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function set.N(self, val)
        [Nisposint, Nround] = isposinteger(val);
        if ~Nisposint
            error('N must be a positive integer');
        end%if

        need2set = self.helpsetX('N', val);
        if need2set
            self.N = Nround;
        end%if
    end%function set.N

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function set.weight(self, val)
        okopts = {'data', 'datafull', 'I', 'Z1', 'Z2'}; % TODO decide on options
        msg = ['weight must be a square, numeric, Hermitian, positive ', ...
               'semidefinite matrix or one of the following character ', ...
               'vectors specifying how it is to be calculated: ''', ...
               strjoin(okopts, ''', '''), ''''];

        if ischarrow(val)
            if ~ismember(val, okopts)
                error(msg);
            end%if
        else
            if ~isnumericmatrix(val) ... 
               || ((~myissquare(val) && ~isempty(val)) ...
               || (~ishermitian(self.weight) ...
               || (myisdefinite(self.weight) == -1)))
                error(msg);
            end%if
        end%if

        need2set = self.helpsetX('weight', val);
        if need2set
            self.weight = val;
        end%if
    end%function set.weight

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function set.fullweightcalc(self, val)
        if ~islogicalscalar(val)
            error('fullweightcalc must be a logical scalar');
        end%if

        need2set = self.helpsetX('fullweightcalc', val);
        if need2set
            self.constrain = val;
        end%if
    end%function set.fullweightcalc

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function set.constrain(self, val)
        if ~islogicalscalar(val)
            error('constrain must be a logical scalar');
        end%if

        need2set = self.helpsetX('constrain', val);
        if need2set
            self.constrain = val;
        end%if
    end%function set.constrain

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function set.solver(self, val)
        oksolvers = {'sdpt3', 'sedumi'};
        if ~isoctave() % If Matlab, mosek is also allowed.
            oksolvers = [oksolvers, 'mosek'];
        end%if

        msg = ['acceptable values for solver: ''', ...
               strjoin(oksolvers, ''', '''), ''''];

        if ~ischarrow(val) || ~ismember(val, oksolvers)
            error(msg);
        end%if

        need2set = self.helpsetX('solver', val);
        if need2set
            self.solver = val;
        end%if
    end%function set.solver

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function set.problem(self, val)
        okprobs = {'QR', 'QRS', 'PhiTheta', 'PhibarThetabar'};

        msg = ['acceptable values for problem: ''', ...
               strjoin(okprobs, ''', '''), ''''];

        if ~ischarrow(val) || ~ismember(val, okprobs)
            error(msg);
        end%if

        need2set = self.helpsetX('problem', val);
        if need2set
            self.problem = val;
        end%if
    end%function set.problem

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function set.exclude(self, val)
        [isnonnegint, eround] = isnonneginteger(val);
        if ~isnonnegint
            error('exclude must be a nonnegative integer');
        end%if

        need2set = self.helpsetX('exclude', val);
        if need2set
            self.exclude = eround;
        end%if
    end%function set.exclude

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function set.tracestates(self, val)
        % TODO implement
        error('tracestates functionality not implemented yet');

% TODO uncomment this when implementing
%        need2set = self.helpsetX('tracestates', val);
%        if need2set
%            self.tracestates = val;
%        endif
%        self.tracestates = [];
    end%function set.tracestates
end%methods

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

end%classdef als

