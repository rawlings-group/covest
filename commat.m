%% Copyright (C) 1995-2015 Kurt Hornik
%% Copyright (C) 2017 Travis Arnold and Michael Risbeck
%%
%% This file is part of Octave.
%%
%% Octave is free software; you can redistribute it and/or modify it
%% under the terms of the GNU General Public License as published by
%% the Free Software Foundation; either version 3 of the License, or (at
%% your option) any later version.
%%
%% Octave is distributed in the hope that it will be useful, but
%% WITHOUT ANY WARRANTY; without even the implied warranty of
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
%% General Public License for more details.
%%
%% You should have received a copy of the GNU General Public License
%% along with Octave; see the file COPYING.  If not, see
%% <http://www.gnu.org/licenses/>.

%% -*- texinfo -*-
%% @deftypefn {Function File} {@var{K} =} commat (@var{m}, @var{n})
%% Return the commutation matrix
%% @tex
%%  $K_{m,n}$
%% @end tex
%% @ifnottex
%%  @nospell{@math{K(m,n)}}
%% @end ifnottex
%% which is the unique
%% @tex
%%  $m n \times m n$
%% @end tex
%% @ifnottex
%%  @var{m}*@var{n} by @var{m}*@var{n}
%% @end ifnottex
%% matrix such that
%% @tex
%%  $K_{m,n} {\rm vec} \, A = {\rm vec} \, A^T$
%% @end tex
%% @ifnottex
%%  @nospell{@math{K(m,n) * vec(A) = vec(A')}}
%% @end ifnottex
%% for all
%% @tex
%%  $m \times n$
%% @end tex
%% @ifnottex
%%  @math{@var{m}} by @math{@var{n}}
%% @end ifnottex
%% matrices
%% @tex
%%  $A$.
%% @end tex
%% @ifnottex
%% @math{A}.
%% @end ifnottex
%%
%% If only one argument @var{m} is given,
%% @tex
%%  $K_{m,m}$
%% @end tex
%% @ifnottex
%%  @nospell{@math{K(m,m)}}
%% @end ifnottex
%% is returned.
%%
%% Unlike Octave's commutation_matrix function, commat returns a sparse
%% matrix, but it is MUCH faster than commutation_matrix. For a
%% demonstration of this, type 'demo commat'.
%%
%% See @nospell{Magnus and Neudecker} (1988), @cite{Matrix Differential
%% Calculus with Applications in Statistics and Econometrics.}
%% @end deftypefn

%% Author: KH <Kurt.Hornik@wu-wien.ac.at>
%% Created: 8 May 1995
%% Adapted-By: jwe
%% Adapted-By: Travis Arnold <tjarnold@wisc.edu> and Michael Risbeck, 
%% January 2017.
%% Adapted to be Matlab compatible, May 2018.

function K = commat(m, n)
    if (nargin() < 1 || nargin() > 2)
        error('commat: one or two input arguments is required');
    end%if

    try
        [misposint, m] = isposinteger(m);
    catch
        error('commat: M must be a positive integer');
    end%trycatch

    if ~misposint
        error('commat: M must be a positive integer');
    end%if

    if (nargin() == 1)
        n = m;
    else
        try
            [nisposint, n] = isposinteger(n);
        catch
            error('commat: N must be a positive integer');
        end%trycatch

        if ~nisposint
            error('commat: N must be a positive integer');
        end%if
    end%if

    i = 1:m;
    j = (1:n)';
    row = bsxfun(@plus, (i-1)*n, j);
    col = bsxfun(@plus, (j-1)*m, i);
    K = sparse(row(:), col(:), 1);
end%function commat

%!assert(full(commat(1,1)), 1);
%!assert(commutation_matrix(10, 20), full(commat(10, 20)));
%!assert(commutation_matrix(7, 4), full(commat(7, 4)));
%!assert(commat(2), commat(2, 2));

%!test
%! A = rand(3, 5);
%! vc = vec(A);
%! vr = vec(A');
%! c = commat(3, 5);
%! assert(c*vc, vr);

%!test
%! A = rand(4,6);
%! vc = vec(A);
%! vr = vec(A');
%! c = commat(4, 6);
%! assert(c*vc, vr);

%!error <one or two input arguments is required> commat()
%!error <one or two input arguments is required> commat(1, 2, 3)
%!error <M must be a positive integer> commat(0,0)
%!error <N must be a positive integer> commat(1,0)
%!error <M must be a positive integer> commat(0,1)

%!demo
%! m1 = 6000;
%! m2 = 155;
%! t1 = cputime();
%! commat(m1);
%! t2 = cputime();
%! commutation_matrix(m2);
%! t3 = cputime();
%! fprintf('commat(%i) : %f seconds\n', m1, t2-t1);
%! fprintf('commutation_matrix(%i) : %f seconds\n', m2, t3-t2);

