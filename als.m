classdef als < handle

%%%%%%% TODO list %%%%%%%
% - make sure that constants that only depend on model can be calculated
%   correctly without a data object attached
% - plots (include an option to show fit to data, but also initial estimator
%   and then new estimator)
% - make sure one can use YALMIP usex0 option. This should be possible with
%   YALMIP's 'assign' function, but I need to test it. NOTE: there's actually
%   no reason to support this, as none of the solvers that als interfaces with
%   (SeDuMi, SDPT3, and MOSEK) support this. Attempting to use the usex0 option
%   produces an error message from YALMIP: 'You have specified an initial
%   point, but the selected solver does not support warm-starts through YALMIP'.
%   More information: https://yalmip.github.io/Initials
% - Add properties like XQ_nonzero, XR_nonzero, etc...? I'm not sure this would
%   be that useful, but maybe it some context it would be. It would also give
%   more of a purpose to properties like O1_nonzero and O2_nonzero.
% - Need to include a note in documetation that using 'data' option for
%   weighting might be dangerous - this option does not guarantee that Pbhathat
%   will be positive semidefinite!
% - Add a warrning if Pbhathat has a negative eigenvalue? Or perhaps an error
%   when YALMIP is needed (it won't work in YALMIP with a semidefinite
%   weighting most of the time I think---I'm not sure if YALMIP would work if W
%   is indefinite but XT * W * X is semidefinite). I would only need to do this
%   when data based weighting is being used.
% - When Pbhathat is indefinite, it generally is only slightly indefinite. I
%   could add a correction whereby I just add an identity matrix multiplied by
%   a positive scalar to make it slightly postive definite.

%%%%%%% Potential ideas %%%%%%%
% - warn if N is too small
% - Automatic calculation for N
% - Automatic calculation for exclude
% - confidence intervals
% - rescale outputs when they are different orders of magnitude? Or will proper
%   weighting matrix take care of this? I could also just leave this out---it
%   wouldn't be too difficult for the user to implement themself on an as
%   needed basis.
% - automatic generation of tradeoff curve and selection of regularization
%   parameters? Probably best to leave this out for now---let the user implement
%   it themself on an as needed basis.
% - custom regularization, where user takes the YALMIP variables and specifies
%   themself the function they want added to the ALS objective function.
% - add a "show" function/property that shows the objective function and
%   constraints of the problem that is being solved.
% - custom weighting matrix. This is a feature I don't anticipate wanting to
%   use myself, so I'm inclined to not add it right now, because it would
%   require some special treatment. Maybe if anyone else ever uses this code
%   and wants it I could add it.
% - allow l2Q, l2R, and l2S to be positive semidefinite matrices to be used as
%   a weighting for the regularization inner product (right now the only option
%   is to use an identity matrix multiplied by a scalar) Note: Lavrentyev
%   regularization is the name of a generalized form of Tikhonov
%   regularization. See Wikipedia page on Tikhonov regularization for more
%   information on generalized Tikhonov regularization. I could also conceive
%   of a way to "weight" the l1 norm similarly, wherein I took the absolute
%   value of each element of the vector, multiplied by different nonnegative
%   constants (instead of 1), and then added.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% properties %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

properties (GetAccess = public, SetAccess = private)
    props
        % Struct. Fields:
        %   base
        %       {'model', 'data', 'estimator', 'N', 'exclude', 'weight', 'M',
        %        'QRS_constrain', 'PhiTheta_constrain',
        %        'PhibbThetabb_constrain', 'YALMIPsettings', 'Szero', 'Qdiag',
        %        'Rdiag', 'rpinvtol', 'l1Q', 'l1R', 'l1S', 'l2Q', 'l2R', 'l2S',
        %        'trQ', 'trR', 'nucQ', 'nucR', 'nucS', 'nucQRS'}
        %   dependent
        %       Cell array of strings givng the current dependent properties of
        %       the als object.
        %   dependsspec
        %       Struct. Field names are the dependent properties of the als
        %       object. Field values are cell arrays of strings of the
        %       corresponding base and dependent properties that are used to
        %       calculate each dependent property.
        %   depends
        %       Struct. Similar to dependsspec, but shows the dependencies in
        %       terms of base properties only.
        %   current
        %       Struct. Field names are the dependent properties of the als
        %       object. Field values are true for dependent properties that are
        %       up to date and false for dependent properties that are out of
        %       date (or that have not been calculated).
        %   store
        %       Struct. Field names are the dependent properties of the als
        %       object. Field values are the cached calculated values of the
        %       dependent properties.
    checks
        % Struct. Fields:
        %   names
        %       Cell array of strings with the names of the checks that are
        %       relevant to the als object.
        %   depends
        %       Struct. Field names are the names of the checks that are
        %       relevant to the als object. Field values are cell arrays of
        %       strings with the base properties that each check depends on.
        %   current
        %       Struct. Field names are the names of the checks that are
        %       relevant to the als object. Field values are true for checks
        %       that are up to date and false for those that are out of
        %       date/have not passed.
    spec
        % Struct. Fields:
        %   model
        %       Logical scalar. True if a cemodel object is directly linked (not
        %       via estimator property), false otherwise.
        %   data
        %       Logical scalar. True if a cedata object is directly linked (not
        %       via estimator property), false otherwise.
        %   estimator
        %       Logical scalar. True if a ceestimator object is linked, false
        %       otherwise.
        %   stoch
        %       Logical scalar. True if the the spec.stochtype property from
        %       self.model or self.estimator.model is not 'none' and false
        %       otherwise. Also false if both model and estimator properties are
        %       empty (that is, if model == [] and estimator == []).
        %   string
        %       Character vector that is a combination of 'm', 'd', 'e', and
        %       's', with each letter being included if each of the last four
        %       fields listed is true, respectively. For example, 'm' is
        %       included if spec.model == true. If spec.model, spec.data, and
        %       spec.estimator are all false, (i.e., model == data == estimator
        %       == []) then spec.string = 'none'.
        %   reg
        %       Cell array of strings representing what kinds of regularization
        %       are to be used. The options are 'l1Q', 'l1R', 'l1S', 'l2Q',
        %       'l2R', 'l2S', 'trQ', 'trR', 'nucQ', 'nucR', 'nucS', and
        %       'nucQRS'.
end%properties

% props_, checks_, and spec_ are identical to props, checks, and spec, but they
% can be accessed without calling the normal get methods.
properties (Hidden, GetAccess = public, SetAccess = private)
    props_
    checks_
    spec_ = struct('model', false(), ...
                   'data', false(), ...
                   'estimator', false(), ...
                   'stoch', false(), ...
                   'string', 'none', ...
                   'reg', {{}})
end%properties

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% base props %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

properties (GetAccess = public, SetAccess = public)
    data
    model
    estimator
    N = 10
    exclude = 0
    weight = 'I' % 'I', 'data', 'exact', 'data_Z1', or 'data_Z2'
    M = 10
    QRS_constrain = false()
    PhiTheta_constrain = 'none'
    PhibbThetabb_constrain = 'none'
    YALMIPsettings % either empty or a struct generated by sdpsettings
    Szero = true()
    Qdiag = false()
    Rdiag = false()
    rpinvtol = 1e-12
    rank_warnings = true()
    l1Q
    l1R
    l1S
    l2Q
    l2R
    l2S
    trQ
    trR
    nucQ
    nucR
    nucS
    nucQRS
end%properties

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% dependent props %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

properties (GetAccess = public, SetAccess = private)
    Ndtotal % total number of data points provided
    Nd % number of data points used in analysis (Ndtotal - exclude)
    Tf % Nd - 1 (final time point, assuming zero based indexing)
    nt % Tf - N + 1
    pt % N * p
    o % o for 'outputs'
    otype % 'raw_y', 'transformed_y', or 'Kcal_innovations'
    b
    bbb
    bhat
    bbbhat
    Lambda
    Lambdahat
    Lambdabb
    Lambdabbhat
    Psi
    Psihat
    Psibb
    Psibbhat
    O1_N
    O2_N
    Obb1_N
    Obb2_N
    Gamma
    Gammabb
    Lambda_nonzero
    Lambdahat_nonzero
    Lambdabb_nonzero
    Lambdabbhat_nonzero
    Psi_nonzero
    Psihat_nonzero
    Psibb_nonzero
    Psibbhat_nonzero
    O1_nonzero
    O2_nonzero
    Obb1_nonzero
    Obb2_nonzero
    Gamma_nonzero
    Gammabb_nonzero
    XQ
    XR
    XS
    XPhi
    XTheta
    XQR
    XQRS
    XPhiTheta
    XQR_rank
    XQRS_rank
    XPhiTheta_rank
    Xbb_temp
    XbbQ
    XbbR
    XbbS
    XbbPhibb
    XbbThetabb
    XbbQR
    XbbQRS
    XbbPhibbThetabb
    XbbQR_rank
    XbbQRS_rank
    XbbPhibbThetabb_rank
    Pbhat
    Pbbbhat
    Pbhathat
    Pbbbhathat
    Wsize
    WQR_pinv
    WQRS_pinv
    WPhiTheta_pinv
    WbbQR_pinv
    WbbQRS_pinv
    WbbPhibbThetabb_pinv
    WQR
    WQRS
    WPhiTheta
    WbbQR
    WbbQRS
    WbbPhibbThetabb
    QRS_YALMIP
    QRS_problem
    Qhat
    Rhat
    Shat
    QRS_objective
    QRS_diagnostic
    QRS_solvetime
    PhiTheta_YALMIP
    PhiTheta_problem
    Phihat
    Thetahat
    PhiTheta_objective
    PhiTheta_diagnostic
    PhiTheta_solvetime
    PhibbThetabb_YALMIP
    PhibbThetabb_problem
    Phibbhat
    Thetabbhat
    PhibbThetabb_objective
    PhibbThetabb_diagnostic
    PhibbThetabb_solvetime
end%properties

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

properties (Hidden, GetAccess = public, SetAccess = private)
    id
end%properties

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% methods %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

methods (Access = public)
    function self = als(varargin)
        % Constructor.

        % Generate uuid.
        self.id = uuidgen();

        % Set initial values for props and checks.
        self.load_constants();

        % Get kwargs struct.
        kwargs = args2kwargs(varargin{:});

        % Make sure kwargs are valid; throw an error if not.
        checkkwargs(kwargs, self.props_.base);

        % Set fields of self.
        for X = fieldnames(kwargs)'
            self.(X{:}) = kwargs.(X{:});
        end%for
    end%function als

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function bool = eq(self, b)
        % Determine if two als instances refer to the same object by comparing
        % their id property. This functionality is built into Matlab, but not
        % Octave.
        if nargin() ~= 2
            error('exactly two input arguments required');
        end%if
        if ~isa(b, 'als')
            error('second argument is not an als object');
        end%if
        bool = strcmp(self.id, b.id);
    end%function eq

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function bool = ne(self, b)
        % Determine if two als instances do not refer to the same object by
        % by comparing their id property. This functionality is built into
        % Matlab, but not Octave.
        if nargin() ~= 2
            error('exactly two input arguments required');
        end%if
        if ~isa(b, 'als')
            error('second argument is not an als object');
        end%if
        bool = ~(self == b);
    end%function ne

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function check(self, varargin)
        % Perform checks on als object.

        % Make sure input arguments are character vectors.
        if ~all(cellfun(@ischarrow, varargin))
            error('all arguments (other than self) must be character vectors');
        end%if

        % Check for unrecoginized check names. Note that this line calls
        % get.checks which will call seeifmdechanged. We don't need to call
        % seeifmdechanged again, so for the rest of this function we use props_,
        % checks_, and spec_.
        unrec = setdiff(varargin, [self.checks.names]);
        if ~isempty(unrec)
            error(['unrecognized input argument(s): ', ...
                   strjoin(unrec, ', '), ' (acceptable arguments are ''', ...
                   strjoin(self.checks_.names, ''', '''), ''')']);
        end%if

        % If no additional arguments are passed, perform all checks.
        checkall = isempty(varargin);

        % Function that tells if each check needs to be performed.
        checkX = @(X) ismember(X, self.checks_.names) ...
                      && (~self.checks_.current.(X) ...
                      && (checkall || ismember(X, varargin)));

        msg = {}; % initialize cell to hold error messages

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Ndsize %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        if checkX('Ndsize')
            % Check that the number of data points is sufficiently large.
            if self.spec_.estimator
                Ndtotal = size(self.estimator.data.y, 2);
            else % data and model given
                Ndtotal = size(self.data.y, 2);
            end%if

            Nd = Ndtotal - self.exclude;
            Tf = Nd - 1;
            bool = (Nd >= self.N);

            if ~bool
                msg = [msg, 'must have Ndtotal - exclude >= N'];
            else
                for Y = {'Ndtotal', 'Nd', 'Tf'}
                    self.props_.store.(Y{:}) = eval(Y{:});
                    self.props_.current.(Y{:}) = true();
                end%for
            end%if
            self.checks_.current.Ndsize = bool;
        end%if checkX('Ndsize')

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%% datamodel %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        if checkX('datamodel')
            % If data and model are given, check that they are compatible. Note
            % that the call to compatible will also cause self.model.check to be
            % called. If only model is given, just run self.model.check. 
            if ~self.spec_.data % (model is given, but not data)
                self.model.check();
                self.checks_.current.datamodel = true();
            else % if self.spec_.data (both model and data are given)
                [bool, cmsg] = self.model.compatible(self.data);
                if ~bool
                    msg = [msg, ['data/model are not compatible: ' cmsg]];
                end%if
                self.checks_.current.datamodel = bool;
            end%if
        end%if checkX('datamodel')

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%% estimator %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        if checkX('estimator')
            % If estimator is given, check it.
            bool = true();
            try
                self.estimator.check();
            catch err
                bool = false();
                msg = [msg, ['estimator check failed with following error ', ...
                             'message: ', err.message]];
            end%trycatch
            self.checks_.current.estimator = bool;
        end%if checkX('estimator')

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Kcal %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        if checkX('Kcal')
            % If estimator is given, we will use the Kcal innovations, property
            % zbb from the estimator. Make sure the model has Kcal given.
            bool = self.estimator.model.spec.Kcal;
            if ~bool
                msg = [msg, ['estimator.model must have property Kcal ', ...
                             'specified']];
            end%if
            self.checks_.current.Kcal = bool;
        end%if checkX('Kcal')

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Astable %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        if checkX('Astable')
            % If data and model are given, check that A is stable.
            self.check('datamodel'); % make sure self.model.Astable will work

            bool = self.model.Astable;
            if ~bool
                msg = [msg, ['A must be stable when data and model are ', ...
                             'specified']];
            end%if

            self.checks_.current.Astable = bool;
        end%if checkX('Astable')

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Abbstable %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        if checkX('Abbstable')
            % If estimator is given, check that Abb is stable.
            self.check('estimator'); % make sure self.estimator.model.Abbstable
            self.check('Kcal');      % will work

            bool = self.estimator.model.Abbstable;
            if ~bool
                msg = [msg, 'Abb must be stable when estimator is specified'];
            end%if

            self.checks_.current.Abbstable = bool;
        end%if checkX('Abbstable')

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%% weightalg %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        if checkX('weightalg')
            % Check that an appropriate algorithm for calculating the weighting
            % matrix has been selected.
            bool = true();
            if strcmp(self.weight, 'exact')
                bool = self.spec_.stoch;
                if ~bool
                    msg = [msg, ...
                         ['cannot have weight = ''exact'' when model does ', ...
                          'not contain any stochastic information']];
                end%if
            elseif strcmp(self.weight, 'data')
                bool = self.spec_.data || self.spec_.estimator;
                if ~bool
                    msg = [msg, ...
               ['cannot have weight = ''data'' when data is not providied ', ...
                '(either on its own or via a ceestimator object)']];
                end%if
            end%if

            self.checks_.current.weightalg = bool;
        end%if checkX('weightalg')

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%% diagS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        if checkX('diagS')
            % Return true for the check no matter what, but if Szero is false
            % and Qdiag and/or Rdiag is true, issue a warning.
            if ~self.Szero && (self.Qdiag || self.Rdiag)
                warning(['Szero is false and Qdiag and/or Rdiag is true---', ...
                         'this is a strange specification, are you sure ', ...
                         'it is what you want?']);
            end%if

            self.checks_.current.diagS = true();
        end%if checkX('diagS')

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Sreg %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        if checkX('Sreg')
            % If Szero is true, check that there are not values in any of l1S,
            % l2S, nucS, or nucQRS.
            bool = ~(self.Szero && (ismember('l1S', self.spec_.reg) ...
                     || (ismember('l2S', self.spec_.reg) ...
                     || (ismember('nucS', self.spec_.reg) ...
                     || ismember('nucQRS', self.spec_.reg)))));
            if ~bool
                msg = [msg, ['when Szero = true(), cannot use l1S, l2S, ', ...
                             'nucS, or nucQRS regularization']];
            end%if

            self.checks_.current.Sreg = bool;
        end%if checkX('Sreg')

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        % If any checks did not pass, throw an error.
        if ~isempty(msg)
            msg = strjoin(msg, char(10));
            error(msg);
        end%if
    end%function check

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function disp(self)
        if isoctave()
            disp('<object als>');
        else % Matlab
            disp(['<object als>', char(10)]);
        end%if
    end%function
end%methods (Access = public)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

methods (Access = private)
    function calculate(self, X)
        % Calculate dependent props. Updates values in self.props.store.

        % Check that als object is valid; throw an error if it is not.
        self.check();

        % TODO at the end, check for props, check, and spec without _.

        % We call check above, which calls get.checks which calls
        % seeifmdechanged. We don't need to call so for the rest of this
        % function we use props_, checks_, and spec_.

        %%%%%%%%%%%%%%%%%%%%%%%%%%% Ndtotal, Nd, Tf %%%%%%%%%%%%%%%%%%%%%%%%%%%%

        if strcmp(X, 'Ndtotal') || (strcmp(X, 'Nd') || strcmp(X, 'Tf'))
            % Ndtotal, Nd, and Tf are calculated and stored by check.
            return;
        end%if strcmp(X, 'Ndtotal', 'Nd', 'Tf')

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% nt %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        if strcmp(X, 'nt')
            self.props_.store.nt = self.Tf - self.N + 1;
            self.props_.current.nt = true();
            return;
        end%if strcmp(X, 'nt')

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% pt %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        if strcmp(X, 'pt')
            if self.spec_.estimator
                p = self.estimator.model.p;
            else % data and model given
                p = self.model.p;
            end%if
            self.props_.store.pt = self.N * p;
            self.props_.current.pt = true();
            return;
        end%if strcmp(X, 'pt')

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% o, otype %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        if strcmp(X, 'o') || strcmp(X, 'otype')
            if self.spec_.estimator
                self.props_.store.o = ...
                                    self.estimator.zbb(:, self.exclude + 1:end);
                self.props_.store.otype = 'Kcal_innovations';
            else % data and model given
                m = self.model;
                d = self.data;
                self.props_.store.o = d.y;
                self.props_.store.otype = 'raw_y';

                % If either B or D are in the model, transform the raw outputs.
                if ismember('B', m.spec.BD)
                    ut = zeros(m.n, self.Ndtotal);
                    for i = 2:self.Ndtotal
                        ut(:, i) = m.A * ut(:, i - 1) + m.B * d.u(:, i - 1);
                    end%for
                    self.props_.store.o = self.props_.store.o - m.C * ut;
                    self.props_.store.otype = 'transformed_y';
                end%if
                if ismember('D', m.spec.BD)
                    self.props_.store.o = self.props_.store.o - m.D * d.u;
                    self.props_.store.otype = 'transformed_y';
                end%if

                % Remove initial excluded columns.
                self.props_.store.o(:, 1:self.exclude) = [];
            end%if

            self.props_.current.o = true();
            self.props_.current.otype = true();
            return;
        end%if strcmp(X, 'o', 'otype')

        %%%%%%%%%%%%%%%%%%%%%%%% Lambdahat, Lambdabbhat %%%%%%%%%%%%%%%%%%%%%%%%

        if strcmp(X, 'Lambdahat') || strcmp(X, 'Lambdabbhat')
            self.props_.store.(X) = self.calcLambdahat(self.o, 1:self.N);
            self.props_.store.(X){1} = forceherm(self.props_.store.(X){1});
            self.props_.current.(X) = true();
            return;
        end%if strcmp(X, 'Lambdahat', 'Lambdabbhat')

        %%%%%%%%%%%%%%%% Lambdahat_nonzero, Lambdabbhat_nonzero %%%%%%%%%%%%%%%%

        if strcmp(X, 'Lambdahat_nonzero') || strcmp(X, 'Lambdabbhat_nonzero')
            str = X;
            str(end - 7:end) = ''; % 'Lambdahat' or 'Lambdabbhat'

            % In the following, we store Lambdahat_j for 0 <= j <= min(M, Tf).
            % There's no need to store Lambdahat_j for j > M becuase we assume
            % Lambda = 0 for these indices. Moreover, we cannot calculate
            % Lambdahat for j > Tf because we don't have enough data points.
            % The indices I refer to in this comment are zero-based, but
            % recall, in Matlab/Octave, arrays use one-based indexing.
            if self.M + 1 <= self.N
                self.props_.store.(X) = self.(str)(1:self.M + 1);
            else
                self.props_.store.(X) = [self.(str); ...
                     self.calcLambdahat(self.o, ...
                                        (self.N + 1):min(self.M + 1, self.Nd))];
            end%if

            self.props_.current.(X) = true();
            return;
        end%if strcmp(X, 'Lambdahat_nonzero', 'Lambdabbhat_nonzero')

        %%%%%%%%%% Psihat, Psibbhat, Psihat_nonzero, Psibbhat_nonzero %%%%%%%%%%

        if strcmp(X, 'Psihat') ...
            || (strcmp(X, 'Psibbhat') ...
            || (strcmp(X, 'Psihat_nonzero') ...
            || strcmp(X, 'Psibbhat_nonzero')))
            str = ['Lambda', X(4:end)]; % 'Lambdahat', 'Lambdabbhat',
                                        % 'Lambdahat_nonzero', or
                                        % 'Lambdabbhat_nonzero'
            self.props_.store.(X) = vertcat(self.(str){:});
            self.props_.current.(X) = true();
            return;
        end%if strcmp(X, 'Psihat', 'Psibbhat', 'Psihat_nonzero
           %             'Psibbhat_nonzero')

        %%%%%%%%%%%%%%%%%%%%%%%%% bhat, bbbhat, b, bbb %%%%%%%%%%%%%%%%%%%%%%%%%

        if strcmp(X, 'bhat') ...
            || strcmp(X, 'bbbhat') ...
            || strcmp(X, 'b') ...
            || strcmp(X, 'bbb')
            str = ['Psi', X(2:end)]; % 'Psihat', 'Psibbhat', 'Psi', or 'Psibb'
            self.props_.store.(X) = myvech(self.(str));
            self.props_.current.(X) = true();
            return;
        end%if strcmp(X, 'bhat', 'bbbhat', 'b', 'bbb')

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%% O1_N, Obb1_N %%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        if strcmp(X, 'O1_N') || strcmp(X, 'Obb1_N')
            self.props_.store.(X) = self.calcO1_N();
            self.props_.current.(X) = true();
            return;
        end%if strcmp(X, 'O1_N', 'Obb1_N')

        %%%%%%%%%%%%%%%%%%%%%%% O1_nonzero, Obb1_nonzero %%%%%%%%%%%%%%%%%%%%%%%

        if strcmp(X, 'O1_nonzero') || strcmp(X, 'Obb1_nonzero')
            str = X; % 'O1_nonzero' or 'Obb1_nonzero'
            str(end - 5:end) = '';
            str(end) = 'N'; % 'O1_N' or 'Obb1_N'
            self.props_.store.(X) = self.calcO1_nonzero(self.(str));
            self.props_.current.(X) = true();
            return;
        end%if strcmp(X, 'O1_nonzero', 'Obb1_nonzero')

        %%%%%%%%%%%%%%%% O2_N, Obb2_N, O2_nonzero, Obb2_nonzero %%%%%%%%%%%%%%%%

        if strcmp(X, 'O2_N') ...
            || (strcmp(X, 'Obb2_N') ...
            || (strcmp(X, 'O2_nonzero') ...
            || strcmp(X, 'Obb2_nonzero')))
            str = X;
            str(str == '2') = '1'; % 'O1_N', 'Obb1_N', 'O1_nonzero', or
                                   % 'Obb1_nonzero'
            self.props_.store.(X) = self.calcO2(self.(str));
            self.props_.current.(X) = true();
            return;
        end%if strcmp(X, 'O2_N', 'Obb2_N', 'O2_nonzero', 'Obb2_nonzero')

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Gamma %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        if strcmp(X, 'Gamma')
            self.props_.store.Gamma = self.calcGamma(self.N);
            self.props_.current.Gamma = true();
            return;
        end%if strcmp(X, 'Gamma')

        %%%%%%%%%%%%%%%%%%%%%%%%%%%% Gamma_nonzero %%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        if strcmp(X, 'Gamma_nonzero')
            self.props_.store.Gamma_nonzero = ...
                                       self.calcGamma(min(self.M + 1, self.Nd));
            self.props_.current.Gamma_nonzero = true();
            return;
        end%if strcmp(X, 'Gamma_nonzero')

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Gammabb %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        if strcmp(X, 'Gammabb')
            self.props_.store.Gammabb = self.Gamma ...
                                      - self.Obb2_N * self.estimator.model.Kcal;
            self.props_.current.Gammabb = true();
            return;
        end%if strcmp(X, 'Gammabb')

        %%%%%%%%%%%%%%%%%%%%%%%%%%% Gammabb_nonzero %%%%%%%%%%%%%%%%%%%%%%%%%%%%

        if strcmp(X, 'Gammabb_nonzero')
            self.props_.store.Gammabb_nonzero = ...
             self.Gamma_nonzero - self.Obb2_nonzero * self.estimator.model.Kcal;
            self.props_.current.Gammabb_nonzero = true();
            return;
        end%if strcmp(X, 'Gammabb_nonzero')

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Psi %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        if strcmp(X, 'Psi')
            self.props_.store.Psi = self.Gamma * self.model.Phi ...
                                     + self.O2_N * self.model.Theta;
            self.props_.current.Psi = true();
            return;
        end%if strcmp(X, 'Psi')

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%% Psi_nonzero %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        if strcmp(X, 'Psi_nonzero')
            self.props_.store.Psi_nonzero = ...
                                         self.Gamma_nonzero * self.model.Phi ...
                                          + self.O2_nonzero * self.model.Theta;
            self.props_.current.Psi_nonzero = true();
            return;
        end%if strcmp(X, 'Psi_nonzero')

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Psibb %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        if strcmp(X, 'Psibb')
            self.props_.store.Psibb = ...
                                  self.Gamma * self.estimator.model.Phibb ...
                                   + self.Obb2_N * self.estimator.model.Thetabb;
            self.props_.current.Psibb = true();
            return;
        end%if strcmp(X, 'Psibb')

        %%%%%%%%%%%%%%%%%%%%%%%%%%%% Psibb_nonzero %%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        if strcmp(X, 'Psibb_nonzero')
            self.props_.store.Psibb_nonzero = ...
                            self.Gamma_nonzero * self.estimator.model.Phibb ...
                             + self.Obb2_nonzero * self.estimator.model.Thetabb;
            self.props_.current.Psibb_nonzero = true();
            return;
        end%if strcmp(X, 'Psibb_nonzero')

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Lambda %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        if strcmp(X, 'Lambda')
            p = self.model.p;
            self.props_.store.Lambda = ...
                                     mat2cell(self.Psi, p * ones(self.N, 1), p);
            self.props_.current.Lambda = true();
            return;
        end%if strcmp(X, 'Lambda')

        %%%%%%%%%%%%%%%%%%%%%%%%%%%% Lambda_nonzero %%%%%%%%%%%%%%%%%%%%%%%%%%%%

        if strcmp(X, 'Lambda_nonzero')
            p = self.model.p;
            self.props_.store.Lambda_nonzero = ...
                             mat2cell(self.Psi_nonzero, ...
                                      p * ones(min(self.M + 1, self.Nd), 1), p);
            self.props_.current.Lambda_nonzero = true();
            return;
        end%if strcmp(X, 'Lambda_nonzero')

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Lambdabb %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        if strcmp(X, 'Lambdabb')
            p = self.estimator.model.p;
            self.props_.store.Lambdabb = ...
                                   mat2cell(self.Psibb, p * ones(self.N, 1), p);
            self.props_.current.Lambdabb = true();
            return;
        end%if strcmp(X, 'Lambdabb')

        %%%%%%%%%%%%%%%%%%%%%%%%%%% Lambdabb_nonzero %%%%%%%%%%%%%%%%%%%%%%%%%%%

        if strcmp(X, 'Lambdabb_nonzero')
            p = self.estimator.model.p;
            self.props_.store.Lambdabb_nonzero = ...
                             mat2cell(self.Psibb_nonzero, ...
                                      p * ones(min(self.M + 1, self.Nd), 1), p);
            self.props_.current.Lambdabb_nonzero = true();
            return;
        end%if strcmp(X, 'Lambdabb_nonzero')

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% XQ %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        if strcmp(X, 'XQ')
            m = self.model;
            self.props_.store.XQ = ...
                      ApinvB(kron(m.C, self.O1_N), eye(m.n^2) - kron(m.A, m.A));

            if ismember('G', m.spec.GH)
                g = m.g;
                self.props_.store.XQ = self.props_.store.XQ * kron(m.G, m.G);
            else
                g = m.n;
            end%if

            self.props_.store.XQ = self.props_.store.XQ * dupmat(g);

            % If diagonal option is on, extract the columns for only the
            % diagonal elements of Q.
            if self.Qdiag
                cols = (1:(g + 1):(g^2)) - cumsum(0:(g - 1));
                self.props_.store.XQ = self.props_.store.XQ(:, cols);
            end%if

            self.props_.current.XQ = true();
            return;
        end%if strcmp(X, 'XQ')

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% XR %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        if strcmp(X, 'XR')
            m = self.model;
            self.props_.store.XR = kron(speye(m.p), self.Gamma);

            if ismember('H', m.spec.GH)
                h = m.h;
                self.props_.store.XR = ...
                                  sparse(self.props_.store.XR * kron(m.H, m.H));
            else
                h = m.p;
            end%if

            self.props_.store.XR = self.props_.store.XR * dupmat(h);

            % If diagonal option is on, extract the columns for only the
            % diagonal elements of R.
            if self.Rdiag
                cols = (1:(h + 1):(h^2)) - cumsum(0:(h - 1));
                self.props_.store.XR = self.props_.store.XR(:, cols);
            end%if

            self.props_.current.XR = true();
            return;
        end%if strcmp(X, 'XR')

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% XS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        if strcmp(X, 'XS')
            m = self.model;

            if ismember('G', m.spec.GH)
                G = m.G;
            else
%                G = eye(m.n); % full
                G = speye(m.n); % sparse
            end%if

            if ismember('H', m.spec.GH)
                H = m.H;
            else
%                H = eye(m.p); % full
                H = speye(m.p); % sparse
            end%if

            self.props_.store.XS = kron(H, self.O2_N * G);
            self.props_.current.XS = true();
            return;
        end%if strcmp(X, 'XS')

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% XPhi %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        if strcmp(X, 'XPhi')
            m = self.model;
            self.props_.store.XPhi = kron(speye(m.p), self.Gamma) * dupmat(m.p);
            self.props_.current.XPhi = true();
            return;
        end%if strcmp(X, 'XPhi')

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% XTheta %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        if strcmp(X, 'XTheta')
            self.props_.store.XTheta = kron(speye(self.model.p), self.O2_N);
            self.props_.current.XTheta = true();
            return;
        end%if strcmp(X, 'XTheta')

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% XQR %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        if strcmp(X, 'XQR')
            self.props_.store.XQR = dmatpinv(self.pt, self.model.p) ...
                                     * full([self.XQ, self.XR]);
            self.props_.current.XQR = true();
            return;
        end%if strcmp(X, 'XQR')

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% XQR_rank %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        if strcmp(X, 'XQR_rank')
            if self.rank_warnings
                self.props_.store.XQR_rank = rank_dyntol(self.XQR, 1e-8);
            else
                self.props_.store.XQR_rank = 'N/A';
            end%if

            self.props_.current.XQR_rank = true();
            return;
        end%if strcmp(X, 'XQR_rank')

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% XQRS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        if strcmp(X, 'XQRS')
            self.props_.store.XQRS = dmatpinv(self.pt, self.model.p) ...
                                      * full([self.XQ, self.XR, self.XS]);
            self.props_.current.XQRS = true();
            return;
        end%if strcmp(X, 'XQRS')

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%% XQRS_rank %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        if strcmp(X, 'XQRS_rank')
            if self.rank_warnings
                self.props_.store.XQRS_rank = rank_dyntol(self.XQRS, 1e-8);
            else
                self.props_.store.XQRS_rank = 'N/A';
            end%if
            self.props_.current.XQRS_rank = true();
            return;
        end%if strcmp(X, 'XQRS_rank')

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%% XPhiTheta %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        if strcmp(X, 'XPhiTheta')
            self.props_.store.XPhiTheta = dmatpinv(self.pt, self.model.p) ...
                                           * [self.XPhi, self.XTheta];
            self.props_.current.XPhiTheta = true();
            return;
        end%if strcmp(X, 'XPhiTheta')

        %%%%%%%%%%%%%%%%%%%%%%%%%%%% XPhiTheta_rank %%%%%%%%%%%%%%%%%%%%%%%%%%%%

        if strcmp(X, 'XPhiTheta_rank')
            if self.rank_warnings
                self.props_.store.XPhiTheta_rank = ...
                                        rank_dyntol(full(self.XPhiTheta), 1e-8);
            else
                self.props_.store.XPhiTheta_rank = 'N/A';
            end%if
            self.props_.current.XPhiTheta_rank = true();
            return;
        end%if strcmp(X, 'XPhiTheta_rank')

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Xbb_temp %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        if strcmp(X, 'Xbb_temp')
            % (C kron Obb1_N) * (I_{n^2} - Abb kron Abb)^{-1}
            m = self.estimator.model;

            self.props_.store.Xbb_temp = ...
                ApinvB(kron(m.C, self.Obb1_N), eye(m.n^2) - kron(m.Abb, m.Abb));

            self.props_.current.Xbb_temp = true();
            return;
        end%if strcmp(X, 'Xbb_temp')

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% XbbQ %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        if strcmp(X, 'XbbQ')
            m = self.estimator.model;
            self.props_.store.XbbQ = self.Xbb_tmp;

            if ismember('G', m.spec.GH)
                g = m.g;
                self.props_.store.XbbQ = ...
                                        self.props_.store.XbbQ * kron(m.G, m.G);
            else
                g = m.n;
            end%if

            self.props_.store.XbbQ = self.props_.store.XbbQ * dupmat(g);

            % If diagonal option is on, extract the columns for only the
            % diagonal elements of Q.
            if self.Qdiag
                cols = (1:(g + 1):(g^2)) - cumsum(0:(g - 1));
                self.props_.store.XbbQ = self.props_.store.XbbQ(:, cols);
            end%if

            self.props_.current.XbbQ = true();
            return;
        end%if strcmp(X, 'XbbQ')

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% XbbR %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        if strcmp(X, 'XbbR')
            m = self.estimator.model;
            self.props_.store.XbbR = self.Xbb_tmp * kron(m.Kcal, m.Kcal) ...
                                      + kron(eye(m.p), self.Gammabb);

            if ismember('H', m.spec.GH)
                h = m.h;
                self.props_.store.XbbR = self.props_.store.XbbR ...
                                          * kron(m.H, m.H);
            else
                h = m.p;
            end%if

            self.props_.store.XbbR = self.props_.store.XbbR * dupmat(h);

            % If diagonal option is on, extract the columns for only the
            % diagonal elements of R.
            if self.Rdiag
                cols = (1:(h + 1):(h^2)) - cumsum(0:(h - 1));
                self.props_.store.XbbR = self.props_.store.XbbR(:, cols);
            end%if

            self.props_.current.XbbR = true();
            return;
        end%if strcmp(X, 'XbbR')

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% XbbS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        if strcmp(X, 'XbbS')
            m = self.estimator.model;

            if ismember('G', m.spec.GH)
                G = m.G;
            else
%                G = eye(m.n); % full
                G = speye(m.n); % sparse
            end%if

            if ismember('H', m.spec.GH)
                H = m.H;
            else
%                H = eye(m.p); % full
                H = speye(m.p); % sparse
            end%if

            self.props_.store.XbbS = -self.Xbb_tmp ...
                                       * (speye(m.n^2) + commat(m.n)) ...
                                       * kron(m.Kcal * H, G) ...
                                       + kron(H, self.Obb2_N * G);
            self.props_.current.XbbS = true();
            return;
        end%if strcmp(X, 'XbbS')

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% XbbPhibb %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        if strcmp(X, 'XbbPhibb')
            m = self.estimator.model;
            self.props_.store.XbbPhibb = ...
                                     kron(speye(m.p), self.Gamma) * dupmat(m.p);
            self.props_.current.XbbPhibb = true();
            return;
        end%if strcmp(X, 'XbbPhibb')

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%% XbbThetabb %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        if strcmp(X, 'XbbThetabb')
            m = self.estimator.model;
            self.props_.store.XbbThetabb = kron(speye(m.p), self.Obb2_N);
            self.props_.current.XbbThetabb = true();
            return;
        end%if strcmp(X, 'XbbThetabb')

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% XbbQR %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        if strcmp(X, 'XbbQR')
            self.props_.store.XbbQR = ...
                                   dmatpinv(self.pt, self.estimator.model.p) ...
                                    * [self.XbbQ, self.XbbR];
            self.props_.current.XbbQR = true();
            return;
        end%if strcmp(X, 'XbbQR')

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%% XbbQR_rank %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        if strcmp(X, 'XbbQR_rank')
            if self.rank_warnings
                self.props_.store.XbbQR_rank = rank_dyntol(self.XbbQR, 1e-8);
            else
                self.props_.store.XbbQR_rank = 'N/A';
            end%if
            self.props_.current.XbbQR_rank = true();
            return;
        end%if strcmp(X, 'XbbQR_rank')

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% XbbQRS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        if strcmp(X, 'XbbQRS')
            self.props_.store.XbbQRS = ...
                                   dmatpinv(self.pt, self.estimator.model.p) ...
                                    * [self.XbbQ, self.XbbR, self.XbbS];
            self.props_.current.XbbQRS = true();
            return;
        end%if strcmp(X, 'XbbQRS')

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%% XbbQRS_rank %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        if strcmp(X, 'XbbQRS_rank')
            if self.rank_warnings
                self.props_.store.XbbQRS_rank = rank_dyntol(self.XbbQRS, 1e-8);
            else
                self.props_.store.XbbQRS_rank = 'N/A';
            end%if
            self.props_.current.XbbQRS_rank = true();
            return;
        end%if strcmp(X, 'XbbQRS_rank')

        %%%%%%%%%%%%%%%%%%%%%%%%%%% XbbPhibbThetabb %%%%%%%%%%%%%%%%%%%%%%%%%%%%

        if strcmp(X, 'XbbPhibbThetabb')
            self.props_.store.XbbPhibbThetabb = ...
                                   dmatpinv(self.pt, self.estimator.model.p) ...
                                    * [self.XbbPhibb, self.XbbThetabb];
            self.props_.current.XbbPhibbThetabb = true();
            return;
        end%if strcmp(X, 'XbbPhibbThetabb')

        %%%%%%%%%%%%%%%%%%%%%%%%% XbbPhibbThetabb_rank %%%%%%%%%%%%%%%%%%%%%%%%%

        if strcmp(X, 'XbbPhibbThetabb_rank')
            if self.rank_warnings
                self.props_.store.XbbPhibbThetabb_rank = ...
                                  rank_dyntol(full(self.XbbPhibbThetabb), 1e-8);
            else
                self.props_.store.XbbPhibbThetabb_rank = 'N/A';
            end%if
            self.props_.current.XbbPhibbThetabb_rank = true();
            return;
        end%if strcmp(X, 'XbbPhibbThetabb_rank')

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Pbhat %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        if strcmp(X, 'Pbhat')
            self.props_.store.Pbhat = self.do_Pbhat_calc('Lambda_nonzero');
            self.props_.current.Pbhat = true();
            return;
        end%if strcmp(X, 'Pbhat')

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Pbbbhat %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        if strcmp(X, 'Pbbbhat')
            self.props_.store.Pbbbhat = self.do_Pbhat_calc('Lambdabb_nonzero');
            self.props_.current.Pbbbhat = true();
            return;
        end%if strcmp(X, 'Pbbbhat')

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Pbhathat %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        if strcmp(X, 'Pbhathat')
            switch self.weight
                case 'data_Z1'
                    self.props_.store.Pbhathat = self.do_Z1_calc();
                case 'data_Z2'
                    self.props_.store.Pbhathat = self.do_Z2_calc();
                otherwise
                    self.props_.store.Pbhathat = ...
                                        self.do_Pbhat_calc('Lambdahat_nonzero');
            end%switch
            self.props_.current.Pbhathat = true();
            return;
        end%if strcmp(X, 'Pbhathat')

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Pbbbhathat %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        if strcmp(X, 'Pbbbhathat')
            switch self.weight
                case 'data_Z1'
                    self.props_.store.Pbbbhathat = self.do_Z1_calc();
                case 'data_Z2'
                    self.props_.store.Pbbbhathat = self.do_Z2_calc();
                otherwise
                    self.props_.store.Pbbbhathat = ...
                                      self.do_Pbhat_calc('Lambdabbhat_nonzero');
            end%switch
            self.props_.current.Pbbbhathat = true();
            return;
        end%if strcmp(X, 'Pbbbhathat')

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Wsize %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        if strcmp(X, 'Wsize')
            if self.spec_.estimator
                p = self.estimator.model.p;
            else % data/model given
                p = self.model.p;
            end%if

            self.props_.store.Wsize = self.pt * p - p * (p - 1) / 2;
            self.props_.current.Wsize = true();
            return;
        end%if strcmp(X, 'Wsize')

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% WQR_pinv %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        if strcmp(X, 'WQR_pinv')
            switch self.weight
                case 'I'
                    self.props_.store.WQR_pinv = speye(self.Wsize);

                case 'exact'
                    self.props_.store.WQR_pinv = ...
                                   forceherm(self.Pbhat + self.XQR * self.XQR');

                otherwise % 'data', 'data_Z1', or 'data_Z2'
                    self.props_.store.WQR_pinv = ...
                                forceherm(self.Pbhathat + self.XQR * self.XQR');
            end%switch

            self.props_.current.WQR_pinv = true();
            return;
        end%if strcmp(X, 'WQR_pinv')

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%% WQRS_pinv %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        if strcmp(X, 'WQRS_pinv')
            switch self.weight
                case 'I'
                    self.props_.store.WQRS_pinv = speye(self.Wsize);

                case 'exact'
                    self.props_.store.WQRS_pinv = ...
                                 forceherm(self.Pbhat + self.XQRS * self.XQRS');

                otherwise % 'data', 'data_Z1', or 'data_Z2'
                    self.props_.store.WQRS_pinv = ...
                              forceherm(self.Pbhathat + self.XQRS * self.XQRS');
            end%switch

            self.props_.current.WQRS_pinv = true();
            return;
        end%if strcmp(X, 'WQRS_pinv')

        %%%%%%%%%%%%%%%%%%%%%%%%%%%% WPhiTheta_pinv %%%%%%%%%%%%%%%%%%%%%%%%%%%%

        if strcmp(X, 'WPhiTheta_pinv')
            switch self.weight
                case 'I'
                    self.props_.store.WPhiTheta_pinv = speye(self.Wsize);

                case 'exact'
                    self.props_.store.WPhiTheta_pinv = ...
                       forceherm(self.Pbhat + self.XPhiTheta * self.XPhiTheta');

                otherwise % 'data', 'data_Z1', or 'data_Z2'
                    self.props_.store.WPhiTheta_pinv = ...
                    forceherm(self.Pbhathat + self.XPhiTheta * self.XPhiTheta');
            end%switch

            self.props_.current.WPhiTheta_pinv = true();
            return;
        end%if strcmp(X, 'WPhiTheta_pinv')

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%% WbbQR_pinv %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        if strcmp(X, 'WbbQR_pinv')
            switch self.weight
                case 'I'
                    self.props_.store.WbbQR_pinv = speye(self.Wsize);

                case 'exact'
                    self.props_.store.WbbQR_pinv = ...
                             forceherm(self.Pbbbhat + self.XbbQR * self.XbbQR');

                otherwise % 'data', 'data_Z1', or 'data_Z2'
                    self.props_.store.WbbQR_pinv = ...
                          forceherm(self.Pbbbhathat + self.XbbQR * self.XbbQR');
            end%switch

            self.props_.current.WbbQR_pinv = true();
            return;
        end%if strcmp(X, 'WbbQR_pinv')

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%% WbbQRS_pinv %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        if strcmp(X, 'WbbQRS_pinv')
            switch self.weight
                case 'I'
                    self.props_.store.WbbQRS_pinv = speye(self.Wsize);

                case 'exact'
                    self.props_.store.WbbQRS_pinv = ...
                           forceherm(self.Pbbbhat + self.XbbQRS * self.XbbQRS');

                otherwise % 'data', 'data_Z1', or 'data_Z2'
                    self.props_.store.WbbQRS_pinv = ...
                        forceherm(self.Pbbbhathat + self.XbbQRS * self.XbbQRS');
            end%switch

            self.props_.current.WbbQRS_pinv = true();
            return;
        end%if strcmp(X, 'WbbQRS_pinv')

        %%%%%%%%%%%%%%%%%%%%%%%%% WbbPhibbThetabb_pinv %%%%%%%%%%%%%%%%%%%%%%%%%

        if strcmp(X, 'WbbPhibbThetabb_pinv')
            switch self.weight
                case 'I'
                    self.props_.store.WbbPhibbThetabb_pinv = speye(self.Wsize);

                case 'exact'
                    self.props_.store.WbbPhibbThetabb_pinv = ...
         forceherm(self.Pbbbhat + self.XbbPhibbThetabb * self.XbbPhibbThetabb');

                otherwise % 'data', 'data_Z1', or 'data_Z2'
                    self.props_.store.WbbPhibbThetabb_pinv = ...
      forceherm(self.Pbbbhathat + self.XbbPhibbThetabb * self.XbbPhibbThetabb');
            end%switch

            self.props_.current.WbbPhibbThetabb_pinv = true();
            return;
        end%if strcmp(X, 'WbbPhibbThetabb_pinv')

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% WQR %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        if strcmp(X, 'WQR')
            if strcmp(self.weight, 'I')
                self.props_.store.WQR = speye(self.Wsize);
            else
                self.props_.store.WQR = rpinv(self.WQR_pinv, self.rpinvtol);
            end%if
            self.props_.current.WQR = true();
            return;
        end%if strcmp(X, 'WQR')

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% WQRS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        if strcmp(X, 'WQRS')
            if strcmp(self.weight, 'I')
                self.props_.store.WQRS = speye(self.Wsize);
            else
                self.props_.store.WQRS = rpinv(self.WQRS_pinv, self.rpinvtol);
            end%if
            self.props_.current.WQRS = true();
            return;
        end%if strcmp(X, 'WQRS')

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%% WPhiTheta %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        if strcmp(X, 'WPhiTheta')
            if strcmp(self.weight, 'I')
                self.props_.store.WPhiTheta = speye(self.Wsize);
            else
                self.props_.store.WPhiTheta = ...
                                      rpinv(self.WPhiTheta_pinv, self.rpinvtol);
            end%if
            self.props_.current.WPhiTheta = true();
            return;
        end%if strcmp(X, 'WPhiTheta')

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% WbbQR %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        if strcmp(X, 'WbbQR')
            if strcmp(self.weight, 'I')
                self.props_.store.WbbQR = speye(self.Wsize);
            else
                self.props_.store.WbbQR = rpinv(self.WbbQR_pinv, self.rpinvtol);
            end%if
            self.props_.current.WbbQR = true();
            return;
        end%if strcmp(X, 'WbbQR')

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% WbbQRS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        if strcmp(X, 'WbbQRS')
            if strcmp(self.weight, 'I')
                self.props_.store.WbbQRS = speye(self.Wsize);
            else
                self.props_.store.WbbQRS = ...
                                         rpinv(self.WbbQRS_pinv, self.rpinvtol);
            end%if
            self.props_.current.WbbQRS = true();
            return;
        end%if strcmp(X, 'WbbQRS')

        %%%%%%%%%%%%%%%%%%%%%%%%%%% WbbPhibbThetabb %%%%%%%%%%%%%%%%%%%%%%%%%%%%

        if strcmp(X, 'WbbPhibbThetabb')
            if strcmp(self.weight, 'I')
                self.props_.store.WbbPhibbThetabb = speye(self.Wsize);
            else
                self.props_.store.WbbPhibbThetabb = ...
                                rpinv(self.WbbPhibbThetabb_pinv, self.rpinvtol);
            end%if
            self.props_.current.WbbPhibbThetabb = true();
            return;
        end%if strcmp(X, 'WbbPhibbThetabb')

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%% QRS_YALMIP %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        if strcmp(X, 'QRS_YALMIP')
            % Use YALMIP if solving constrained problem and/or if using
            % regularization.
            useYALMIP = self.QRS_constrain || ~isempty(self.spec_.reg);

            if useYALMIP
                % Make sure YALMIP is loaded.
                if exist('yalmip.m', 'file') ~= 2
                    error('cannot find YALMIP; are you sure it is ', ...
                          'installed and correctly loaded on the path?');
                end%if

                % Extract model.
                if self.spec_.model
                    m = self.model;
                else
                    m = self.estimator.model;
                end%if

                % Extract value of g.
                if ismember('G', m.spec.GH)
                    g = m.g;
                else
                    g = m.n;
                end%if

                % Extract value of h.
                if ismember('H', m.spec.GH)
                    h = m.h;
                else
                    h = m.p;
                end%if

                % Create YALMIP variables.
                self.props_.store.QRS_YALMIP = struct();

                % Create Q and Qss variables.
                if self.Qdiag
                    % vector (only diagonal elements):
                    self.props_.store.QRS_YALMIP.Qss = sdpvar(g, 1);
                    % matrix:
                    self.props_.store.QRS_YALMIP.Q = ...
                                         diag(self.props_.store.QRS_YALMIP.Qss);
                else % ~self.Qdiag
                    % matrix:
                    self.props_.store.QRS_YALMIP.Q = sdpvar(g);
                    % vector (elements on and below diagonal):
                    self.props_.store.QRS_YALMIP.Qss = ...
                                         myvech(self.props_.store.QRS_YALMIP.Q);
                end%if

                % Create R and Rss variables.
                if self.Rdiag
                    % vector (only diagonal elements):
                    self.props_.store.QRS_YALMIP.Rss = sdpvar(h, 1);
                    % matrix:
                    self.props_.store.QRS_YALMIP.R = ...
                                         diag(self.props_.store.QRS_YALMIP.Rss);
                else % ~self.Rdiag
                    % matrix:
                    self.props_.store.QRS_YALMIP.R = sdpvar(h);
                    % vector (elements on and below diagonal):
                    self.props_.store.QRS_YALMIP.Rss = ...
                                         myvech(self.props_.store.QRS_YALMIP.R);
                end%if

                % Create S and Ss variables.
                if self.Szero
                    % Remove S variables if they exist.
                    if isfield(self.props_.store.QRS_YALMIP, 'S')
                        self.props_.store.QRS_YALMIP = ...
                             rmfield(self.props_.store.QRS_YALMIP, {'S', 'Ss'});
                    end%if
                else % ~self.Szero
                    % matrix:
                    self.props_.store.QRS_YALMIP.S = sdpvar(g, h, 'full');
                    % vector:
                    self.props_.store.QRS_YALMIP.Ss = ...
                                              self.props_.store.QRS_YALMIP.S(:);
                end%if

                % Create YALMIP constraints.
                self.props_.store.QRS_YALMIP.constraints = [];
                if self.QRS_constrain
                    if self.Szero
                        if self.Qdiag
                            self.props_.store.QRS_YALMIP.constraints ...
                                = [self.props_.store.QRS_YALMIP.constraints; ...
                                   self.props_.store.QRS_YALMIP.Qss >= 0];
                        else % if ~self.Qdiag
                            self.props_.store.QRS_YALMIP.constraints ...
                                = [self.props_.store.QRS_YALMIP.constraints; ...
                                   self.props_.store.QRS_YALMIP.Q >= 0];
                        end%if
                        if self.Rdiag
                            self.props_.store.QRS_YALMIP.constraints ...
                                = [self.props_.store.QRS_YALMIP.constraints; ...
                                   self.props_.store.QRS_YALMIP.Rss >= 0];
                        else % if ~self.Rdiag
                            self.props_.store.QRS_YALMIP.constraints ...
                                = [self.props_.store.QRS_YALMIP.constraints; ...
                                   self.props_.store.QRS_YALMIP.R >= 0];
                        end%if
                    else % ~self.Szero
                        self.props_.store.QRS_YALMIP.constraints ...
                                      = [[self.props_.store.QRS_YALMIP.Q, ...
                                          self.props_.store.QRS_YALMIP.S; ...
                                          self.props_.store.QRS_YALMIP.S', ...
                                          self.props_.store.QRS_YALMIP.R] >= 0];
                    end%if
                end%if

                % Struct to hold parts of YALMIP objective function.
                self.props_.store.QRS_YALMIP.objective = struct();

                % Extract Xname, bhatname, and Wname, and create decision
                % variable vector.
                if self.spec_.estimator
                    Xname = 'XbbQR';
                    bhatname = 'bbbhat';
                else % ~self.spec_.estimator
                    Xname = 'XQR';
                    bhatname = 'bhat';
                end%if
                if self.Szero
                    Y = [self.props_.store.QRS_YALMIP.Qss; ...
                         self.props_.store.QRS_YALMIP.Rss];
                else % ~self.Szero
                    Y = [self.props_.store.QRS_YALMIP.Qss; ...
                         self.props_.store.QRS_YALMIP.Rss; ...
                         self.props_.store.QRS_YALMIP.Ss];
                    Xname = [Xname, 'S']; % 'XQRS' or 'XbbQRS'
                end%if
                Wname = Xname;
                Wname(1) = 'W'; % 'WQR', 'WbbQR', 'WQRS' or 'WbbQRS'
                Wnamepinv = [Wname, '_pinv'];

                % Create least squares term of objective function.
                dff = self.(Xname) * Y - self.(bhatname);
                if strcmp(self.weight, 'I')
                    self.props_.store.QRS_YALMIP.objective.lsq = ...
                                                               0.5 * dff' * dff;
                else
                    % Here we need to use slashes instead of pinvAB and
                    % ApinvB---the latter will not work with YALMIP sdpvars!
                    % TODO try all three of these options
%                    self.props_.store.QRS_YALMIP.objective.lsq = ...
%                                                0.5 * dff' * self.(Wname) * dff;
%                    self.props_.store.QRS_YALMIP.objective.lsq = ...
%                                          0.5 * (dff' / self.(Wnamepinv)) * dff;
                    self.props_.store.QRS_YALMIP.objective.lsq = ...
                                          0.5 * dff' * (self.(Wnamepinv) \ dff);
                end%if

                % Create regularization terms of objective function.
                self.props_.store.QRS_YALMIP.objective.total = ...
                                     self.props_.store.QRS_YALMIP.objective.lsq;
                for Z = self.spec_.reg
                    Z = Z{:};
                    switch Z
                        case 'l1Q'
                            self.props_.store.QRS_YALMIP.objective.l1Q = ...
                           self.l1Q * norm(self.props_.store.QRS_YALMIP.Qss, 1);

                        case 'l1R'
                            self.props_.store.QRS_YALMIP.objective.l1R = ...
                           self.l1R * norm(self.props_.store.QRS_YALMIP.Rss, 1);

                        case 'l1S'
                            self.props_.store.QRS_YALMIP.objective.l1S = ...
                            self.l1S * norm(self.props_.store.QRS_YALMIP.Ss, 1);

                        case 'l2Q'
                            self.props_.store.QRS_YALMIP.objective.l2Q = ...
                               self.l2Q * (self.props_.store.QRS_YALMIP.Qss' ...
                                           * self.props_.store.QRS_YALMIP.Qss);

                        case 'l2R'
                            self.props_.store.QRS_YALMIP.objective.l2R = ...
                               self.l2R * (self.props_.store.QRS_YALMIP.Rss' ...
                                           * self.props_.store.QRS_YALMIP.Rss);

                        case 'l2S'
                            self.props_.store.QRS_YALMIP.objective.l2S = ...
                                self.l2S * (self.props_.store.QRS_YALMIP.Ss' ...
                                            * self.props_.store.QRS_YALMIP.Ss);

                        case 'trQ'
                            if isscalar(self.trQ)
                                self.props_.store.QRS_YALMIP.objective.trQ = ...
                               self.trQ * trace(self.props_.store.QRS_YALMIP.Q);
                            else % self.trQ is a vector
                                if numel(self.trQ) ~= g
                                    error(['when it is a vector, trQ must ', ...
                                           'have the same number of ', ...
                                           'elements as the dimesion of Q']);
                                end%if
                                self.props_.store.QRS_YALMIP.objective.trQ = ...
                            self.trQ(:)' * diag(self.props_.store.QRS_YALMIP.Q);
                            end%if

                        case 'trR'
                            if isscalar(self.trR)
                                self.props_.store.QRS_YALMIP.objective.trR = ...
                               self.trR * trace(self.props_.store.QRS_YALMIP.R);
                            else % self.trR is a vector
                                if numel(self.trR) ~= h
                                    error(['when it is a vector, trR must ', ...
                                           'have the same number of ', ...
                                           'elements as the dimesion of R']);
                                end%if
                                self.props_.store.QRS_YALMIP.objective.trR = ...
                            self.trR(:)' * diag(self.props_.store.QRS_YALMIP.R);
                            end%if

                        case 'nucQ'
                            if self.QRS_constrain
                                % nuclear norm is equal to trace
                               self.props_.store.QRS_YALMIP.objective.nucQ = ...
                              self.nucQ * trace(self.props_.store.QRS_YALMIP.Q);
                            else%if ~self.QRS_constrain
                                if self.Qdiag
                               self.props_.store.QRS_YALMIP.objective.nucQ = ...
                          self.nucQ * norm(self.props_.store.QRS_YALMIP.Qss, 1);
                                else%if ~self.Qdiag
                               self.props_.store.QRS_YALMIP.objective.nucQ = ...
                    self.nucQ * norm(self.props_.store.QRS_YALMIP.Q, 'nuclear');
                                end%if
                            end%if

                        case 'nucR'
                            if self.QRS_constrain
                                % nuclear norm is equal to trace
                               self.props_.store.QRS_YALMIP.objective.nucR = ...
                              self.nucR * trace(self.props_.store.QRS_YALMIP.R);
                            else%if ~self.QRS_constrain
                                if self.Rdiag
                               self.props_.store.QRS_YALMIP.objective.nucR = ...
                          self.nucR * norm(self.props_.store.QRS_YALMIP.Rss, 1);
                                else%if ~self.Rdiag
                               self.props_.store.QRS_YALMIP.objective.nucR = ...
                    self.nucR * norm(self.props_.store.QRS_YALMIP.R, 'nuclear');
                                end%if
                            end%if

                        case 'nucS'
                           self.props_.store.QRS_YALMIP.objective.nucS = ...
                    self.nucS * norm(self.props_.store.QRS_YALMIP.S, 'nuclear');

                        case 'nucQRS'
                            if self.QRS_constrain
                                % nuclear norm is equal to trace
                             self.props_.store.QRS_YALMIP.objective.nucQRS = ...
                        self.nucQRS * (trace(self.props_.store.QRS_YALMIP.Q) ...
                                       + trace(self.props_.store.QRS_YALMIP.R));
                            else%if ~self.QRS_constrain
                             self.props_.store.QRS_YALMIP.objective.nucQRS = ...
                        self.nucQRS * norm([self.props_.store.QRS_YALMIP.Q, ...
                                            self.props_.store.QRS_YALMIP.S; ...
                                            self.props_.store.QRS_YALMIP.S', ...
                                            self.props_.store.QRS_YALMIP.R], ...
                                            'nuclear');
                            end%if
                    end%switch

                    self.props_.store.QRS_YALMIP.objective.total = ...
                                self.props_.store.QRS_YALMIP.objective.total ...
                                   + self.props_.store.QRS_YALMIP.objective.(Z);
                end%for Z = self.spec_.reg
            else % if ~useYALMIP
                self.props_.store.QRS_YALMIP = [];
            end%if useYALMIP

            self.props_.current.QRS_YALMIP = true();

            return;
        end%if strcmp(X, 'QRS_YALMIP')

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%% QRS_problem %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        if strcmp(X, 'QRS_problem')
            % Extract names of variables.
            if self.spec_.estimator
                Xname = 'XbbQR';
                bhatname = 'bbbhat';
            else % ~self.spec_.estimator
                Xname = 'XQR';
                bhatname = 'bhat';
            end%if

            if self.Szero
                decvars = '{Q, R}';
                decvarvec = '[Qss; Rss]';
            else % ~self.Szero
                decvars = '{Q, R, S}';
                decvarvec = '[Qss; Rss; Ss]';
                Xname = [Xname, 'S']; % 'XQRS' or 'XbbQRS'
            end%if

            Wname = Xname;
            Wname(1) = 'W'; % 'WQR', 'WbbQR', 'WQRS' or 'WbbQRS'

            % Least squares part of objective function.
            self.props_.store.QRS_problem = ...
                ['min_', decvars, ' 0.5 * || ', Xname, ' * ', decvarvec, ...
                 ' - ', bhatname, ' ||_{', Wname, '}^2'];

            % Regularization parts of objective function.
            for X = self.spec_.reg
                switch X{:}
                    case 'l1Q'
                        add = ' + l1Q * || Qss ||_1';

                    case 'l1R'
                        add = ' + l1R * || Rss ||_1';

                    case 'l1S'
                        add = ' + l1S * || Ss ||_1';

                    case 'l2Q'
                        add = ' + l2Q * || Qss ||_2^2';

                    case 'l2R'
                        add = ' + l2R * || Rss ||_2^2';

                    case 'l2S'
                        add = ' + l2S * || Ss ||_2^2';

                    case 'trQ'
                        if isscalar(self.trQ)
                            add = ' + trQ * trace(Q)';
                        else % self.trQ is a vector
                            add = ' + trQ(:)'' * diag(Q)';
                        end%if

                    case 'trR'
                        if isscalar(self.trR)
                            add = ' + trR * trace(R)';
                        else % self.trR is a vector
                            add = ' + trR(:)'' * diag(R)';
                        end%if

                    case 'nucQ'
                        add = ' + nucQ * || Q ||_*';

                    case 'nucR'
                        add = ' + nucR * || R ||_*';

                    case 'nucS'
                        add = ' + nucS * || S ||_*';

                    case 'nucQRS'
                        add = ' + nucQRS * || [Q, S; S'', R] ||_*';
                end%switch

                self.props_.store.QRS_problem = ...
                                           [self.props_.store.QRS_problem, add];
            end%for

            % Constraints.
            cnstr = {};
            if self.QRS_constrain
                if self.Szero
                    cnstr = [cnstr, 'Q >= 0', 'R >= 0'];
                else % ~self.Szero
                    cnstr = [cnstr, '[Q, S; S'', R] >= 0'];
                end%if
            end%if

            if self.Qdiag
                cnstr = [cnstr, 'Q diagonal']
            end%if

            if self.Rdiag
                cnstr = [cnstr, 'R diagonal']
            end%if

            if numel(cnstr) > 0
                self.props_.store.QRS_problem = ...
                     [self.props_.store.QRS_problem, ...
                      [char(10), 's.t. ', strjoin(cnstr, [char(10), '     '])]];
            end%if

            % Notes.
            notes = {};

            % Unbounded objective function.
            if ~self.QRS_constrain && (ismember('trQ', self.spec_.reg) ...
                                       || ismember('trR', self.spec_.reg))
                notes = [notes, ...
     ['Note: the objective function to the specificed QRS problem is ', ...
      'unbounded because semidefinite constraints are off, but trQ and/or ', ...
      'trR regularization is enabled; you will get an error if you try to ', ...
      'solve this problem---consider turning on semidefinite constraints ', ...
      'and/or using nucQ, nucR, nucS, or nucQRS regularization']];
            end%if

            if numel(notes) > 0
                self.props_.store.QRS_problem = ...
                     [self.props_.store.QRS_problem, ...
                      char(10), strjoin(notes, char(10))];
            end%if

            self.props_.current.QRS_problem = true();
            return;
        end%if strcmp(X, 'QRS_problem')

        %%%% Qhat, Rhat, Shat, QRS_objective, QRS_diagnostic, QRS_solvetime %%%%

        if strcmp(X, 'Qhat') || (strcmp(X, 'Rhat') || (strcmp(X, 'Shat') ...
           || (strcmp(X, 'QRS_objective') || (strcmp(X, 'QRS_diagnostic') ...
           || strcmp(X, 'QRS_solvetime')))))

            % Extract model.
            if self.spec_.model
                m = self.model;
            else
                m = self.estimator.model;
            end%if

            % Extract value of g.
            if ismember('G', m.spec.GH)
                g = m.g;
            else
                g = m.n;
            end%if

            % Extract value of h.
            if ismember('H', m.spec.GH)
                h = m.h;
            else
                h = m.p;
            end%if

            % Extract Xname, bhatname, and Wname.
            if self.spec_.estimator
                Xname = 'XbbQR';
                bhatname = 'bbbhat';
            else % ~self.spec_.estimator
                Xname = 'XQR';
                bhatname = 'bhat';
            end%if
            if ~self.Szero
                Xname = [Xname, 'S']; % 'XQRS' or 'XbbQRS'
            end%if
            Wname = Xname;
            Wname(1) = 'W'; % 'WQR', 'WbbQR', 'WQRS' or 'WbbQRS'
            Wnamepinv = [Wname, '_pinv'];
            Xnamerank = [Xname, '_rank'];

            % Check linear dependence of ALS matrix.
            if self.rank_warnings && (self.(Xnamerank) ~= size(self.(Xname), 2))
                warning([Xname, ' has linearly dependent columns']);
            end%if

            % Use YALMIP if solving constrained problem and/or if using
            % regularization.
            useYALMIP = self.QRS_constrain || ~isempty(self.spec_.reg);

            if useYALMIP
                if ~self.QRS_constrain ...
                    && (ismember('trQ', self.spec_.reg) ...
                        || ismember('trR', self.spec_.reg))
                    error( ...
    ['QRS problem: semidefinite constraints are off, but trQ and/or trR ', ...
     'regularization is enabled---the objective function is unbounded in ', ...
     'this case; consider turning on constraints and/or using nucQ, nucR, ', ...
     'nucS, or nucQRS regularization']);
                end%if

                % Get this property now so it doesn't influence solve time.
                self.QRS_YALMIP;
                timer = tic();
                % Solve problem using YALMIP optimize function.
                if isempty(self.YALMIPsettings)
                    self.props_.store.QRS_diagnostic = ...
                                      optimize(self.QRS_YALMIP.constraints, ...
                                               self.QRS_YALMIP.objective.total);
                else % if ~isempty(self.YALMIPsettings)
                    self.props_.store.QRS_diagnostic = ...
                                   optimize(self.QRS_YALMIP.constraints, ...
                                            self.QRS_YALMIP.objective.total, ...
                                            self.YALMIPsettings);
                end%if
                self.props_.store.QRS_solvetime = toc(timer);

                % Extract optimal values of variables---Qhat, Rhat, and Shat.
                self.props_.store.Qhat = value(self.QRS_YALMIP.Q);
                self.props_.store.Rhat = value(self.QRS_YALMIP.R);
                if self.Szero
                    self.props_.store.Shat = zeros(g, h);
                else
                    self.props_.store.Shat = value(self.QRS_YALMIP.S);
                end%if

                % Extract optimal value of objective function.
                self.props_.store.QRS_objective = struct();
                for Z = fieldnames(self.QRS_YALMIP.objective)'
                    Z = Z{:};
                    self.props_.store.QRS_objective.(Z) = ...
                                           value(self.QRS_YALMIP.objective.(Z));
                end%for
            else% if ~useYALMIP (unconstrained, unregularized problem)
                % Get these properties now so it doesn't influence solve time.
                self.(Xname);
                self.(Wnamepinv);
                self.(bhatname);
                timer = tic();
                % Solve unconstrained, unregularized ALS problem.
                if strcmp(self.weight, 'I')
                    XTW = self.(Xname)';
                else
                    XTW = ApinvB(self.(Xname)', self.(Wnamepinv));
                end%if
                Ystar = pinvAB(forceherm(XTW * self.(Xname)), ...
                               (XTW * self.(bhatname)));
                self.props_.store.QRS_solvetime = toc(timer);

                % Extract Qhat.
                if self.Qdiag
                    Qnumel = g;
                    self.props_.store.Qhat = diag(Ystar(1:Qnumel));
                else % ~self.Qdiag
                    Qnumel = g * (g + 1) / 2;
                    self.props_.store.Qhat = ...
                                     reshape(dupmat(g) * Ystar(1:Qnumel), g, g);
                end%if self.Qdiag

                % Extract Rhat.
                if self.Rdiag
                    Rnumel = h;
                    self.props_.store.Rhat = ...
                                    diag(Ystar((Qnumel + 1):(Qnumel + Rnumel)));
                else % ~self.Rdiag
                    Rnumel = h * (h + 1) / 2;
                    self.props_.store.Rhat = ...
               reshape(dupmat(h) * Ystar((Qnumel + 1):(Qnumel + Rnumel)), h, h);
                end%if self.Rdiag

                % Extract Shat, if necessary.
                if self.Szero
                    self.props_.store.Shat = zeros(g, h);
                else % if ~self.Szero
                    self.props_.store.Shat = ...
                                reshape(Ystar((Qnumel + Rnumel + 1):end), g, h);
                end%if

                % Extract optimal value of objective function.
                self.props_.store.QRS_objective = struct();
                dff = self.(Xname) * Ystar - self.(bhatname);
                if strcmp(self.weight, 'I')
                    self.props_.store.QRS_objective.lsq = 0.5 * dff' * dff;
                else
                    self.props_.store.QRS_objective.lsq = ...
                                     0.5 * dff' * pinvAB(self.(Wnamepinv), dff);
                end%if
                self.props_.store.QRS_objective.total = ...
                                            self.props_.store.QRS_objective.lsq;

                % Return empty for QRS_diagnostic.
                self.props_.store.QRS_diagnostic = [];
            end%if

            self.props_.current.Qhat = true();
            self.props_.current.Rhat = true();
            self.props_.current.Shat = true();
            self.props_.current.QRS_objective = true();
            self.props_.current.QRS_diagnostic = true();
            self.props_.current.QRS_solvetime = true();
            return;
        end%if strcmp(X, 'Qhat', 'Rhat', 'Shat', 'QRS_objective',
           %             'QRS_diagnostic', 'QRS_solvetime')

        %%%%%%%%%%%%%%%%%%%%%%%%%%% PhiTheta_YALMIP %%%%%%%%%%%%%%%%%%%%%%%%%%%%

        if strcmp(X, 'PhiTheta_YALMIP')
            use_semidef_constraint = strcmp(self.PhiTheta_constrain, ...
                                            'semidefinite');
            use_equality_constraints = use_semidef_constraint ...
                                        || strcmp(self.PhiTheta_constrain, ...
                                                  'equality');
            % Use YALMIP if using any kind of constraints.
            useYALMIP = use_semidef_constraint || use_equality_constraints;

            if useYALMIP
                % Make sure YALMIP is loaded.
                if exist('yalmip.m', 'file') ~= 2
                    error('cannot find YALMIP; are you sure it is ', ...
                          'installed and correctly loaded on the path?');
                end%if

                Ggiven = ismember('G', self.model.spec.GH);
                Hgiven = ismember('H', self.model.spec.GH);

                % Extract value of g.
                if Ggiven
                    g = self.model.g;
                else % ~Ggiven
                    g = self.model.n;
                end%if

                % Extract value of h.
                if Hgiven
                    h = self.model.h;
                else % ~Hgiven
                    h = self.model.p;
                end%if

                % Create YALMIP variables.
                self.props_.store.PhiTheta_YALMIP = struct();

                % Create Q and Qss variables.
                if self.Qdiag
                    % vector (only diagonal elements):
                    self.props_.store.PhiTheta_YALMIP.Qss = sdpvar(g, 1);
                    % matrix:
                    self.props_.store.PhiTheta_YALMIP.Q = ...
                                    diag(self.props_.store.PhiTheta_YALMIP.Qss);
                else % ~self.Qdiag
                    % matrix:
                    self.props_.store.PhiTheta_YALMIP.Q = sdpvar(g);
                    % vector (elements on and below diagonal):
                    self.props_.store.PhiTheta_YALMIP.Qss = ...
                                    myvech(self.props_.store.PhiTheta_YALMIP.Q);
                end%if

                % Create R and Rss variables.
                if self.Rdiag
                    % vector (only diagonal elements):
                    self.props_.store.PhiTheta_YALMIP.Rss = sdpvar(h, 1);
                    % matrix:
                    self.props_.store.PhiTheta_YALMIP.R = ...
                                    diag(self.props_.store.PhiTheta_YALMIP.Rss);
                else % ~self.Rdiag
                    % matrix:
                    self.props_.store.PhiTheta_YALMIP.R = sdpvar(h);
                    % vector (elements on and below diagonal):
                    self.props_.store.PhiTheta_YALMIP.Rss = ...
                                    myvech(self.props_.store.PhiTheta_YALMIP.R);
                end%if

                % Create S and Ss variables.
                if self.Szero
                    if isfield(self.props_.store.PhiTheta_YALMIP, 'S')
                        self.props_.store.PhiTheta_YALMIP = ...
                        rmfield(self.props_.store.PhiTheta_YALMIP, {'S', 'Ss'});
                    end%if
                else % ~self.Szero
                    % matrix:
                    self.props_.store.PhiTheta_YALMIP.S = sdpvar(g, h, 'full');
                    % vector:
                    self.props_.store.PhiTheta_YALMIP.Ss = ...
                                         self.props_.store.PhiTheta_YALMIP.S(:);
                end%if

                % Create P variable. We create the Lyapunov constraint later.
                self.props_.store.PhiTheta_YALMIP.P = sdpvar(self.model.n);

                % Create Phi and Phiss variables.
                if Hgiven
                    self.props_.store.PhiTheta_YALMIP.Phi = forceherm( ...
          self.model.C * self.props_.store.PhiTheta_YALMIP.P * self.model.C' ...
          + self.model.H * self.props_.store.PhiTheta_YALMIP.R * self.model.H');
                else % if ~Hgiven
                    self.props_.store.PhiTheta_YALMIP.Phi = forceherm( ...
          self.model.C * self.props_.store.PhiTheta_YALMIP.P * self.model.C' ...
                                         + self.props_.store.PhiTheta_YALMIP.R);
                end%if
                self.props_.store.PhiTheta_YALMIP.Phiss = ...
                                  myvech(self.props_.store.PhiTheta_YALMIP.Phi);

                % Create Theta and Thetas variables.
                self.props_.store.PhiTheta_YALMIP.Theta = ...
             self.model.A * self.props_.store.PhiTheta_YALMIP.P * self.model.C';
                if ~self.Szero
                    GSH = self.props_.store.PhiTheta_YALMIP.S;
                    if Ggiven
                        GSH = self.model.G * GSH;
                    end%if Ggiven
                    if Hgiven
                        GSH = GSH * self.model.H';
                    end%if Hgiven
                    self.props_.store.PhiTheta_YALMIP.Theta = ...
                                  self.props_.store.PhiTheta_YALMIP.Theta + GSH;
                end%if ~self.Szero
                self.props_.store.PhiTheta_YALMIP.Thetas = ...
                                     self.props_.store.PhiTheta_YALMIP.Theta(:);

                % Create YALMIP constraints.

                % We know that we at least need equality constraints. We first
                % create the P constraint.
                GQG = self.props_.store.PhiTheta_YALMIP.Q;
                if Ggiven
                    GQG = self.model.G * GQG * self.model.G';
                end%if Ggiven
                self.props_.store.PhiTheta_YALMIP.constraints = ...
  [forceherm(self.props_.store.PhiTheta_YALMIP.P - self.model.A ...
             * self.props_.store.PhiTheta_YALMIP.P * self.model.A' - GQG) == 0];

                % Don't need to create the Phi or Theta constraints---they are
                % enforced by the way that the YALMIP variables are defined.

                % Semidefinite constraint.
                if use_semidef_constraint
                    if self.Szero
                        if self.Qdiag
                            self.props_.store.PhiTheta_YALMIP.constraints ...
                           = [self.props_.store.PhiTheta_YALMIP.constraints; ...
                              self.props_.store.PhiTheta_YALMIP.Qss >= 0];
                        else % if ~self.Qdiag
                            self.props_.store.PhiTheta_YALMIP.constraints ...
                           = [self.props_.store.PhiTheta_YALMIP.constraints; ...
                              self.props_.store.PhiTheta_YALMIP.Q >= 0];
                        end%if
                        if self.Rdiag
                            self.props_.store.PhiTheta_YALMIP.constraints ...
                           = [self.props_.store.PhiTheta_YALMIP.constraints; ...
                              self.props_.store.PhiTheta_YALMIP.Rss >= 0];
                        else % if ~self.Rdiag
                            self.props_.store.PhiTheta_YALMIP.constraints ...
                           = [self.props_.store.PhiTheta_YALMIP.constraints; ...
                              self.props_.store.PhiTheta_YALMIP.R >= 0];
                        end%if
                    else % ~self.Szero
                        self.props_.store.PhiTheta_YALMIP.constraints ...
                           = [self.props_.store.PhiTheta_YALMIP.constraints; ...
                              [self.props_.store.PhiTheta_YALMIP.Q, ...
                               self.props_.store.PhiTheta_YALMIP.S; ...
                               self.props_.store.PhiTheta_YALMIP.S', ...
                               self.props_.store.PhiTheta_YALMIP.R] >= 0];
                    end%if
                end%if use_semidef_constraint

                % Struct to hold parts of YALMIP objective function.
                self.props_.store.PhiTheta_YALMIP.objective = struct();

                % Extract Xname, bhatname, and Wname, and create decision
                % variable vector.
                Xname = 'XPhiTheta';
                bhatname = 'bhat';
                Wname = 'WPhiTheta';
                Wnamepinv = 'WPhiTheta_pinv';
                Y = [self.props_.store.PhiTheta_YALMIP.Phiss; ...
                     self.props_.store.PhiTheta_YALMIP.Thetas];

                % Create least squares term of objective function.
                dff = self.(Xname) * Y - self.(bhatname);
                if strcmp(self.weight, 'I')
                    self.props_.store.PhiTheta_YALMIP.objective.lsq = ...
                                                               0.5 * dff' * dff;
                else
                    % Here we need to use slashes instead of pinvAB and
                    % ApinvB---the latter will not work with YALMIP sdpvars!
                    % TODO try all three of these options
%                    self.props_.store.PhiTheta_YALMIP.objective.lsq = ...
%                                                0.5 * dff' * self.(Wname) * dff;
%                    self.props_.store.PhiTheta_YALMIP.objective.lsq = ...
%                                          0.5 * (dff' / self.(Wnamepinv)) * dff;
                    self.props_.store.PhiTheta_YALMIP.objective.lsq = ...
                                          0.5 * dff' * (self.(Wnamepinv) \ dff);
                end%if

                % Create regularization terms of objective function.
                self.props_.store.PhiTheta_YALMIP.objective.total = ...
                                self.props_.store.PhiTheta_YALMIP.objective.lsq;
                for Z = self.spec_.reg
                    Z = Z{:};
                    switch Z
                        case 'l1Q'
                           self.props_.store.PhiTheta_YALMIP.objective.l1Q = ...
                      self.l1Q * norm(self.props_.store.PhiTheta_YALMIP.Qss, 1);

                        case 'l1R'
                           self.props_.store.PhiTheta_YALMIP.objective.l1R = ...
                      self.l1R * norm(self.props_.store.PhiTheta_YALMIP.Rss, 1);

                        case 'l1S'
                           self.props_.store.PhiTheta_YALMIP.objective.l1S = ...
                       self.l1S * norm(self.props_.store.PhiTheta_YALMIP.Ss, 1);

                        case 'l2Q'
                           self.props_.store.PhiTheta_YALMIP.objective.l2Q = ...
                          self.l2Q * (self.props_.store.PhiTheta_YALMIP.Qss' ...
                                      * self.props_.store.PhiTheta_YALMIP.Qss);

                        case 'l2R'
                           self.props_.store.PhiTheta_YALMIP.objective.l2R = ...
                          self.l2R * (self.props_.store.PhiTheta_YALMIP.Rss' ...
                                      * self.props_.store.PhiTheta_YALMIP.Rss);

                        case 'l2S'
                           self.props_.store.PhiTheta_YALMIP.objective.l2S = ...
                           self.l2S * (self.props_.store.PhiTheta_YALMIP.Ss' ...
                                       * self.props_.store.PhiTheta_YALMIP.Ss);

                        case 'trQ'
                            if isscalar(self.trQ)
                           self.props_.store.PhiTheta_YALMIP.objective.trQ = ...
                          self.trQ * trace(self.props_.store.PhiTheta_YALMIP.Q);
                            else % self.trQ is a vector
                                if numel(self.trQ) ~= g
                                    error(['when it is a vector, trQ must ', ...
                                           'have the same number of ', ...
                                           'elements as the dimesion of Q']);
                                end%if
                           self.props_.store.PhiTheta_YALMIP.objective.trQ = ...
                       self.trQ(:)' * diag(self.props_.store.PhiTheta_YALMIP.Q);
                            end%if

                        case 'trR'
                            if isscalar(self.trR)
                           self.props_.store.PhiTheta_YALMIP.objective.trR = ...
                          self.trR * trace(self.props_.store.PhiTheta_YALMIP.R);
                            else % self.trR is a vector
                                if numel(self.trR) ~= h
                                    error(['when it is a vector, trR must ', ...
                                           'have the same number of ', ...
                                           'elements as the dimesion of R']);
                                end%if
                           self.props_.store.PhiTheta_YALMIP.objective.trR = ...
                       self.trR(:)' * diag(self.props_.store.PhiTheta_YALMIP.R);
                            end%if

                        case 'nucQ'
                            if use_semidef_constraint
                                % nuclear norm is equal to trace
                          self.props_.store.PhiTheta_YALMIP.objective.nucQ = ...
                         self.nucQ * trace(self.props_.store.PhiTheta_YALMIP.Q);
                            else%if ~use_semidef_constraint
                                if self.Qdiag
                          self.props_.store.PhiTheta_YALMIP.objective.nucQ = ...
                     self.nucQ * norm(self.props_.store.PhiTheta_YALMIP.Qss, 1);
                                else%if ~self.Qdiag
                          self.props_.store.PhiTheta_YALMIP.objective.nucQ = ...
               self.nucQ * norm(self.props_.store.PhiTheta_YALMIP.Q, 'nuclear');
                                end%if
                            end%if

                        case 'nucR'
                            if use_semidef_constraint
                                % nuclear norm is equal to trace
                          self.props_.store.PhiTheta_YALMIP.objective.nucR = ...
                         self.nucR * trace(self.props_.store.PhiTheta_YALMIP.R);
                            else%if ~use_semidef_constraint
                                if self.Rdiag
                          self.props_.store.PhiTheta_YALMIP.objective.nucR = ...
                     self.nucR * norm(self.props_.store.PhiTheta_YALMIP.Rss, 1);
                                else%if ~self.Rdiag
                          self.props_.store.PhiTheta_YALMIP.objective.nucR = ...
               self.nucR * norm(self.props_.store.PhiTheta_YALMIP.R, 'nuclear');
                                end%if
                            end%if

                        case 'nucS'
                          self.props_.store.PhiTheta_YALMIP.objective.nucS = ...
               self.nucS * norm(self.props_.store.PhiTheta_YALMIP.S, 'nuclear');

                        case 'nucQRS'
                            if use_semidef_constraint
                                % nuclear norm is equal to trace
                        self.props_.store.PhiTheta_YALMIP.objective.nucQRS = ...
                   self.nucQRS * (trace(self.props_.store.PhiTheta_YALMIP.Q) ...
                                  + trace(self.props_.store.PhiTheta_YALMIP.R));
                            else%if ~use_semidef_constraint
                        self.props_.store.PhiTheta_YALMIP.objective.nucQRS = ...
                   self.nucQRS * norm([self.props_.store.PhiTheta_YALMIP.Q, ...
                                       self.props_.store.PhiTheta_YALMIP.S; ...
                                       self.props_.store.PhiTheta_YALMIP.S', ...
                                       self.props_.store.PhiTheta_YALMIP.R], ...
                                       'nuclear');
                            end%if
                    end%switch

                    self.props_.store.PhiTheta_YALMIP.objective.total = ...
                           self.props_.store.PhiTheta_YALMIP.objective.total ...
                              + self.props_.store.PhiTheta_YALMIP.objective.(Z);
                end%for Z = self.spec_.reg
            else % if ~useYALMIP
                self.props_.store.PhiTheta_YALMIP = [];
            end%if useYALMIP

            self.props_.current.PhiTheta_YALMIP = true();
            return;
        end%if strcmp(X, 'PhiTheta_YALMIP')

        %%%%%%%%%%%%%%%%%%%%%%%%%%% PhiTheta_problem %%%%%%%%%%%%%%%%%%%%%%%%%%%

        if strcmp(X, 'PhiTheta_problem')
            % Determine what constraints are used.
            use_semidef_constraint = strcmp(self.PhiTheta_constrain, ...
                                            'semidefinite');
            use_equality_constraints = use_semidef_constraint ...
                                        || strcmp(self.PhiTheta_constrain, ...
                                                  'equality');

            % Check if G and H are given.
            Ggiven = ismember('G', self.model.spec.GH);
            Hgiven = ismember('H', self.model.spec.GH);

            % Names of variables.
            Xname = 'XPhiTheta';
            bhatname = 'bhat';
            Wname = 'WPhiTheta';

            % Constraints of any kind introduce Q, R, S, and P as variables.
            if use_semidef_constraint || use_equality_constraints
                if ~self.Szero
                    decvars = '{Phi, Theta, Q, R, S, P}';
                else
                    decvars = '{Phi, Theta, Q, R, P}';
                end%if
            else % no constraints
                decvars = '{Phi, Theta}';
            end%if
            decvarvec = ['{Phiss; Thetas}'];

            % Least squares part of objective function.
            self.props_.store.PhiTheta_problem = ...
                ['min_', decvars, ' 0.5 * || ', Xname, ' * ', decvarvec, ...
                 ' - ', bhatname, ' ||_{', Wname, '}^2'];

            % Add regularization terms only if constraints are used. If there
            % are no constraints, then any regularization terms are ignored.
            if use_semidef_constraint || use_equality_constraints
                for X = self.spec_.reg
                    switch X{:}
                        case 'l1Q'
                            add = ' + l1Q * || Qss ||_1';

                        case 'l1R'
                            add = ' + l1R * || Rss ||_1';

                        case 'l1S'
                            add = ' + l1S * || Ss ||_1';

                        case 'l2Q'
                            add = ' + l2Q * || Qss ||_2^2';

                        case 'l2R'
                            add = ' + l2R * || Rss ||_2^2';

                        case 'l2S'
                            add = ' + l2S * || Ss ||_2^2';

                        case 'trQ'
                            if isscalar(self.trQ)
                                add = ' + trQ * trace(Q)';
                            else % self.trQ is a vector
                                add = ' + trQ(:)'' * diag(Q)';
                            end%if

                        case 'trR'
                            if isscalar(self.trR)
                                add = ' + trR * trace(R)';
                            else % self.trR is a vector
                                add = ' + trR(:)'' * diag(R)';
                            end%if

                        case 'nucQ'
                            add = ' + nucQ * || Q ||_*';

                        case 'nucR'
                            add = ' + nucR * || R ||_*';

                        case 'nucS'
                            add = ' + nucS * || S ||_*';

                        case 'nucQRS'
                            add = ' + nucQRS * || [Q, S; S'', R] ||_*';
                    end%switch X{:}

                    self.props_.store.PhiTheta_problem = ...
                                      [self.props_.store.PhiTheta_problem, add];
                end%for X = self.spec_.reg
            end%if use_semidef_constraint || use_equality_constraints

            % Constraints.
            cnstr = {};

            % Equality constraints.
            if use_semidef_constraint || use_equality_constraints
                % Phi constraint.
                if Hgiven
                    cnstr = [cnstr, 'Phi = C * P * C'' + H * R * H'''];
                else % ~Hgiven
                    cnstr = [cnstr, 'Phi = C * P * C'' + R'];
                end%if

                % Theta constraint.
                theta_cnstr = 'Theta = A * P * C''';
                if ~self.Szero
                    theta_cnstr = [theta_cnstr, ' + '];
                    if Ggiven
                        theta_cnstr = [theta_cnstr, 'G * '];
                    end%if Ggiven
                    theta_cnstr = [theta_cnstr, 'S'];
                    if Hgiven
                        theta_cnstr = [theta_cnstr, ' * H'''];
                    end%if Hgiven
                end%if ~self.Szero
                cnstr = [cnstr, theta_cnstr];

                % P constraint.
                if Ggiven
                    cnstr = [cnstr, 'P = A * P * A'' + G * Q * G'''];
                else % ~Ggiven
                    cnstr = [cnstr, 'P = A * P * A'' + Q'];
                end%if
            end%if use_semidef_constraint || use_equality_constraints

            % Semidefinite constraint.
            if use_semidef_constraint
                if self.Szero
                    cnstr = [cnstr, 'Q >= 0', 'R >= 0'];
                else % ~self.Szero
                    cnstr = [cnstr, '[Q, S; S'', R] >= 0'];
                end%if
            end%if use_semidef_constraint

            if self.Qdiag
                cnstr = [cnstr, 'Q diagonal']
            end%if

            if self.Rdiag
                cnstr = [cnstr, 'R diagonal']
            end%if

            if numel(cnstr) > 0
                self.props_.store.PhiTheta_problem = ...
                     [self.props_.store.PhiTheta_problem, ...
                      [char(10), 's.t. ', strjoin(cnstr, [char(10), '     '])]];
            end%if

            % Notes.
            notes = {};

            % Ignoring regularization parameters.
            if ~(use_semidef_constraint || use_equality_constraints) ...
                && ~isempty(self.spec_.reg)
                if self.Szero
                    notes = [notes, ...
   ['Note: the given regularization parameters (', ...
    strjoin(self.spec_.reg, ', '), ') have been ignored, as the Q and R ', ...
    'variables do not exist for the unconstrained Phi/Theta problem']];
                else % ~self.Szero
                    notes = [notes, ...
   ['Note: the given regularization parameters (', ...
    strjoin(self.spec_.reg, ', '), ') have been ignored, as the Q, R, and ', ...
    'S variables do not exist for the unconstrained Phi/Theta problem']];
                end%if
            end%if

            % Ignoring G and/or H.
            if ~(use_semidef_constraint || use_equality_constraints) ...
                && (Ggiven || Hgiven)
                notes = [notes, ...
                   ['Note: the G and/or H matrices from the model are not ', ...
                    'accounted for in the unconstrained Phi/Theta problem']];
            end%if

            % Unbounded objective function.
            if ~use_semidef_constraint && (use_equality_constraints ...
                && (ismember('trQ', self.spec_.reg) ...
                    || ismember('trR', self.spec_.reg)))
                notes = [notes, ...
    ['Note: the objective function to the specificed Phi/Theta problem is ', ...
     'unbounded because semidefinite constraints are off, but trQ and/or ', ...
     'trR regularization is enabled; you will get an error if you try to ', ...
     'solve this problem---consider turning on semidefinite constraints ', ...
     'and/or using nucQ, nucR, nucS, or nucQRS regularization']];
            end%if

            if numel(notes) > 0
                self.props_.store.PhiTheta_problem = ...
                     [self.props_.store.PhiTheta_problem, ...
                      char(10), strjoin(notes, char(10))];
            end%if

            self.props_.current.PhiTheta_problem = true();
            return;
        end%if strcmp(X, 'PhiTheta_problem')

        %%%%%%%%%%%%%%%% Phihat, Thetahat, PhiTheta_objective, %%%%%%%%%%%%%%%%%
        %%%%%%%%%%%%%%% PhiTheta_diagnostic, PhiTheta_solvetime %%%%%%%%%%%%%%%%

        if strcmp(X, 'Phihat') || (strcmp(X, 'Thetahat') ...
            || (strcmp(X, 'PhiTheta_objective') ...
            || (strcmp(X, 'PhiTheta_diagnostic') ...
            || strcmp(X, 'PhiTheta_solvetime'))))

            Ggiven = ismember('G', self.model.spec.GH);
            Hgiven = ismember('H', self.model.spec.GH);

            % Extract Xname, bhatname, and Wname, and create decision
            % variable vector.
            Xname = 'XPhiTheta';
            Xnamerank = [Xname, '_rank'];
            bhatname = 'bhat';
            Wname = 'WPhiTheta';
            Wnamepinv = 'WPhiTheta_pinv';

            % Check linear dependence of ALS matrix.
            if self.rank_warnings && (self.(Xnamerank) ~= size(self.(Xname), 2))
                warning([Xname, ' has linearly dependent columns']);
            end%if

            use_semidef_constraint = strcmp(self.PhiTheta_constrain, ...
                                            'semidefinite');
            use_equality_constraints = use_semidef_constraint ...
                                        || strcmp(self.PhiTheta_constrain, ...
                                                  'equality');
            % Use YALMIP if using any kind of constraints.
            useYALMIP = use_semidef_constraint || use_equality_constraints;

            if useYALMIP
                if ~use_semidef_constraint && (use_equality_constraints ...
                    && (ismember('trQ', self.spec_.reg) ...
                        || ismember('trR', self.spec_.reg)))
                    % Remember, if equality constraints are also off, all
                    % regularization gets ignored.
                    error( ...
    ['Phi/Theta problem: semidefinite constraints are off, but trQ and/or ', ...
     'trR regularization is enabled---the objective function is unbounded ', ...
     'in this case; consider turning on semidefinite constraints and/or ', ...
     'using nucQ, nucR, nucS, or nucQRS regularization']);
                end%if

                % Get this property now so it doesn't influence solve time.
                self.PhiTheta_YALMIP;
                timer = tic();
                % Solve problem using YALMIP optimize function.
                if isempty(self.YALMIPsettings)
                    self.props_.store.PhiTheta_diagnostic = ...
                                 optimize(self.PhiTheta_YALMIP.constraints, ...
                                          self.PhiTheta_YALMIP.objective.total);
                else % if ~isempty(self.YALMIPsettings)
                    self.props_.store.PhiTheta_diagnostic = ...
                              optimize(self.PhiTheta_YALMIP.constraints, ...
                                       self.PhiTheta_YALMIP.objective.total, ...
                                       self.YALMIPsettings);
                end%if
                self.props_.store.PhiTheta_solvetime = toc(timer);

                % Extract optimal values of variables---Phihat and Thetahat.
                % Right now there is not a direct mechanism for the user to get
                % the optimal values of the other variables Q, R, S, and P. But
                % this could easily be accomplished by calling
                % value(self.PhiTheta_YALMIP.Q), for example.
                self.props_.store.Phihat = value(self.PhiTheta_YALMIP.Phi);
                self.props_.store.Thetahat = value(self.PhiTheta_YALMIP.Theta);

                % Extract optimal value of objective function.
                self.props_.store.PhiTheta_objective = struct();
                for Z = fieldnames(self.PhiTheta_YALMIP.objective)'
                    Z = Z{:};
                    self.props_.store.PhiTheta_objective.(Z) = ...
                                      value(self.PhiTheta_YALMIP.objective.(Z));
                end%for
            else% if ~useYALMIP (unconstrained, unregularized problem)
                if ~isempty(self.spec_.reg)
                    warning( ...
    ['Phi/Theta problem: when no constraints are used, you cannot use any ', ...
     'type of regularization on Q, R, and S, since these variables do not ', ...
     'exist in this form of the problem; I am ignoring the given ', ...
     'regularization parameters (', strjoin(self.spec_.reg, ', '),')']);
                end%if

                if Ggiven || Hgiven
                    warning( ...
['Phi/Theta problem: when solving the unconstrained problem, given G ', ...
 'and/or H are ignored (this is equivalent to assuming that G and H are ', ...
 'identity matrices); only the constrained form of the problem accounts ', ...
 'for non-identity G and H']);
                end%if

                % Get these properties now so it doesn't influence solve time.
                self.(Xname);
                self.(Wnamepinv);
                self.(bhatname);
                timer = tic();
                % Solve unconstrained, unregularized ALS problem.
                if strcmp(self.weight, 'I')
                    XTW = self.(Xname)';
                else
                    XTW = ApinvB(self.(Xname)', self.(Wnamepinv));
                end%if
                Ystar = pinvAB(forceherm(XTW * self.(Xname)), ...
                               (XTW * self.(bhatname)));
                self.props_.store.PhiTheta_solvetime = toc(timer);

                % Extract Phihat.
                Phinumel = self.model.p * (self.model.p + 1) / 2;
                self.props_.store.Phihat = ...
                           reshape(dupmat(self.model.p) * Ystar(1:Phinumel), ...
                                   self.model.p, self.model.p);

                % Extract Thetahat.
                self.props_.store.Thetahat = ...
                 reshape(Ystar((Phinumel + 1):end), self.model.n, self.model.p);

                % Extract optimal value of objective function.
                self.props_.store.PhiTheta_objective = struct();
                dff = self.(Xname) * Ystar - self.(bhatname);
                if strcmp(self.weight, 'I')
                    self.props_.store.PhiTheta_objective.lsq = 0.5 * dff' * dff;
                else
                    self.props_.store.PhiTheta_objective.lsq = ...
                                     0.5 * dff' * pinvAB(self.(Wnamepinv), dff);
                end%if
                self.props_.store.PhiTheta_objective.total = ...
                                       self.props_.store.PhiTheta_objective.lsq;

                % Return empty for PhiTheta_diagnostic.
                self.props_.store.PhiTheta_diagnostic = [];
            end%if

            self.props_.current.Phihat = true();
            self.props_.current.Thetahat = true();
            self.props_.current.PhiTheta_objective = true();
            self.props_.current.PhiTheta_diagnostic = true();
            return;
        end%if strcmp(X, 'Phihat', 'Thetahat', 'PhiTheta_objective', ...
           %             'PhiTheta_diagnostic', 'PhiTheta_solvetime')

        %%%%%%%%%%%%%%%%%%%%%%%%% PhibbThetabb_YALMIP %%%%%%%%%%%%%%%%%%%%%%%%%%

        if strcmp(X, 'PhibbThetabb_YALMIP')
            % TODO
            self.props_.store.PhibbThetabb_YALMIP = 1; % TODO

            self.props_.current.PhibbThetabb_YALMIP = true();
            return;
        end%if strcmp(X, 'PhibbThetabb_YALMIP')

        %%%%%%%%%%%%%%%%%%%%%%%%% PhibbThetabb_problem %%%%%%%%%%%%%%%%%%%%%%%%%

        if strcmp(X, 'PhibbThetabb_problem')
            % TODO

            self.props_.store.PhibbThetabb_problem = 1; % TODO

            self.props_.current.PhibbThetabb_problem = true();
            return;
        end%if strcmp(X, 'PhiTheta_problem')

        %%%%%%%%%%%%%%%%%%%%%%%%% Phibbhat, Thetabbhat %%%%%%%%%%%%%%%%%%%%%%%%%

        % TODO get all of the right variables in the top line here
        if strcmp(X, 'Phibbhat') || (strcmp(X, 'Thetabbhat') ...
            || (strcmp(X, 'PhibbThetabb_objective') ...
            || (strcmp(X, 'PhibbThetabb_diagnostic') ...
            || strcmp(X, 'PhibbThetabb_solvetime'))))

            % TODO what to do if G and/or H are provided in model?

            % TODO remember, if constraints are on, we can use regularization on Q, R, and S.
            % TODO with constraints, respect Qdiag and Rdiag
            % TODO if constraints are off, then we can't use regularization on Q, R, S. Warning if the user tries to do this?

            self.props_.store.Phibbhat = 1; % TODO
            self.props_.current.Phibbhat = true();
            return;
        end%if strcmp(X, 'Phibbhat', 'Thetabbhat', 'PhibbThetabb_objective', ...
           %             'PhibbThetabb_diagnostic', 'PhibbThetabb_solvetime')
    end%function calculate

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function Pbhat = do_Pbhat_calc(self, Y)
        % Y is 'Lambda_nonzero', 'Lambdahat_nonzero', 'Lambdabb_nonzero', or
        % 'Lambdabbhat_nonzero'.

        % Constants.
        if self.spec_.estimator
            p = self.estimator.model.p;
        else % data and model given
            p = self.model.p;
        end%if
        ptp = p * self.pt;
        Fcal = kron(spdiags((1 ./ ((self.Tf + 1):-1:(self.nt + 1)))', 0, ...
                            self.N, self.N), speye(p));
        Vcal = dmatpinv(self.pt, p) * kron(speye(p), Fcal);

        % Initialize.
        if (self.N - 1) <= self.M
            C = self.(Y)(1:self.N);
        else
            C = [self.(Y); ...
                 mat2cell(zeros(p * (self.N - self.M - 1), p), ...
                          repmat(p, self.N - self.M - 1, 1), p)];
        end%if
        R = cellctrans(C);
        Psi_kron_sum = zeros(ptp);

        M_Tf = min(self.M, self.Tf);
        for i = 0:M_Tf
            if i >= 1
                if (self.N + i - 1) <= M_Tf
                    C = [C(2:end); self.(Y){self.N + i}];
                else
                    C = [C(2:end); zeros(p)];
                end%if
                R = [C{1}; R(1:(end - 1))];
            end%if

            Pc = blktoep(C, R);
            if i > self.nt
                Pc((end - (i - self.nt) * p + 1):end , :) = 0;
            end%if
            Lam_kron_Pc = kron(Pc(1:p, 1:p), Pc);
            Psi_kron = kron(Pc(:, 1:p), Pc(1:p, :));

            num = max(self.nt + 1 - i, 1);
            Lam_kron_Pc_sum = num * Lam_kron_Pc;
            Psi_kron_sum = Psi_kron_sum + num * Psi_kron;

            for k = (max(1, i - self.nt + 1)):(self.N - 1)
                zerorows_LP = arrayfun( ...
         @(l) (((l * self.N - k) * p + 1):((l * self.N - k + 1) * p))', 1:p, ...
         'UniformOutput', false());
                zerorows_LP = horzcat(zerorows_LP{:});
                zerorows_Psi = ...
                           (((self.N - k) * p^2 + 1):((self.N - k + 1) * p^2))';
                Lam_kron_Pc(zerorows_LP, :) = 0;
                Psi_kron(zerorows_Psi, :) = 0;

                if k > i
                    zerocols = zerorows_LP + (i * p);
                    Lam_kron_Pc(:, zerocols) = 0;
                    Psi_kron(:, zerocols) = 0;
                end%if

                Lam_kron_Pc_sum = Lam_kron_Pc_sum + Lam_kron_Pc;
                Psi_kron_sum = Psi_kron_sum + Psi_kron;
            end%for

            if i == 0
                % Divide by 2 to avoid double counting from forceherm2.
                Pbhat = Lam_kron_Pc_sum / 2;
                Psi_kron_sum = Psi_kron_sum / 2;
            else
                Pbhat = Pbhat + Lam_kron_Pc_sum;
            end%if
        end%for

        Pbhat = forceherm2( ...
                    Vcal * (Pbhat + commat(p, self.pt) * Psi_kron_sum) * Vcal');
    end%function do_Pbhat_calc

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function Pbhathat = do_Z1_calc(self)
        if self.spec_.estimator
            p = self.estimator.model.p;
        else % data/model given
            p = self.model.p;
        end%if
        D = dmatpinv(self.pt, p);
        MM = min(self.N + self.M, self.nt + 1);

%        % For loop to get PhatcZ.
%        PhatcZ_2 = zeros(self.pt);
%        for i = 1:MM
%            Ns = floor((self.nt + 1 - i) / (self.N + self.M)) + 1;
%            end_idx = (Ns - 1) * (self.N + self.M) + i;
%            Yi = [];
%            for k = i:(self.N + self.M):end_idx
%                Yi = [Yi, vec(self.o(:, k:(k + self.N - 1)))];
%            end%for
%            Phatci = (Yi * Yi') / Ns;
%            PhatcZ_2 = PhatcZ_2 + Phatci;
%        end%for
%        PhatcZ_2 = PhatcZ_2 / MM;

        % One line vectorized version to get PhatcZ.
        horzcatcell = @(X) horzcat(X{:});
        outerprod = @(X) X * X';
        PhatcZ = addcell(arrayfun(@(i) outerprod(horzcatcell(arrayfun(@(k) vec(self.o(:, k:(k + self.N - 1))), i:(self.N + self.M):(floor((self.nt + 1 - i) / (self.N + self.M)) * (self.N + self.M) + i), 'UniformOutput', false()))) / (floor((self.nt + 1 - i) / (self.N + self.M)) + 1), 1:MM, 'UniformOutput', false())) / MM;
        Pbhathat = forceherm(D * ((kron(PhatcZ(1:p, 1:p), PhatcZ) ...
                    + commat(p, self.pt) * kron(PhatcZ(:, 1:p), ...
                      PhatcZ(1:p, :))) / (self.nt + 1)) * D');

%        % Test to make sure PhatcZ is calculated correctly.
%        norm(PhatcZ - PhatcZ_2, 'fro') % should be 0

%        % Test to make sure Pbhathat is calculated correctly.
%        Jcal = sparse(1:(p * self.pt), 1:(p * self.pt), 1, ...
%                      p * self.pt, self.pt^2);
%        DJcal = D * Jcal;
%        Pbhathat_2 = forceherm(DJcal ...
%                              * ((speye(self.pt^2) + commat(self.pt)) ...
%                              * kron(PhatcZ, PhatcZ) / (self.nt + 1)) * DJcal');
%        norm(Pbhathat - Pbhathat_2, 'fro') % should be 0
    end%function do_Z1_calc

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function Pbhathat = do_Z2_calc(self)
        if self.spec_.estimator
            p = self.estimator.model.p;
        else % data/model given
            p = self.model.p;
        end%if
        D = dmatpinv(self.pt, p);
        MM = min(self.N + self.M, self.nt + 1);

%        % For loop to get Pbhathat.
%        Jcal = sparse(1:(p * self.pt), 1:(p * self.pt), 1, ...
%                      p * self.pt, self.pt^2);
%        DJcal = D * Jcal;
%        PhatPhatcZ2 = zeros(self.pt^2);
%        for i = 1:MM
%            Ns = floor((self.nt + 1 - i) / (self.N + self.M)) + 1;
%            end_idx = (Ns - 1) * (self.N + self.M) + i;
%            Yi = [];
%            for k = i:(self.N + self.M):end_idx
%                Yi = [Yi, vec(self.o(:, k:(k + self.N - 1)))];
%            end%for
%            Phatci = (Yi * Yi') / Ns;
%            PhatPhatci = (speye(self.pt^2) + commat(self.pt)) ...
%                          * kron(Phatci, Phatci) / Ns;
%            PhatPhatcZ2 = PhatPhatcZ2 + PhatPhatci;
%        end%for
%        PhatPhatcZ2 = PhatPhatcZ2 / MM;
%        Pbhathat_2 = forceherm(DJcal * PhatPhatcZ2 * DJcal');

%        % Alternate for loop to get Pbhathat.
%        Kppt = commat(p, self.pt);
%        Jcal_PhatPhatcZ2_JcalT = zeros(p * self.pt);
%        for i = 1:MM
%            Ns = floor((self.nt + 1 - i) / (self.N + self.M)) + 1;
%            end_idx = (Ns - 1) * (self.N + self.M) + i;
%            Yi = [];
%            for k = i:(self.N + self.M):end_idx
%                Yi = [Yi, vec(self.o(:, k:(k + self.N - 1)))];
%            end%for
%            Phatci = (Yi * Yi') / Ns;
%            Jcal_PhatPhatci_JcalT = (kron(Phatci(1:p, 1:p), Phatci) ...
%                        + Kppt * kron(Phatci(:, 1:p), Phatci(1:p, :))) / Ns;
%            Jcal_PhatPhatcZ2_JcalT = Jcal_PhatPhatcZ2_JcalT ...
%                                      + Jcal_PhatPhatci_JcalT;
%        end%for
%        Jcal_PhatPhatcZ2_JcalT = Jcal_PhatPhatcZ2_JcalT / MM;
%        Pbhathat_3 = forceherm(D * Jcal_PhatPhatcZ2_JcalT * D');

        % One line vectorized version to get Pbhathat.
        horzcatcell = @(X) horzcat(X{:});
        outerprod = @(X) X * X';
        Kppt = commat(p, self.pt);
        kronfun = @(X) kron(X(1:p, 1:p), X) ...
                        + Kppt * kron(X(:, 1:p), X(1:p, :));
        Pbhathat = forceherm(D * (addcell(arrayfun(@(i) kronfun(outerprod(horzcatcell(arrayfun(@(k) vec(self.o(:, k:(k + self.N - 1))) , i:(self.N + self.M):(floor((self.nt + 1 - i) / (self.N + self.M)) * (self.N + self.M) + i),'UniformOutput', false()))) / (floor((self.nt + 1 - i) / (self.N + self.M)) + 1)) / (floor((self.nt + 1 - i) / (self.N + self.M)) + 1), 1:MM, 'UniformOutput', false())) / MM) * D');

%        % Test to maake sure all methods are consistent.
%        norm(Pbhathat - Pbhathat_2, 'fro') % should be 0
%        norm(Pbhathat - Pbhathat_3, 'fro') % should be 0
    end%function do_Z2_calc

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function L = calcLambdahat(self, y, ind)
        L = arrayfun(@(k) y(:, k:end) * y(:, 1:end - k + 1)' ...
                                          / (self.Nd - k + 1), ...
                     ind, 'UniformOutput', false())';
    end%function calcLambdahat

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function O1_N = calcO1_N(self)
        if self.spec_.estimator
            A = self.estimator.model.Abb;
            CAprod = self.estimator.model.C;
            p = self.estimator.model.p;
            n = self.estimator.model.n;
        else % data/model given
            A = self.model.A;
            CAprod = self.model.C;
            p = self.model.p;
            n = self.model.n;
        end%if

        O1_N = zeros(self.pt, n); % preallocate
        for i = 1:self.N
            rowidx = ((i - 1) * p + 1):(i * p);
            O1_N(rowidx, :) = CAprod;
            if i < self.N
                CAprod = CAprod * A;
            end%if
        end%for
    end%function calcO1_N

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function O1_nonzero = calcO1_nonzero(self, O1_N)
        if self.M + 1 <= self.N
            if self.spec_.estimator
                p = self.estimator.model.p;
            else % data/model given
                p = self.model.p;
            end%if
            O1_nonzero = O1_N(1:((self.M + 1) * p), :);
        else
            if self.spec_.estimator
                A = self.estimator.model.Abb;
                p = self.estimator.model.p;
                n = self.estimator.model.n;
            else % data/model given
                A = self.model.A;
                p = self.model.p;
                n = self.model.n;
            end%if

            lastidx = min(self.M + 1, self.Nd);
            % Preallocate.
            O1_nonzero = [O1_N; zeros(p * (lastidx - self.N), n)];
            % Now calculate.
            CAprod = O1_N((end - p + 1):end, :) * A;
            for i = (self.N + 1):lastidx
                rowidx = ((i - 1) * p + 1):(i * p);
                O1_nonzero(rowidx, :) = CAprod;
                if i < self.Nd
                    CAprod = CAprod * A;
                end%if
            end%for
        end%if
    end%function calcO1_nonzero

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function O2 = calcO2(self, O1)
        if self.spec_.estimator
            p = self.estimator.model.p;
            n = self.estimator.model.n;
        else % data/model given
            p = self.model.p;
            n = self.model.n;
        end%if

        O2 = [zeros(p, n); O1(1:(end - p), :)];
    end%function calcO2

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function Gam = calcGamma(self, N)
        if self.spec_.estimator
            p = self.estimator.model.p;
        else % data/model given
            p = self.model.p;
        end%if

%        Gam = [eye(p); zeros(p * (N - 1), p)]; % full
        Gam = sparse(1:p, 1:p, 1, p * N, p); % sparse
    end%function calcGamma

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function load_constants(self)
        filename = [als.installdir(), 'als_constants.mat'];
        if exist(filename, 'file') ~= 2
            try
                als.generate_constants();
            catch err
                error(['unable to generate file ', filename, ' from ', ...
                       'function als.generate_constants(); got the ', ...
                       'following error message: ', err.message]);
            end%trycatch
        end%if
        try
            if isoctave()
                S = load('-7', filename);
            else % Matlab
                S = load(filename);
            end%if
            str = self.spec_.string;
            self.props_ = S.props.(str);
            self.checks_ = S.checks.(str);
        catch err
            error(['unable to load props.', str, ' and checks.', str, ...
                   ' from file ', filename, '; got the following error ', ...
                   'message: ', err.message]);
        end%trycatch
    end%function load_constants

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function link(self, mdenew, y)
        % Link self to given cedata, cemodel, or ceestimator object mdenew. y is
        % 'data', 'model', or 'estimator'.

        % Create a separate handle first to avoid calling
        % set.model/data/estimator.
        mdeold = self.(y);

        oldempty = isequal(mdeold, []);
        newempty = isequal(mdenew, []);

        % If the same object is being relinked for some reason, return.
        if (~oldempty && (~newempty && mdeold == mdenew));
            return;
        end%if

        % Unlink in old cedata, cemodel, or ceestimator object if necessary.
        if ~oldempty
            mdeold.flags.als = rmfield(mdeold.flags.als, self.id);
        end%if

        % Link the new cedata, cemodel, or ceestimator object if necessary.
        if ~newempty
            mdenew.flags.als.(self.id) = true();
        end%if

        % Run updatespec so that self.spec is updated if necessary. If
        % updatespec does not reload self.props and self.checks, we still need
        % to set self.checks.current and self.props.current to false for
        % dependent checks and dependent properties.
        if strcmp(y, 'data')
            need2updatespec = (oldempty ~= newempty);
        else % y is 'model' or 'estimator'
            need2updatespec = ~(oldempty && newempty);
        end%if

        if need2updatespec
            need2reload = self.updatespec(y, mdenew);
            if ~need2reload
                self.updatecurrent(y);
            end%if
        end%if
    end%function link

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% get methods %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = getdependentprop(self, X)
        % Helper method used to get properties in self.props.dependent.

        % Make sure input argument is a recognized value.
        if ~ismember(X, self.props.dependent)
            error(['cannot access property ''', X, '''; props.dependent ', ...
                   'of als object is currently: ''', ...
                   strjoin(self.props_.dependent, ''', '''), '''']);
        end%if

        % If self.X isn't current, update self.props.store.X. Remember,
        % self.calculate will update self.props.current.
        if ~self.props_.current.(X)
            self.calculate(X);
        end%if

        % Return self.props.store.X.
        val = self.props_.store.(X);
    end%function getdependentprop
end%methods (Access = private)

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

methods
    function seeifmdechanged(self)
        % Check if model, data, or estimator have changed, and if so make the
        % necessary updates.

        for X = {'model', 'data', 'estimator'}
            if self.spec_.(X{:}) && self.(X{:}).flags.als.(self.id)
                % check if model/data/estimator is linked, and if so, if it has
                % changed

                % only need to call updatespec for model and estimator
                if ~strcmp(X{:}, 'data') % 'model' or 'estimator'
                    % Update self.spec and reload self.props and self.checks if
                    % necessary.
                    need2reload = self.updatespec(X{:});
                    if ~need2reload
                        % If updatespec did not reload self.props and
                        % self.checks, we still need to set self.checks.current
                        % and self.props.current to false for dependent checks
                        % and dependent properties.
                        self.updatecurrent(X{:});
                    end%if
                else % 'data'
                    self.updatecurrent(X{:});
                end%if

                % Lower the flag in linked cemodel/cedata/ceestimator object.
                % Create a separate handle first to avoid calling
                % set.model/set.data/set.estimator.
                mde = self.(X{:});
                mde.flags.als.(self.id) = false();
            end%if
        end%for
    end%function seeifmdechanged

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.Ndtotal(self)
        val = self.getdependentprop('Ndtotal');
    end%function get.Ndtotal

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.Nd(self)
        val = self.getdependentprop('Nd');
    end%function get.Nd

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.Tf(self)
        val = self.getdependentprop('Tf');
    end%function get.Tf

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.nt(self)
        val = self.getdependentprop('nt');
    end%function get.nt

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.pt(self)
        val = self.getdependentprop('pt');
    end%function get.pt

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.o(self)
        val = self.getdependentprop('o');
    end%function get.o

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.otype(self)
        val = self.getdependentprop('otype');
    end%function get.otype

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.b(self)
        val = self.getdependentprop('b');
    end%function get.b

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.bbb(self)
        val = self.getdependentprop('bbb');
    end%function get.bbb

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.bhat(self)
        val = self.getdependentprop('bhat');
    end%function get.bhat

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.bbbhat(self)
        val = self.getdependentprop('bbbhat');
    end%function get.bbbhat

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.Lambda(self)
        val = self.getdependentprop('Lambda');
    end%function get.Lambda

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.Lambdahat(self)
        val = self.getdependentprop('Lambdahat');
    end%function get.Lambdahat

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.Lambdabb(self)
        val = self.getdependentprop('Lambdabb');
    end%function get.Lambdabb

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.Lambdabbhat(self)
        val = self.getdependentprop('Lambdabbhat');
    end%function get.Lambdabbhat

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.Psi(self)
        val = self.getdependentprop('Psi');
    end%function get.Psi

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.Psihat(self)
        val = self.getdependentprop('Psihat');
    end%function get.Psihat

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.Psibb(self)
        val = self.getdependentprop('Psibb');
    end%function get.Psibb

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.Psibbhat(self)
        val = self.getdependentprop('Psibbhat');
    end%function get.Psibbhat

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.O1_N(self)
        val = self.getdependentprop('O1_N');
    end%function get.O1_N

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.O2_N(self)
        val = self.getdependentprop('O2_N');
    end%function get.O2_N

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.Obb1_N(self)
        val = self.getdependentprop('Obb1_N');
    end%function get.Obb1_N

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.Obb2_N(self)
        val = self.getdependentprop('Obb2_N');
    end%function get.Obb2_N

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.Gamma(self)
        val = self.getdependentprop('Gamma');
    end%function get.Gamma

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.Gammabb(self)
        val = self.getdependentprop('Gammabb');
    end%function get.Gammabb

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.Lambda_nonzero(self)
        val = self.getdependentprop('Lambda_nonzero');
    end%function get.Lambda_nonzero

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.Lambdahat_nonzero(self)
        val = self.getdependentprop('Lambdahat_nonzero');
    end%function get.Lambdahat_nonzero

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.Lambdabb_nonzero(self)
        val = self.getdependentprop('Lambdabb_nonzero');
    end%function get.Lambdabb_nonzero

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.Lambdabbhat_nonzero(self)
        val = self.getdependentprop('Lambdabbhat_nonzero');
    end%function get.Lambdabbhat_nonzero

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.Psi_nonzero(self)
        val = self.getdependentprop('Psi_nonzero');
    end%function get.Psi_nonzero

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.Psihat_nonzero(self)
        val = self.getdependentprop('Psihat_nonzero');
    end%function get.Psihat_nonzero

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.Psibb_nonzero(self)
        val = self.getdependentprop('Psibb_nonzero');
    end%function get.Psibb_nonzero

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.Psibbhat_nonzero(self)
        val = self.getdependentprop('Psibbhat_nonzero');
    end%function get.Psibbhat_nonzero

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.O1_nonzero(self)
        val = self.getdependentprop('O1_nonzero');
    end%function get.O1_nonzero

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.O2_nonzero(self)
        val = self.getdependentprop('O2_nonzero');
    end%function get.O2_nonzero

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.Obb1_nonzero(self)
        val = self.getdependentprop('Obb1_nonzero');
    end%function get.Obb1_nonzero

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.Obb2_nonzero(self)
        val = self.getdependentprop('Obb2_nonzero');
    end%function get.Obb2_nonzero

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.Gamma_nonzero(self)
        val = self.getdependentprop('Gamma_nonzero');
    end%function get.Gamma_nonzero

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.Gammabb_nonzero(self)
        val = self.getdependentprop('Gammabb_nonzero');
    end%function get.Gammabb_nonzero

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.XQ(self)
        val = self.getdependentprop('XQ');
    end%function get.XQ

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.XR(self)
        val = self.getdependentprop('XR');
    end%function get.XR

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.XS(self)
        val = self.getdependentprop('XS');
    end%function get.XS

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.XPhi(self)
        val = self.getdependentprop('XPhi');
    end%function get.XPhi

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.XTheta(self)
        val = self.getdependentprop('XTheta');
    end%function get.XTheta

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.XQR(self)
        val = self.getdependentprop('XQR');
    end%function get.XQR

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.XQRS(self)
        val = self.getdependentprop('XQRS');
    end%function get.XQRS

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.XPhiTheta(self)
        val = self.getdependentprop('XPhiTheta');
    end%function get.XPhiTheta

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.XQR_rank(self)
        val = self.getdependentprop('XQR_rank');
    end%function get.XQR_rank

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.XQRS_rank(self)
        val = self.getdependentprop('XQRS_rank');
    end%function get.XQRS_rank

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.XPhiTheta_rank(self)
        val = self.getdependentprop('XPhiTheta_rank');
    end%function get.XPhiTheta_rank

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.Xbb_temp(self)
        val = self.getdependentprop('Xbb_temp');
    end%function get.Xbb_temp

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.XbbQ(self)
        val = self.getdependentprop('XbbQ');
    end%function get.XbbQ

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.XbbR(self)
        val = self.getdependentprop('XbbR');
    end%function get.XbbR

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.XbbS(self)
        val = self.getdependentprop('XbbS');
    end%function get.XbbS

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.XbbPhibb(self)
        val = self.getdependentprop('XbbPhibb');
    end%function get.XbbPhibb

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.XbbThetabb(self)
        val = self.getdependentprop('XbbThetabb');
    end%function get.XbbThetabb

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.XbbQR(self)
        val = self.getdependentprop('XbbQR');
    end%function get.XbbQR

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.XbbQRS(self)
        val = self.getdependentprop('XbbQRS');
    end%function get.XbbQRS

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.XbbPhibbThetabb(self)
        val = self.getdependentprop('XbbPhibbThetabb');
    end%function get.XbbPhibbThetabb

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.XbbQR_rank(self)
        val = self.getdependentprop('XbbQR_rank');
    end%function get.XbbQR_rank

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.XbbQRS_rank(self)
        val = self.getdependentprop('XbbQRS_rank');
    end%function get.XbbQRS_rank

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.XbbPhibbThetabb_rank(self)
        val = self.getdependentprop('XbbPhibbThetabb_rank');
    end%function get.XbbPhibbThetabb_rank

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.Pbhat(self)
        val = self.getdependentprop('Pbhat');
    end%function get.Pbhat

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.Pbbbhat(self)
        val = self.getdependentprop('Pbbbhat');
    end%function get.Pbbbhat

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.Pbhathat(self)
        val = self.getdependentprop('Pbhathat');
    end%function get.Pbhathat

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.Pbbbhathat(self)
        val = self.getdependentprop('Pbbbhathat');
    end%function get.Pbbbhathat

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.Wsize(self)
        val = self.getdependentprop('Wsize');
    end%function get.Wsize

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.WQR_pinv(self)
        val = self.getdependentprop('WQR_pinv');
    end%function get.WQR_pinv

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.WQRS_pinv(self)
        val = self.getdependentprop('WQRS_pinv');
    end%function get.WQRS_pinv

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.WPhiTheta_pinv(self)
        val = self.getdependentprop('WPhiTheta_pinv');
    end%function get.WPhiTheta_pinv

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.WbbQR_pinv(self)
        val = self.getdependentprop('WbbQR_pinv');
    end%function get.WbbQR_pinv

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.WbbQRS_pinv(self)
        val = self.getdependentprop('WbbQRS_pinv');
    end%function get.WbbQRS_pinv

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.WbbPhibbThetabb_pinv(self)
        val = self.getdependentprop('WbbPhibbThetabb_pinv');
    end%function get.WbbPhibbThetabb_pinv

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.WQR(self)
        val = self.getdependentprop('WQR');
    end%function get.WQR

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.WQRS(self)
        val = self.getdependentprop('WQRS');
    end%function get.WQRS

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.WPhiTheta(self)
        val = self.getdependentprop('WPhiTheta');
    end%function get.WPhiTheta

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.WbbQR(self)
        val = self.getdependentprop('WbbQR');
    end%function get.WbbQR

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.WbbQRS(self)
        val = self.getdependentprop('WbbQRS');
    end%function get.WbbQRS

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.WbbPhibbThetabb(self)
        val = self.getdependentprop('WbbPhibbThetabb');
    end%function get.WbbPhibbThetabb

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.QRS_YALMIP(self)
        val = self.getdependentprop('QRS_YALMIP');
    end%function get.QRS_YALMIP

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.QRS_problem(self)
        val = self.getdependentprop('QRS_problem');
    end%function get.QRS_problem

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.Qhat(self)
        val = self.getdependentprop('Qhat');
    end%function get.Qhat

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.Rhat(self)
        val = self.getdependentprop('Rhat');
    end%function get.Rhat

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.Shat(self)
        val = self.getdependentprop('Shat');
    end%function get.Shat

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.QRS_objective(self)
        val = self.getdependentprop('QRS_objective');
    end%function get.QRS_objective

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.QRS_diagnostic(self)
        val = self.getdependentprop('QRS_diagnostic');
    end%function get.QRS_diagnostic

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.QRS_solvetime(self)
        val = self.getdependentprop('QRS_solvetime');
    end%function get.QRS_solvetime

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.PhiTheta_YALMIP(self)
        val = self.getdependentprop('PhiTheta_YALMIP');
    end%function get.PhiTheta_YALMIP

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.PhiTheta_problem(self)
        val = self.getdependentprop('PhiTheta_problem');
    end%function get.PhiTheta_problem

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.Phihat(self)
        val = self.getdependentprop('Phihat');
    end%function get.Phihat

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.Thetahat(self)
        val = self.getdependentprop('Thetahat');
    end%function get.Thetahat

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.PhiTheta_objective(self)
        val = self.getdependentprop('PhiTheta_objective');
    end%function get.PhiTheta_objective

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.PhiTheta_diagnostic(self)
        val = self.getdependentprop('PhiTheta_diagnostic');
    end%function get.PhiTheta_diagnostic

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.PhiTheta_solvetime(self)
        val = self.getdependentprop('PhiTheta_solvetime');
    end%function get.PhiTheta_solvetime

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.PhibbThetabb_YALMIP(self)
        val = self.getdependentprop('PhibbThetabb_YALMIP');
    end%function get.PhibbThetabb_YALMIP

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.PhibbThetabb_problem(self)
        val = self.getdependentprop('PhibbThetabb_problem');
    end%function get.PhibbThetabb_problem

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.Phibbhat(self)
        val = self.getdependentprop('Phibbhat');
    end%function get.Phibbhat

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.Thetabbhat(self)
        val = self.getdependentprop('Thetabbhat');
    end%function get.Thetabbhat

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.PhibbThetabb_objective(self)
        val = self.getdependentprop('PhibbThetabb_objective');
    end%function get.PhibbThetabb_objective

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.PhibbThetabb_diagnostic(self)
        val = self.getdependentprop('PhibbThetabb_diagnostic');
    end%function get.PhibbThetabb_diagnostic

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.PhibbThetabb_solvetime(self)
        val = self.getdependentprop('PhibbThetabb_solvetime');
    end%function get.PhibbThetabb_solvetime

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.props(self)
        self.seeifmdechanged();
        val = self.props_;
    end%function get.props

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.checks(self)
        self.seeifmdechanged();
        val = self.checks_;
    end%function get.checks

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function val = get.spec(self)
        self.seeifmdechanged();
        val = self.spec_;
    end%function get.spec
end%methods

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% set methods %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

methods (Access = private)
    function updatecurrent(self, X)
        % For base property X, mark all dependent checks and dependent
        % properties as out of date.
        for Y = self.checks_.names
            if ismember(X, self.checks_.depends.(Y{:}))
                self.checks_.current.(Y{:}) = false();
            end%if
        end%for
        for Y = self.props_.dependent
            if ismember(X, self.props_.depends.(Y{:}))
                self.props_.current.(Y{:}) = false();
            end%if
        end%for
    end%function updatecurrent

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function need2reload = updatespec(self, X, mde)
        % Updatespec can be called by link and seeifmdechanged. If called by
        % link, X is 'model', 'data', or 'estimator', and the third argument is
        % the cemodel, cedata, or ceestimator object that is being linked. If
        % called by link, X is 'model' or 'estimator', and the third argument is
        % not passed.

        newmde = nargin() >= 3;
        need2reload = false();

        old_spec = self.spec_;

        if newmde % coming from linked
            self.spec_.(X) = ~isequal(mde, []);

            if strcmp(X, 'model')
                if self.spec_.model 
                    self.spec_.stoch = ~strcmp(mde.spec.stochtype, 'none');
                else
                    self.spec_.stoch = false();
                end%if
            end%if

            if strcmp(X, 'estimator')
                if self.spec_.estimator 
                    self.spec_.stoch = ~strcmp(mde.model.spec.stochtype, ...
                                               'none');
                else
                    self.spec_.stoch = false();
                end%if
            end%if

            need2reload = (self.spec_.(X) ~= old_spec.(X)) ...
                          || (self.spec_.stoch ~= old_spec.(X));
        else % coming from seeifmdechanged. only need to check if stoch changed
            if strcmp(X, 'model')
                self.spec_.stoch = ~strcmp(self.model.spec.stochtype, 'none');
            else % X is 'estimator'
                self.spec_.stoch = ...
                           ~strcmp(self.estimator.model.spec.stochtype, 'none');
            end%if
            need2reload = self.spec_.stoch ~= old_spec.(X);
        end%if

        if need2reload
            str = '';
            for s = {'model', 'data', 'estimator', 'stoch'}
                if self.spec_.(s{:})
                    str = [str, s{:}(1)];
                end%if
            end%for

            if isempty(str)
                str = 'none';
            end%if

            self.spec_.string = str;
            self.load_constants(); % load self.props and self.checks
        end%if
    end%function updatespec
end%methods (Access = private)

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

methods
    function set.data(self, val)
        if self.spec_.estimator
            error('cannot set data property when estimator is given');
        end%if
        if ~(isequal(val, []) || isa(val, 'cedata'))
            error('data must be a cedata object or []');
        end%if
        self.link(val, 'data');
        self.data = val;
    end%function set.data

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function set.model(self, val)
        if self.spec_.estimator
            error('cannot set model property when estimator is given');
        end%if
        if ~(isequal(val, []) || isa(val, 'cemodel'))
            error('model must be a cemodel object or []');
        end%if
        self.link(val, 'model');
        self.model = val;
    end%function set.model

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function set.estimator(self, val)
        if self.spec_.model || self.spec_.data
            error('cannot set estimator property when data or model is given');
        end%if
        if ~(isequal(val, []) || isa(val, 'ceestimator'))
            error('estimator must be a ceestimator object or []');
        end%if
        self.link(val, 'estimator');
        self.estimator = val;
    end%function set.estimator

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function set.N(self, val)
        [isposint, rnd] = isposinteger(val);
        if ~isposint
            error('N must be a positive integer');
        end%if

        if ~isequal(rnd, self.N);
            self.updatecurrent('N');
            self.N = rnd;
        end%if
    end%function set.N

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function set.M(self, val)
        [isnonneg, rnd] = isnonneginteger(val);
        if ~isnonneg
            error('M must be a non-negative integer');
        end%if

        if ~isequal(rnd, self.M);
            self.updatecurrent('M');
            self.M = rnd;
        end%if
    end%function set.M

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function set.exclude(self, val)
        [isnonnegint, rnd] = isnonneginteger(val);
        if ~isnonnegint
            error('exclude must be a non-negative integer');
        end%if

        if ~isequal(rnd, self.exclude);
            self.updatecurrent('exclude');
            self.exclude = rnd;
        end%if
    end%function set.exclude

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function set.weight(self, val)
        if ~ischarvector(val)
            error(['weight must be ''I'', ''data'', ''exact'', ', ...
                   '''data_Z1'', or ''data_Z2''']);
        end%if

        if ~(strcmp(val, 'I') ...
             || (strcmp(val, 'data') ...
             || (strcmp(val, 'exact') ...
             || (strcmp(val, 'data_Z1') ...
             || strcmp(val, 'data_Z2')))))
            error(['weight must be ''I'', ''data'', ''exact'', ', ...
                   '''data_Z1'', or ''data_Z2''']);
        end%if

        if strcmp(val, 'data_Z1') || strcmp(val, 'data_Z2')
            warning('als:Z1Z2', ...
                    ['the ''data_Z1'' and ''data_Z2'' options for weight ', ...
                     'generally are not recommended---they have been ', ...
                     'implemented for research purposes']);
        end%if

        if ~isequal(val, self.weight)
            self.updatecurrent('weight');
            self.weight = val;
        end%if
    end%function set.weight

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function set.QRS_constrain(self, val)
        if ~islogicalscalar(val)
            error(['QRS_constrain must be a logical scalar (i.e., true() ', ...
                   'or false())']);
        end%if

        if ~isequal(val, self.QRS_constrain)
            self.updatecurrent('QRS_constrain');
            self.QRS_constrain = val;
        end%if
    end%function set.QRS_constrain

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function set.PhiTheta_constrain(self, val)
        if ~ischarvector(val)
            error(['PhiTheta_constrain must be ''none'', ''equality'', or ', ...
                   '''semidefinite''']);
        end%if

        if ~(strcmp(val, 'none') || (strcmp(val, 'equality') ...
            || strcmp(val, 'semidefinite')))
            error(['PhiTheta_constrain must be ''none'', ''equality'', or ', ...
                   '''semidefinite''']);
        end%if

        if ~isequal(val, self.PhiTheta_constrain)
            self.updatecurrent('PhiTheta_constrain');
            self.PhiTheta_constrain = val;
        end%if
    end%function set.PhiTheta_constrain

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function set.PhibbThetabb_constrain(self, val)
        if ~ischarvector(val)
            error(['PhibbThetabb_constrain must be ''none'', ', ...
                   '''equality'', or ''semidefinite''']);
        end%if

        if ~(strcmp(val, 'none') || (strcmp(val, 'equality') ...
            || strcmp(val, 'semidefinite')))
            error(['PhibbThetabb_constrain must be ''none'', ', ...
                   '''equality'', or ''semidefinite''']);
        end%if

        if ~isequal(val, self.PhibbThetabb_constrain)
            self.updatecurrent('PhibbThetabb_constrain');
            self.PhibbThetabb_constrain = val;
        end%if
    end%function set.PhibbThetabb_constrain

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function set.YALMIPsettings(self, val)
        if ~(isempty(val) || isstruct(val))
            error(['YALMIPsettings must be either empty or a struct ', ...
                   'generated by YALMIP''s sdpsettings function']);
        end%if

        if ~isequal(val, self.YALMIPsettings)
            self.updatecurrent('YALMIPsettings');
            self.YALMIPsettings = val;
        end%if
    end%function set.YALMIPsettings(self, val)

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function set.Szero(self, val)
        if ~islogicalscalar(val)
            error('Szero must be a logical scalar (i.e., true() or false())');
        end%if

        if ~isequal(val, self.Szero)
            self.updatecurrent('Szero');
            self.Szero = val;
        end%if
    end%function set.Szero

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function set.Qdiag(self, val)
        if ~islogicalscalar(val)
            error('Qdiag must be a logical scalar (i.e., true() or false())');
        end%if

        if ~isequal(val, self.Qdiag)
            self.updatecurrent('Qdiag');
            self.Qdiag = val;
        end%if
    end%function set.Qdiag

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function set.Rdiag(self, val)
        if ~islogicalscalar(val)
            error('Rdiag must be a logical scalar (i.e., true() or false())');
        end%if

        if ~isequal(val, self.Rdiag)
            self.updatecurrent('Rdiag');
            self.Rdiag = val;
        end%if
    end%function set.Rdiag

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function set.rpinvtol(self, val)
        X = 'rpinvtol';

        if ~isnumericscalar(val) || ~(val > 0)
            error([X, ' must be a positive scalar']);
        end%if

        if ~isequal(val, self.(X))
            self.updatecurrent(X);
            self.rpinvtol = val;
        end%if
    end%function set.rpinvtol(self, val)

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function set.rank_warnings(self, val)
        if ~islogicalscalar(val)
            error(['rank_warnings must be a logical scalar (i.e., true() ', ...
                   'or false())']);
        end%if

        if ~isequal(val, self.rank_warnings)
            self.updatecurrent('rank_warnings');
            self.rank_warnings = val;
        end%if
    end%function set.rank_warnings

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function set.l1Q(self, val)
        X = 'l1Q';

        if ~isnumeric(val)
            error([X, ' must be a positive scalar']);
        end%if

        empt = isempty(val);
        if ~empt && (~isscalar(val) || val <= 0)
            error([X, ' must be a positive scalar']);
        end%if

        if ~isequal(val, self.(X))
            if empt
                % X changing from nonempty to empty; remove from self.spec.reg.
                self.spec_.reg = setdiff(self.spec_.reg, {X});
            else
                % X is nonempty; add it to self.spec.reg if necessary.
                self.spec_.reg = union(self.spec_.reg, {X});
            end%if

            self.updatecurrent(X);
            self.(X) = val;
        end%if
    end%function set.l1Q

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function set.l1R(self, val)
        X = 'l1R';

        if ~isnumeric(val)
            error([X, ' must be a positive scalar']);
        end%if

        empt = isempty(val);
        if ~empt && (~isscalar(val) || val <= 0)
            error([X, ' must be a positive scalar']);
        end%if

        if ~isequal(val, self.(X))
            if empt
                % X changing from nonempty to empty; remove from self.spec.reg.
                self.spec_.reg = setdiff(self.spec_.reg, {X});
            else
                % X is nonempty; add it to self.spec.reg if necessary.
                self.spec_.reg = union(self.spec_.reg, {X});
            end%if

            self.updatecurrent(X);
            self.(X) = val;
        end%if
    end%function set.l1R

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function set.l1S(self, val)
        X = 'l1S';

        if ~isnumeric(val)
            error([X, ' must be a positive scalar']);
        end%if

        empt = isempty(val);
        if ~empt && (~isscalar(val) || val <= 0)
            error([X, ' must be a positive scalar']);
        end%if

        if ~isequal(val, self.(X))
            if empt
                % X changing from nonempty to empty; remove from self.spec.reg.
                self.spec_.reg = setdiff(self.spec_.reg, {X});
            else
                % X is nonempty; add it to self.spec.reg if necessary.
                self.spec_.reg = union(self.spec_.reg, {X});
            end%if

            self.updatecurrent(X);
            self.(X) = val;
        end%if
    end%function set.l1S

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function set.l2Q(self, val)
        X = 'l2Q';

        if ~isnumeric(val)
            error([X, ' must be a positive scalar']);
        end%if

        empt = isempty(val);
        if ~empt && (~isscalar(val) || val <= 0)
            error([X, ' must be a positive scalar']);
        end%if

        if ~isequal(val, self.(X))
            if empt
                % X changing from nonempty to empty; remove from self.spec.reg.
                self.spec_.reg = setdiff(self.spec_.reg, {X});
            else
                % X is nonempty; add it to self.spec.reg if necessary.
                self.spec_.reg = union(self.spec_.reg, {X});
            end%if

            self.updatecurrent(X);
            self.(X) = val;
        end%if
    end%function set.l2Q

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function set.l2R(self, val)
        X = 'l2R';

        if ~isnumeric(val)
            error([X, ' must be a positive scalar']);
        end%if

        empt = isempty(val);
        if ~empt && (~isscalar(val) || val <= 0)
            error([X, ' must be a positive scalar']);
        end%if

        if ~isequal(val, self.(X))
            if empt
                % X changing from nonempty to empty; remove from self.spec.reg.
                self.spec_.reg = setdiff(self.spec_.reg, {X});
            else
                % X is nonempty; add it to self.spec.reg if necessary.
                self.spec_.reg = union(self.spec_.reg, {X});
            end%if

            self.updatecurrent(X);
            self.(X) = val;
        end%if
    end%function set.l2R

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function set.l2S(self, val)
        X = 'l2S';

        if ~isnumeric(val)
            error([X, ' must be a positive scalar']);
        end%if

        empt = isempty(val);
        if ~empt && (~isscalar(val) || val <= 0)
            error([X, ' must be a positive scalar']);
        end%if

        if ~isequal(val, self.(X))
            if empt
                % X changing from nonempty to empty; remove from self.spec.reg.
                self.spec_.reg = setdiff(self.spec_.reg, {X});
            else
                % X is nonempty; add it to self.spec.reg if necessary.
                self.spec_.reg = union(self.spec_.reg, {X});
            end%if

            self.updatecurrent(X);
            self.(X) = val;
        end%if
    end%function set.l2S

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function set.nucQ(self, val)
        X = 'nucQ';

        if ~isnumeric(val)
            error([X, ' must be a positive scalar']);
        end%if

        empt = isempty(val);
        if ~empt && (~isscalar(val) || val <= 0)
            error([X, ' must be a positive scalar']);
        end%if

        if ~isequal(val, self.(X))
            if empt
                % X changing from nonempty to empty; remove from self.spec.reg.
                self.spec_.reg = setdiff(self.spec_.reg, {X});
            else
                % X is nonempty; add it to self.spec.reg if necessary.
                self.spec_.reg = union(self.spec_.reg, {X});
            end%if

            self.updatecurrent(X);
            self.(X) = val;
        end%if
    end%function set.nucQ

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function set.nucR(self, val)
        X = 'nucR';

        if ~isnumeric(val)
            error([X, ' must be a positive scalar']);
        end%if

        empt = isempty(val);
        if ~empt && (~isscalar(val) || val <= 0)
            error([X, ' must be a positive scalar']);
        end%if

        if ~isequal(val, self.(X))
            if empt
                % X changing from nonempty to empty; remove from self.spec.reg.
                self.spec_.reg = setdiff(self.spec_.reg, {X});
            else
                % X is nonempty; add it to self.spec.reg if necessary.
                self.spec_.reg = union(self.spec_.reg, {X});
            end%if

            self.updatecurrent(X);
            self.(X) = val;
        end%if
    end%function set.nucR

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function set.nucS(self, val)
        X = 'nucS';

        if ~isnumeric(val)
            error([X, ' must be a positive scalar']);
        end%if

        empt = isempty(val);
        if ~empt && (~isscalar(val) || val <= 0)
            error([X, ' must be a positive scalar']);
        end%if

        if ~isequal(val, self.(X))
            if empt
                % X changing from nonempty to empty; remove from self.spec.reg.
                self.spec_.reg = setdiff(self.spec_.reg, {X});
            else
                % X is nonempty; add it to self.spec.reg if necessary.
                self.spec_.reg = union(self.spec_.reg, {X});
            end%if

            self.updatecurrent(X);
            self.(X) = val;
        end%if
    end%function set.nucS

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function set.nucQRS(self, val)
        X = 'nucQRS';

        if ~isnumeric(val)
            error([X, ' must be a positive scalar']);
        end%if

        empt = isempty(val);
        if ~empt && (~isscalar(val) || val <= 0)
            error([X, ' must be a positive scalar']);
        end%if

        if ~isequal(val, self.(X))
            if empt
                % X changing from nonempty to empty; remove from self.spec.reg.
                self.spec_.reg = setdiff(self.spec_.reg, {X});
            else
                % X is nonempty; add it to self.spec.reg if necessary.
                self.spec_.reg = union(self.spec_.reg, {X});
            end%if

            self.updatecurrent(X);
            self.(X) = val;
        end%if
    end%function set.nucQRS

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function set.trQ(self, val)
        X = 'trQ';
        msg = [X, ' must be a positive scalar or a vector of nonnegative ', ...
                  'scalars'];

        if ~isnumeric(val)
            error(msg);
        end%if

        empt = isempty(val);
        if ~empt && (~isvector(val) || any(val < 0))
            error(msg);
        end%if

        if ~empt && (isscalar(val) && val <= 0)
            error(msg);
        end%if

        if ~isequal(val, self.(X))
            if empt
                % X changing from nonempty to empty; remove from self.spec.reg.
                self.spec_.reg = setdiff(self.spec_.reg, {X});
            else
                % X is nonempty; add it to self.spec.reg if necessary.
                self.spec_.reg = union(self.spec_.reg, {X});
            end%if

            self.updatecurrent(X);
            self.(X) = val;
        end%if
    end%function set.trQ

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function set.trR(self, val)
        X = 'trR';
        msg = [X, ' must be a positive scalar or a vector of nonnegative ', ...
                  'scalars'];

        if ~isnumeric(val)
            error(msg);
        end%if

        empt = isempty(val);
        if ~empt && (~isvector(val) || any(val < 0))
            error(msg);
        end%if

        if ~empt && (isscalar(val) && val <= 0)
            error(msg);
        end%if

        if ~isequal(val, self.(X))
            if empt
                % X changing from nonempty to empty; remove from self.spec.reg.
                self.spec_.reg = setdiff(self.spec_.reg, {X});
            else
                % X is nonempty; add it to self.spec.reg if necessary.
                self.spec_.reg = union(self.spec_.reg, {X});
            end%if

            self.updatecurrent(X);
            self.(X) = val;
        end%if
    end%function set.trR
end%methods

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

methods (Static)
    function generate_constants()

        % TODO need to do 'none', 'md', 'e', 'mds', and 'es'. Also need to do 'm', 'ms' and 'd'.

        % TODO make sure Ndsize check is always included (except for 'none' and equivalents), since it is necessary for calculating Nd, Ndtotal, and Tf

        props = struct();
        checks = struct();
        base = {'model', ...
                'data', ...
                'estimator', ...
                'N', ...
                'exclude', ...
                'weight', ...
                'M', ...
                'QRS_constrain', ...
                'PhiTheta_constrain', ...
                'PhibbThetabb_constrain', ...
                'YALMIPsettings', ...
                'Szero', ...
                'Qdiag', ...
                'Rdiag', ...
                'rpinvtol', ...
                'rank_warnings', ...
                'l1Q', ...
                'l1R', ...
                'l1S', ...
                'l2Q', ...
                'l2R', ...
                'l2S', ...
                'trQ', ...
                'trR', ...
                'nucQ', ...
                'nucR', ...
                'nucS', ...
                'nucQRS'};
        allchecks = struct( ...
                       'Ndsize',    {{'data', 'estimator', 'exclude', 'N'}}, ...
                       'datamodel', {{'model', 'data'}}, ...
                       'estimator', {{'estimator'}}, ...
                       'Kcal',      {{'estimator'}}, ...
                       'Astable',   {{'model'}}, ...
                       'Abbstable', {{'estimator'}}, ...
                       'weightalg', {{'weight', 'model', 'estimator'}}, ...
                       'diagS',     {{'Szero', 'Qdiag', 'Rdiag'}}, ...
                       'Sreg',      {{'Szero', 'l1S', 'l2S', 'nucS', ...
                                      'nucQRS'}});

        allX = {};
        ds = 'dependsspec';
        dp = 'dependent';
        b = 'base';
        n = 'names';

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% none %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        X = 'none';
        allX = [allX, X];
        props.(X).(ds) = struct();
        checks.(X).(n) = {'diagS', 'Sreg'};

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% d %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        X = 'd';
        allX = [allX, X];
        props.(X).(ds) = struct('Ndtotal', {{'data'}}, ...
                                'Nd',      {{'Ndtotal', 'exclude'}}, ...
                                'Tf',      {{'Nd'}}, ...
                                'nt',      {{'Tf', 'N'}});
        checks.(X).(n) = {'Ndsize', 'diagS', 'Sreg'};

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% m %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        X = 'm';
        allX = [allX, X];
        props.(X).(ds) = struct( ...
'pt',               {{'model', 'N'}}, ...
'O1_N',             {{'model', 'N'}}, ...
'O2_N',             {{'model', 'N'}}, ...
'Gamma',            {{'model', 'N'}}, ...
'XQ',               {{'model', 'O1_N', 'Qdiag'}}, ...
'XR',               {{'model', 'Gamma', 'Rdiag'}}, ...
'XS',               {{'model', 'O2_N'}}, ...
'XPhi',             {{'model', 'Gamma'}}, ...
'XTheta',           {{'model', 'O2_N'}}, ...
'XQR',              {{'pt', 'model', 'XQ', 'XR'}}, ...
'XQRS',             {{'pt', 'model', 'XQ', 'XR', 'XS'}}, ...
'XPhiTheta',        {{'pt', 'model', 'XPhi', 'XTheta'}}, ...
'XQR_rank',         {{'XQR'}}, ...
'XQRS_rank',        {{'XQRS'}}, ...
'XPhiTheta_rank',   {{'XPhiTheta'}}, ...
'Wsize',            {{'model', 'pt'}}, ...
'QRS_problem',      {{'Szero', 'l1Q', 'l1R', 'l1S', 'l2Q', 'l2R', 'l2S', ...
                      'trQ', 'trR', 'nucQ', 'nucR', 'nucS', 'nucQRS', ...
                      'QRS_constrain', 'Qdiag', 'Rdiag'}}, ...
'PhiTheta_problem', {{'PhiTheta_constrain', 'model', 'l1Q', 'l1R', 'l1S', ...
                      'l2Q', 'l2R', 'l2S', 'trQ', 'trR', 'nucQ', 'nucR', ...
                      'nucS', 'nucQRS', 'Szero', 'Qdiag', 'Rdiag'}});
        checks.(X).(n) = {'datamodel', 'Astable', 'diagS', 'Sreg'};

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% ms %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        X = 'ms';
        allX = [allX, X];
        props.(X).(ds) = catstruct(props.m.(ds), struct( ...
'b',              {{'Psi'}}, ...
'Lambda',         {{'model', 'Psi', 'N'}}, ...
'Psi',            {{'model', 'Gamma', 'O2_N'}}));
        checks.(X).(n) = {'datamodel', 'Astable', 'diagS', 'Sreg'};

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% md %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        X = 'md';
        allX = [allX, X];
        props.(X).(ds) = catstruct(props.m.(ds), props.d.(ds), struct( ...
'o',                   {{'model', 'data', 'Ndtotal', 'exclude'}}, ...
'otype',               {{'model'}}, ...
'bhat',                {{'Psihat'}}, ...
'Lambdahat',           {{'o', 'N', 'Nd'}}, ...
'Psihat',              {{'Lambdahat'}}, ...
'Lambdahat_nonzero',   {{'o', 'M', 'N', 'Nd', 'model'}}, ...
'Psihat_nonzero',      {{'Lambdahat_nonzero'}}, ...
'O1_nonzero',          {{'model', 'M', 'Nd'}}, ...
'O2_nonzero',          {{'model', 'M'}}, ...
'Gamma_nonzero',       {{'model', 'M', 'Nd'}}, ...
'Pbhathat',            {{'model', 'pt', 'nt', 'Tf', 'N', 'M', ...
                         'Lambdahat_nonzero', 'weight', 'o'}}, ...
'WQR_pinv',            {{'weight', 'Wsize', 'Pbhathat', 'XQR'}}, ...
'WQRS_pinv',           {{'weight', 'Wsize', 'Pbhathat', 'XQRS'}}, ...
'WPhiTheta_pinv',      {{'weight', 'Wsize', 'Pbhathat', 'XPhiTheta'}}, ...
'WQR',                 {{'weight', 'Wsize', 'WQR_pinv', 'rpinvtol'}}, ...
'WQRS',                {{'weight', 'Wsize', 'WQRS_pinv', 'rpinvtol'}}, ...
'WPhiTheta',           {{'weight', 'Wsize', 'WPhiTheta_pinv', 'rpinvtol'}}, ...
'QRS_YALMIP',          {{'QRS_constrain', 'model', 'Qdiag', 'Rdiag', ...
                         'Szero', 'XQR', 'XQRS', 'WQR', 'WQRS', 'WQR_pinv', ...
                         'WQRS_pinv', 'bhat', 'weight', 'l1Q', 'l1R', 'l1S', ...
                         'l2Q', 'l2R', 'l2S', 'trQ', 'trR', 'nucQ', 'nucR', ...
                         'nucS', 'nucQRS'}}, ...
'Qhat',                {{'model', 'Szero', 'XQR', 'XQRS', 'XQR_rank', ...
                         'XQRS_rank', 'WQR', 'WQRS', 'WQR_pinv', ...
                         'WQRS_pinv', 'bhat', 'QRS_constrain', 'trQ', 'trR', ...
                         'YALMIPsettings', 'QRS_YALMIP', 'weight', 'Qdiag', ...
                         'Rdiag'}}, ...
'Rhat',                {{'Qhat'}}, ... % All of these depend on all of the same
'Shat',                {{'Qhat'}}, ... % parameters as Qhat.
'QRS_objective',       {{'Qhat'}}, ...
'QRS_diagnostic',      {{'Qhat'}}, ...
'QRS_solvetime',       {{'Qhat'}}, ...
'PhiTheta_YALMIP',     {{'PhiTheta_constrain', 'model', 'Qdiag', 'Rdiag', ...
                         'Szero', 'XPhiTheta', 'WPhiTheta', ...
                         'WPhiTheta_pinv', 'bhat', 'weight', 'l1Q', 'l1R', ...
                         'l1S', 'l2Q', 'l2R', 'l2S', 'trQ', 'trR', 'nucQ', ...
                         'nucR', 'nucS', 'nucQRS'}}, ...
'Phihat',              {{'model', 'XPhiTheta', 'XPhiTheta_rank', 'bhat', ...
                         'WPhiTheta', 'WPhiTheta_pinv', ...
                         'PhiTheta_constrain', 'trQ', 'trR', ...
                         'YALMIPsettings', 'PhiTheta_YALMIP', 'weight'}}, ...
'Thetahat',            {{'Phihat'}}, ... % All of these depend on the same
'PhiTheta_objective',  {{'Phihat'}}, ... % parameters as Phihat.
'PhiTheta_diagnostic', {{'Phihat'}}, ...
'PhiTheta_solvetime',  {{'Phihat'}}));
        checks.(X).(n) = {'Ndsize', 'datamodel', 'Astable', 'weightalg', ...
                          'diagS', 'Sreg'};

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% mds %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        X = 'mds';
        allX = [allX, X];
        props.(X).(ds) = catstruct(props.md.(ds), struct( ...
'b',              {{'Psi'}}, ...
'Lambda',         {{'model', 'Psi', 'N'}}, ...
'Psi',            {{'model', 'Gamma', 'O2_N'}}, ...
'Lambda_nonzero', {{'model', 'Psi_nonzero', 'M', 'Nd'}}, ...
'Psi_nonzero',    {{'model', 'Gamma_nonzero', 'O2_nonzero'}}, ...
'Pbhat',          {{'model', 'pt', 'nt', 'Tf', 'N', 'M', 'Lambda_nonzero'}}));
        props.(X).(ds).WQR_pinv = {'weight', 'Wsize', 'Pbhathat', 'Pbhat', ...
                                   'XQR'};
        props.(X).(ds).WQRS_pinv = {'weight', 'Wsize', 'Pbhathat', 'Pbhat', ...
                                    'XQRS'};
        props.(X).(ds).WPhiTheta_pinv = {'weight', 'Wsize', 'Pbhathat', ...
                                         'Pbhat', 'XPhiTheta'};
        checks.(X).(n) = checks.md.(n);

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% e %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        % TODO shouldn't have model anywhere, just estimator.
        % TODO I think some of the stuff in here is wrong---for example, shouldn't XbbQ depend on Obb2_N?

        X = 'e';
        allX = [allX, X];
        props.(X).(ds) = struct( ...
'Ndtotal',              {{'estimator'}}, ...
'Nd',                   {{'Ndtotal', 'exclude'}}, ...
'Tf',                   {{'Nd'}}, ...
'nt',                   {{'Tf', 'N'}}, ...
'pt',                   {{'estimator', 'N'}}, ...
'o',                    {{'estimator', 'exclude'}}, ...
'otype',                {{'estimator'}}, ...
'bbbhat',               {{'Psibbhat'}}, ...
'Lambdabbhat',          {{'o', 'N', 'Nd'}}, ...
'Psibbhat',             {{'Lambdabbhat'}}, ...
'Obb1_N',               {{'estimator', 'N'}}, ...
'Obb2_N',               {{'estimator', 'N'}}, ...
'Gamma',                {{'estimator', 'N'}}, ...
'Gammabb',              {{'estimator', 'Gamma', 'Obb2_N'}}, ...
'Lambdabbhat_nonzero',  {{'o', 'M', 'N', 'Nd', 'estimator', 'model'}}, ...
'Psibbhat_nonzero',     {{'Lambdabbhat_nonzero'}}, ...
'Obb1_nonzero',         {{'estimator', 'M'}}, ...
'Obb2_nonzero',         {{'estimator', 'M', 'Nd'}}, ...
'Gamma_nonzero',        {{'estimator', 'M', 'Nd'}}, ...
'Gammabb_nonzero',      {{'estimator', 'Gamma_nonzero', 'Obb2_nonzero'}}, ...
'Xbb_temp',             {{'estimator', 'Obb1_N'}}, ...
'XbbQ',                 {{'estimator', 'Xbb_temp', 'Qdiag'}}, ...
'XbbR',                 {{'estimator', 'Xbb_temp', 'Gammabb', 'Rdiag'}}, ...
'XbbS',                 {{'estimator', 'Xbb_temp', 'Obb2_N'}}, ...
'XbbPhibb',             {{'estimator', 'Gamma'}}, ...
'XbbThetabb',           {{'estimator', 'Obb2_N'}}, ...
'XbbQR',                {{'pt', 'estimator', 'XbbQ', 'XbbR'}}, ...
'XbbQRS',               {{'pt', 'estimator', 'XbbQ', 'XbbR', 'XbbS'}}, ...
'XbbPhibbThetabb',      {{'pt', 'estimator', 'XbbPhibb', 'XbbThetabb'}}, ...
'XbbQR_rank',           {{'XbbQR'}}, ...
'XbbQRS_rank',          {{'XbbQRS'}}, ...
'XbbPhibbThetabb_rank', {{'XbbPhibbThetabb'}}, ...
'Pbbbhathat',           {{'model', 'pt', 'nt', 'Tf', 'N', 'M', ...
                          'Lambdabbhat_nonzero', 'weight', 'o'}}, ...
'Wsize',                {{'estimator', 'pt'}}, ...
'WbbQR_pinv',           {{'weight', 'Wsize', 'Pbbbhathat', 'XbbQR'}}, ...
'WbbQRS_pinv',          {{'weight', 'Wsize', 'Pbbbhathat', 'XbbQRS'}}, ...
'WbbPhibbThetabb_pinv', {{'weight', 'Wsize', 'Pbbbhathat', ...
                          'XbbPhibbThetabb'}}, ...
'WbbQR',                {{'WbbQR_pinv', 'rpinvtol'}}, ...
'WbbQRS',               {{'WbbQRS_pinv', 'rpinvtol'}}, ...
'WbbPhibbThetabb',      {{'WbbPhibbThetabb_pinv', 'rpinvtol'}}, ...
'Qhat',                 {{}}, ... TODO
'Rhat',                 {{}}, ... TODO
'Shat',                 {{}}, ... TODO
'Phibbhat',             {{}}, ... TODO
'Thetabbhat',           {{}}); % TODO
        checks.(X).(n) = {'Ndsize', 'estimator', 'Kcal', 'Abbstable'}; % TODO

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% es %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        X = 'es';
        allX = [allX, X];
        props.(X).(ds) = catstruct(props.e.(ds), struct( ...
'bbb',          {{'Psibb'}}, ...
'Lambdabb',     {{'estimator', 'Psibb', 'N'}}, ...
'Psibb',        {{'estimator', 'Gamma', 'Obb2_N'}}, ...
'Lambdabb_nonzero', {{'estimator', 'Psibb_nonzero', 'M', 'Nd'}}, ...
'Psibb_nonzero',    {{'estimator', 'Gamma_nonzero', 'Obb2_nonzero'}}, ...
'Pbbbhat',      {{'model', 'pt', 'nt', 'Tf', 'N', 'M', 'Lambdabb_nonzero'}}));
        props.(X).(ds).WbbQR_pinv = {'weight', 'Wsize', 'Pbbbhathat', ...
                                     'Pbbbhat', 'XbbQR'};
        props.(X).(ds).WbbQRS_pinv = {'weight', 'Wsize', 'Pbbbhathat', ...
                                      'Pbbbhat','XbbQRS'};
        props.(X).(ds).WbbPhibbThetabb_pinv = {'weight', 'Wsize', ...
                                               'Pbbbhathat', 'Pbbbhat', ...
                                               'XbbPhibbThetabb'};
        checks.(X).(n) = checks.e.(n); % TODO

        %%%%%%%%%%%%%%%%%%%%%%%%% fill in other fields %%%%%%%%%%%%%%%%%%%%%%%%%

        for X = allX
            props.(X{:}).(b) = base;
            props.(X{:}).(dp) = fieldnames(props.(X{:}).(ds))';
            props.(X{:}).depends = calcdepends(props.(X{:}).(b), ...
                                               props.(X{:}).(dp), ...
                                               props.(X{:}).(ds));
            nd = numel(props.(X{:}).(dp));
            props.(X{:}).current = cell2struct( ...
                                     mat2cell(false(nd, 1), ones(nd, 1), 1), ...
                                     props.(X{:}).(dp));
            props.(X{:}).store = structfun(@(x) [], props.(X{:}).current, ...
                                           'UniformOutput', false());
            checks.(X{:}).depends = struct();
            for cn = fieldnames(allchecks)'
                if ismember(cn{:}, checks.(X{:}).(n))
                    checks.(X{:}).depends.(cn{:}) = allchecks.(cn{:});
                end%if
            end%for
            nc = numel(checks.(X{:}).(n));
            checks.(X{:}).current = cell2struct( ...
                                     mat2cell(false(nc, 1), ones(nc, 1), 1), ...
                                     checks.(X{:}).(n));
        end%for

        %%%%%%%%%%%%%%%%%%%%%%%%%%%% save mat file %%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        filename = [als.installdir(), 'als_constants.mat'];
        if isoctave()
            save('-7', filename, 'props', 'checks');
        else % Matlab
            save(filename, 'props', 'checks', '-v7');
        end%if
    end%function generate_constants

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function d = installdir()
        mf = mfilename();
        mff = mfilename('fullpath');
        d = mff(1:end - numel(mf));
    end%function installdir

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function unittests()
        msg = {};

        try
            if isoctave()
                pkg('load', 'statistics');
            end%if

            n = 3;
            p = 2;
            g = 2;
            h = p;

%            A = [0.5, 0; 1, 0.4];
%            C = [1, 0];
%            G = [0.3; 0.7];
%            H = 1.1;
%            QRS = [1, 2; 3, 4];

            A = randn(n);
            A = A / max(abs(eig(A))) * 0.5;
            C = randn(p, n);
            G = randn(n, g);
            H = randn(p, h);
            QRS = randn(g + h);

%            A = magic(n);
%            A = A / max(abs(eig(A))) * 0.5;
%            C = magic(max(p, n));
%            C = C(1:p, 1:n);
%            G = magic(max(n, g));
%            G = G(1:n, 1:g);
%            H = magic(max(p, h));
%            H = C(1:p, 1:h);
%            QRS = magic(g + h);

            QRS = (QRS * QRS') / 2;
            Q = QRS(1:g, 1:g);
            R = QRS(g + 1:end, g + 1:end);
            S = QRS(1:g, g + 1:end);

            mdl = cemodel('A', A, 'C', C, 'G', G, 'H', H, 'Q', Q, 'R', R, ...
                          'S', S);

            Nd = 20;
            Tf = Nd - 1;
            exclude = 50;
            Ndtotal = Nd + exclude;
            dat = mdl.simulate('Nd', Ndtotal);

            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

            % Create empty als object.
            a = als();
            if a.spec.model ...
                || (a.spec.data ...
                || (a.spec.estimator ...
                || (a.spec.stoch ...
                || (~strcmp(a.spec.string, 'none') ...
                || ~isequal(a.spec.reg, {})))))
                msg = [msg, ['failed (line ', num2str(getline()), '): ', ...
                             'error with empty als']];
            end%if

            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

            % Add model.
            a.model = mdl;
            if ~a.spec.model ...
                || (a.spec.data ...
                || (a.spec.estimator ...
                || (~a.spec.stoch ...
                || (~strcmp(a.spec.string, 'ms') ...
                || ~isequal(a.spec.reg, {})))))
                msg = [msg, ['failed (line ', num2str(getline()), '): ', ...
                             'error adding model']];
            end%if

            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

            % Add data.
            a.data = dat;
            if ~a.spec.model ...
                || (~a.spec.data ...
                || (a.spec.estimator ...
                || (~a.spec.stoch ...
                || (~strcmp(a.spec.string, 'mds') ...
                || ~isequal(a.spec.reg, {})))))
                msg = [msg, ['failed (line ', num2str(getline()), '): ', ...
                             'error adding model']];
            end%if

            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

            % See if calculation of any properties causes an error.
            a.exclude = exclude;
            a.M = 7;
            a.N = 3;

            dep = {'Ndtotal', 'Nd', 'Tf', 'nt', 'pt', 'O1_N', 'O2_N', 'Gamma', 'O1_nonzero', 'O2_nonzero', 'Gamma_nonzero', 'XQ', 'XR', 'XS', 'XPhi', 'XTheta', 'XQR', 'XQRS', 'XPhiTheta', 'XQR_rank', 'XQRS_rank', 'XPhiTheta_rank', 'Wsize', 'QRS_problem', 'PhiTheta_problem', 'o', 'otype', 'Lambdahat', 'Lambdahat_nonzero', 'Psihat', 'Psihat_nonzero', 'bhat', 'Lambda', 'Lambda_nonzero', 'Psi', 'Psi_nonzero', 'b', 'Pbhathat', 'Pbhat', 'WQR_pinv', 'WQRS_pinv', 'WPhiTheta_pinv', 'WQR', 'WQRS', 'WPhiTheta', 'QRS_YALMIP', 'PhiTheta_YALMIP', 'Qhat', 'Rhat', 'Shat', 'QRS_objective', 'QRS_diagnostic', 'QRS_solvetime', 'Phihat', 'Thetahat', 'PhiTheta_objective', 'PhiTheta_diagnostic', 'PhiTheta_solvetime'};
            if ~isequal(sort(a.props.dependent), sort(dep))
                error('dep and a.props.dependent are inconsistent');
            end%if
            for X = dep
                a.(X{:});
            end%for

            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

            a = als('model', mdl, 'data', dat, 'exclude', exclude, 'M', Tf, ...
                    'N', 3);

            [Pbhat_test1, Pbhat_test2] = a.test_Pbhat('Lambda_nonzero');
            if ~comp(a.Pbhat, Pbhat_test1) || ~comp(a.Pbhat, Pbhat_test2)
                msg = [msg, ['failed (line ', num2str(getline()), '): ', ...
                             'test_Pbhat is inconsitent with do_Pbhat_calc']];
            end%if

            [Pbhathat_test1, Pbhathat_test2] = ...
                                              a.test_Pbhat('Lambdahat_nonzero');
            if ~comp(a.Pbhathat, Pbhathat_test1) ...
                || ~comp(a.Pbhathat, Pbhathat_test2)
                msg = [msg, ['failed (line ', num2str(getline()), '): ', ...
                             'test_Pbhat is inconsitent with do_Pbhat_calc']];
            end%if

        catch err
            lns = cellfun(@num2str, {err.stack.line}, 'UniformOutput', false());
            msg = [msg, ['UNEXPECTED ERROR: ', err.message, char(10), ...
                         'look at line(s) ', strjoin(lns, ', ')]];
        end%trycatch

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        if ~isempty(msg)
            disp(['als: ', strjoin(msg, char(10))]);
        else
            disp('als: all unittests passed');
        end%if
    end%function unittests
end%methods (Static)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

methods (Access = private)
    function [Pbhat, Pbhat2] = test_Pbhat(self, Y)
        % Y is 'Lambda_nonzero', 'Lambdahat_nonzero', 'Lambdabb_nonzero', or
        % 'Lambdabbhat_nonzero'.

        if self.spec_.estimator
            p = self.estimator.model.p;
        else % data and model given
            p = self.model.p;
        end%if

        Fcal = kron(spdiags((1 ./ ((self.Tf + 1):-1:(self.nt + 1)))', 0, ...
                            self.N, self.N), speye(p));
        Gcal = sparse(1:p, 1:p, 1, p, self.pt);
        Hcal = dmatpinv(self.pt, p) * kron(Gcal, Fcal);

        K = commat(self.pt, self.Nd);
        Lcal = kron(speye(self.pt^2), vec(speye(self.Nd)))' ...
                * kron(kron(speye(self.pt), K), speye(self.Nd));

        Ecal = arrayfun(@(k) spdiags(ones(self.N * p, 1), ...
                                     k, self.pt, self.pt), ...
                        0:p:(p * (self.N - 1)), 'UniformOutput', false());
        Ncal = blkdiag(speye(self.nt * self.pt), vertcat(Ecal{:}));

        rws = 1:(self.pt * (self.nt + 1));
        cls = arrayfun(@(k) (k * p + 1):(k * p + self.pt), 0:self.nt, ...
                       'UniformOutput', false());
        cls = horzcat(cls{:});
        Tcal = sparse(rws, cls, 1);

        Ecalt = spalloc(self.pt^2, self.pt^2, ...
                        self.N * (self.pt - (self.N - 1) / 2));
        for i = 1:self.N
            Ecalt = Ecalt + kron(Ecal{i}, Ecal{i});
        end%for

        IK = speye(self.pt^2) + commat(self.pt);

        %%%%%%%%%%%% first test method - Rajamani's direct approach %%%%%%%%%%%%

        if self.M < self.Tf
            C = [self.(Y); mat2cell(zeros(p * (self.Tf - self.M), p), ...
                                    repmat(p, self.Tf - self.M, 1), p)];
        else
            C = self.(Y)(1:(self.Tf + 1));
        end%if
        Pyt = blktoep(C, cellctrans(C));

        NcalTcal = Ncal * Tcal;
        PYs = forceherm(NcalTcal * Pyt * NcalTcal');
        KPYsKT = forceherm(K * PYs * K');
        Pa = forceherm(Lcal ...
                * (speye((self.Nd * self.pt)^2) + commat(self.Nd * self.pt)) ...
                * kron(KPYsKT, KPYsKT) * Lcal');

        Pbhat = forceherm(Hcal * Pa * Hcal');

        %%%%%%%%%% second test method - iterative approach to get Pa %%%%%%%%%%%

        %%%%%% lag = 0 %%%%%%

        if (self.N - 1) <= self.M
            C = self.(Y)(1:self.N);
        else
            C = [self.(Y); mat2cell(zeros(p * (self.N - self.M - 1), p), ...
                                    repmat(p, self.N - self.M - 1, 1), p)];
        end%if
        R = cellctrans(C);

        Pc = blktoep(C, R);
        Pkron = IK * kron(Pc, Pc);
        Pa2 = forceherm(self.nt * Pkron + Ecalt * Pkron * Ecalt');

        Mtnt_1 = min(self.M + self.N - 1, self.nt - 1);
        Mtnt = min(self.M + self.N - 1, self.nt);
        Pkron_sum = zeros(self.pt^2);

        %%%%%% lag >= 1 %%%%%%

        for i = 1:Mtnt
            if (self.N + i - 1) <= self.M
                C = [C(2:end); self.(Y){self.N + i}];
            else
                C = [C(2:end); zeros(p)];
            end%if
            R = [C{1}; R(1:(end - 1))];

            Pc = blktoep(C, R);
            Pkron = IK * kron(Pc, Pc);
            Pkron_sum = Pkron_sum + Pkron;

            if i <= Mtnt_1
                Pa2 = Pa2 + (self.nt - i) * forceherm2(Pkron);
            end%if
        end%for

        Pa2 = Pa2 + forceherm2(Ecalt * Pkron_sum);
        Pbhat2 = forceherm(Hcal * Pa2 * Hcal');
    end%function test_Pbhat
end%methods (Access = private)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

end%classdef als

