%% Copyright (C) 2017 Travis Arnold
%%
%% This file is part of Octave.
%%
%% Octave is free software; you can redistribute it and/or modify it
%% under the terms of the GNU General Public License as published by
%% the Free Software Foundation; either version 3 of the License, or (at
%% your option) any later version.
%%
%% Octave is distributed in the hope that it will be useful, but
%% WITHOUT ANY WARRANTY; without even the implied warranty of
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
%% General Public License for more details.
%%
%% You should have received a copy of the GNU General Public License
%% along with Octave; see the file COPYING.  If not, see
%% <http://www.gnu.org/licenses/>.

%% -*- texinfo -*-
%% @deftypefn {Function File} {@var{P} =} dmatpinv (@var{m}, @var{n})
%% Return the Moore-Penrose pseudoinverse
%% @tex
%%  $P_{m,n}$
%% @end tex
%% @ifnottex
%%  @math{P(m,n)}
%% @end ifnottex
%% of
%% @tex
%%  $D_{m,n} = \texttt{dmat}(m,n)$.
%% @end tex
%% @ifnottex
%%  @math{D(m,n) = dmat(m,n)}.
%% @end ifnottex
%% This is a 
%% @tex
%%  $mn - n(n-1)/2 \times mn$
%% @end tex
%% @ifnottex
%%  @math{@var{m}*@var{n}-@var{n}*(@var{n}-1)/2} by @math{@var{m}*@var{n}}
%% @end ifnottex
%% matrix that "undoes" the effect of
%% @tex
%%  $D_{m,n}$.
%% @end tex
%% @ifnottex
%%  @math{D(m,n)}.
%% @end ifnottex
%% That is, 
%% @tex
%%  $P_{m,n} {\rm vec} \, A = {\rm myvech} \, A$
%% @end tex
%% @ifnottex
%%  @math{P(m,n)*vec (A) = myvech (A)}
%% @end ifnottex
%% for all
%% @tex
%%  $m \times n$
%% @end tex
%% @ifnottex
%%  @math{@var{m}} by @math{@var{n}}
%% @end ifnottex
%% matrices
%% @tex
%%  $A$
%% @end tex
%% @ifnottex
%%  @math{A}
%% @end ifnottex
%% with
%% @tex
%%  $m \geq n$
%% @end tex
%% @ifnottex
%%  @math{@var{m} >= @var{n}}
%% @end ifnottex
%% and the upper
%% @tex
%%  $n \times n$
%% @end tex
%% @ifnottex
%%  @math{@var{n} by @var{n}}
%% @end ifnottex
%% block of 
%% @tex
%%  $A$
%% @end tex
%% @ifnottex
%%  @math{A}
%% @end ifnottex
%% symmetric.
%%
%% If only one argument @var{m} is given,
%% @tex
%%  $P_{m,m}$
%% @end tex
%% @ifnottex
%%  @nospell{@math{P(m,m)}}
%% @end ifnottex
%% is returned.
%%
%% Returns a sparse matrix.  Equivalent to pinv (dmat (@var{m}, @var{n})),
%% but MUCH faster.  For a demonstration of this, type 'demo dmatpinv'.
%%
%% See Magnus and Neudecker (1988), @cite{Matrix Differential Calculus with
%% Applications in Statistics and Econometrics}.
%% @seealso{duplication_matrix, dupmat, dupmatpinv, dmat, vec, vech, myvech}
%% @end deftypefn

%% Author: Travis Arnold <tjarnold@wisc.edu>, December 2017.
%% Adapted to be Matlab compatible, May 2018.

function P = dmatpinv(m, n)
    if (nargin() < 1 || nargin() > 2)
        error('dmatpinv: one or two input arguments is required');
    end%if

    try
        [misposint, m] = isposinteger(m);
    catch
        error('dmatpinv: M must be a positive integer');
    end%trycatch

    if ~misposint
        error('dmatpinv: M must be a positive integer');
    end%if

    if (nargin() == 1)
        P = dupmatpinv(m);
        return;
    end%if

    try
        [nisposint, n] = isposinteger(n);
    catch
        error('dmatpinv: N must be a positive integer');
    end%trycatch

    if ~nisposint
        error('dmatpinv: N must be a positive integer');
    end%if

    if (~(m >= n))
        error('dmatpinv: M must be greater than or equal to N');
    end%if

    count = [0, arrayfun(@(k) sum(m-1:-1:k), m-1:-1:m-n+1)];
    j = 1:n;
    col1 = (j-1)*m + j;
    row1 = count + j;

    row23 = arrayfun(@(k) count(k) + (k+1:n), 1:n-1, 'UniformOutput', false);
    row23 = horzcat(row23{:});

    col2 = arrayfun(@(k) (k-1)*m + (k+1:n) , 1:n-1, 'UniformOutput', false);
    col2 = horzcat(col2{:});

    col3 = arrayfun(@(k) ((k+1:n)-1)*m + k , 1:n-1, 'UniformOutput', false);
    col3 = horzcat(col3{:});

    row4 = arrayfun(@(k) (k*m-m+n+1-k*(k-1)/2):(k*m-k*(k-1)/2), 1:n, ...
                    'UniformOutput', false);
    row4 = horzcat(row4{:});

    col4 = arrayfun(@(k) k*m-m+n+1:k*m, 1:n, 'UniformOutput', false);
    col4 = horzcat(col4{:});

    P = sparse([row1 row4], [col1 col4], 1, m*n-n*(n-1)/2, m*n) ...
        + sparse([row23 row23], [col2 col3], 0.5, m*n-n*(n-1)/2, m*n);
end%function dmatpinv

%!assert(full(dmatpinv(1)), 1);
%!assert(full(dmatpinv(2)), [1 0 0 0; 0 0.5 0.5 0; 0 0 0 1]);
%!assert(full(dmatpinv(4)), pinv(dmat(4)), 1e-6);
%!assert(full(dmatpinv(2, 1)), pinv(dmat(2, 1)), 1e-6);
%!assert(full(dmatpinv(11, 6)), pinv(dmat(11, 6)), 1e-6);
%!assert(full(dmatpinv(12, 8)), pinv(dmat(12, 8)), 1e-6);
%!assert(full(dmatpinv(23, 15)), pinv(dmat(23, 15)), 1e-6);
%!assert(dmatpinv(2), dmatpinv(2, 2));

%!test
%! n = 7;
%! A = randn(n);
%! A = A + A';
%! assert(dmatpinv(n) * vec(A), myvech(A));

%!test
%! n = 17;
%! A = randn(n);
%! A = A + A';
%! assert(dmatpinv(n) * vec(A), myvech(A));

%!test
%! M = 2;
%! N = 1;
%! A = rand(M);
%! B = A * A'; B = B(:, 1:N);
%! C = A + A'; C = C(:, 1:N);
%! D = dmatpinv(M, N);
%! assert(myvech(B), D * vec(B), 1e-6);
%! assert(myvech(C), D * vec(C), 1e-6);

%!test
%! M = 2;
%! N = 1;
%! A = rand(M);
%! B = A * A'; B = B(:, 1:N);
%! C = A + A'; C = C(:, 1:N);
%! D = dmatpinv(M, N);
%! assert(myvech(B), D * vec(B), 1e-6);
%! assert(myvech(C), D * vec(C), 1e-6);

%!test
%! M = 3;
%! N = 2;
%! A = rand(M);
%! B = A * A'; B = B(:, 1:N);
%! C = A + A'; C = C(:, 1:N);
%! D = dmatpinv(M, N);
%! assert(myvech(B), D * vec(B), 1e-6);
%! assert(myvech(C), D * vec(C), 1e-6);

%!test
%! M = 4;
%! N = 3;
%! A = rand(M);
%! B = A * A'; B = B(:, 1:N);
%! C = A + A'; C = C(:, 1:N);
%! D = dmatpinv(M, N);
%! assert(myvech(B), D * vec(B), 1e-6);
%! assert(myvech(C), D * vec(C), 1e-6);

%!test
%! M = 4;
%! N = 2;
%! A = rand(M);
%! B = A * A'; B = B(:, 1:N);
%! C = A + A'; C = C(:, 1:N);
%! D = dmatpinv(M, N);
%! assert(myvech(B), D * vec(B), 1e-6);
%! assert(myvech(C), D * vec(C), 1e-6);

%!test
%! M = 10;
%! N = 7;
%! A = rand(M);
%! B = A * A'; B = B(:, 1:N);
%! C = A + A'; C = C(:, 1:N);
%! D = dmatpinv(M, N);
%! assert(myvech(B), D * vec(B), 1e-6);
%! assert(myvech(C), D * vec(C), 1e-6);

%!error <one or two input arguments is required> dmatpinv()
%!error <one or two input arguments is required> dmatpinv(1, 2, 3)
%!error <M must be a positive integer> dmatpinv(0.5)
%!error <M must be a positive integer> dmatpinv(-1)
%!error <M must be a positive integer> dmatpinv(ones(1,4))
%!error <N must be a positive integer> dmatpinv(1, 0.5)
%!error <N must be a positive integer> dmatpinv(1, -1)
%!error <N must be a positive integer> dmatpinv(1, ones(1,4))
%!error <M must be greater than or equal to N> dmatpinv(1, 2)

%!demo
%! m = 100;
%! n = 20;
%! timer = tic();
%! dmatpinv(m, n);
%! t1 = toc(timer);
%! pinv(dmat(m, n));
%! t2 = toc(timer);
%! fprintf('dmatpinv(%i, %i) : %f seconds\n', m, n, t1);
%! fprintf('pinv(dmat(%i, %i) : %f seconds\n', m, n, t2-t1);

