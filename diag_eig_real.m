function A = diag_eig_real(lam)

    n = numel(lam);
    A = zeros(n);

    complex_conjugate_pair = false();
    for i = 1:n
        if complex_conjugate_pair
            complex_conjugate_pair = false();
            continue;
        end%if

        if isreal(lam(i))
            A(i, i) = lam(i);
        else % lam is complex
            A(i, i) = real(lam(i));
            A(i + 1, i + 1) = real(lam(i));
            A(i + 1, i) = imag(lam(i));
            A(i, i + 1) = -imag(lam(i));
            complex_conjugate_pair = true();
        end%if
    end%for

end%function diag_eig_real

