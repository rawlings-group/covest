%% Copyright (C) 2018 Travis Arnold
%%
%% This file is part of Octave.
%%
%% Octave is free software; you can redistribute it and/or modify it
%% under the terms of the GNU General Public License as published by
%% the Free Software Foundation; either version 3 of the License, or (at
%% your option) any later version.
%%
%% Octave is distributed in the hope that it will be useful, but
%% WITHOUT ANY WARRANTY; without even the implied warranty of
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
%% General Public License for more details.
%%
%% You should have received a copy of the GNU General Public License
%% along with Octave; see the file COPYING.  If not, see
%% <http://www.gnu.org/licenses/>.

%% [unrec, unrecstring] = checkstructfields(s, okfields)
%%
%% Return the field names of structure s that aren't in cell array okfields.
%% unrec is a cell array of character vectors. unrecstring contains the names in
%% one character vector with the names separated by commas.

%% Author: Travis Arnold <tjarnold@wisc.edu>
%% July 2018.

function [unrec, unrecstring] = checkstructfields(s, okfields)

if nargin() ~= 2
    error('exactly two input arguments required');
end%if

% Make sure s is a struct.
if ~isstruct(s)
    error('s must be a struct');
end%if

% Make sure okfields is a cell.
if ~iscell(okfields)
    error('okfields must be a cell');
end%if

% Make sure all elements of okfields are character vectors.
if ~all(cellfun(@ischarrow, okfields))
    error('all elements of okfields must be character vectors');
end%if

% Extract fieldnames from s and number of fields.
sfields = fieldnames(s);
ns = numel(sfields);

% unrec is a cell array containing the field names of s that aren't in okfields.
% unrecstring contains these in one character array with the names separated by
% commas.
unrec = {};
unrecstring = '';
for i = 1:ns
    % if sfields{i} doesn't match anything in okfields, then add this field name
    % to the list.
    if ~any(vec(strcmp(sfields{i}, okfields)))
        unrec = [unrec, sfields{i}];
        unrecstring = [unrecstring, sfields{i}, ', '];
    end%if
end%for

if ~isempty(unrecstring)
    unrecstring(end-1:end) = '';
end%if

end%function checkstructfields

%!error <exactly two input arguments required> checkstructfields();
%!error <exactly two input arguments required> checkstructfields(1);
%!error <exactly two input arguments required> checkstructfields(1, 2, 3);
%!error <s must be a struct> checkstructfields(1, 2);

%!error <okfields must be a cell>
%! s.apple = 1;
%! okayfields = 1;
%! checkstructfields(s, okayfields);

%!error <all elements of okfields must be character vectors>
%! s.apple = 1;
%! okayfields = {1};
%! checkstructfields(s, okayfields);

%!error <all elements of okfields must be character vectors>
%! s.apple = 1;
%! okayfields = {'apple', 1};
%! checkstructfields(s, okayfields);

%!test
%! s.apple = 1;
%! s.banana = 1;
%! s.cantaloupe = 1;
%! s.date = 1;
%! s.elderberry = 1;
%!
%! [unrec, unrecstring] = checkstructfields(s, {'apple', 'cantaloupe'});
%! assert(unrec, {'banana', 'date', 'elderberry'});
%! assert(unrecstring, 'banana, date, elderberry');
%!
%! [unrec, unrecstring] = checkstructfields(s, {'apple', 'banana', ...
%!                                              'date', 'cantaloupe'});
%! assert(unrec, {'elderberry'});
%! assert(unrecstring, 'elderberry');
%!
%! [unrec, unrecstring] = checkstructfields(s, {'apple', 'banana', 'date', ...
%!                                              'elderberry', 'cantaloupe'});
%! assert(unrec, {});
%! assert(unrecstring, '');

